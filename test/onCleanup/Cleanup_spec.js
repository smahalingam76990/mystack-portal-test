xdescribe("Notifications", function() {

  xit("Total number of notifications for bubble creation", function() {
    fn.login("mystack.user1", "");

    element(by.xpath("//*[@href='/notifications']")).click();
    // expect(element(by.className("notification-count")).getText()).toEqual('15');
  });

  xit("Verify notifications are grouped on message type and user can expand them", function() {
    element(by.xpath("//*[@href='/notifications']")).click();

    expect(element(by.className("notification-count")).getText()).toEqual('15');
    expect(element.all(by.className("NotificationGroup")).count()).toEqual(1);

    expect(element.all(by.className("NotificationGroup")).get(0).getText()).toEqual('Bubble Creating');
    expect(element.all(by.className("NotificationGroup")).get(0).element(by.className("Notifications")).isDisplayed()).toBeFalsy();
    element.all(by.className("NotificationGroup")).get(0).click();
    expect(element.all(by.className("NotificationGroup")).get(0).element(by.className("Notifications")).isDisplayed()).toBeTruthy();
    expect(element.all(by.className("NotificationGroup")).get(0).all(by.css(".Notifications em")).count()).toEqual(15);
  });

  xit("Verify user able to see all the notifications for bubble creation", function() {
    element(by.xpath("//*[@href='/notifications']")).click();

    element.all(by.className("NotificationGroup")).get(0).click();
    // TODO - Add test for user add notification
    expect(element.all(by.className("NotificationGroup")).get(0).all(by.css(".Notifications em")).get(0).getText()).toEqual('Organisation xxx-TestBubble-01 has been created');
    expect(element.all(by.className("NotificationGroup")).get(0).all(by.css(".Notifications em")).get(7).getText()).toEqual('vDC for xxx-TestBubble-01 has been created');
    expect(element.all(by.className("NotificationGroup")).get(0).all(by.css(".Notifications em")).get(8).getText()).toEqual('Edge for xxx-TestBubble-01 is creating');
    expect(element.all(by.className("NotificationGroup")).get(0).all(by.css(".Notifications em")).get(9).getText()).toEqual('Edge for xxx-TestBubble-01 has been created');
    expect(element.all(by.className("NotificationGroup")).get(0).all(by.css(".Notifications em")).get(10).getText()).toEqual('FE network for xxx-TestBubble-01 is creating');
    expect(element.all(by.className("NotificationGroup")).get(0).all(by.css(".Notifications em")).get(11).getText()).toEqual('FE network for xxx-TestBubble-01 has been created');
    expect(element.all(by.className("NotificationGroup")).get(0).all(by.css(".Notifications em")).get(12).getText()).toEqual('BE network for xxx-TestBubble-01 is creating');
    expect(element.all(by.className("NotificationGroup")).get(0).all(by.css(".Notifications em")).get(13).getText()).toEqual('BE network for xxx-TestBubble-01 has been created');
    expect(element.all(by.className("NotificationGroup")).get(0).all(by.css(".Notifications em")).get(14).getText()).toEqual('Bubble status is now CREATED');
  });

  xit("Verify notifications are available while user logs out and log back in", function() {
    element(by.xpath("//*[@href='/notifications']")).click();

    var result;
    element(by.className("notification-count")).isPresent().then(function(result) {
      if (result) {
        element(by.className("notification-count")).getText().then(function(totalNotificationsBeforeLogout) {
          fn.logout();
          fn.login("mystack.user1", "");
          fn.waitForNotifications(10000, 1);

          expect(totalNotificationsBeforeLogout).toEqual(element(by.className("notification-count")).getText());
        });
      } else {
        expect(result).toBeTruthy();
      }
    });
  });

  xit("Verify user able to clear the notifications", function() {
    element(by.xpath("//*[@href='/notifications']")).click();

    var result;
    element(by.className("notification-count")).isPresent().then(function(result) {
      if (result) {
        element(by.buttonText("Clear All")).click();
        expect(element(by.className("notification-count")).isPresent()).toBeFalsy();
        browser.refresh();
        expect(element(by.className("notification-count")).isPresent()).toBeFalsy();
      } else {
        expect(result).toBeTruthy();
      }
    });
  });

});

// serviceNow default values
var serviceNowAuthorization = serviceNowAuthKey;
var snowHeaders = {"Accept": "application/json", "Authorization" : serviceNowAuthorization};
var org_id      = ""

describe("ServiceNowIntegration", function() {

  var org_id  = "";

  it("Get the org_id for the bubble - 'xxx-TestBubble-01'", function(done) {
    var errorMessage;
    // Get org_id for the bubble 'xxx-TestBubble-01'
    db.execute("SELECT * FROM bubble where name='xxx-TestBubble-01'", function(err, result) {
      if (err) {
        errorMessage = "Failure message - " + err;
        this.fail(errorMessage);
      } else {
        if (result.rows.length == 1) {
          org_id = result.rows[0].org_id;
        } else {
          expect(result.rows.length).toEqual(1);
        }
      }
    });

    done();
  });

  it("Verify CMDB is updated for recently created bubble - 'xxx-TestBubble-01'", function() {
    var response, jsonResponse, errorMessage;
    if (org_id != "") {
      var href = "https://pearsondev.service-now.com/api/now/v1/table/cmdb_ci_zone?sysparm_query=correlation_id=" + org_id;
      response = fn.runHttpRequests("GET", snowHeaders, href);
      if (response.statusCode == 200) {
        jsonResponse = JSON.parse(response.body);

        expect(jsonResponse["result"].length).toEqual(1);
        expect(jsonResponse["result"][0]["name"]).toEqual("xxx-TestBubble-01");
        expect(jsonResponse["result"][0]["u_retirement_date"]).toEqual("");
        expect(jsonResponse["result"][0]["support_group"]).toEqual("");
      } else {
        errorMessage = "Failed to run the query - " + href;
        this.fail(errorMessage);
      }
    } else {
      errorMessage = "Org 'xxx-TestBubble-01' not exists in DB so bubble might not be created.";
      this.fail(errorMessage);
    }
  });

  it("Verify user able to search for list of ServiceNow support groups by typing partial text", function() {
    fn.login("mystack.user1", "");

    element(by.linkText("BUBBLES")).click();
    browser.wait(EC.elementToBeClickable(element(by.cssContainingText('.Bubble-name', 'xxx-TestBubble-01'))), waitSeconds);
    element(by.cssContainingText('.Bubble-name', 'xxx-TestBubble-01')).click();
    expect(element(by.className("BubbleView")).isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(element(by.className("BubbleView-close"))), waitSeconds);

    element(by.className("rf-combobox-input")).clear().sendKeys("test");
    expect(element(by.className("rf-combobox-option")).isPresent()).toBeTruthy();
    expect(element.all(by.className("rf-combobox-option")).count()).toBeGreaterThan(5);
  });

  it("Verify user able to assign 'ServiceNow Support Group' for a bubble - 'xxx-TestBubble-01'", function() {
    // Add ServiceNow Support group to this bubble
    element(by.linkText("BUBBLES")).click();
    browser.wait(EC.elementToBeClickable(element(by.cssContainingText('.Bubble-name', 'xxx-TestBubble-01'))), waitSeconds);
    element(by.cssContainingText('.Bubble-name', 'xxx-TestBubble-01')).click();
    expect(element(by.className("BubbleView")).isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(element(by.className("BubbleView-close"))), waitSeconds);
    element(by.className("rf-combobox-input")).clear().sendKeys("PI LTG Test");
    expect(element(by.className("rf-combobox-option")).isPresent()).toBeTruthy();
    element(by.cssContainingText('.rf-combobox-option', 'PI LTG Testing')).click();
    element(by.className("BubbleView-close")).click();
  });

  it("Verify 'ServiceNow Support Group' (PI LTG Testing) is updated on the CMDB for bubble - 'xxx-TestBubble-01'", function() {
    var response, jsonResponse, errorMessage;
    if (org_id != "") {

      // Verify ServiceNow Support group is added on the CMDB for the bubble 'xxx-TestBubble-01'
      var href = "https://pearsondev.service-now.com/api/now/v1/table/cmdb_ci_zone?sysparm_query=correlation_id=" + org_id;
      response = fn.runHttpRequests("GET", snowHeaders, href);
      if (response.statusCode == 200) {
        jsonResponse = JSON.parse(response.body);
        supportGroupHref = jsonResponse["result"][0]["support_group"]["link"];
        expect(supportGroupHref).not.toEqual("");

        // Verify bubble is assigned to correct support group
        response = fn.runHttpRequests("GET", snowHeaders, supportGroupHref);
        if (response.statusCode == 200) {
          jsonResponse = JSON.parse(response.body);
          expect(jsonResponse["result"]["name"]).toEqual("PI LTG Testing");
          expect(jsonResponse["result"]["active"]).toBeTruthy();
        } else {
          errorMessage = "Failed to run the query - " + href;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "Failed to run the query - " + href;
        this.fail(errorMessage);
      }
    } else {
      errorMessage = "Org 'xxx-TestBubble-01' not exists in DB (bubble might not be created). So can't continue this test";
      this.fail(errorMessage);
    }
  });

  it("Verify user able to update ServiceNow support group", function() {
    var response, jsonResponse, errorMessage;
    // Update the ServiceNow Support group
    element(by.linkText("BUBBLES")).click();
    browser.wait(EC.elementToBeClickable(element(by.cssContainingText('.Bubble-name', 'xxx-TestBubble-01'))), waitSeconds);
    element(by.cssContainingText('.Bubble-name', 'xxx-TestBubble-01')).click();
    expect(element(by.className("BubbleView")).isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(element(by.className("BubbleView-close"))), waitSeconds);
    element(by.className("rf-combobox-input")).clear().sendKeys("SOS test 2");
    expect(element(by.className("rf-combobox-option")).isPresent()).toBeTruthy();
    element(by.cssContainingText('.rf-combobox-option', 'SOS test 2')).click();
    element(by.className("BubbleView-close")).click();
  });

  it("Verify 'ServiceNow Support Group' (SOS test 2) is updated on the CMDB for bubble - 'xxx-TestBubble-01'", function() {
    var response, jsonResponse, errorMessage;
    if (org_id != "") {
      // Verify ServiceNow Support group gets updated in the CMDB
      var href = "https://pearsondev.service-now.com/api/now/v1/table/cmdb_ci_zone?sysparm_query=correlation_id=" + org_id;
      response = fn.runHttpRequests("GET", snowHeaders, href);
      if (response.statusCode == 200) {
        jsonResponse = JSON.parse(response.body);
        supportGroupHref = jsonResponse["result"][0]["support_group"]["link"];
        expect(supportGroupHref).not.toEqual("");

        // Verify bubble is assigned to correct support group
        response = fn.runHttpRequests("GET", snowHeaders, supportGroupHref);
        if (response.statusCode == 200) {
          jsonResponse = JSON.parse(response.body);
          expect(jsonResponse["result"]["name"]).toEqual("SOS test 2");
          expect(jsonResponse["result"]["active"]).toBeTruthy();
        } else {
          errorMessage = "Failed to run the query - " + href;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "Failed to run the query - " + href;
        this.fail(errorMessage);
      }
    } else {
      errorMessage = "Org 'xxx-TestBubble-01' not exists in DB (bubble might not be created). So can't continue this test";
      this.fail(errorMessage);
    }
  });

});
/*


element(by.className("notification-count")).getText()
element(by.buttonText("Clear All")).click();


element(by.className("notification-link")).click()

Total notifications Group:
element.all(by.className("NotificationGroup")).count()

Notification header:
element.all(by.className("NotificationGroup")).get(0).getText()
element.all(by.className("NotificationGroup")).get(0).element(by.className("Notifications")).isDisplayed()

Total notification in each header:
element.all(by.className("NotificationGroup")).get(0).all(by.css(".Notifications em")).count()

To get individual notification:
element.all(by.className("NotificationGroup")).get(0).all(by.css(".Notifications em")).get(0).getText()



Bubble Delete:
BE network for xxx-TestBubble-01 is being deleted
BE network for xxx-TestBubble-01 has been deleted
FE network for xxx-TestBubble-01 is being deleted
FE network for xxx-TestBubble-01 has been deleted
Edge for xxx-TestBubble-01 is being deleted
Edge for xxx-TestBubble-01 has now been deleted
xxx-TestBubble-01 has now been deleted



External IP Purchase:
Adding external ip address to xxx-TestBubble-01. Please wait
Public IPs 159.182.214.20 are being added to xxx-TestBubble-01
*/

















