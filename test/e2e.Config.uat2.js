// Configuration for E2E testting for my stack portal
exports.config = {
  //seleniumServerJar: '',
  seleniumAddress: 'http://10.72.197.171:4444/wd/hub',

  allScriptsTimeout: 11000,

  // How long to wait for a page to load.
  getPageTimeout: 10000,

  // A base URL for your application under test. Calls to protractor.get() with relative paths will be prepended with this.
  baseUrl: 'https://uat2-mystack.pearson.com/',

  // Alternatively, suites may be used. When run without a command line parameter, all suites will run.
  // If run with --suite=sanity, only the patterns matched by that suite will run.
  suites: {
    // Sanity Tests
    sanity: [
          './**/UsersAndTeams_spec.js',
          './**/BubbleCreate-01_spec.js',
          './**/myIP_spec.js',
          './**/BubbleDelete-01_spec.js',
          './**/BubbleDelete_spec.js',
          './**/BubbleCreatePage_spec.js'
          ],

    regression: [
          './**/UsersAndTeams_spec.js',
          './**/BubbleCreate-01_spec.js',
          './**/BubbleCreate-02_spec.js',
          './**/myIP_spec.js',
          './**/BubbleMove_spec.js',
          './**/BubbleDelete-01_spec.js',
          './**/BubbleDelete-02_spec.js',
          './**/BubbleDelete_spec.js',
          './**/BubbleCreatePage_spec.js',
          './**/ErrorMessage_spec.js',
          './**/BubbleDelete-MPSPLAT-410_spec.js',
          './**/HomePage_spec.js',
          './**/Bugs_spec.js'
          ],
  },

  // Patterns to exclude.
  exclude: [],

  framework: 'jasmine2',

  // resultJsonOutputFile:'result/report/jsonoutput.json',

  // Options to be passed to jasmine2.
  //
  // See the full list at https://github.com/jasmine/jasmine-npm
  jasmineNodeOpts: {
    // If true, print colors to the terminal.
    showColors: true,
    // Default time to wait in ms before a test fails.
    defaultTimeoutInterval: 620000,
    // Function called to print jasmine results.
    // print: function() {},
    // If set, only execute specs whose names match the pattern, which is
    // internally compiled to a RegExp.
    // grep: 'Cleanup_spec.js',
    // Inverts 'grep' matches
    invertGrep: false
  },

  // A callback function called once protractor is ready and available, and before the specs are executed
  // You can specify a file containing code to run by setting onPrepare to the filename string.
  onPrepare: './onPrepare/startup.js',

  params: {
    url: {
      homePage:   'https://uat2-mystack.pearson.com/',
      loginPage:  'https://sts.mandc-test.com/',
      neoPearson: 'https://neo.pearson.com/groups/login.jspa',
      servicenow: 'https://pearson.service-now.com/navpage.do'
    },

    // Specify the region where the bubble has to be created and also specify the credentials for vCD - Developer region
    // vcd: {
    //   instance:  'https://developer-mystack.pearson.com',
    //   key:       "Basic dmNkYWRtaW5AU3lzdGVtOiNlcjd5ZUQzb00wUnk=-"

    // },

    // Specify the region where the bubble has to be created and also specify the credentials for vCD - LO3REF region
    vcd: {
      instance:  'https://lo3ref-mystack.pearson.com',
      key:       "Basic bG8zcmVmX2FwaV9wcm9kdWN0aW9uQFN5c3RlbTpMOHNySHFXdHgmMHg="
    },

    // DB details
    db: {
      host:     "10.76.131.76",
      keyspace: "mystack"
    },

    // Authkey for ServiceNow
    snowKey: "Basic bXlzdGFja2FwaTpQYXNzd29yZDY3",

    waitSeconds: 10000,

    // Waits till the bubble is created. Make sure the 'bubbleCreateWaitSeconds' is higher than this value
    bubbleCreateWaitSeconds: 600000,

    // Specify location to save the screen shot
    screenShotDirectory: 'result/screenshot/',
  }

};
