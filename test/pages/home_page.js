var HomePage = function() {
  this.userMenuName   = element(by.className("UserMenu-name"));
  this.headerSignOn   = element(by.className("headerSignOn"));
  this.signOut        = element(by.linkText("Sign out"));

  this.notifications  = element(by.xpath("//*[@href='/notifications']"));
  this.bubbles        = element(by.linkText("BUBBLES"));
  this.teams          = element(by.linkText("TEAMS"));
  this.users          = element(by.linkText("USERS"));
  this.marketplace    = element(by.linkText("MARKETPLACE"));
  this.usage          = element(by.linkText("USAGE"));
  this.search         = element(by.linkText("SEARCH"));
  // this.navigateToUsersPage = function() {
  //   this.users.click();
  // };
};

module.exports = new HomePage();
