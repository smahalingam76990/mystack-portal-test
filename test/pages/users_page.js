var UsersPage = function() {
  // Locators in users page
  this.users           = element.all(by.className("User"));
  this.createUser      = element(by.className("UsersPage-createUser"));
  this.createUsereMail = element(by.className('CreateUser-email'));
  this.createButton    = element(by.buttonText("create"));
  this.searchUser      = element(by.xpath('//*[@type="search"]'));
  this.createUserError     = element(by.className("CreateUser-error"));
  this.createTeamCancel    = element(by.className("CreateUser-cancel"));

  // Valid user role(s) - 'Portal', 'Admistrator' & 'Adoption'. NOTE - Any other roles are not accepted & you might see problem locating the element/attribute
  this.chooseUserRole = function(role) {
    element(by.cssContainingText('.CreateUser option', role)).click();
  };

  // To locate a user by username in the 'Users' page
  this.user = function(user) {
    return element(by.cssContainingText('.User-name', user));
  };

  // Locators in UserView page
  this.userView        = element(by.className("UserView"));
  this.userViewName    = element(by.className("UserView")).element(by.className("UserView-name"));
  this.userViewTeams   = element(by.className("UserView")).all(by.className("UserView-team"));
  this.userViewDelete  = element(by.className("UserView")).element(by.className("UserView-delete"));
  this.userViewClose   = element(by.className("UserView")).element(by.className("UserView-close"));

  // Valid user role(s) in UserView page - 'Portal', 'Admistrator' & 'Adoption'. NOTE - Any other roles are not accepted & you might see problem locating the element/attribute
  this.chooseUserViewRole = function(role) {
    element(by.cssContainingText('.UserView-role option', role)).click();
  };

  // Returns locator for team in Userview page
  this.UserViewTeam = function(team) {
    return element(by.cssContainingText('.UserView-team', team));
  };

};

module.exports = new UsersPage();

