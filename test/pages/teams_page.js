var TeamsPage = function() {
  this.createTeam     = element(by.className("TeamsPage-createTeam"));
  this.createButton   = element(by.buttonText("create"));
  this.createTeamName = element(by.className('CreateTeam-name'));
  this.teams            = element.all(by.className("Team"));
  this.teamName         = element(by.className("Team-name"));
  this.teamBubble       = element(by.className("Team-bubbles"));
  this.teamMemberCount  = element(by.className("Team-memberCount"));
  this.teamMember       = element(by.className("Team-member"));
  this.searchTeam       = element(by.xpath('//*[@type="search"]'));
  this.createTeamCancel = element(by.className("CreateTeam-cancel"));
  this.teamCreateError  = element(by.className("CreateTeam-error"));

  // Locate the element by team name
  this.team = function(team) {
    return element(by.cssContainingText('.Team-name', team));
  };
  this.getTeamNameByindex = function(index) {
    return element.all(by.className("Team")).get(index).element(by.className("Team-name")).getText();
  };
  this.getTeamBubblesByindex = function(index) {
    return element.all(by.className("Team")).get(index).element(by.className("Team-bubbles")).getText();
  };
  this.getTeamMemberCountByindex = function(index) {
    return element.all(by.className("Team")).get(index).element(by.className("Team-memberCount")).getText();
  };
  this.getTeamMembersByindex = function(index) {
    return element.all(by.className("Team")).get(index).all(by.className("Team-member"));
  };

  // elements on the TeamView page
  this.teamView           = element(by.className("TeamView"));
  this.teamViewAddMember  = element(by.className("TeamView")).element(by.className("TeamView-addMember"));
  this.teamVieweMail      = element(by.className("TeamView")).element(by.xpath('//*[@type="email"]'));
  this.teamViewAddButton  = element(by.className("TeamView")).element(by.buttonText("add"));
  this.teamViewClose      = element(by.className("TeamView")).element(by.className("TeamView-close"));
  this.teamViewName       = element(by.className("TeamView")).element(by.className("TeamViewName-value"));
  this.teamViewDelete     = element(by.className("TeamView")).element(by.className("TeamView-delete"));
  this.teamViewMembers    = element(by.className("TeamView-members")).all(by.className("TeamMember"));
  this.TeamViewBubbles    = element(by.className("TeamView-bubbles")).all(by.className("TeamBubble"));
  this.TeamInviteMemberError = element(by.className("TeamInviteMember-error"));

  this.TeamInviteMemberRole = function(role) {
    return element(by.cssContainingText('.TeamInviteMember-role option', role)).click();
  };
  this.TeamViewMemberNameByindex = function(index) {
    return element(by.className("TeamView-members")).all(by.className("TeamMember")).get(index).element(by.className("TeamMember-name"));
  };
  this.TeamViewMemberDeleteByindex = function(index) {
    return element(by.className("TeamView-members")).all(by.className("TeamMember")).get(index).element(by.className("TeamMember-delete"));
  };
  this.TeamViewMemberRoleByindex = function(index) {
    return element(by.className("TeamView-members")).all(by.className("TeamMember")).get(index).element(by.className('TeamMember-role'));
  };
  this.TeamViewBubbleNameByindex = function(index) {
    return element(by.className("TeamView-bubbles")).all(by.className("TeamBubble")).get(index).element(by.className("TeamBubble-name"));
  };
  this.TeamViewBubbleByname = function(bubble) {
    return element(by.cssContainingText('.TeamBubble-name span', bubble));
  };

};

module.exports = new TeamsPage();
