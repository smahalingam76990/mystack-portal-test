var Notifications = function() {

  this.notificationCount  = element(by.className("notification-count"));
  this.notificationHeader = element.all(by.className("NotificationHeader"));
  this.notificationClear  = element(by.buttonText("Clear All"));

  this.NotificationsByindex = function(index) {
    return element.all(by.className("NotificationGroup")).get(index).all(by.css(".Notifications em"));
  };
};

module.exports = new Notifications();
