var BubblesPage = function() {
  this.createBubble     =  element(by.className("BubblesPage-createBubble"));
  this.bubbles          = element.all(by.className("Bubble"));
  this.searchBubble     = element(by.xpath('//*[@type="search"]'));

  // Locators in Bubble create page
  this.createBubbleName   = element(by.className("CreateBubble-name"));
  this.createBubbleTeam   = element.all(by.css('.CreateBubble-team option'));
  this.createBubbleRegion = element.all(by.css('.CreateBubble-region option'));
  this.createBubblelob    = element.all(by.css('.CreateBubble-lob option'));
  this.createBubbleTicket = element(by.className("CreateBubble-ticketnumber"));
  this.tandcCheckbox      = element(by.xpath('//*[@type="checkbox"]'));
  this.tandc              = element(by.linkText("Accept T&Cs"));
  this.tandcAccept        = element(by.buttonText("ACCEPT"));
  this.tandcDecline       = element(by.buttonText("DECLINE"));
  this.createBubbleCancel = element(by.className("CreateBubble-cancel"));
  this.createButton       = element(by.buttonText("create"));
  this.bubbleCreateError  = element(by.className("CreateBubble-error"));

  this.createBubbleChooseTeam = function(team) {
    element(by.cssContainingText('.CreateBubble-team option', team)).click();
  };

  this.createBubbleChooseRegion = function(region) {
    element(by.cssContainingText('.CreateBubble-region option', region)).click();
  };

  this.bubble = function(bubble) {
    return element(by.cssContainingText('.Bubble-name', bubble));
  };

  this.getBubbleNameByindex = function(index) {
    return element.all(by.className("Bubble")).get(index).element(by.className("Bubble-name")).getText();
  };

  this.getBubbleRegionByindex = function(index) {
    return element.all(by.className("Bubble")).get(index).element(by.className("Bubble-region")).getText();
  };

  this.getBubbleTeamByindex = function(index) {
    return element.all(by.className("Bubble")).get(index).element(by.className("Bubble-team")).getText();
  };

  this.getBubbleCreatedDateByindex = function(index) {
    return element.all(by.className("Bubble")).get(index).element(by.className("Bubble-created-date")).getText();
  };

  this.getBubbleProductionFlagByindex = function(index) {
    return element.all(by.className("Bubble")).get(index).element(by.className("Bubble-production"));
  };

  this.BubblevCloudLink = function(index) {
    return element.all(by.className("Bubble")).get(index).element(by.className("Bubble-vCloudLink"));
  };

  // Locators in BubbleView page
  this.BubbleView       = element(by.className("BubbleView"));
  this.BubbleViewName   = element(by.className("BubbleView")).element(by.className("BubbleView-name"));
  this.BubbleViewDelete = element(by.className("BubbleView")).element(by.className("BubbleView-delete"));
  this.BubbleViewRegion = element(by.className("BubbleView")).element(by.className("BubbleView-region"));
  this.BubbleViewStatus = element(by.className("BubbleView")).element(by.className("BubbleView-status"));
  this.BubbleViewToken        = element(by.className("BubbleView")).element(by.className("BubbleToken"));
  this.BubbleViewMoveTeamOptions = element(by.className("BubbleView")).all(by.css('.BubbleView-team option'));
  this.BubbleViewMoveteams    = element(by.className("BubbleView")).element(by.className("BubbleView-moveTeams"));
  this.BubbleViewTeamSubmit   = element(by.className("BubbleView")).element(by.className("BubbleView-teamSubmit"));
  this.BubbleViewTeamCancel   = element(by.className("BubbleView")).element(by.className("BubbleView-teamCancel"));
  this.BubbleViewTeam         = element(by.className("BubbleView")).element(by.className("BubbleView-team"));
  this.BubbleViewMember       = element(by.className("BubbleView")).element(by.className("BubbleView-members")).all(by.className("BubbleView-member"));
  this.BubbleViewip           = element(by.className("BubbleView")).element(by.className("BubbleView-networking")).all(by.className("BubbleView-ip"));
  this.BubbleViewGateway      = element(by.className("BubbleView")).element(by.className("BubbleView-networking")).element(by.className("BubbleView-gateway"));
  this.BubbleViewEdgeGateway  = element(by.className("BubbleView")).element(by.className("BubbleView-networking")).element(by.className("BubbleView-edgeGateway"));
  this.BubbleViewNetwork      = element(by.className("BubbleView")).all(by.css('.BubbleView-networking tr'));
  this.BubbleViewAddLocalUser = element(by.className("BubbleView")).element(by.className("BubbleView-addLocalUser"));
  this.BubbleViewAddPublicip  = element(by.className("BubbleView")).element(by.className("AddPublicIP"));
  this.BubbleViewAddPublicIPError = element(by.className("BubbleView")).element(by.className("AddPublicIP-error"));
  this.BubbleViewRefreshVms   = element(by.className("BubbleView")).element(by.className("BubbleView-refreshVms"));
  this.BubbleViewNotes        = element(by.className("BubbleView")).element(by.className("BubbleNotes"));
  this.BubbbleViewProduction  = element(by.className("BubbleView")).element(by.className("BubbleView-production"));
  this.BubbleViewClose        = element(by.className("BubbleView")).element(by.className("BubbleView-close"));

  this.BubbleViewLocalUser          = element(by.className("BubbleView")).all(by.className("BubbleLocalUsers-user"));
  this.BubbleViewLocalUserName      = element(by.className("BubbleView")).element(by.className("BubbleLocalUsers-add")).element(by.xpath('//input[@placeholder="Name"]'));
  this.BubbleViewLocalUserPassword  = element(by.className("BubbleView")).element(by.className("BubbleLocalUsers-add")).element(by.xpath('//input[@type="password"]'));
  this.BubbleViewLocalUserAddButton = element(by.className("BubbleView")).element(by.className("BubbleLocalUsers-add")).element(by.buttonText("add"));

  this.BubbleViewMemberByindex = function(index) {
    return element(by.className("BubbleView")).element(by.className("BubbleView-members")).all(by.className("BubbleView-member")).get(index);
  };

  this.BubbleViewLocalUserByindex = function(index) {
    return element(by.className("BubbleView")).all(by.className("BubbleLocalUsers-user")).get(index);
  };

  this.BubbleViewLocalUserDeleteByindex = function(index) {
    return element(by.className("BubbleView")).all(by.className("BubbleLocalUsers-deleteUser")).get(index);
  };

  this.BubbleViewVmNameByindex  = function(index) {
    return element(by.className("BubbleView")).all(by.className("BubbleVM")).get(index).element(by.className("BubbleVM-name"));
  };

  this.BubbleViewVmIpByindex = function(index) {
    return element(by.className("BubbleView")).all(by.className("BubbleVM")).get(index).all(by.className("BubbleVM-ip"));
  };

  this.BubbleViewVmMemoryByindex = function(index) {
    return element(by.className("BubbleView")).all(by.className("BubbleVM")).get(index).element(by.className("BubbleVM-mem"));
  };

  this.BubbleViewVmCpu = function(index) {
    return element(by.className("BubbleView")).all(by.className("BubbleVM")).get(index).element(by.className("BubbleVM-vcpus"));
  };

  // element/locators for SNOW integration
  this.SnowSelectArrow    = element(by.className("BubbleView")).element(by.className("Select-arrow"));
  this.SnowNoResults      = element(by.className("BubbleView")).element(by.className("Select-menu")).element(by.className("Select-noresults"));
  this.SnowInput          = element(by.className("BubbleView")).element(by.className("Select-input")).element(by.xpath('//input[@tabindex="0"]'));
  this.SnowSearchResults  = element(by.className("BubbleView")).all(by.className("Select-option"));

  this.SnowGroupSelect = function(group) {
    element(by.cssContainingText('.Select-option', group)).click();
  };

  this.SnowGroupGetName = function() {
    return element(by.className("Select-placeholder")).getText();
  };

};

module.exports = new BubblesPage();
