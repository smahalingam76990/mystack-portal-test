fn        = require('./../lib/common_lib.js');
ip        = require('ip');
fs        = require('fs');
url       = require("url");
httpSync  = require('http-sync');

// Declaration for XML parsing
UTIL      = {};
UTIL.XML  = require('objtree');
xmlobj    = new UTIL.XML.ObjTree();

// Get the details from Config file
homePageUrl             = browser.params.url.homePage;
loginPage               = browser.params.url.loginPage;
neoPearsonUrl           = browser.params.url.neoPearson;
servicenowUrl           = browser.params.url.servicenow;
waitSeconds             = browser.params.waitSeconds;
bubbleCreateWaitSeconds  = browser.params.bubbleCreateWaitSeconds;


// Set the vCD URL's to get create sessions and get the list of Orgs
vcd = browser.params.vcd.instance;
vcdAuthKey = browser.params.vcd.key;

// Service Authkey
serviceNowAuthKey = browser.params.snowKey;

// Create connection to DB
var cql = require('node-cassandra-cql');
db  = new cql.Client({hosts: [browser.params.db.host], keyspace: browser.params.db.keyspace});

// Test ID start value
testid = 0;

// Report style
jasmineReporters = require('jasmine-reporters');
jasmine.getEnv().addReporter(new jasmineReporters.TeamCityReporter());
// jasmine.getEnv().addReporter(new jasmineReporters.TapReporter());
// var SpecReporter = require('jasmine-spec-reporter');
// jasmine.getEnv().addReporter(new SpecReporter({displayStacktrace: true, displayPendingSpec: true, displaySpecDuration: true}));


// Make full screen browser
browser.manage().window().maximize();
// Implicit wait seconds - Waits for elements to appear on the page
browser.manage().timeouts().implicitlyWait(3000);
// Turn off the protractor synchronisation
browser.ignoreSynchronization = true;
// Create instance for Expected Conditions
EC = protractor.ExpectedConditions;


// This function would be run before each test
// beforeEach(function() {
//   // Open the Home page on the UUT (Unit Under Test)
//   browser.get('/');
// });


// This function would be run before each test
afterEach(function() {
  testid += 1;

  jasmine.getEnv().addReporter(new function() {
    this.specDone = function(result) {
      if (result.failedExpectations.length > 0) {
        browser.takeScreenshot().then(function(data) {
          //var suiteid = jasmine.getEnv().currentSpec.suite.description;
          // Set screenshot name in the format - 'FailedScreenshot-12.png'
          fs.writeFileSync(browser.params.screenShotDirectory + "FailedScreenshot-" + testid + ".png", data, 'base64');
        });
      }
    };
  });

});






