var myCloudPage = require("../pages/myCloud_page.js");
var homePage    = require("../pages/home_page.js");
var notificationsPage = require("../pages/notifications_page.js");

/*************************************************************
Purpose:   Login to the portal
Input:     Username and Password
Comments:  If it was logged in already, it checks whether it
            logged in as expected user and logs/login accordingly
*************************************************************/
function login(username, password) {

  homePage.userMenuName.isPresent().then(function(result) {
    // Not logged in already ?.
    if (!result) {
      homePage.headerSignOn.click();

      // Fill Username, Passsword on the portal and Press Login button - Valid login details
      myCloudPage.userName.clear().sendKeys(username);
      myCloudPage.password.clear().sendKeys("Password1!");
      myCloudPage.submitButton.click();

      // TODO - Temporary fix untill login page is loaded
      browser.wait(EC.visibilityOf(homePage.userMenuName), 10000);

      // Verify user logged in
      expect(browser.getCurrentUrl()).toEqual(homePageUrl);
      expect(homePage.userMenuName.getText()).toEqual(username.replace(".", " "));
    // Logged in already ?. Check whether it logged in as expected user
    } else {
      homePage.userMenuName.getText().then(function(loggedinuser) {
        // Not logged in as expected user ?
        if (loggedinuser != username.replace(".", " ")) {
          // Logout of the current user and login as correct user
          homePage.userMenuName.click();
          homePage.signOut.click();

          homePage.headerSignOn.click();
          // Fill Username, Passsword on the portal and Press Login button - Valid login details
          myCloudPage.userName.clear().sendKeys(username);
          myCloudPage.password.clear().sendKeys("Password1!");
          myCloudPage.submitButton.click();

          // TODO - Temporary fix untill login page is loaded
          browser.wait(EC.visibilityOf(homePage.userMenuName), 10000);

          // Verify user logged in
          expect(browser.getCurrentUrl()).toEqual(homePageUrl);
          expect(homePage.userMenuName.getText()).toEqual(username.replace(".", " "));
        }
      });
    }
  });

}


/************************************************************
Purpose:   Logout from the portal
Input:     None
Comments:  If it was logged out already - dosen't do anything
*************************************************************/
function logout() {

  homePage.userMenuName.isPresent().then(function(result) {
    if (result) {
      homePage.userMenuName.click();
      homePage.signOut.click();
      /* Wait till it logs out
      browser.wait(function() {
        return browser.getCurrentUrl().then(function (currentUrl) {
          return url !== currentUrl;
        });
      }, 10000).then(function() {
        // Just a empty for now
      }, function(err) {
        // Just a empty for now
      });
      */
    }
  });

}


function waitForPageToLoad(milliseconds, exp_url) {
  browser.wait(function() {
    return browser.getCurrentUrl().then(function(url) {
      return url == exp_url;
    });
  }, milliseconds).then(function() {
    // Just a empty for now
  }, function(err) {
    // Just a empty for now
  });
}


function waitForNotifications(milliseconds, exp_notifications) {

  // browser.wait(function() {
  //   return notificationsPage.notificationCount.getText().then(function(totalNotifications) {
  //     return totalNotifications >= exp_notifications;
  //   });
  // }, milliseconds).then(function() {
  //   // Just a empty for now
  // }, function(err) {
  //   // Just a empty for now
  // });

  var result;
  notificationsPage.notificationCount.isPresent().then(function(result) {
    if (result) {
      browser.wait(function() {
        return notificationsPage.notificationCount.getText().then(function(totalNotifications) {
          return totalNotifications >= exp_notifications;
        });
      }, milliseconds).then(function() {
        // Just a empty for now
      }, function(err) {
        // Just a empty for now
      });
    }
  });
}


function clearNotifications() {

  waitForNotifications(500, 1);
  // Clear the notifications
  homePage.notifications.click();
  notificationsPage.notificationClear.click();
}

/*
Purpose: Runs http requests and returns the header & response
Input:   method   - GET, PUT, POST and etc.,
         headers
         address  - In the format - https://developer-mystack.pearson.com/sessions
Output:  Header and response
*/
function runHttpRequests(method, headers, address) {

  var request = httpSync.request({
    method:  method,
    headers: headers,
    body:    '',

    protocol: url.parse(address).protocol,
    host:     url.parse(address).host,
    port:     443,
    path:     url.parse(address).path
  });

  //  Return the XML reponse
  return request.end();
}

function bytesToSize(bytes) {
  bytes *= 1000000;
  var sizes = ['Bytes', 'kb', 'Mb', 'Gb', 'Tb'];

  var result;
  if (bytes == 0) {
    result = '0 Byte';
  } else if (bytes == 1024000000) {
    result = '1Gb';
  } else {
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    result = Math.round(bytes / Math.pow(1024, i), 2) + sizes[i];
  }

  // Return the converted value
  return result;

};

// Get instance of all the functions
exports.logout                = logout;
exports.login                 = login;
exports.waitForPageToLoad     = waitForPageToLoad;
exports.waitForNotifications  = waitForNotifications;
exports.clearNotifications    = clearNotifications;
exports.runHttpRequests       = runHttpRequests;
exports.bytesToSize           = bytesToSize;
