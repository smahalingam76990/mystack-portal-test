var authorization = vcdAuthKey;

var homePage          = require("../../pages/home_page.js");
var teamsPage         = require("../../pages/teams_page.js");
var bubblesPage       = require("../../pages/bubbles_page.js");
var notificationsPage = require("../../pages/notifications_page.js");

describe("Bugs", function() {

  /*
    This test assumes following,
      - Team 'xxx-BrokenTeam' exists and it got bubble - 'xxx-BrokenBubbleDoNotDelete'
      - Local user mystack.user5 & mystack.user6 are exists on the bubble with eMail address field as mystack.user5@pearson.com & mystack.user6@pearson.com respectively
  */

  // Verify users in vCD are deleted by searching name and not by eMail address
  describe("MPSPLAT-418", function() {

    var headers       = {"Accept": "application/*+xml;version=5.1", "Authorization" : authorization};
    var orgExists     = false;
    var orghref       = "";

    beforeAll(function() {
      browser.get('/');
      fn.login("mystack.root", "");
      // Clear the existing notifications on the UI
      fn.clearNotifications();
    });

    beforeEach(function() {
      browser.get('/');
    });

    it("Add users to team 'xxx-BrokenTeam'", function() {
      homePage.teams.click();
      browser.wait(EC.elementToBeClickable(teamsPage.team( 'xxx-BrokenTeam')), waitSeconds);
      teamsPage.team( 'xxx-BrokenTeam').click();
      browser.wait(EC.elementToBeClickable(teamsPage.teamViewAddMember), waitSeconds);
      teamsPage.teamViewAddMember.click();
      teamsPage.TeamInviteMemberRole('Creator');
      teamsPage.teamVieweMail.sendKeys('mystack.user5@pearson.com');
      teamsPage.teamViewAddButton.click();

      browser.wait(EC.elementToBeClickable(teamsPage.teamViewAddMember), waitSeconds);
      teamsPage.teamViewAddMember.click();
      teamsPage.TeamInviteMemberRole('Creator');
      teamsPage.teamVieweMail.sendKeys('mystack.user6@pearson.com');
      teamsPage.teamViewAddButton.click();

      teamsPage.teamViewClose.click();
      fn.waitForNotifications(20000, 4);
    });

    it("Verify users are actually added to the vCD", function() {
      var users = [];

      response = fn.runHttpRequests("POST", headers, vcd + '/api/sessions');
      if (response.statusCode == 200) {
        headers = {"Accept": "application/*+xml;version=5.1", "x-vcloud-authorization": response.headers['x-vcloud-authorization']};

        // Run the Query and get list of Orgs
        response = fn.runHttpRequests("GET", headers, vcd + '/api/admin');
        jsonResult = xmlobj.parseXML(response.body.toString());
        for(var key in jsonResult.VCloud.OrganizationReferences.OrganizationReference) {
          if (jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-name"] == "xxx-BrokenBubbleDoNotDelete") {
            orgExists = true;
            orghref = jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-href"];
            break;
          }
        }
      } else {
        this.fail("Failed to get the auth key. statusCode - ", response.statusCode);
      }

      // Does org 'xxx-DoNotDeleteBubble' exists ?.
      if (orgExists) {
        response = fn.runHttpRequests("GET", headers, orghref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResultOrg = xmlobj.parseXML(response.body.toString());

          if (jsonResultOrg.AdminOrg.Users !== undefined) {
            // Store the href for each user
            for (var key in jsonResultOrg.AdminOrg.Users.UserReference) {
              // Append list of users to the list
              users.push(jsonResultOrg.AdminOrg.Users.UserReference[key]["-name"]);
            }
            // Verify the total number of users
            expect(users).toContain("mystack.user5@pearson.com");
            expect(users).toContain("mystack.user6@pearson.com");
            expect(users).toContain("mystack.local5");
            expect(users).toContain("mystack.local6");
          } else {
            errorMessage = "User is not created on the vCD. So can't continue this test";
            this.fail(errorMessage);
          }
        } else {
          errorMessage = "Failed to run the URL - " + orghref + ". statusCode - " + response.statusCode + ". So can't verify whether Org vdc is created Or not";
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "Org not created for bubble xxx-TestBubble-01. So can't verify whether Org vdc is created Or not";
        this.fail(errorMessage);
      }
    });

    it("Delete user from team - 'xxx-BrokenTeam'", function() {
      // Clear the existing notifications on the UI
      fn.clearNotifications();

      homePage.teams.click();
      browser.wait(EC.elementToBeClickable(teamsPage.team( 'xxx-BrokenTeam')), waitSeconds);
      teamsPage.team( 'xxx-BrokenTeam').click();
      browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);

      // Verify Creator role able to delete user from the team
      browser.actions().mouseUp(element.all(by.className("TeamMember")).last()).perform();
      element.all(by.className("TeamMember")).last().element(by.className("TeamMember-delete")).click();
      browser.sleep(1000);

      // Delete the user 'mystack.user5' from the team
      browser.actions().mouseUp(element.all(by.className("TeamMember")).last()).perform();
      element.all(by.className("TeamMember")).last().element(by.className("TeamMember-delete")).click();

      teamsPage.teamViewClose.click();
      fn.waitForNotifications(20000, 4);
    });

    it("Verify users are not deleted by eMail address field", function() {
      var users = [];

      // Does org 'xxx-DoNotDeleteBubble' exists ?.
      if (orgExists) {
        response = fn.runHttpRequests("GET", headers, orghref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResultOrg = xmlobj.parseXML(response.body.toString());

          if (jsonResultOrg.AdminOrg.Users !== undefined) {
            // Store the href for each user
            for (var key in jsonResultOrg.AdminOrg.Users.UserReference) {
              // Append list of users to the list
              users.push(jsonResultOrg.AdminOrg.Users.UserReference[key]["-name"]);
            }
            // Verify the total number of users
            expect(users).not.toContain("mystack.user5@pearson.com");
            expect(users).not.toContain("mystack.user6@pearson.com");
            expect(users).toContain("mystack.local5");
            expect(users).toContain("mystack.local6");
          } else {
            errorMessage = "User is not created on the vCD. So can't continue this test";
            this.fail(errorMessage);
          }
        } else {
          errorMessage = "Failed to run the URL - " + orghref + ". statusCode - " + response.statusCode + ". So can't verify whether Org vdc is created Or not";
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "Org not created for bubble xxx-TestBubble-01. So can't verify whether Org vdc is created Or not";
        this.fail(errorMessage);
      }
    });

  });


  // Verify bubble creation not stops, if adding user to the org fails
  describe("MPSPLAT-408", function() {

    beforeAll(function() {
      var query1 = "INSERT INTO team(id,bubbles,ips,members,name) VALUES (b1701645-6b4e-11e4-9334-b8e856309420,{b411245a-6b4e-11e4-94a3-b8e856309420},null,{b9d6dce7-538c-11e4-ba85-b8e856309001:'ROLE_CREATOR', b9d6dce7-538c-11e4-ba85-b8e856309421:'ROLE_CREATOR'},'xxx-BrokenTeam')";
      db.execute(query1, function(err, result) {
        if (err) {
          var errorMessage = "Failure message - " + err;
          this.fail(errorMessage);
        }
      });

      var query2 = "INSERT INTO user(id,email,name,role,teams) VALUES (b9d6dce7-538c-11e4-ba85-b8e856309001,'mystack.NotinAD@pearson.com','myStack [NOT IN AD]','ROLE_ROOT',{b1701645-6b4e-11e4-9334-b8e856309420:'ROLE_CREATOR'})";
      db.execute(query2, function(err, result) {
        if (err) {
          var errorMessage = "Failure message - " + err;
          this.fail(errorMessage);
        }
      });

      var query3 = "INSERT INTO user_email(email,user) VALUES ('mystack.NotinAD@pearson.com',b9d6dce7-538c-11e4-ba85-b8e856309001)";
      db.execute(query3, function(err, result) {
        if (err) {
          var errorMessage = "Failure message - " + err;
          this.fail(errorMessage);
        }
      });

      // Set status of network & status of public_ip as 'Available' for the region 'XXXLO3'
      // Set network available for region 'XXXLO3'
      db.execute("SELECT * FROM network WHERE region='XXXLO3'", function(err, result) {
        if (err) {
          var errorMessage = "Failure message - " + err;
          this.fail(errorMessage);
        } else {
          for (var i=0; i < result.rows.length; i++) {
            var query = "UPDATE network SET bubble=null, status=0 WHERE id=" + result.rows[i].id;
            db.execute(query, function(err, update) {
              if (err) {
                var errorMessage = "Failed to run - " + query + ". Failure message - " + err;
                this.fail(errorMessage);
              }
            });
          }
        }
      });

      browser.get('/');
      fn.login("mystack.user4", "");
      // Clear the existing notifications on the UI
      fn.clearNotifications();
    });

    beforeEach(function() {
      browser.get('/');
    });

    it("Verify user able to submit request to create bubble - 'xxx-UserNotinAD'", function() {
      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.createBubble), waitSeconds);
      bubblesPage.createBubble.click();
      bubblesPage.createBubbleName.sendKeys("xxx-UserNotinAD");
      bubblesPage.createBubbleChooseTeam('xxx-FailureTests-01');
      bubblesPage.createBubbleChooseRegion('XXXLO3 - Iceland');
      bubblesPage.tandc.click();
      bubblesPage.tandcAccept.click();
      expect(bubblesPage.tandcCheckbox.isSelected()).toBeTruthy();
      bubblesPage.createButton.click();

      expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
      browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
      bubblesPage.BubbleViewClose.click();
      fn.waitForNotifications(120000, 6);

      // Verify user 'mystack.root' gets notifications while bubble creation fails at Edgegateway - 'xxx-UserNotinADUserNotinAD'
      expect(notificationsPage.notificationCount.getText()).toEqual('6');
      homePage.notifications.click();
      notificationsPage.notificationHeader.get(0).click();
      expect(notificationsPage.NotificationsByindex(0).count()).toEqual(6);
      expect(notificationsPage.NotificationsByindex(0).get(5).getText()).toEqual("A problem occurred creating your bubble. We are investigating");
    });

    it("Verify Org, Org vDC's are created for bubble - 'xxx-UserNotinAD'", function() {
      //  Default values
      var headers           = {"Accept": "application/*+xml;version=5.1", "Authorization" : vcdAuthKey};
      var response, key;
      var orgExists         = false;
      var orghref           = "";
      var orgvDCExists      = false;

      response = fn.runHttpRequests("POST", headers, vcd + '/api/sessions');
      if (response.statusCode == 200) {
        headers = {"Accept": "application/*+xml;version=5.1", "x-vcloud-authorization": response.headers['x-vcloud-authorization']};
        // Run the Query and get list of Orgs
        response = fn.runHttpRequests("GET", headers, vcd + '/api/admin');
        jsonResult = xmlobj.parseXML(response.body.toString());
        for(key in jsonResult.VCloud.OrganizationReferences.OrganizationReference) {
          if (jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-name"] == "xxx-UserNotinAD") {
            orgExists = true;
            orghref = jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-href"];
            break;
          }
        }
      } else {
        this.fail("Failed to get the auth key. statusCode - ", response.statusCode);
      }

      if (orghref !== "") {
        response = fn.runHttpRequests("GET", headers, orghref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          var jsonResultOrg = xmlobj.parseXML(response.body.toString());

          // Loop through each Org and check Org vDC created - Set the FLAG
          if (jsonResultOrg.AdminOrg.Vdcs !== undefined) {
            if (jsonResultOrg.AdminOrg.Vdcs.Vdc["-type"] == "application/vnd.vmware.admin.vdc+xml") {
              orgvDCExists = true;
              orgvdchref = jsonResultOrg.AdminOrg.Vdcs.Vdc["-href"];
            }
          }

        }
      }

      // Verify Org and Org vDC are created for bubble - 'xxx-UserNotinAD'
      expect(orgExists).toBeTruthy();
      expect(orgvDCExists).toBeTruthy();
    });

    it("Verify Administrator/Adoption user able to submit bubble delete request - 'xxx-UserNotinAD'", function() {
      // Clear the existing notifications on the UI
      fn.clearNotifications();

      fn.login("mystack.root", "");
      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-UserNotinAD')), waitSeconds);
      bubblesPage.bubble('xxx-UserNotinAD').click();
      expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();

      // Submit request to 'delete' the bubble
      browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
      browser.actions().mouseUp(bubblesPage.BubbleViewDelete).perform();
      bubblesPage.BubbleViewDelete.click();
      browser.switchTo().alert().accept();
    });

    it("Verify bubble 'xxx-UserNotinAD' is removed from the portal DB", function () {
      fn.login("mystack.user4", "");
      fn.waitForNotifications(180000, 3);
      homePage.bubbles.click();

      browser.wait(EC.invisibilityOf(bubblesPage.bubble('xxx-UserNotinAD')), 10000);
      expect(bubblesPage.bubble('xxx-UserNotinAD').isPresent()).toBeFalsy();
    });

    it("Verify org 'xxx-UserNotinAD' is deleted on vCD", function() {
      var headers = {"Accept": "application/*+xml;version=5.1", "Authorization": authorization};
      var orgExists = false;

      response = fn.runHttpRequests("POST", headers, vcd + '/api/sessions');
      if (response.statusCode == 200) {
        headers = {"Accept": "application/*+xml;version=5.1", "x-vcloud-authorization": response.headers['x-vcloud-authorization']};

        // Run the Query and get list of Orgs
        response = fn.runHttpRequests("GET", headers, vcd + '/api/admin');
        jsonResult = xmlobj.parseXML(response.body.toString());
        for(var key in jsonResult.VCloud.OrganizationReferences.OrganizationReference) {
          if (jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-name"] == "xxx-UserNotinAD") {
            orgExists = true;
            break;
          }
        }
        expect("Org 'xxx-UserNotinAD' exits - false").toEqual("Org 'xxx-UserNotinAD' exits - " + orgExists);
      } else {
        this.fail("Failed to get the auth key. statusCode - ", response.statusCode);
      }
    });

  });

});
