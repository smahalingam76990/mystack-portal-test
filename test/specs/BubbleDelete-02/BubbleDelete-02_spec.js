/*
*****************************************************************************************************************************************************
SUMMARY:      Verify user able to delete the bubble
ATTENTION:    Should be run after 'UserAndTeams_spec.js', 'BubbleCreate-01_spec.js' & 'BubbleCreate-02_spec.js'

List Of Tests:
    BubbleDelete-02: Verify user with 'CREATOR' role on a team able to cancel the bubble delete by pressing the 'Cancel' button on the pop-up window - 'xxx-TestBubble-02'
    BubbleDelete-02: Verify user with 'CREATOR' role on a team able to submit the bubble request - 'xxx-TestBubble-02'
    BubbleDelete-02: Verify user gets notifications for bubble delete - xxx-TestTeam-01'
    BubbleDelete-02: Verify org 'xxx-TestBubble-02' is deleted on vCD
*****************************************************************************************************************************************************
*/

var authorization = vcdAuthKey;
var headers       = {"Accept": "application/*+xml;version=5.1", "Authorization" : vcdAuthKey};
var orgExists     = false;

var homePage          = require("../../pages/home_page.js");
var bubblesPage       = require("../../pages/bubbles_page.js");
var notificationsPage = require("../../pages/notifications_page.js");


describe('BubbleDelete-02', function() {

  beforeAll(function() {
    browser.get('/');
    fn.login("mystack.user2", "");
    fn.clearNotifications();
    fn.login("mystack.user3", "");
    fn.clearNotifications();
  });

  beforeEach(function() {
    browser.get('/');
  });

  it("Verify user with 'CREATOR' role on a team able to cancel the bubble delete by pressing the 'Cancel' button on the pop-up window - 'xxx-TestBubble-02'", function() {
    fn.login("mystack.user1", "");

    homePage.bubbles.click();
    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-02')), waitSeconds);
    bubblesPage.bubble('xxx-TestBubble-02').click();

    // Verify BubbleView page is displayed while submitting request to create bubble
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();

    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    // browser.actions().mouseUp(bubblesPage.BubbleViewDelete).perform();
    bubblesPage.BubbleViewDelete.click();
    expect(browser.switchTo().alert().getText()).toContain("Are you sure you want to delete bubble");
    browser.switchTo().alert().dismiss();
    // TODO - Verify cancel was successful
  });

  it("Verify user with 'CREATOR' role on a team able to submit the bubble delete request - 'xxx-TestBubble-02'", function() {
    // Clear the existing notifications on the UI
    fn.clearNotifications();

    homePage.bubbles.click();
    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-02')), waitSeconds);
    bubblesPage.bubble('xxx-TestBubble-02').click();

    // Verify BubbleView page is displayed while submitting request to create bubble
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();

    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    // browser.actions().mouseUp(bubblesPage.BubbleViewDelete).perform();
    bubblesPage.BubbleViewDelete.click();
    expect(browser.switchTo().alert().getText()).toContain("Are you sure you want to delete bubble");
    browser.switchTo().alert().accept();

    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-02')), waitSeconds);
    bubblesPage.bubble('xxx-TestBubble-02').click();
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    // Verify details Bubble view page while bubble is being deleted
    expect(bubblesPage.BubbleViewDelete.isPresent()).toBeFalsy();
    expect(bubblesPage.BubbleViewRegion.getText()).toEqual('LO3REF');
    expect(bubblesPage.BubbleViewStatus.getText()).toEqual('DELETING');
    expect(bubblesPage.BubbleViewToken.isPresent()).toBeFalsy();
    expect(bubblesPage.BubbleViewTeam.getText()).toEqual('xxx-TestTeam-01');
    expect(bubblesPage.BubbleViewMoveteams.isDisplayed()).toBeFalsy();
    expect(bubblesPage.BubbleViewAddLocalUser.isDisplayed()).toBeFalsy();
    expect(bubblesPage.BubbleViewAddPublicip.isDisplayed()).toBeFalsy();
    expect(bubblesPage.BubbleViewip.isDisplayed()).toEqual([]);
    expect(bubblesPage.BubbleViewGateway.isDisplayed()).toBeFalsy();
    expect(bubblesPage.BubbleViewEdgeGateway.isDisplayed()).toBeFalsy();
    expect(bubblesPage.BubbleViewNetwork.get(0).isDisplayed()).toBeFalsy();
    expect(bubblesPage.BubbleViewNetwork.get(1).isDisplayed()).toBeFalsy();
    // Verify Bubble View page is closed by pressing the 'Close' button
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    bubblesPage.BubbleViewClose.click();
    expect(bubblesPage.BubbleView.isPresent()).toBeFalsy();
  });

  it("Verify user gets notifications for bubble delete - xxx-TestBubble-02'", function() {
    // Verify deleted bubble is removed from UI without Manual refresh - MPSPLAT-407
    homePage.bubbles.click();
    fn.waitForNotifications(bubbleCreateWaitSeconds, 7);
    browser.wait(EC.invisibilityOf(bubblesPage.bubble('xxx-TestBubble-02')), 10000);
    expect(bubblesPage.bubble('xxx-TestBubble-02').isPresent()).toBeFalsy();

    // Verify the content on the notification messages
    expect(notificationsPage.notificationCount.getText()).toEqual('7');
    homePage.notifications.click();
    // Verify total number of 'NotificationGroup' and click on first one
    expect(notificationsPage.notificationHeader.count()).toEqual(1);
    notificationsPage.notificationHeader.get(0).click();
    expect(notificationsPage.notificationHeader.get(0).getText()).toEqual("Bubble Deleting");
    expect(notificationsPage.NotificationsByindex(0).count()).toEqual(7);
    expect(notificationsPage.NotificationsByindex(0).get(0).getText()).toContain("network for xxx-TestBubble-02 is being deleted");
    expect(notificationsPage.NotificationsByindex(0).get(1).getText()).toContain("network for xxx-TestBubble-02 has been deleted");
    expect(notificationsPage.NotificationsByindex(0).get(2).getText()).toContain("network for xxx-TestBubble-02 is being deleted");
    expect(notificationsPage.NotificationsByindex(0).get(3).getText()).toContain("network for xxx-TestBubble-02 has been deleted");
    expect(notificationsPage.NotificationsByindex(0).get(4).getText()).toEqual("Edge for xxx-TestBubble-02 is being deleted");
    expect(notificationsPage.NotificationsByindex(0).get(5).getText()).toEqual("Edge for xxx-TestBubble-02 has now been deleted");
    expect(notificationsPage.NotificationsByindex(0).get(6).getText()).toEqual("xxx-TestBubble-02 has now been deleted");
  });

   it("Verify Bubble delete notifications are sent to all members of a team - (mystack.user2)", function() {
    fn.login("mystack.user2", "");

    // Verify the content on the notification messages
    expect(notificationsPage.notificationCount.getText()).toEqual('7');
    homePage.notifications.click();
    // Verify total number of 'NotificationGroup' and click on first one
    expect(notificationsPage.notificationHeader.count()).toEqual(1);
    notificationsPage.notificationHeader.get(0).click();
    expect(notificationsPage.notificationHeader.get(0).getText()).toEqual("Bubble Deleting");
    expect(notificationsPage.NotificationsByindex(0).count()).toEqual(7);
    expect(notificationsPage.NotificationsByindex(0).get(0).getText()).toContain("network for xxx-TestBubble-02 is being deleted");
    expect(notificationsPage.NotificationsByindex(0).get(6).getText()).toEqual("xxx-TestBubble-02 has now been deleted");
  });

  it("Verify Bubble delete notifications are sent to all members of a team - (mystack.user3)", function() {
    fn.login("mystack.user3", "");

    // Verify the content on the notification messages
    expect(notificationsPage.notificationCount.getText()).toEqual('7');
    homePage.notifications.click();
    // Verify total number of 'NotificationGroup' and click on first one
    expect(notificationsPage.notificationHeader.count()).toEqual(1);
    notificationsPage.notificationHeader.get(0).click();
    expect(notificationsPage.notificationHeader.get(0).getText()).toEqual("Bubble Deleting");
    expect(notificationsPage.NotificationsByindex(0).count()).toEqual(7);
    expect(notificationsPage.NotificationsByindex(0).get(0).getText()).toContain("network for xxx-TestBubble-02 is being deleted");
    expect(notificationsPage.NotificationsByindex(0).get(6).getText()).toEqual("xxx-TestBubble-02 has now been deleted");
  });

  it("Verify org 'xxx-TestBubble-02' is deleted on vCD", function() {
    var response = fn.runHttpRequests("POST", headers, vcd + '/api/sessions');
    if (response.statusCode == 200) {
      headers = {"Accept": "application/*+xml;version=5.1", "x-vcloud-authorization": response.headers['x-vcloud-authorization']};

      // Run the Query and get list of Orgs
      response = fn.runHttpRequests("GET", headers, vcd + '/api/admin');
      jsonResult = xmlobj.parseXML(response.body.toString());
      for(var key in jsonResult.VCloud.OrganizationReferences.OrganizationReference) {
        if (jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-name"] == "xxx-TestBubble-02") {
          orgExists = true;
          break;
        }
      }
      expect("Org 'xxx-TestBubble-02' exits - false").toEqual("Org 'xxx-TestBubble-02' exits - " + orgExists);
    } else {
      this.fail("Failed to get the auth key. statusCode - ", response.statusCode);
    }
  });

});
