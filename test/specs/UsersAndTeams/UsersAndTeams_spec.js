/*
*****************************************************************************************************************************************************
SUMMARY:       Tests to verify whether users can be added/deleted to Portal DB, create/delete team, add/delete users to team and Permission check for
               user with different rights
ATTENTION:     Should start the test with fresh DB

List Of Tests:
    UsersAndTeams: Adding Users: Verify list of users in USERS page
    UsersAndTeams: Adding Users: Root user should able to add new user 'mystack.user1' to portal (user exits in AD)
    UsersAndTeams: Adding Users: Root user should able to add new user 'mystack.user2' to portal (user exits in AD)
    UsersAndTeams: Adding Users: Root user should able to add new user 'mystack.user4' to portal (user exits in AD)
    UsersAndTeams: Adding Users: Root user should able to add new user 'mystack.user5' to portal (user exits in AD)
    UsersAndTeams: Adding Users: Root user should able to add new user 'mystack.user6' to portal (user exits in AD)
    UsersAndTeams: Adding Users: Verify user able to expand to UserView page from user search results - 'mystack.user4'
    Verify user able to close the User View page by clicking the 'Close' button - 'mystack.user4'
    UsersAndTeams: Adding Users: Verify user able to expand to UserView page from user search results - 'mystack.user6'
    Verify user able to close the User View page by clicking the 'Close' button - 'mystack.user6'
    UsersAndTeams: User Delete: Add user 'mystack.user3' to the portal DB
    UsersAndTeams: User Delete: Verify root user able to delete any users from the portal DB
    UsersAndTeams: User Delete: Verify user is actually deleted from the portal DB
    UsersAndTeams: ErrorHandling: User should get error notification while adding invalid user to the portal (user not on AD) - 'mystack.xxx@pearson.com'
    UsersAndTeams: ErrorHandling: User should get error notification while adding user that is already exists - 'mystack.user1@pearson.com'
    UsersAndTeams: User Permissions: Verify permissions for Portal user - 'mystack.user1'
    UsersAndTeams: User Permissions: Verify permissions for Portal user - 'mystack.user2'
    UsersAndTeams: User Permissions: Verify permissions for Adoption user - 'mystack.user5'
    UsersAndTeams: User Permissions: Verify permissions for Administrator user - 'mystack.user6'
    UsersAndTeams: User Permissions: Verify Admin user able to change the permission for other users ie.,Portal->Administrator
    UsersAndTeams: User Permissions: Verify Admin user able to change the permission for other users ie.,Administrator->Portal
    UsersAndTeams: TEAMS: Verify Portal user not able to create new team
    UsersAndTeams: TEAMS: Verify Adoption/Administrator user able to create new team - 'xxx-TestTeam-01'
    UsersAndTeams: TEAMS: Verify Adoption/Administrator user able to create new team - 'xxx-TestTeam-02'
    UsersAndTeams: TEAMS: Verify user able to see all the teams in 'TEAMS' page
    UsersAndTeams: TEAMS: Verify user able to filter the team by typing partial team name on the search box - 'xxx'
    UsersAndTeams: TEAMS: Verify user able to filter the bubble by typing partial bubble name on the search box (different case) - 'XXX'
    UsersAndTeams: TEAMS: Verify user able to see the team details - 'xxx-BrokenTeam'
    UsersAndTeams: TEAMS: Verify user able to see the team details - 'xxx-TestTeam-01'
    UsersAndTeams: TEAMS: Verify user able to see the team details - 'xxx-TestTeam-02'
    UsersAndTeams: TEAMS: Verify Adoption/Administrator user able to add user to the team - 'xxx-TestTeam-01
    UsersAndTeams: TEAMS: Verify Adoption/Administrator user able to add user to the Portal through TEAMS page - 'xxx-TestTeam-01
    UsersAndTeams: TEAMS: Verify user 'mystack.user3' is added to the Portal database
    UsersAndTeams: TEAMS: Verify Adoption/Administrator user able to add user to the team - 'xxx-TestTeam-02
    UsersAndTeams: TEAMS: Verify user with 'CREATOR' role able to add/delete more users to the team - 'xxx-TestTeam-01
    UsersAndTeams: TEAMS: Verify permissions for user 'mystack.user1' in team 'xxx-TestTeam-01'
    UsersAndTeams: TEAMS: Verify permissions for user 'mystack.user1' in team 'xxx-TestTeam-02'
    UsersAndTeams: TEAMS: Verify permissions for user 'mystack.user2' in team 'xxx-TestTeam-01'
    UsersAndTeams: TEAMS: Verify permissions for user 'mystack.user2' in team 'xxx-TestTeam-02'
    UsersAndTeams: TEAMS: Verify permissions for Portal user - 'mystack.user3'
    UsersAndTeams: TEAMS: ErrorHandling: Create team for error message
    UsersAndTeams: TEAMS: ErrorHandling: Verify user not able to create a team which already exists - 'xxx-TestTeam-03'
    UsersAndTeams: TEAMS: ErrorHandling: Verify user not able to rename team with exiting name
    UsersAndTeams: TEAMS: ErrorHandling: Verify user with 'CREATOR' role able to delete the team - 'xxx-TestTeam-03'
    UsersAndTeams: TEAMS: ErrorHandling: Verify user with 'CREATOR' role able to delete the team - 'xxx-TestTeam-04'
*****************************************************************************************************************************************************
*/

describe('UsersAndTeams', function() {

  var usersPage = require("../../pages/users_page.js");
  var homePage  = require("../../pages/home_page.js");
  var teamsPage = require("../../pages/teams_page.js");

  describe('Adding Users', function() {

    beforeAll(function() {
      browser.get('/');
      fn.login("mystack.root", "");
    });

    beforeEach(function() {
      browser.get('/');
      homePage.users.click();
      browser.wait(EC.elementToBeClickable(usersPage.createUser), waitSeconds);
    });

    it("Verify list of users in USERS page", function() {
      expect(browser.getCurrentUrl()).toEqual(homePageUrl + "users/");
      expect(usersPage.users.count()).toBeGreaterThan(10);
    });

    it("Root user should able to add new user 'mystack.user1' to portal (user exits in AD)", function() {
      usersPage.createUser.click();
      usersPage.chooseUserRole('Portal');
      usersPage.createUsereMail.sendKeys('mystack.user1@pearson.com');
      usersPage.createButton.click();

      // Verify the details in User view page
      expect(usersPage.userView.isPresent()).toBeTruthy();
      browser.wait(EC.elementToBeClickable(usersPage.userViewClose), waitSeconds);
      expect(usersPage.userViewName.getText()).toEqual('mystack user1');
      expect(usersPage.userViewTeams).toEqual([]);
      usersPage.userViewClose.click();
      expect(usersPage.userView.isPresent()).toBeFalsy();
    });

    it("Root user should able to add new user 'mystack.user2' to portal (user exits in AD)", function() {
      usersPage.createUser.click();
      usersPage.chooseUserRole('Portal');
      usersPage.createUsereMail.sendKeys('mystack.user2@pearson.com');
      usersPage.createButton.click();

      // Verify the details in User view page
      expect(usersPage.userView.isPresent()).toBeTruthy();
      browser.wait(EC.elementToBeClickable(usersPage.userViewClose), waitSeconds);
      expect(usersPage.userViewName.getText()).toEqual('mystack user2');
      usersPage.userViewClose.click();
    });

    it("Root user should able to add new user 'mystack.user4' to portal (user exits in AD)", function() {
      usersPage.createUser.click();
      usersPage.chooseUserRole('Portal');
      usersPage.createUsereMail.sendKeys('mystack.user4@pearson.com');
      usersPage.createButton.click();

      // Verify the details in User view page
      expect(usersPage.userView.isPresent()).toBeTruthy();
      browser.wait(EC.elementToBeClickable(usersPage.userViewClose), waitSeconds);
      expect(usersPage.userViewName.getText()).toEqual('mystack user4');
      usersPage.userViewClose.click();
    });

    it("Root user should able to add new user 'mystack.user5' to portal (user exits in AD)", function() {
     usersPage.createUser.click();
      usersPage.chooseUserRole('Adoption');
      usersPage.createUsereMail.sendKeys('mystack.user5@pearson.com');
      usersPage.createButton.click();

      // Verify the details in User view page
      expect(usersPage.userView.isPresent()).toBeTruthy();
      browser.wait(EC.elementToBeClickable(usersPage.userViewClose), waitSeconds);
      expect(usersPage.userViewName.getText()).toEqual('mystack user5');
      usersPage.userViewClose.click();
    });

    it("Root user should able to add new user 'mystack.user6' to portal (user exits in AD)", function() {
      usersPage.createUser.click();
      usersPage.chooseUserRole('Administrator');
      usersPage.createUsereMail.sendKeys('mystack.user6@pearson.com');
      usersPage.createButton.click();

      // Verify the details in User view page
      expect(usersPage.userView.isPresent()).toBeTruthy();
      browser.wait(EC.elementToBeClickable(usersPage.userViewClose), waitSeconds);
      expect(usersPage.userViewName.getText()).toEqual('mystack user6');
      expect(usersPage.userViewTeams).toEqual([]);
      usersPage.userViewClose.click();
      expect(usersPage.userView.isPresent()).toBeFalsy();
    });

    it("Verify user able to expand to UserView page from user search results - 'mystack.user4'", function() {
      browser.wait(EC.elementToBeClickable(usersPage.user('mystack user4')), waitSeconds);

      expect(usersPage.user('mystack root').isPresent()).toBeTruthy();
      expect(usersPage.user('mystack user4').isPresent()).toBeTruthy();

      usersPage.user('mystack user4').click();
      expect(usersPage.userView.isPresent()).toBeTruthy();
      browser.wait(EC.elementToBeClickable(usersPage.userViewClose), waitSeconds);
      expect(usersPage.userViewName.getText()).toEqual('mystack user4');
      expect(usersPage.userViewTeams).toEqual([]);
      // Verify user delete option is available
      expect(usersPage.userViewDelete.isPresent()).toBeTruthy();
      expect(usersPage.userViewDelete.isDisplayed()).toBeFalsy();
      browser.actions().mouseUp(usersPage.userViewDelete).perform();
      expect(usersPage.userViewDelete.isDisplayed()).toBeTruthy();
    });

    it("Verify user able to close the User View page by clicking the 'Close' button - 'mystack.user4'", function() {
      browser.wait(EC.elementToBeClickable(usersPage.user('mystack user4')), waitSeconds);
      usersPage.user('mystack user4').click();

      expect(usersPage.userView.isPresent()).toBeTruthy();
      browser.wait(EC.elementToBeClickable(usersPage.userViewClose), waitSeconds);
      expect(usersPage.userViewName.getText()).toEqual('mystack user4');

      usersPage.userViewClose.click();
      expect(usersPage.userView.isPresent()).toBeFalsy();
    });

    it("Verify user able to expand to UserView page from user search results - 'mystack.user6'", function() {
      browser.wait(EC.elementToBeClickable(usersPage.user('mystack user6')), waitSeconds);

      usersPage.user('mystack user6').click();
      expect(usersPage.userView.isPresent()).toBeTruthy();
      browser.wait(EC.elementToBeClickable(usersPage.userViewClose), waitSeconds);
      expect(usersPage.userViewName.getText()).toEqual('mystack user6');
      expect(usersPage.userViewTeams).toEqual([]);
      // Verify user delete option is available
      expect(usersPage.userViewDelete.isPresent()).toBeTruthy();
      expect(usersPage.userViewDelete.isDisplayed()).toBeFalsy();
      browser.actions().mouseUp(usersPage.userViewDelete).perform();
      browser.wait(EC.visibilityOf(usersPage.userViewDelete), waitSeconds);
      expect(usersPage.userViewDelete.isDisplayed()).toBeTruthy();
    });

    it("Verify user able to close the User View page by clicking the 'Close' button - 'mystack.user6'", function() {
      browser.wait(EC.elementToBeClickable(usersPage.user('mystack user6')), waitSeconds);
      usersPage.user('mystack user6').click();

      expect(usersPage.userView.isPresent()).toBeTruthy();
      browser.wait(EC.elementToBeClickable(usersPage.userViewClose), waitSeconds);
      expect(usersPage.userViewName.getText()).toEqual('mystack user6');

      usersPage.userViewClose.click();
      expect(usersPage.userView.isPresent()).toBeFalsy();
    });

    it("Added to verify whether user able to filter the users by typing partial text (matching case) - 'mystack.user'", function() {
      browser.wait(EC.elementToBeClickable(usersPage.searchUser), waitSeconds);
      usersPage.searchUser.click();
      usersPage.searchUser.sendKeys('mystack.');

      expect(usersPage.users.count()).toEqual(6);
      expect(usersPage.user('mystack user1').isPresent()).toBeTruthy();
      expect(usersPage.user('mystack user6').isPresent()).toBeTruthy();
    });

    it("Added to verify whether user able to filter the users by typing partial text (different case) - 'mystack.user'", function() {
      browser.wait(EC.elementToBeClickable(usersPage.searchUser), waitSeconds);
      usersPage.searchUser.click();
      usersPage.searchUser.sendKeys('MYSTACK.');

      expect(usersPage.users.count()).toEqual(6);
      expect(usersPage.user('mystack user1').isPresent()).toBeTruthy();
      expect(usersPage.user('mystack user6').isPresent()).toBeTruthy();
    });

  });

  describe("User Delete", function() {

    beforeAll(function() {
      browser.get('/');
      fn.login("mystack.root", "");
    });

    beforeEach(function() {
      browser.get('/');
      homePage.users.click();
      browser.wait(EC.elementToBeClickable(usersPage.createUser), waitSeconds);
    });

    it("Add user 'mystack.user3' to the portal DB", function() {
      usersPage.createUser.click();
      usersPage.chooseUserRole('Portal');
      usersPage.createUsereMail.sendKeys('mystack.user3@pearson.com');
      usersPage.createButton.click();

      // Verify the details in User view page
      expect(usersPage.userView.isPresent()).toBeTruthy();
      browser.wait(EC.elementToBeClickable(usersPage.userViewClose), waitSeconds);
      expect(usersPage.userViewName.getText()).toEqual('mystack user3');
      usersPage.userViewClose.click();
    });

    it("Verify root user able to delete any users from the portal DB", function() {
      browser.wait(EC.elementToBeClickable(usersPage.user('mystack user3')), waitSeconds);
      usersPage.user('mystack user3').click();

      // Cancel the user delete by pressing the 'Cancel' button on the pop-up window
      expect(usersPage.userViewDelete.isPresent()).toBeTruthy();

      browser.actions().mouseUp(usersPage.userViewDelete).perform();
      usersPage.userViewDelete.click();
      browser.switchTo().alert().dismiss();
      expect(usersPage.userView.isPresent()).toBeTruthy();

      // Now press 'OK' on the pop-up window
      browser.actions().mouseUp(usersPage.userViewDelete).perform();
      usersPage.userViewDelete.click();
      browser.switchTo().alert().accept();

      // Might have to untill the UserView page is closed
      browser.sleep(2000);
      expect(usersPage.userView.isPresent()).toBeFalsy();
    });

    it("Verify user is actually deleted from the portal DB", function() {
      usersPage.searchUser.click();
      usersPage.searchUser.sendKeys('mystack.user3');

      expect(usersPage.users).toEqual([]);
    });

  });

  describe("ErrorHandling", function() {

    beforeAll(function() {
      browser.get('/');
      fn.login("mystack.root", "");
    });

    beforeEach(function() {
      browser.get('/');
      homePage.users.click();
      browser.wait(EC.elementToBeClickable(usersPage.createUser), waitSeconds);
    });

    it("User should get error notification while adding invalid user to the portal (user not on AD) - 'mystack.xxx@pearson.com'", function() {
      usersPage.createUser.click();
      usersPage.chooseUserRole('Portal');
      usersPage.createUsereMail.sendKeys('mystack.xxx@pearson.com');
      usersPage.createButton.click();
      browser.wait(EC.visibilityOf(usersPage.createUserError), 5000);
      expect(usersPage.createUserError.getText()).toEqual("User Not Found");
      // Error message go away while pressing 'cancel' button
      usersPage.createTeamCancel.click();
      expect(usersPage.createUserError.isPresent()).toBeFalsy();
    });

    it("User should get error notification while adding user that is already exists - 'mystack.user1@pearson.com'", function() {
      usersPage.createUser.click();
      usersPage.chooseUserRole('Portal');
      usersPage.createUsereMail.sendKeys('mystack.user1@pearson.com');
      usersPage.createButton.click();
      browser.wait(EC.visibilityOf(usersPage.createUserError), 5000);
      expect(usersPage.createUserError.getText()).toEqual("Duplicate Email");
      // Error message go away while pressing 'cancel' button
      usersPage.createTeamCancel.click();
      expect(usersPage.createUserError.isPresent()).toBeFalsy();
    });

  });


  describe("User Permissions", function() {

    beforeAll(function() {
      browser.get('/');
      fn.login("mystack.root", "");
    });

    beforeEach(function() {
      browser.get('/');
    });

    it("Verify permissions for Portal user - 'mystack.user1'", function() {
      // Login as 'mystack.user1'
      fn.login("mystack.user1", "");

      expect(homePage.bubbles.isPresent()).toBeTruthy();
      expect(homePage.teams.isPresent()).toBeTruthy();
      expect(homePage.users.isPresent()).toBeFalsy();
      expect(homePage.marketplace.isPresent()).toBeTruthy();
      expect(homePage.usage.isPresent()).toBeFalsy();
      expect(homePage.search.isPresent()).toBeFalsy();
    });

    it("Verify permissions for Portal user - 'mystack.user4'", function() {
      // Login as 'mystack.user1'
      fn.login("mystack.user4", "");

      expect(homePage.bubbles.isPresent()).toBeTruthy();
      expect(homePage.teams.isPresent()).toBeTruthy();
      expect(homePage.users.isPresent()).toBeFalsy();
      expect(homePage.marketplace.isPresent()).toBeTruthy();
      expect(homePage.usage.isPresent()).toBeFalsy();
      expect(homePage.search.isPresent()).toBeFalsy();
    });

    it("Verify permissions for Adoption user - 'mystack.user5'", function() {
      // Login as 'mystack.user1'
      fn.login("mystack.user5", "");

      expect(homePage.bubbles.isPresent()).toBeTruthy();
      expect(homePage.teams.isPresent()).toBeTruthy();
      expect(homePage.users.isPresent()).toBeTruthy();
      expect(homePage.marketplace.isPresent()).toBeTruthy();
      expect(homePage.usage.isPresent()).toBeTruthy();
      expect(homePage.search.isPresent()).toBeTruthy();
    });

    it("Verify permissions for Administrator user - 'mystack.user6'", function() {
      // Login as 'mystack.user1'
      fn.login("mystack.user6", "");

      expect(homePage.bubbles.isPresent()).toBeTruthy();
      expect(homePage.teams.isPresent()).toBeTruthy();
      expect(homePage.users.isPresent()).toBeTruthy();
      expect(homePage.marketplace.isPresent()).toBeTruthy();
      expect(homePage.usage.isPresent()).toBeTruthy();
      expect(homePage.search.isPresent()).toBeTruthy();
    });

    it("Verify Admin user able to change the permission for other users ie.,Portal->Administrator", function() {
      // Login as 'mystack.user1'
      fn.login("mystack.root", "");

      // Change role for the user 'mystack.user2' ie.,Portal -> Administrator
      homePage.users.click();
      browser.wait(EC.elementToBeClickable(usersPage.user('mystack user2')), waitSeconds);
      usersPage.user('mystack user2').click();
      browser.wait(EC.elementToBeClickable(usersPage.userViewClose), waitSeconds);
      expect(usersPage.userViewName.getText()).toEqual('mystack user2');

      usersPage.chooseUserViewRole('Adoption');
      usersPage.userViewClose.click();

      browser.refresh();
      fn.login("mystack.user2", "");
      expect(homePage.bubbles.isPresent()).toBeTruthy();
      expect(homePage.teams.isPresent()).toBeTruthy();
      expect(homePage.users.isPresent()).toBeTruthy();
      expect(homePage.marketplace.isPresent()).toBeTruthy();
      expect(homePage.usage.isPresent()).toBeTruthy();
      expect(homePage.search.isPresent()).toBeTruthy();

      // Revert role for user 'mystack.user2' ie.,Administrator -> Portal
      fn.login("mystack.root", "");
      homePage.users.click();
      browser.wait(EC.elementToBeClickable(usersPage.user('mystack user2')), waitSeconds);
      usersPage.user('mystack user2').click();
      browser.wait(EC.elementToBeClickable(usersPage.userViewClose), waitSeconds);
      expect(usersPage.userViewName.getText()).toEqual('mystack user2');
      usersPage.chooseUserViewRole('Portal');
      usersPage.userViewClose.click();
      browser.refresh();
      fn.login("mystack.user2", "");
      expect(homePage.bubbles.isPresent()).toBeTruthy();
      expect(homePage.teams.isPresent()).toBeTruthy();
      expect(homePage.users.isPresent()).toBeFalsy();
      expect(homePage.marketplace.isPresent()).toBeTruthy();
      expect(homePage.usage.isPresent()).toBeFalsy();
      expect(homePage.search.isPresent()).toBeFalsy();
    });

    it("Verify Admin user able to change the permission for other users ie.,Administrator->Portal", function() {
      // Login as 'mystack.user1'
      fn.login("mystack.root", "");

      // Change role for the user 'mystack.user6' ie.,Administrator -> Portal
      homePage.users.click();
      browser.wait(EC.elementToBeClickable(usersPage.user('mystack user5')), waitSeconds);
      usersPage.user('mystack user5').click();
      browser.wait(EC.elementToBeClickable(usersPage.userViewClose), waitSeconds);
      expect(usersPage.userViewName.getText()).toEqual('mystack user5');
      usersPage.chooseUserViewRole('Portal');
      usersPage.userViewClose.click();
      browser.refresh();
      fn.login("mystack.user5", "");
      expect(homePage.bubbles.isPresent()).toBeTruthy();
      expect(homePage.teams.isPresent()).toBeTruthy();
      expect(homePage.users.isPresent()).toBeFalsy();
      expect(homePage.marketplace.isPresent()).toBeTruthy();
      expect(homePage.usage.isPresent()).toBeFalsy();
      expect(homePage.search.isPresent()).toBeFalsy();

      // Revert role for user 'mystack.user6' ie.,Portal -> Administrator
      fn.login("mystack.root", "");
      homePage.users.click();
      browser.wait(EC.elementToBeClickable(usersPage.user('mystack user5')), waitSeconds);
      usersPage.user('mystack user5').click();
      browser.wait(EC.elementToBeClickable(usersPage.userViewClose), waitSeconds);
      expect(usersPage.userViewName.getText()).toEqual('mystack user5');
      usersPage.chooseUserViewRole('Adoption');
      usersPage.userViewClose.click();
      browser.refresh();
      fn.login("mystack.user5", "");
      expect(homePage.bubbles.isPresent()).toBeTruthy();
      expect(homePage.teams.isPresent()).toBeTruthy();
      expect(homePage.users.isPresent()).toBeTruthy();
      expect(homePage.marketplace.isPresent()).toBeTruthy();
      expect(homePage.usage.isPresent()).toBeTruthy();
      expect(homePage.search.isPresent()).toBeTruthy();
    });

  });


  describe("TEAMS", function() {

    beforeAll(function() {
      browser.get('/');
      fn.login("mystack.user6", "");
    });

    beforeEach(function() {
      browser.get('/');
    });

    it("Verify Adoption/Administrator user able to create new team - 'xxx-TestTeam-01'", function() {
      homePage.teams.click();

      browser.wait(EC.elementToBeClickable(teamsPage.createTeam), waitSeconds);
      teamsPage.createTeam.click();
      teamsPage.createTeamName.sendKeys('xxx-TestTeam-01');
      teamsPage.createButton.click();

      expect(teamsPage.teamView.isPresent()).toBeTruthy();
      // Verify user able to close the teamview by clicking the 'Close' button
      browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
      teamsPage.teamViewClose.click();
      expect(teamsPage.teamView.isPresent()).toBeFalsy();
    });

    it("Verify Adoption/Administrator user able to create new team - 'xxx-TestTeam-02'", function() {
      homePage.teams.click();

      browser.wait(EC.elementToBeClickable(teamsPage.createTeam), waitSeconds);
      teamsPage.createTeam.click();
      teamsPage.createTeamName.sendKeys('xxx-TestTeam-02');
      teamsPage.createButton.click();

      expect(teamsPage.teamView.isPresent()).toBeTruthy();
      // Verify user able to close the teamview by clicking the 'Close' button
      browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
      teamsPage.teamViewClose.click();
      expect(teamsPage.teamView.isPresent()).toBeFalsy();
    });

    it("Verify user able to see all the teams in 'TEAMS' page", function() {
      fn.login("mystack.root", "");
      homePage.teams.click();
      expect(browser.getCurrentUrl()).toEqual(homePageUrl + "teams/");
      expect(teamsPage.teams.count()).toBeGreaterThan(10);
    });

    it("Verify user able to filter the team by typing partial team name on the search box - 'xxx'", function() {
      homePage.teams.click();
      browser.wait(EC.elementToBeClickable(teamsPage.searchTeam), waitSeconds);
      teamsPage.searchTeam.click();
      teamsPage.searchTeam.sendKeys('xxx-');

      // Verify the team details - 'xxx-BrokenTeam'
      expect(teamsPage.teams.count()).toEqual(3);
      expect(teamsPage.getTeamNameByindex(0)).toEqual('xxx-BrokenTeam');
      expect(teamsPage.getTeamBubblesByindex(0)).toEqual('1');
      expect(teamsPage.getTeamMemberCountByindex(0)).toEqual('1');
      expect(teamsPage.getTeamMembersByindex(0).get(0).getText()).toEqual('mystack root');

      // Verify the team details - 'xxx-TestTeam-01'
      expect(teamsPage.getTeamNameByindex(1)).toEqual('xxx-TestTeam-01');
      expect(teamsPage.getTeamBubblesByindex(1)).toEqual('0');
      expect(teamsPage.getTeamMemberCountByindex(1)).toEqual('0');
      expect(teamsPage.getTeamMembersByindex(1)).toEqual([]);

      // Verify the team details - 'xxx-TestTeam-02'
      expect(teamsPage.getTeamNameByindex(2)).toEqual('xxx-TestTeam-02');
      expect(teamsPage.getTeamBubblesByindex(2)).toEqual('0');
      expect(teamsPage.getTeamMemberCountByindex(2)).toEqual('0');
      expect(teamsPage.getTeamMembersByindex(2)).toEqual([]);
    });

    it("Verify user able to filter the bubble by typing partial bubble name on the search box (different case) - 'XXX'", function() {
      homePage.teams.click();
      browser.wait(EC.elementToBeClickable(teamsPage.searchTeam), waitSeconds);
      teamsPage.searchTeam.click();
      teamsPage.searchTeam.sendKeys('XXX-');

      // Verify the team details - 'xxx-BrokenTeam'
      expect(teamsPage.teams.count()).toEqual(3);
      expect(teamsPage.getTeamNameByindex(0)).toEqual('xxx-BrokenTeam');
      expect(teamsPage.getTeamBubblesByindex(0)).toEqual('1');
      expect(teamsPage.getTeamMemberCountByindex(0)).toEqual('1');
      expect(teamsPage.getTeamMembersByindex(0).get(0).getText()).toEqual('mystack root');

      // Verify the team details - 'xxx-TestTeam-01'
      expect(teamsPage.getTeamNameByindex(1)).toEqual('xxx-TestTeam-01');
      expect(teamsPage.getTeamBubblesByindex(1)).toEqual('0');
      expect(teamsPage.getTeamMemberCountByindex(1)).toEqual('0');
      expect(teamsPage.getTeamMembersByindex(1)).toEqual([]);

      // Verify the team details - 'xxx-TestTeam-02'
      expect(teamsPage.getTeamNameByindex(2)).toEqual('xxx-TestTeam-02');
      expect(teamsPage.getTeamBubblesByindex(2)).toEqual('0');
      expect(teamsPage.getTeamMemberCountByindex(2)).toEqual('0');
      expect(teamsPage.getTeamMembersByindex(2)).toEqual([]);
    });

    it("Verify user able to see the team details - 'xxx-BrokenTeam'", function() {
      homePage.teams.click();

      expect(teamsPage.teamView.isPresent()).toBeFalsy();
      browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-BrokenTeam')), waitSeconds);
      teamsPage.team('xxx-BrokenTeam').click();
      expect(teamsPage.teamView.isPresent()).toBeTruthy();

      // Verify bubble details on 'Bubble View' page
      browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
      expect(teamsPage.teamViewName.getText()).toEqual('xxx-BrokenTeam');
      expect(teamsPage.teamViewMembers.count()).toEqual(1);
      expect(teamsPage.TeamViewMemberNameByindex(0).getText()).toEqual('mystack root');
      expect(teamsPage.TeamViewMemberDeleteByindex(0).isDisplayed()).toBeFalsy();
      browser.actions().mouseUp(teamsPage.teamViewMembers.first()).perform();
      expect(teamsPage.TeamViewMemberDeleteByindex(0).isDisplayed()).toBeTruthy();
      expect(teamsPage.TeamViewBubbles.count()).toEqual(1);
      expect(teamsPage.TeamViewBubbleNameByindex(0).getText()).toEqual('xxx-BrokenBubbleDoNotDelete (XXXLO3)');
    });

    it("Verify user able to see the team details - 'xxx-TestTeam-01'", function() {
      homePage.teams.click();

      expect(teamsPage.teamView.isPresent()).toBeFalsy();
      browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-01')), waitSeconds);
      teamsPage.team('xxx-TestTeam-01').click();
      // element.all(by.className("Team")).get(1).click();
      expect(teamsPage.teamView.isPresent()).toBeTruthy();

      // Verify bubble details on 'Bubble View' page
      browser.wait(EC.elementToBeClickable(teamsPage.teamViewAddMember), waitSeconds);
      expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-01');
      expect(teamsPage.teamViewAddMember.isDisplayed()).toBeTruthy();
      expect(teamsPage.teamViewMembers.count()).toEqual(0);
      expect(teamsPage.TeamViewBubbles.count()).toEqual(0);

      // Verify user able to close the teamview by clicking the 'Close' button
      expect(teamsPage.teamViewClose.isEnabled()).toBeTruthy();
    });

    it("Verify user able to see the team details - 'xxx-TestTeam-02'", function() {
      homePage.teams.click();

      expect(teamsPage.teamView.isPresent()).toBeFalsy();
      browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-02')), waitSeconds);
      teamsPage.team('xxx-TestTeam-02').click();
      expect(teamsPage.teamView.isPresent()).toBeTruthy();

      // Verify bubble details on 'Bubble View' page
      browser.wait(EC.elementToBeClickable(teamsPage.teamViewAddMember), waitSeconds);
      expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-02');
      expect(teamsPage.teamViewAddMember.isDisplayed()).toBeTruthy();
      expect(teamsPage.teamViewMembers.count()).toEqual(0);
      expect(teamsPage.TeamViewBubbles.count()).toEqual(0);

      // Verify user able to close the teamview by clicking the 'Close' button
      expect(teamsPage.teamViewClose.isEnabled()).toBeTruthy();
    });

    it("Verify Adoption/Administrator user able to add user to the team - 'xxx-TestTeam-01", function() {
      homePage.teams.click();

      browser.wait(EC.elementToBeClickable(teamsPage.searchTeam), waitSeconds);
      teamsPage.searchTeam.click();
      teamsPage.searchTeam.sendKeys('xxx-TestTeam-01');

      expect(teamsPage.teams.count()).toEqual(1);
      browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-01')), waitSeconds);
      teamsPage.team('xxx-TestTeam-01').click();
      browser.wait(EC.elementToBeClickable(teamsPage.teamViewAddMember), waitSeconds);
      teamsPage.teamViewAddMember.click();
      teamsPage.TeamInviteMemberRole('Creator').click();
      teamsPage.teamVieweMail.sendKeys('mystack.user1@pearson.com');
      teamsPage.teamViewAddButton.click();
      // Verify details in 'TEAMS' page got updated while adding user to the teams
      // TODO - Have to replace below
      // expect(element.all(by.className("Team")).get(0).element(by.className("Team-memberCount")).getText()).toEqual('1');

      browser.wait(EC.elementToBeClickable(teamsPage.teamViewAddMember), waitSeconds);
      teamsPage.teamViewAddMember.click();
      teamsPage.TeamInviteMemberRole('User').click();
      teamsPage.teamVieweMail.sendKeys('mystack.user2@pearson.com');
      teamsPage.teamViewAddButton.click();

      browser.sleep(1000);
      expect(teamsPage.teamViewMembers.count()).toEqual(2);
      // Verify details in 'TEAMS' page got updated while adding user to the teams
      // expect(element.all(by.className("Team")).get(0).element(by.className("Team-memberCount")).getText()).toEqual('2');
    });

    it("Verify Adoption/Administrator user able to add user to the Portal through TEAMS page - 'xxx-TestTeam-01", function() {
      homePage.teams.click();

      browser.wait(EC.elementToBeClickable(teamsPage.searchTeam), waitSeconds);
      teamsPage.searchTeam.click();
      teamsPage.searchTeam.sendKeys('xxx-TestTeam-01');

      expect(teamsPage.teams.count()).toEqual(1);
      browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-01')), waitSeconds);
      teamsPage.team('xxx-TestTeam-01').click();
      browser.wait(EC.elementToBeClickable(teamsPage.teamViewAddMember), waitSeconds);
      teamsPage.teamViewAddMember.click();
      teamsPage.TeamInviteMemberRole('Creator').click();
      teamsPage.teamVieweMail.sendKeys('mystack.user3@pearson.com');
      teamsPage.teamViewAddButton.click();

      browser.sleep(1000);
      expect(teamsPage.teamViewMembers.count()).toEqual(3);
      // Verify details in 'TEAMS' page got updated while adding user to the teams
      // TODO - Have to replace below
      // expect(element.all(by.className("Team")).get(0).element(by.className("Team-memberCount")).getText()).toEqual('3');
    });

    it("Verify user 'mystack.user3' is added to the Portal database", function() {
      homePage.users.click();

      browser.wait(EC.elementToBeClickable(teamsPage.searchTeam), waitSeconds);
      teamsPage.searchTeam.click();
      teamsPage.searchTeam.sendKeys('mystack.user');

      expect(usersPage.user('mystack user3').isPresent()).toBeTruthy();
      expect(usersPage.user('mystack user6').isPresent()).toBeTruthy();

      browser.wait(EC.elementToBeClickable(usersPage.user('mystack user3')), waitSeconds);
      usersPage.user('mystack user3').click();
      browser.wait(EC.elementToBeClickable(usersPage.userViewClose), waitSeconds);
      expect(usersPage.userViewName.getText()).toEqual('mystack user3');
      expect(usersPage.userViewTeams.count()).toEqual(1);
      usersPage.userViewClose.click();
      expect(usersPage.userView.isPresent()).toBeFalsy();
    });

    it("Verify Adoption/Administrator user able to add user to the team - 'xxx-TestTeam-02", function() {
      homePage.teams.click();

      browser.wait(EC.elementToBeClickable(teamsPage.searchTeam), waitSeconds);
      teamsPage.searchTeam.click();
      teamsPage.searchTeam.sendKeys('xxx-TestTeam-02');

      expect(teamsPage.teams.count()).toEqual(1);
      browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-02')), waitSeconds);
      teamsPage.team('xxx-TestTeam-02').click();
      browser.wait(EC.elementToBeClickable(teamsPage.teamViewAddMember), waitSeconds);
      teamsPage.teamViewAddMember.click();
      teamsPage.TeamInviteMemberRole('Creator').click();
      teamsPage.teamVieweMail.sendKeys('mystack.user1@pearson.com');
      teamsPage.teamViewAddButton.click();
      // Verify details in 'TEAMS' page got updated while adding user to the teams
      // TODO - Have to replace below
      // expect(element.all(by.className("Team")).get(0).element(by.className("Team-memberCount")).getText()).toEqual('1');

      browser.wait(EC.elementToBeClickable(teamsPage.teamViewAddMember), waitSeconds);
      teamsPage.teamViewAddMember.click();
      teamsPage.TeamInviteMemberRole('Creator').click();
      teamsPage.teamVieweMail.sendKeys('mystack.user2@pearson.com');
      teamsPage.teamViewAddButton.click();
      // Verify details in 'TEAMS' page got updated while adding user to the teams
      // TODO - Have to replace below
      // expect(element.all(by.className("Team")).get(0).element(by.className("Team-memberCount")).getText()).toEqual('2');

      browser.wait(EC.elementToBeClickable(teamsPage.teamViewAddMember), waitSeconds);
      teamsPage.teamViewAddMember.click();
      teamsPage.TeamInviteMemberRole('User').click();
      teamsPage.teamVieweMail.sendKeys('mystack.user3@pearson.com');
      teamsPage.teamViewAddButton.click();
      browser.sleep(1000);
      expect(teamsPage.teamViewMembers.count()).toEqual(3);
      // Verify details in 'TEAMS' page got updated while adding user to the teams
      // TODO - Have to replace below
      // expect(element.all(by.className("Team")).get(0).element(by.className("Team-memberCount")).getText()).toEqual('3');
    });

    it("Verify user with 'CREATOR' role able to add/delete more users to the team - 'xxx-TestTeam-01", function() {
      homePage.teams.click();

      browser.wait(EC.elementToBeClickable(teamsPage.searchTeam), waitSeconds);
      teamsPage.searchTeam.click();
      teamsPage.searchTeam.sendKeys('xxx-TestTeam-01');

      expect(teamsPage.teams.count()).toEqual(1);
      browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-01')), waitSeconds);
      teamsPage.team('xxx-TestTeam-01').click();
      browser.wait(EC.elementToBeClickable(teamsPage.teamViewAddMember), waitSeconds);
      teamsPage.teamViewAddMember.click();
      teamsPage.TeamInviteMemberRole('Creator').click();
      teamsPage.teamVieweMail.sendKeys('mystack.user4@pearson.com');
      teamsPage.teamViewAddButton.click();

      browser.sleep(1000);
      expect(teamsPage.teamViewMembers.count()).toEqual(4);
      // Verify details in 'TEAMS' page got updated while adding user to the teams
      // TODO - Have to replace below
      // expect(element.all(by.className("Team")).get(0).element(by.className("Team-memberCount")).getText()).toEqual('4');

      expect(teamsPage.TeamViewMemberNameByindex(0).getText()).toEqual('mystack user1');
      expect(teamsPage.TeamViewMemberNameByindex(1).getText()).toEqual('mystack user2');
      expect(teamsPage.TeamViewMemberNameByindex(2).getText()).toEqual('mystack user3');
      expect(teamsPage.TeamViewMemberNameByindex(3).getText()).toEqual('mystack user4');

      // Verify Creator role able to delete user from the team
      browser.actions().mouseUp(teamsPage.teamViewMembers.get(3)).perform();
      teamsPage.TeamViewMemberDeleteByindex(3).click();
      browser.sleep(1000);
      expect(teamsPage.teamViewMembers.count()).toEqual(3);

      // Verify details in 'TEAMS' page got updated while deleting user from the team
      // TODO - Have to replace below
      // expect(element.all(by.className("Team")).get(0).element(by.className("Team-memberCount")).getText()).toEqual('3');
    });

    it("Verify Portal user (mystack.user4) not able to create new team", function() {
      fn.login("mystack.user4", "");
      homePage.teams.click();
      expect(teamsPage.createTeam.isDisplayed()).toBeFalsy();
    });

    it("Verify Portal user (mystack.user1) not able to create new team", function() {
      fn.login("mystack.user1", "");
      homePage.teams.click();
      expect(teamsPage.createTeam.isDisplayed()).toBeFalsy();
    });

    it("Verify permissions for user 'mystack.user1' in team 'xxx-TestTeam-01'", function() {
      fn.login("mystack.user1", "");

      homePage.teams.click();
      expect(teamsPage.teams.count()).toEqual(2);
      expect(teamsPage.getTeamNameByindex(0)).toEqual('xxx-TestTeam-01');
      expect(teamsPage.getTeamBubblesByindex(0)).toEqual('0');
      expect(teamsPage.getTeamMemberCountByindex(0)).toEqual('3');
      expect(teamsPage.getTeamMembersByindex(0).get(0).getText()).toEqual('mystack user1');
      expect(teamsPage.getTeamMembersByindex(0).get(1).getText()).toEqual('mystack user3');
      expect(teamsPage.getTeamMembersByindex(0).get(2).getText()).toEqual('mystack user2');

      // Details in TeamView page
      // element.all(by.className("Team")).get(0).click();
      teamsPage.team('xxx-TestTeam-01').click();
      browser.wait(EC.elementToBeClickable(teamsPage.teamViewAddMember), waitSeconds);
      expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-01');
      expect(teamsPage.teamViewDelete.isPresent()).toBeTruthy();
      expect(teamsPage.teamViewAddMember.isPresent()).toBeTruthy();
      expect(teamsPage.teamViewAddMember.isDisplayed()).toBeTruthy();
      expect(teamsPage.teamViewMembers.count()).toEqual(3);
      expect(teamsPage.TeamViewBubbles.count()).toEqual(0);
      expect(teamsPage.TeamViewMemberNameByindex(0).getText()).toEqual('mystack user1');
      expect(teamsPage.TeamViewMemberNameByindex(1).getText()).toEqual('mystack user2');
      expect(teamsPage.TeamViewMemberNameByindex(2).getText()).toEqual('mystack user3');

      // Verify team delete option is displayed while moving mouse over to it
      browser.actions().mouseUp(teamsPage.teamViewMembers.get(0)).perform();
      expect(teamsPage.TeamViewMemberDeleteByindex(0).isDisplayed()).toBeTruthy();
      expect(teamsPage.TeamViewMemberDeleteByindex(1).isDisplayed()).toBeFalsy();
      browser.actions().mouseUp(teamsPage.teamViewMembers.get(2)).perform();
      expect(teamsPage.TeamViewMemberDeleteByindex(2).isDisplayed()).toBeTruthy();
      expect(teamsPage.TeamViewMemberDeleteByindex(1).isDisplayed()).toBeFalsy();

      // Verify it is possible to change the user permission
      expect(teamsPage.TeamViewMemberRoleByindex(0).isEnabled()).toBeTruthy();
      expect(teamsPage.TeamViewMemberRoleByindex(1).isEnabled()).toBeTruthy();
      expect(teamsPage.TeamViewMemberRoleByindex(2).isEnabled()).toBeTruthy();
    });

    it("Verify permissions for user 'mystack.user1' in team 'xxx-TestTeam-02'", function() {
      homePage.teams.click();
      expect(teamsPage.teams.count()).toEqual(2);

      expect(teamsPage.getTeamNameByindex(1)).toEqual('xxx-TestTeam-02');
      expect(teamsPage.getTeamBubblesByindex(1)).toEqual('0');
      expect(teamsPage.getTeamMemberCountByindex(1)).toEqual('3');
      expect(teamsPage.getTeamMembersByindex(1).get(0).getText()).toEqual('mystack user1');
      expect(teamsPage.getTeamMembersByindex(1).get(1).getText()).toEqual('mystack user2');
      expect(teamsPage.getTeamMembersByindex(1).get(2).getText()).toEqual('mystack user3');

      // Details in TeamView page
      // element.all(by.className("Team")).get(1).click();
      teamsPage.team('xxx-TestTeam-02').click();
      browser.wait(EC.elementToBeClickable(teamsPage.teamViewAddMember), waitSeconds);
      expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-02');
      expect(teamsPage.teamViewDelete.isPresent()).toBeTruthy();
      expect(teamsPage.teamViewAddMember.isPresent()).toBeTruthy();
      expect(teamsPage.teamViewAddMember.isDisplayed()).toBeTruthy();
      expect(teamsPage.teamViewMembers.count()).toEqual(3);
      expect(teamsPage.TeamViewBubbles.count()).toEqual(0);
      expect(teamsPage.TeamViewMemberNameByindex(0).getText()).toEqual('mystack user1');
      expect(teamsPage.TeamViewMemberNameByindex(1).getText()).toEqual('mystack user2');
      expect(teamsPage.TeamViewMemberNameByindex(2).getText()).toEqual('mystack user3');

      // Verify team delete option is displayed while moving mouse over to it
      browser.actions().mouseUp(teamsPage.teamViewMembers.get(0)).perform();
      expect(teamsPage.TeamViewMemberDeleteByindex(0).isDisplayed()).toBeTruthy();
      expect(teamsPage.TeamViewMemberDeleteByindex(1).isDisplayed()).toBeFalsy();
      browser.actions().mouseUp(teamsPage.teamViewMembers.get(2)).perform();
      expect(teamsPage.TeamViewMemberDeleteByindex(2).isDisplayed()).toBeTruthy();
      expect(teamsPage.TeamViewMemberDeleteByindex(1).isDisplayed()).toBeFalsy();

      // Verify it is possible to change the user permission
      expect(teamsPage.TeamViewMemberRoleByindex(0).isEnabled()).toBeTruthy();
      expect(teamsPage.TeamViewMemberRoleByindex(1).isEnabled()).toBeTruthy();
      expect(teamsPage.TeamViewMemberRoleByindex(2).isEnabled()).toBeTruthy();
    });

    it("Verify permissions for user 'mystack.user2' in team 'xxx-TestTeam-01'", function() {
      fn.login("mystack.user2", "");

      homePage.teams.click();
      expect(teamsPage.teams.count()).toEqual(2);
      expect(teamsPage.getTeamNameByindex(0)).toEqual('xxx-TestTeam-01');
      expect(teamsPage.getTeamBubblesByindex(0)).toEqual('0');
      expect(teamsPage.getTeamMemberCountByindex(0)).toEqual('3');
      expect(teamsPage.getTeamMembersByindex(0).get(0).getText()).toEqual('mystack user1');
      expect(teamsPage.getTeamMembersByindex(0).get(1).getText()).toEqual('mystack user3');
      expect(teamsPage.getTeamMembersByindex(0).get(2).getText()).toEqual('mystack user2');

      // Details in TeamView page
      // element.all(by.className("Team")).get(0).click();
      teamsPage.team('xxx-TestTeam-01').click();
      browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
      expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-01');
      expect(teamsPage.teamViewDelete.isPresent()).toBeFalsy();
      expect(teamsPage.teamViewAddMember.isPresent()).toBeFalsy();
      expect(teamsPage.teamViewMembers.count()).toEqual(3);
      expect(teamsPage.TeamViewBubbles.count()).toEqual(0);
      expect(teamsPage.TeamViewMemberNameByindex(0).getText()).toEqual('mystack user1');
      expect(teamsPage.TeamViewMemberNameByindex(1).getText()).toEqual('mystack user2');
      expect(teamsPage.TeamViewMemberNameByindex(2).getText()).toEqual('mystack user3');

      // Verify team delete option is not displayed/available for user who didn't got permission for the team
      browser.actions().mouseUp(teamsPage.teamViewMembers.get(0)).perform();
      expect(teamsPage.TeamViewMemberDeleteByindex(0).isPresent()).toBeFalsy();
      expect(teamsPage.TeamViewMemberDeleteByindex(1).isPresent()).toBeFalsy();
      browser.actions().mouseUp(teamsPage.teamViewMembers.get(2)).perform();
      expect(teamsPage.TeamViewMemberDeleteByindex(2).isPresent()).toBeFalsy();
      expect(teamsPage.TeamViewMemberDeleteByindex(1).isPresent()).toBeFalsy();

      // Verify it is not possible to change the user permission
      expect(teamsPage.TeamViewMemberRoleByindex(0).isEnabled()).toBeFalsy();
      expect(teamsPage.TeamViewMemberRoleByindex(1).isEnabled()).toBeFalsy();
      expect(teamsPage.TeamViewMemberRoleByindex(2).isEnabled()).toBeFalsy();
    });

    it("Verify permissions for user 'mystack.user2' in team 'xxx-TestTeam-02'", function() {
      homePage.teams.click();

      expect(teamsPage.teams.count()).toEqual(2);
      expect(teamsPage.getTeamNameByindex(1)).toEqual('xxx-TestTeam-02');
      expect(teamsPage.getTeamBubblesByindex(1)).toEqual('0');
      expect(teamsPage.getTeamMemberCountByindex(1)).toEqual('3');
      expect(teamsPage.getTeamMembersByindex(0).get(0).getText()).toEqual('mystack user1');
      expect(teamsPage.getTeamMembersByindex(1).get(1).getText()).toEqual('mystack user2');
      expect(teamsPage.getTeamMembersByindex(1).get(2).getText()).toEqual('mystack user3');

      // Details in TeamView page
      // element.all(by.className("Team")).get(1).click();
      teamsPage.team('xxx-TestTeam-02').click();
      browser.wait(EC.elementToBeClickable(teamsPage.teamViewAddMember), waitSeconds);
      expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-02');
      expect(teamsPage.teamViewDelete.isPresent()).toBeTruthy();
      expect(teamsPage.teamViewAddMember.isPresent()).toBeTruthy();
      expect(teamsPage.teamViewAddMember.isDisplayed()).toBeTruthy();
      expect(teamsPage.teamViewMembers.count()).toEqual(3);
      expect(teamsPage.TeamViewBubbles.count()).toEqual(0);
      expect(teamsPage.TeamViewMemberNameByindex(0).getText()).toEqual('mystack user1');
      expect(teamsPage.TeamViewMemberNameByindex(1).getText()).toEqual('mystack user2');
      expect(teamsPage.TeamViewMemberNameByindex(2).getText()).toEqual('mystack user3');

      // Verify team delete option is displayed while moving mouse over to it
      browser.actions().mouseUp(teamsPage.teamViewMembers.get(0)).perform();
      expect(teamsPage.TeamViewMemberDeleteByindex(0).isDisplayed()).toBeTruthy();
      expect(teamsPage.TeamViewMemberDeleteByindex(1).isDisplayed()).toBeFalsy();
      browser.actions().mouseUp(teamsPage.teamViewMembers.get(2)).perform();
      expect(teamsPage.TeamViewMemberDeleteByindex(2).isDisplayed()).toBeTruthy();
      expect(teamsPage.TeamViewMemberDeleteByindex(1).isDisplayed()).toBeFalsy();

      // Verify it is possible to change the user permission
      expect(teamsPage.TeamViewMemberRoleByindex(0).isEnabled()).toBeTruthy();
      expect(teamsPage.TeamViewMemberRoleByindex(1).isEnabled()).toBeTruthy();
      expect(teamsPage.TeamViewMemberRoleByindex(2).isEnabled()).toBeTruthy();
    });

    xit("Verify permissions for Portal user - 'mystack.user3'", function() {
      // Login as 'mystack.user1'
      fn.login("mystack.user3", "");

      expect(homePage.bubbles.isPresent()).toBeTruthy();
      expect(homePage.teams.isPresent()).toBeTruthy();
      expect(homePage.users.isPresent()).toBeFalsy();
      expect(homePage.marketplace.isPresent()).toBeTruthy();
      expect(homePage.usage.isPresent()).toBeFalsy();
      expect(homePage.search.isPresent()).toBeFalsy();
    });

    describe("ErrorHandling", function() {

      beforeAll(function() {
        browser.get('/');
        fn.login("mystack.root", "");
      });

      it("Create team for error message", function() {
        homePage.teams.click();
        expect(teamsPage.createTeam.isDisplayed()).toBeTruthy();

        // Create team 'xxx-TestTeam-03'
        browser.wait(EC.elementToBeClickable(teamsPage.createTeam), waitSeconds);
        teamsPage.createTeam.click();
        teamsPage.createTeamName.sendKeys('xxx-TestTeam-03');
        teamsPage.createButton.click();
        // Add user to this team - 'mystack.user1@pearson.com'
        browser.wait(EC.elementToBeClickable(teamsPage.teamViewAddMember), waitSeconds);
        teamsPage.teamViewAddMember.click();
        teamsPage.TeamInviteMemberRole('Creator').click();
        teamsPage.teamVieweMail.sendKeys('mystack.user1@pearson.com');
        teamsPage.teamViewAddButton.click();
        // Add user to team - 'mystack.user4@pearson.com'
        browser.wait(EC.elementToBeClickable(teamsPage.teamViewAddMember), waitSeconds);
        teamsPage.teamViewAddMember.click();
        teamsPage.TeamInviteMemberRole('User').click();
        teamsPage.teamVieweMail.sendKeys('mystack.user4@pearson.com');
        teamsPage.teamViewAddButton.click();
        teamsPage.teamViewClose.click();

        // Create team 'xxx-TestTeam-04'
        browser.wait(EC.elementToBeClickable(teamsPage.createTeam), waitSeconds);
        teamsPage.createTeam.click();
        teamsPage.createTeamName.sendKeys('xxx-TestTeam-04');
        teamsPage.createButton.click();
        // Add user to this team
        browser.wait(EC.elementToBeClickable(teamsPage.teamViewAddMember), waitSeconds);
        teamsPage.teamViewAddMember.click();
        teamsPage.TeamInviteMemberRole('Creator').click();
        teamsPage.teamVieweMail.sendKeys('mystack.user1@pearson.com');
        teamsPage.teamViewAddButton.click();
        teamsPage.teamViewClose.click();

        // Create team 'xxx-FailureTests-01'
        browser.wait(EC.elementToBeClickable(teamsPage.createTeam), waitSeconds);
        teamsPage.createTeam.click();
        teamsPage.createTeamName.sendKeys('xxx-FailureTests-01');
        teamsPage.createButton.click();
        // Add user to this team
        browser.wait(EC.elementToBeClickable(teamsPage.teamViewAddMember), waitSeconds);
        teamsPage.teamViewAddMember.click();
        teamsPage.TeamInviteMemberRole('Creator').click();
        teamsPage.teamVieweMail.sendKeys('mystack.user4@pearson.com');
        teamsPage.teamViewAddButton.click();
      });

      it("Verify user not able to create a team which already exists - 'xxx-TestTeam-03'", function() {
        homePage.teams.click();

        browser.wait(EC.elementToBeClickable(teamsPage.createTeam), waitSeconds);
        teamsPage.createTeam.click();
        teamsPage.createTeamName.sendKeys('xxx-TestTeam-03');
        teamsPage.createButton.click();
        browser.wait(EC.visibilityOf(teamsPage.teamCreateError), 5000);
        expect(teamsPage.teamCreateError.getText()).toEqual("Duplicate Team");

        // Verify use able to close the error message by pressing 'Cancel' button
        teamsPage.createTeamCancel.click();
        expect(teamsPage.teamCreateError.isPresent()).toBeFalsy();
      });

      // xit("Verify user not able to rename team with exiting name", function() {
      //   homePage.teams.click();
      // });

      it("User should get failure message while trying to add Invalid user to portal through teams page (user not exists in AD)", function() {
        homePage.teams.click();

        browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-03')), waitSeconds);
        teamsPage.team('xxx-TestTeam-03').click();
        browser.wait(EC.elementToBeClickable(teamsPage.teamViewAddMember), waitSeconds);
        teamsPage.teamViewAddMember.click();
        teamsPage.TeamInviteMemberRole('Creator').click();
        teamsPage.teamVieweMail.sendKeys('mystack.xxxx@pearson.com');
        teamsPage.teamViewAddButton.click();
        browser.wait(EC.visibilityOf(teamsPage.TeamInviteMemberError), 5000);
        expect(teamsPage.TeamInviteMemberError.getText()).toEqual("User Not Found");
        // Verify details in 'TEAMS' page got updated while adding user to the teams
        expect(teamsPage.teamViewMembers.count()).toEqual(2);
      });

      it("Verify it is possible to change user permissions/role on team", function() {
        fn.login("mystack.user4", "");

        homePage.teams.click();
        // Verify user permissions for user with 'USER' role on team
        browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-03')), waitSeconds);

        teamsPage.team('xxx-TestTeam-03').click();
        // element.all(by.className("Team")).get(0).click();
        browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
        expect(teamsPage.teamViewDelete.isPresent()).toBeFalsy();
        expect(teamsPage.teamViewAddMember.isPresent()).toBeFalsy();
        expect(teamsPage.teamViewMembers.count()).toEqual(2);
        expect(teamsPage.TeamViewBubbles.count()).toEqual(0);
        expect(teamsPage.TeamViewMemberNameByindex(0).getText()).toEqual('mystack user1');
        expect(teamsPage.TeamViewMemberNameByindex(1).getText()).toEqual('mystack user4');
        expect(teamsPage.TeamViewMemberDeleteByindex(0).isPresent()).toBeFalsy();
        expect(teamsPage.TeamViewMemberDeleteByindex(1).isPresent()).toBeFalsy();
        expect(teamsPage.TeamViewMemberRoleByindex(0).isEnabled()).toBeFalsy();
        expect(teamsPage.TeamViewMemberRoleByindex(1).isEnabled()).toBeFalsy();
        teamsPage.teamViewClose.click();

        browser.refresh();
        // Now change the user permission for 'mystack.user4'
        fn.login("mystack.user1", "");
        homePage.teams.click();
        browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-03')), waitSeconds);
        teamsPage.team('xxx-TestTeam-03').click();
        // element.all(by.className("Team")).get(0).click();
        browser.wait(EC.elementToBeClickable(teamsPage.teamViewAddMember), waitSeconds);
        element.all(by.className("TeamMember")).get(1).element(by.cssContainingText('.TeamMember-role option', 'Creator')).click();
        teamsPage.teamViewClose.click();

        browser.refresh();
        // Verify permissions for 'mystack.user4' updated
        fn.login("mystack.user4", "");
        homePage.teams.click();
        browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-03')), waitSeconds);
        teamsPage.team('xxx-TestTeam-03').click();
        browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
        expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-03');
        expect(teamsPage.teamViewDelete.isPresent()).toBeTruthy();
        expect(teamsPage.teamViewAddMember.isPresent()).toBeTruthy();
        expect(teamsPage.teamViewAddMember.isDisplayed()).toBeTruthy();
        expect(teamsPage.teamViewMembers.count()).toEqual(2);
        expect(teamsPage.TeamViewBubbles.count()).toEqual(0);
        expect(teamsPage.TeamViewMemberNameByindex(0).getText()).toEqual('mystack user1');
        expect(teamsPage.TeamViewMemberNameByindex(1).getText()).toEqual('mystack user4');
        // Verify team delete option is displayed while moving mouse over to it
        browser.actions().mouseUp(teamsPage.teamViewMembers.get(0)).perform();
        expect(teamsPage.TeamViewMemberDeleteByindex(0).isDisplayed()).toBeTruthy();
        expect(teamsPage.TeamViewMemberDeleteByindex(1).isDisplayed()).toBeFalsy();
        browser.actions().mouseUp(teamsPage.teamViewMembers.get(1)).perform();
        expect(teamsPage.TeamViewMemberDeleteByindex(0).isDisplayed()).toBeFalsy();
        expect(teamsPage.TeamViewMemberDeleteByindex(1).isDisplayed()).toBeTruthy();
        // Verify it is possible to change the user permission
        expect(teamsPage.TeamViewMemberRoleByindex(0).isEnabled()).toBeTruthy();
        expect(teamsPage.TeamViewMemberRoleByindex(1).isEnabled()).toBeTruthy();
      });

      it("Verify user with 'CREATOR' role able to delete the team - 'xxx-TestTeam-03'", function() {
        fn.login("mystack.user1", "");

        homePage.teams.click();
        browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-03')), waitSeconds);
        teamsPage.team('xxx-TestTeam-03').click();
        // element.all(by.className("Team")).get(0).click();
        expect(teamsPage.teamView.isPresent()).toBeTruthy();
        browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
        browser.actions().mouseUp(teamsPage.teamViewDelete).perform();
        browser.wait(EC.visibilityOf(teamsPage.teamViewDelete), waitSeconds);
        teamsPage.teamViewDelete.click();
        browser.switchTo().alert().dismiss();
        teamsPage.teamViewDelete.click();
        browser.switchTo().alert().accept();

        // Verify team is actually deleted
        homePage.teams.click();
        browser.wait(EC.elementToBeClickable(teamsPage.searchTeam), waitSeconds);
        expect(teamsPage.team('xxx-TestTeam-03').isPresent()).toBeFalsy();
      });

      it("Verify user with 'CREATOR' role able to delete the team - 'xxx-TestTeam-04'", function() {
        homePage.teams.click();
        browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-04')), waitSeconds);
        teamsPage.team('xxx-TestTeam-04').click();
        // element.all(by.className("Team")).get(0).click();
        expect(teamsPage.teamView.isPresent()).toBeTruthy();
        browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
        browser.actions().mouseUp(teamsPage.teamViewDelete).perform();
        browser.wait(EC.visibilityOf(teamsPage.teamViewDelete), waitSeconds);
        teamsPage.teamViewDelete.click();
        browser.switchTo().alert().dismiss();
        teamsPage.teamViewDelete.click();
        browser.switchTo().alert().accept();

        // Verify team is actually deleted
        homePage.teams.click();
        browser.wait(EC.elementToBeClickable(teamsPage.searchTeam), waitSeconds);
        expect(teamsPage.team('xxx-TestTeam-04').isPresent()).toBeFalsy();
      });
    });

    // Verify user able to navigate to 'Teams' page by clicking teams deep link on 'Users' page
    describe("UserPageDeepLinking", function() {

      beforeAll(function() {
        browser.get('/');
        fn.login("mystack.root", "");
      });

      it("Verify user (from mystack.user1) able to navigate to teams page by clicking teams link in Users page - 'xxx-TestTeam-01'", function() {
        fn.login("mystack.root", "");

        homePage.users.click();
        browser.wait(EC.elementToBeClickable(usersPage.user('mystack user1')), waitSeconds);
        usersPage.user('mystack user1').click();
        expect(browser.getCurrentUrl()).toContain(homePageUrl + "users/");
        browser.wait(EC.elementToBeClickable(usersPage.userViewClose), waitSeconds);

        usersPage.UserViewTeam('xxx-TestTeam-01').click();
        expect(teamsPage.teamView.isPresent()).toBeTruthy();
        browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
        expect(browser.getCurrentUrl()).toContain(homePageUrl + "teams/");
        expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-01');
      });

      it("Verify user (from mystack.user1) able to navigate to teams page by clicking teams link in Users page - 'xxx-TestTeam-02'", function() {
        homePage.users.click();
        browser.wait(EC.elementToBeClickable(usersPage.user('mystack user1')), waitSeconds);
        usersPage.user('mystack user1').click();

        browser.wait(EC.elementToBeClickable(usersPage.userViewClose), waitSeconds);
        expect(browser.getCurrentUrl()).toContain(homePageUrl + "users/");
        usersPage.UserViewTeam('xxx-TestTeam-02').click();
        expect(teamsPage.teamView.isPresent()).toBeTruthy();
        browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
        expect(browser.getCurrentUrl()).toContain(homePageUrl + "teams/");
        expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-02');
      });

      it("Verify user (from mystack.user2) able to navigate to teams page by clicking teams link in Users page - 'xxx-TestTeam-01'", function() {
        homePage.users.click();
        browser.wait(EC.elementToBeClickable(usersPage.user('mystack user2')), waitSeconds);
        usersPage.user('mystack user2').click();

        browser.wait(EC.elementToBeClickable(usersPage.userViewClose), waitSeconds);
        expect(browser.getCurrentUrl()).toContain(homePageUrl + "users/");

        usersPage.UserViewTeam('xxx-TestTeam-01').click();
        expect(teamsPage.teamView.isPresent()).toBeTruthy();
        browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
        expect(browser.getCurrentUrl()).toContain(homePageUrl + "teams/");
        expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-01');
      });

      it("Verify user (from mystack.user2) able to navigate to teams page by clicking teams link in Users page - 'xxx-TestTeam-02'", function() {
        homePage.users.click();
        browser.wait(EC.elementToBeClickable(usersPage.user('mystack user2')), waitSeconds);
        usersPage.user('mystack user2').click();

        browser.wait(EC.elementToBeClickable(usersPage.userViewClose), waitSeconds);
        expect(browser.getCurrentUrl()).toContain(homePageUrl + "users/");

        usersPage.UserViewTeam('xxx-TestTeam-02').click();
        expect(teamsPage.teamView.isPresent()).toBeTruthy();
        browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
        expect(browser.getCurrentUrl()).toContain(homePageUrl + "teams/");
        expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-02');
      });
    });

    // Verify user able to navigate to 'Bubbles' page by clicking teams deep link on 'Teams' page
    describe("TeamsPageDeepLinking", function() {

      beforeAll(function() {
        browser.get('/');
        fn.login("mystack.root", "");
      });

      it("Verify user able to navigate to Bubbles page by clicking bubble name in Teams page - 'xxx-BrokenBubbleDoNotDelete'", function() {
        homePage.teams.click();
        browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-BrokenTeam')), waitSeconds);
        teamsPage.team('xxx-BrokenTeam').click();
        // element.all(by.className("Team")).get(0).click();

        browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
        expect(browser.getCurrentUrl()).toContain(homePageUrl + "teams/");
        teamsPage.TeamViewBubbleByname('xxx-BrokenBubbleDoNotDelete').click();

        // Verify bubbles page is loaded
        expect(element(by.className("BubbleView")).isPresent()).toBeTruthy();
        browser.wait(EC.elementToBeClickable(element(by.className("BubbleView-close"))), waitSeconds);
        expect(browser.getCurrentUrl()).toContain(homePageUrl + "bubbles/");
        expect(element(by.className("BubbleView-name")).getText()).toEqual('xxx-BrokenBubbleDoNotDelete');
      });
    });

  });

});
