var authorization = vcdAuthKey;
var headers       = {"Accept": "application/*+xml;version=5.1", "Authorization" : vcdAuthKey};
var orgExists     = false;

var homePage          = require("../../pages/home_page.js");
var bubblesPage       = require("../../pages/bubbles_page.js");
var notificationsPage = require("../../pages/notifications_page.js");

describe('MPSPLAT-410', function() {

  // Verify Admin user able to delete the bubble which was not successfully created - Failed at edge gateway creation
  describe("DeleteBubbleInCreatingStatus", function() {

    beforeAll(function() {
      // Set network available for region 'XXXLO3'
      db.execute("SELECT * FROM network WHERE region='XXXLO3'", function(err, result) {
        if (err) {
          var errorMessage = "Failure message - " + err;
          this.fail(errorMessage);
        } else {
          for (var i=0; i < result.rows.length; i++) {
            var query = "UPDATE network SET bubble=null, status=0 WHERE id=" + result.rows[i].id;
            db.execute(query, function(err, update) {
              if (err) {
                var errorMessage = "Failed to run - " + query + ". Failure message - " + err;
                this.fail(errorMessage);
              }
            });
          }
        }
      });

      browser.get('/');
      fn.login("mystack.user4", "");
      // Clear the existing notifications on the UI
      fn.clearNotifications();

      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.createBubble), waitSeconds);
      bubblesPage.createBubble.click();
      bubblesPage.createBubbleName.sendKeys("xxx-DeleteBubbleInCreatingStatus");
      bubblesPage.createBubbleChooseTeam('xxx-FailureTests-01');
      bubblesPage.createBubbleChooseRegion('XXXLO3 - Iceland');
      bubblesPage.tandc.click();
      bubblesPage.tandcAccept.click();
      expect(bubblesPage.tandcCheckbox.isSelected()).toBeTruthy();
      bubblesPage.createButton.click();

      expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
      browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
      bubblesPage.BubbleViewClose.click();
      fn.waitForNotifications(120000, 6);

      // Verify user 'mystack.root' gets notifications while bubble creation fails at Edgegateway - 'xxx-DeleteBubbleInCreatingStatus'
      expect(notificationsPage.notificationCount.getText()).toEqual('6');
      homePage.notifications.click();
      notificationsPage.notificationHeader.get(0).click();
      expect(notificationsPage.NotificationsByindex(0).count()).toEqual(6);
      expect(notificationsPage.NotificationsByindex(0).get(5).getText()).toEqual("A problem occurred creating your bubble. We are investigating");
    });

    beforeEach(function() {
      browser.get('/');
    });

    it("Verify Administrator/Adoption user able to submit bubble delete request - 'xxx-DeleteBubbleInCreatingStatus'", function() {
      // Clear the existing notifications on the UI
      fn.login("mystack.user4", "");
      fn.clearNotifications();

      fn.login("mystack.root", "");

      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-DeleteBubbleInCreatingStatus')), waitSeconds);
      bubblesPage.bubble('xxx-DeleteBubbleInCreatingStatus').click();
      expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();

      // Submit request to 'delete' the bubble
      browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
      browser.actions().mouseUp(bubblesPage.BubbleViewDelete).perform();
      bubblesPage.BubbleViewDelete.click();
      browser.switchTo().alert().accept();
    });

    it("Verify bubble 'xxx-DeleteBubbleInCreatingStatus' is removed from the portal DB", function () {
      fn.login("mystack.user4", "");
      fn.waitForNotifications(180000, 3);
      homePage.bubbles.click();
      browser.wait(EC.invisibilityOf(bubblesPage.bubble('xxx-DeleteBubbleInCreatingStatus')), 5000);
      expect(bubblesPage.bubble('xxx-DeleteBubbleInCreatingStatus').isPresent()).toBeFalsy();
    });

    it("Verify org 'xxx-DeleteBubbleInCreatingStatus' is deleted on vCD", function() {
      response = fn.runHttpRequests("POST", headers, vcd + '/api/sessions');
      if (response.statusCode == 200) {
        headers = {"Accept": "application/*+xml;version=5.1", "x-vcloud-authorization": response.headers['x-vcloud-authorization']};

        // Run the Query and get list of Orgs
        response = fn.runHttpRequests("GET", headers, vcd + '/api/admin');
        jsonResult = xmlobj.parseXML(response.body.toString());
        for(var key in jsonResult.VCloud.OrganizationReferences.OrganizationReference) {
          if (jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-name"] == "xxx-DeleteBubbleInCreatingStatus") {
            orgExists = true;
            break;
          }
        }
        expect("Org 'xxx-DeleteBubbleInCreatingStatus' exits - false").toEqual("Org 'xxx-DeleteBubbleInCreatingStatus' exits - " + orgExists);
      } else {
        this.fail("Failed to get the auth key. statusCode - ", response.statusCode);
      }
    });
  });

  //   xit("Clean the bubble created by the test - 'xxx-BubbleDeleteEdgeCreationFailr'", function() {
  //     //  Default values
  //     var response, key;
  //     var orghref           = "";
  //     var orgDisablehref    = "";
  //     var orgvdchref        = "";
  //     var orgvdcDisablehref = "";
  //     var edgeGwqueryhref   = "";

  //     headers = {"Accept": "application/*+xml;version=5.1", "Authorization": authorization};
  //     response = fn.runHttpRequests("POST", headers, vcd + '/api/sessions');
  //     if (response.statusCode == 200) {
  //       headers = {"Accept": "application/*+xml;version=5.1", "x-vcloud-authorization": response.headers['x-vcloud-authorization']};
  //       // Run the Query and get list of Orgs
  //       response = fn.runHttpRequests("GET", headers, vcd + '/api/admin');
  //       jsonResult = xmlobj.parseXML(response.body.toString());
  //       for(key in jsonResult.VCloud.OrganizationReferences.OrganizationReference) {
  //         if (jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-name"] == "xxx-BubbleDeleteEdgeCreationFailr") {
  //           orghref = jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-href"];
  //           break;
  //         }
  //       }
  //     } else {
  //       this.fail("Failed to get the auth key. statusCode - ", response.statusCode);
  //     }

  //     if (orghref !== "") {
  //       response = fn.runHttpRequests("GET", headers, orghref);
  //       if (response.statusCode == 200) {
  //         // Convert XML -> JSON
  //         jsonResultOrg = xmlobj.parseXML(response.body.toString());

  //         // Get the disable link for Org href
  //         for(key in jsonResultOrg.AdminOrg.Link) {
  //           if (jsonResultOrg.AdminOrg.Link[key]["-rel"] == "disable") {
  //             orgDisablehref = jsonResultOrg.AdminOrg.Link[key]["-href"];
  //             break;
  //           }
  //         }

  //         // Loop through each Org and check Org vDC created - Set the FLAG
  //         if (jsonResultOrg.AdminOrg.Vdcs !== undefined) {
  //           if (jsonResultOrg.AdminOrg.Vdcs.Vdc["-type"] == "application/vnd.vmware.admin.vdc+xml") {
  //             orgvdchref = jsonResultOrg.AdminOrg.Vdcs.Vdc["-href"];
  //           }
  //         }
  //       }
  //     }

  //     if (orgvdchref !== "") {
  //       response = fn.runHttpRequests("GET", headers, orgvdchref);
  //       if (response.statusCode == 200) {
  //         // Convert XML -> JSON
  //         jsonResultOrgVdc = xmlobj.parseXML(response.body.toString());

  //         // Get the disable link for Org vDC href
  //         for(key in jsonResultOrgVdc.AdminVdc.Link) {
  //           if (jsonResultOrgVdc.AdminVdc.Link[key]["-rel"] == "disable") {
  //             orgvdcDisablehref = jsonResultOrgVdc.AdminVdc.Link[key]["-href"];
  //             break;
  //           }
  //         }

  //         // Loop through and get the href for Edgegateway Query
  //         for(key in jsonResultOrgVdc.AdminVdc.Link) {
  //           if (jsonResultOrgVdc.AdminVdc.Link[key]["-type"] == "application/vnd.vmware.vcloud.query.records+xml" && jsonResultOrgVdc.AdminVdc.Link[key]["-rel"] == "edgeGateways") {
  //             edgeGwqueryhref = jsonResultOrgVdc.AdminVdc.Link[key]["-href"];
  //             break;
  //           }
  //         }
  //       } else {
  //         errorMessage = "Failed while running - " + orgvdchref;
  //         this.fail(errorMessage);
  //       }
  //     }

  //     if (edgeGwqueryhref !== "") {
  //       response = fn.runHttpRequests("GET", headers, edgeGwqueryhref);
  //       if (response.statusCode == 200) {
  //         // Convert XML -> JSON
  //         jsonResult = xmlobj.parseXML(response.body.toString());

  //         // Edge gateway exists?. Delete it
  //         if (jsonResult.QueryResultRecords.EdgeGatewayRecord !== undefined) {
  //           response = fn.runHttpRequests("DELETE", headers, jsonResult.QueryResultRecords.EdgeGatewayRecord["-href"]);
  //         }
  //       } else {
  //         errorMessage = "Failed while running - " + edgeGwqueryhref;
  //         this.fail(errorMessage);
  //       }
  //     }
  //     // Delet Org & Org vDC
  //     if (orgvdcDisablehref !== "") {fn.runHttpRequests("POST",   headers, orgvdcDisablehref);}
  //     if (orgvdchref        !== "") {fn.runHttpRequests("DELETE", headers, orgvdchref);}
  //     if (orgDisablehref    !== "") {fn.runHttpRequests("POST",   headers, orgDisablehref);}
  //     browser.sleep(5000);
  //     if (orghref           !== "") {fn.runHttpRequests("DELETE", headers, orghref);}

  //     // Verify whether org is actually deleted on the vCD
  //     var orgExists = false;
  //     response = fn.runHttpRequests("GET", headers, vcd + '/api/admin');
  //     jsonResult = xmlobj.parseXML(response.body.toString());
  //     for(key in jsonResult.VCloud.OrganizationReferences.OrganizationReference) {
  //       if (jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-name"] == "xxx-BubbleDeleteEdgeCreationFailr") {
  //         orgExists = true;
  //         break;
  //       }
  //     }
  //     expect("Org 'xxx-BubbleDeleteEdgeCreationFailr' exits - false").toEqual("Org 'xxx-BubbleDeleteEdgeCreationFailr' exits - " + orgExists);
  //   });

  // Test to verify whether user able to delete the bubble which exists in Portal but not on vCD
  describe("DeleteBubbleFromPortalDB", function() {

    beforeAll(function() {
      // Set network available for region 'XXXLO3'
      db.execute("SELECT * FROM network WHERE region='XXXLO3'", function(err, result) {
        if (err) {
          var errorMessage = "Failure message - " + err;
          this.fail(errorMessage);
        } else {
          for (var i=0; i < result.rows.length; i++) {
            var query = "UPDATE network SET bubble=null, status=0 WHERE id=" + result.rows[i].id;
            db.execute(query, function(err, update) {
              if (err) {
                var errorMessage = "Failed to run - " + query + ". Failure message - " + err;
                this.fail(errorMessage);
              }
            });
          }
        }
      });

      browser.get('/');
      fn.login("mystack.user4", "");
      // Clear the existing notifications on the UI
      fn.clearNotifications();

      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.createBubble), waitSeconds);
      bubblesPage.createBubble.click();
      bubblesPage.createBubbleName.sendKeys("xxx-DeleteBubbleFromPortalDB");
      bubblesPage.createBubbleChooseTeam('xxx-FailureTests-01');
      bubblesPage.createBubbleChooseRegion('XXXLO3 - Iceland');
      bubblesPage.tandc.click();
      bubblesPage.tandcAccept.click();
      expect(bubblesPage.tandcCheckbox.isSelected()).toBeTruthy();
      bubblesPage.createButton.click();

      expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
      browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
      bubblesPage.BubbleViewClose.click();
      fn.waitForNotifications(120000, 6);

      // Verify user 'mystack.root' gets notifications while bubble creation fails at Edgegateway - 'xxx-DeleteBubbleFromPortalDB'
      expect(notificationsPage.notificationCount.getText()).toEqual('6');
      homePage.notifications.click();
      notificationsPage.notificationHeader.get(0).click();
      expect(notificationsPage.NotificationsByindex(0).count()).toEqual(6);
      expect(notificationsPage.NotificationsByindex(0).get(5).getText()).toEqual("A problem occurred creating your bubble. We are investigating");
    });

    beforeEach(function() {
      browser.get('/');
    });

    // Scenario: Bubble exists in Portal but not in vCD.
    it("Delete the bubble from vCD - 'xxx-DeleteBubbleFromPortalDB'", function() {
      //  Default values
      var response, key;
      var orghref           = "";
      var orgDisablehref    = "";
      var orgvdchref        = "";
      var orgvdcDisablehref = "";
      var edgeGwqueryhref   = "";

      headers = {"Accept": "application/*+xml;version=5.1", "Authorization": authorization};
      response = fn.runHttpRequests("POST", headers, vcd + '/api/sessions');
      if (response.statusCode == 200) {
        headers = {"Accept": "application/*+xml;version=5.1", "x-vcloud-authorization": response.headers['x-vcloud-authorization']};
        // Run the Query and get list of Orgs
        response = fn.runHttpRequests("GET", headers, vcd + '/api/admin');
        jsonResult = xmlobj.parseXML(response.body.toString());
        for(key in jsonResult.VCloud.OrganizationReferences.OrganizationReference) {
          if (jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-name"] == "xxx-DeleteBubbleFromPortalDB") {
            orghref = jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-href"];
            break;
          }
        }
      } else {
        this.fail("Failed to get the auth key. statusCode - ", response.statusCode);
      }

      if (orghref !== "") {
        response = fn.runHttpRequests("GET", headers, orghref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResultOrg = xmlobj.parseXML(response.body.toString());

          // Get the disable link for Org href
          for(key in jsonResultOrg.AdminOrg.Link) {
            if (jsonResultOrg.AdminOrg.Link[key]["-rel"] == "disable") {
              orgDisablehref = jsonResultOrg.AdminOrg.Link[key]["-href"];
              break;
            }
          }

          // Loop through each Org and check Org vDC created - Set the FLAG
          if (jsonResultOrg.AdminOrg.Vdcs !== undefined) {
            if (jsonResultOrg.AdminOrg.Vdcs.Vdc["-type"] == "application/vnd.vmware.admin.vdc+xml") {
              orgvdchref = jsonResultOrg.AdminOrg.Vdcs.Vdc["-href"];
            }
          }
        }
      }

      if (orgvdchref !== "") {
        response = fn.runHttpRequests("GET", headers, orgvdchref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResultOrgVdc = xmlobj.parseXML(response.body.toString());

          // Get the disable link for Org vDC href
          for(key in jsonResultOrgVdc.AdminVdc.Link) {
            if (jsonResultOrgVdc.AdminVdc.Link[key]["-rel"] == "disable") {
              orgvdcDisablehref = jsonResultOrgVdc.AdminVdc.Link[key]["-href"];
              break;
            }
          }

          // Loop through and get the href for Edgegateway Query
          for(key in jsonResultOrgVdc.AdminVdc.Link) {
            if (jsonResultOrgVdc.AdminVdc.Link[key]["-type"] == "application/vnd.vmware.vcloud.query.records+xml" && jsonResultOrgVdc.AdminVdc.Link[key]["-rel"] == "edgeGateways") {
              edgeGwqueryhref = jsonResultOrgVdc.AdminVdc.Link[key]["-href"];
              break;
            }
          }
        } else {
          errorMessage = "Failed while running - " + orgvdchref;
          this.fail(errorMessage);
        }
      }

      if (edgeGwqueryhref !== "") {
        response = fn.runHttpRequests("GET", headers, edgeGwqueryhref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResult = xmlobj.parseXML(response.body.toString());

          // Edge gateway exists?. Delete it
          if (jsonResult.QueryResultRecords.EdgeGatewayRecord !== undefined) {
            response = fn.runHttpRequests("DELETE", headers, jsonResult.QueryResultRecords.EdgeGatewayRecord["-href"]);
          }
        } else {
          errorMessage = "Failed while running - " + edgeGwqueryhref;
          this.fail(errorMessage);
        }
      }
      // Delet Org & Org vDC
      if (orgvdcDisablehref !== "") {fn.runHttpRequests("POST",   headers, orgvdcDisablehref);}
      if (orgvdchref        !== "") {fn.runHttpRequests("DELETE", headers, orgvdchref);}
      if (orgDisablehref    !== "") {fn.runHttpRequests("POST",   headers, orgDisablehref);}
      browser.sleep(8000);
      if (orghref           !== "") {fn.runHttpRequests("DELETE", headers, orghref);}

      // Verify whether org is actually deleted on the vCD
      var orgExists = false;
      browser.sleep(10000);
      response = fn.runHttpRequests("GET", headers, vcd + '/api/admin');
      jsonResult = xmlobj.parseXML(response.body.toString());
      for(key in jsonResult.VCloud.OrganizationReferences.OrganizationReference) {
        if (jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-name"] == "xxx-DeleteBubbleFromPortalDB") {
          orgExists = true;
          break;
        }
      }
      browser.sleep(5000);
      expect("Org 'xxx-DeleteBubbleFromPortalDB' exits - false").toEqual("Org 'xxx-DeleteBubbleFromPortalDB' exits - " + orgExists);
    });

    it("Verify Administrator/Adoption user able to submit bubble delete request - 'xxx-DeleteBubbleFromPortalDB'", function() {
      fn.login("mystack.root", "");

      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-DeleteBubbleFromPortalDB')), waitSeconds);
      bubblesPage.bubble('xxx-DeleteBubbleFromPortalDB').click();
      expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();

      // Submit request to 'delete' the bubble
      browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
      browser.actions().mouseUp(bubblesPage.BubbleViewDelete).perform();
      bubblesPage.BubbleViewDelete.click();
      browser.switchTo().alert().accept();
    });

    it("Verify bubble 'xxx-DeleteBubbleFromPortalDB' is removed from the portal DB", function () {
      fn.login("mystack.user4", "");

      homePage.bubbles.click();
      browser.wait(EC.invisibilityOf(bubblesPage.bubble('xxx-DeleteBubbleFromPortalDB')), 5000);
      expect(bubblesPage.bubble('xxx-DeleteBubbleFromPortalDB').isPresent()).toBeFalsy();
    });

  });

  // Test to verify whether user able to delete dia-abled bubble (on vCD) through Portal UI
  describe("DeleteDisabledOrg", function() {

    beforeAll(function() {
      // Set network available for region 'XXXLO3'
      db.execute("SELECT * FROM network WHERE region='XXXLO3'", function(err, result) {
        if (err) {
          var errorMessage = "Failure message - " + err;
          this.fail(errorMessage);
        } else {
          for (var i=0; i < result.rows.length; i++) {
            var query = "UPDATE network SET bubble=null, status=0 WHERE id=" + result.rows[i].id;
            db.execute(query, function(err, update) {
              if (err) {
                var errorMessage = "Failed to run - " + query + ". Failure message - " + err;
                this.fail(errorMessage);
              }
            });
          }
        }
      });

      browser.get('/');
      fn.login("mystack.user4", "");
      // Clear the existing notifications on the UI
      fn.clearNotifications();

      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.createBubble), waitSeconds);
      bubblesPage.createBubble.click();
      bubblesPage.createBubbleName.sendKeys("xxx-DeleteDisabledOrg");
      bubblesPage.createBubbleChooseTeam('xxx-FailureTests-01');
      bubblesPage.createBubbleChooseRegion('XXXLO3 - Iceland');
      bubblesPage.tandc.click();
      bubblesPage.tandcAccept.click();
      expect(bubblesPage.tandcCheckbox.isSelected()).toBeTruthy();
      bubblesPage.createButton.click();

      expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
      browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
      bubblesPage.BubbleViewClose.click();
      fn.waitForNotifications(120000, 6);

      // Verify user 'mystack.root' gets notifications while bubble creation fails at Edgegateway - 'xxx-DeleteDisabledOrg'
      expect(notificationsPage.notificationCount.getText()).toEqual('6');
      homePage.notifications.click();
      notificationsPage.notificationHeader.get(0).click();
      expect(notificationsPage.NotificationsByindex(0).count()).toEqual(6);
      expect(notificationsPage.NotificationsByindex(0).get(5).getText()).toEqual("A problem occurred creating your bubble. We are investigating");
    });

    beforeEach(function() {
      browser.get('/');
    });

    it("Dis-able the org and org vDC - 'xxx-DeleteDisabledOrg'", function() {
      //  Default values
      var response, key;
      var orghref           = "";
      var orgDisablehref    = "";
      var orgvdchref        = "";
      var orgvdcDisablehref = "";
      var edgeGwqueryhref   = "";

      headers = {"Accept": "application/*+xml;version=5.1", "Authorization": authorization};
      response = fn.runHttpRequests("POST", headers, vcd + '/api/sessions');
      if (response.statusCode == 200) {
        headers = {"Accept": "application/*+xml;version=5.1", "x-vcloud-authorization": response.headers['x-vcloud-authorization']};
        // Run the Query and get list of Orgs
        response = fn.runHttpRequests("GET", headers, vcd + '/api/admin');
        jsonResult = xmlobj.parseXML(response.body.toString());
        for(key in jsonResult.VCloud.OrganizationReferences.OrganizationReference) {
          if (jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-name"] == "xxx-DeleteDisabledOrg") {
            orghref = jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-href"];
            break;
          }
        }
      } else {
        this.fail("Failed to get the auth key. statusCode - ", response.statusCode);
      }

      if (orghref !== "") {
        response = fn.runHttpRequests("GET", headers, orghref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResultOrg = xmlobj.parseXML(response.body.toString());

          // Get the disable link for Org href
          for(key in jsonResultOrg.AdminOrg.Link) {
            if (jsonResultOrg.AdminOrg.Link[key]["-rel"] == "disable") {
              orgDisablehref = jsonResultOrg.AdminOrg.Link[key]["-href"];
              break;
            }
          }

          // Loop through each Org and check Org vDC created - Set the FLAG
          if (jsonResultOrg.AdminOrg.Vdcs !== undefined) {
            if (jsonResultOrg.AdminOrg.Vdcs.Vdc["-type"] == "application/vnd.vmware.admin.vdc+xml") {
              orgvdchref = jsonResultOrg.AdminOrg.Vdcs.Vdc["-href"];
            }
          }
        }
      }

      // Dis-able Org & Org vDC
      if (orgvdcDisablehref !== "") {fn.runHttpRequests("POST", headers, orgvdcDisablehref);}
      if (orgDisablehref    !== "") {fn.runHttpRequests("POST", headers, orgDisablehref);}
    });

    it("Verify Administrator/Adoption user able to submit bubble delete request - 'xxx-DeleteDisabledOrg'", function() {
      // Clear the existing notifications on the UI
      fn.clearNotifications();

      fn.login("mystack.root", "");
      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-DeleteDisabledOrg')), waitSeconds);
      bubblesPage.bubble('xxx-DeleteDisabledOrg').click();
      expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();

      // Submit request to 'delete' the bubble
      browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
      browser.actions().mouseUp(bubblesPage.BubbleViewDelete).perform();
      bubblesPage.BubbleViewDelete.click();
      browser.switchTo().alert().accept();
    });

    it("Verify bubble 'xxx-DeleteDisabledOrg' is removed from the portal DB", function () {
      fn.login("mystack.user4", "");

      fn.waitForNotifications(120000, 3);

      homePage.bubbles.click();
      browser.wait(EC.invisibilityOf(bubblesPage.bubble('xxx-DeleteDisabledOrg')), 5000);
      expect(bubblesPage.bubble('xxx-DeleteDisabledOrg').isPresent()).toBeFalsy();
    });

    it("Verify org 'xxx-DeleteDisabledOrg' is deleted on vCD", function() {
      headers = {"Accept": "application/*+xml;version=5.1", "Authorization": authorization};
      response = fn.runHttpRequests("POST", headers, vcd + '/api/sessions');
      if (response.statusCode == 200) {
        headers = {"Accept": "application/*+xml;version=5.1", "x-vcloud-authorization": response.headers['x-vcloud-authorization']};

        // Run the Query and get list of Orgs
        response = fn.runHttpRequests("GET", headers, vcd + '/api/admin');
        jsonResult = xmlobj.parseXML(response.body.toString());
        for(var key in jsonResult.VCloud.OrganizationReferences.OrganizationReference) {
          if (jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-name"] == "xxx-DeleteDisabledOrg") {
            orgExists = true;
            break;
          }
        }
        expect("Org 'xxx-DeleteDisabledOrg' exits - false").toEqual("Org 'xxx-DeleteDisabledOrg' exits - " + orgExists);
      } else {
        this.fail("Failed to get the auth key. statusCode - ", response.statusCode);
      }
    });

  });

});
