/*
*****************************************************************************************************************************************************
SUMMARY:         Creates bubble, adding user to team
ATTENTION:       Should be run after 'UserAndTeams_spec.js'

List Of Tests:
    BubblesPage: Verify user able to see all the bubbles in the 'BUBBLES' page
    BubblesPage: Verify user able to filter the bubble by typing partial bubble name on the search box - 'xxx-BrokenBubbleDoNotDelete'
    BubblesPage: Verify user able to filter the bubble by typing partial bubble name on the search box (different case) - 'xxx-BrokenBubbleDoNotDelete'
    BubblesPage: Verify user able to see the bubble details - 'xxx-BrokenBubbleDoNotDelete'
    BubblesPage: Verify details in bubbles page while no bubble exists for this user - 'mystack.user2'
    BubblesPage: Verify default values on bubble create page for user 'mystack.user2
    BubblesPage: Verify details in bubbles page while no bubble exists for this user - 'mystack.user3'
    BubblesPage: Verify default values on bubble create page for user 'mystack.user3
    BubblesPage: Verify details in bubbles page while no bubble exists for this user - 'mystack.user1'
    BubblesPage: Verify default values on bubble create page for user 'mystack.user1
    BubblesPage: BubbleCreate-01: User should be notified if fields in bubble create page is empty
    BubblesPage: BubbleCreate-01: Verify user able to submit request to create a bubble - 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Verify user gets notifications for bubble create - xxx-TestTeam-01'
    BubblesPage: BubbleCreate-01: Verify details in the bubbles page while bubble is created (mystack.user1) - 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Verify status of the bubble is changed while it is created - 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Verify team 'xxx-TestTeam-01' got updated for recently created bubble - 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Verify team 'xxx-TestTeam-02' not changed for recently created bubble - 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Verify permission check on bubbles page for user with 'CREATOR' role on a team - 'mystack.user3'
    BubblesPage: BubbleCreate-01: Verify permission check on bubbles page for use with 'USER' role on team - 'mystack.user2'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify org 'xxx-TestBubble-01' is created on vCD
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Check Org is enabled for the Bubble 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify Org VApp Lease settings for the bubble 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify Org VApp Template Lease settings for the bubble 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify Org Default Quota settings for the bubble 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify Org Limit settings for the bubble 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify Org Password Policy settings for the bubble 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify Org Federation settings for the bubble 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify Guest Personalisation settings for the bubble 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify Org vDC is enabled for the bubble 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify name and description of Org vDC for the bubble 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify vDC Allocation for the bubble 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify vDC Storage (Enable thin provisioning, Enable fast provisioning & Storage Profile) name for the bubble 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify edge gateway created and also check name of the edge gateway for Master Bubble 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify Description, Edge Gateway configuration, Enable High Availabiltiy in the Edge gateway for the Bubble - 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify Firewall configuration in the Edge gateway for the Bubble - 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify Firewall configuration (Default-RDP-Inbound) in the Edge gateway for the Bubble - 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify Firewall configuration (Default-SSH-Inbound) in the Edge gateway for the Bubble - 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify Firewall configuration (Default-ICMP-Inbound) in the Edge gateway for the Bubble - 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify Firewall configuration (Default-Any-Outbound) in the Edge gateway for the Bubble - 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify networks are created for the bubble 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify FE networks details for the bubble 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify BE networks details for the bubble 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify number of users for the Bubble - 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify details for user mysatck.user1@pearson for bubble - 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify details for user mysatck.user2@pearson for bubble - 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify details for user mysatck.user3@pearson for bubble - 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: Verify Bubble's Public IP's, Edge gateway IP & networks value in Portal is matching with vCD
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: AddLocalUserToBubble: Verify user with CREATOR role of a team able to add local user to the bubble
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: AddLocalUserToBubble: Verify user gets notifications while adding local user(s) to the bubble - 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: AddLocalUserToBubble: Verify whether local user added on the vcd -  'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: AddLocalUserToBubble: Verify details for user mysatck.local1 for bubble - 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: AddLocalUserToBubble: Verify user with CREATOR role of a team able to delete a local user
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: AddLocalUserToBubble: Verify user gets notifications while deleting local user from the bubble - 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: AddLocalUserToBubble: Verify whether local users are deleted on the vCD - 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: AddingUserToTeam: Verify user with CREATOR role able to add user to the team - 'xxx-TestTeam-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: AddingUserToTeam: Verify user gets notifications while adding uesr to the team - 'xxx-TestTeam-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: AddingUserToTeam: Verify newly added user to the team is reflected on the vCD - 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: AddingUserToTeam: Verify user with CREATOR role able to delete a user from the team - 'xxx-TestTeam-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: AddingUserToTeam: Verify user gets notifications while deleting user from the team - 'xxx-TestTeam-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: AddingUserToTeam: Verify recently deleted user from the team reflects on the vCD - 'xxx-TestBubble-01'
    BubblesPage: BubbleCreate-01: Bubble creation on vCD - xxx-TestBubble-01: AddingUserToTeam: Verify user (mystack.user4) is not removed/deleted from Portal while deleting a user from the team which got bubble - 'xxx-TestTeam-01'
    BubblesPage: BubbleRefresh: Get the href for the vApps
    BubblesPage: BubbleRefresh: Refresh the bubbles page to get latest details from the vCD - 'xxx-BrokenBubbleDoNotDelete'
    BubblesPage: BubbleRefresh: Verify details are correct for vApp which has mulitple VM's - 'vAppWithMultipleVM'
    BubblesPage: BubbleRefresh: Verify details are correct for the vApp - 'vAppWithNoIp'
    BubblesPage: BubbleRefresh: Verify details are correct for the vApp - 'vAppWithOneVM'
    BubblesPage: BubbleRefresh: Verify details are correct for the vApp - 'vmWithMultipleNetworks'
*****************************************************************************************************************************************************
*/

// Set the default values
var authorization       = vcdAuthKey;
var headers             = {"Accept": "application/*+xml;version=5.1", "Authorization" : vcdAuthKey};
var orgExists           = false;
var orghref             = "";
var jsonResultOrg       = "";
var guestSettingshref   = "";
var orgvdcExists        = false;
var orgvdchref          = "";
var jsonResultOrgVdc    = "";
var edgeGwqueryhref     = "";
var edgeGwExists        = false;
var edgeGwhref          = "";
var jsonResultEdgeGw    = "";
var firewallExists      = false;
var networkshref        = true;
var feNetworkhref       = "";
var beNetworkhref       = "";
var user1_href          = "";
var user2_href          = "";
var user3_href          = "";

// Default values for vApp/VM
var vAppWithOneVM_Exists      = false;
var vAppWithOneVM_href        = "";
var vAppWithMultipleVM_Exists = false;
var vAppWithMultipleVM_href   = "";
var vAppWithNoIp_Exists       = false;
var vAppWithNoIp_href         = "";
var vmWithMultipleNetworks_Exists = false;
var vmWithMultipleNetworks_href   = "";

// serviceNow default values
var serviceNowAuthorization = serviceNowAuthKey;
var snowHeaders = {"Accept": "application/json", "Authorization" : serviceNowAuthorization};
var org_id      = ""

var homePage          = require("../../pages/home_page.js");
var usersPage         = require("../../pages/users_page.js");
var teamsPage         = require("../../pages/teams_page.js");
var bubblesPage       = require("../../pages/bubbles_page.js");
var notificationsPage = require("../../pages/notifications_page.js");

describe("BubbleCreate-01", function() {

  beforeAll(function() {
    browser.get('/');
    fn.login("mystack.user2", "");
    fn.clearNotifications();
    fn.login("mystack.user3", "");
    fn.clearNotifications();
    fn.login("mystack.user4", "");
    fn.clearNotifications();
    fn.login("mystack.user5", "");
    fn.clearNotifications();
  });

  beforeEach(function() {
    browser.get('/');
  });

  it("Verify user able to submit request to create a bubble - 'xxx-TestBubble-01'", function() {
    fn.login("mystack.user1", "");

    // Clear the existing notifications on the UI
    fn.clearNotifications();

    homePage.bubbles.click();

    browser.wait(EC.elementToBeClickable(bubblesPage.createBubble), waitSeconds);
    bubblesPage.createBubble.click();
    bubblesPage.createBubbleName.sendKeys("xxx-TestBubble-01");
    bubblesPage.createBubbleChooseTeam('xxx-TestTeam-01');
    bubblesPage.createBubbleChooseRegion('LO3REF - Reference');
    bubblesPage.tandc.click();
    bubblesPage.tandcAccept.click();
    expect(bubblesPage.tandcCheckbox.isSelected()).toBeTruthy();
    bubblesPage.createButton.click();

    // Verify BubbleView page is displayed while submitting request to create bubble
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    expect(bubblesPage.BubbleViewName.getText()).toEqual('xxx-TestBubble-01');
    expect(bubblesPage.BubbleViewDelete.isPresent()).toBeFalsy();
    expect(bubblesPage.BubbleViewRegion.getText()).toEqual('LO3REF');
    expect(bubblesPage.BubbleViewStatus.getText()).toEqual('CREATING');
    expect(bubblesPage.BubbleViewToken.isPresent()).toBeFalsy();
    expect(bubblesPage.BubbleViewTeam.getText()).toEqual('xxx-TestTeam-01');
    expect(bubblesPage.BubbleViewMoveteams.isDisplayed()).toBeFalsy();
    expect(bubblesPage.BubbleViewMember.count()).toEqual(3);
    expect(bubblesPage.BubbleViewMemberByindex(0).getText()).toEqual('mystack user1');
    expect(bubblesPage.BubbleViewMemberByindex(1).getText()).toEqual('mystack user3');
    expect(bubblesPage.BubbleViewMemberByindex(2).getText()).toEqual('mystack user2');
    expect(bubblesPage.BubbleViewAddLocalUser.isDisplayed()).toBeFalsy();
    expect(bubblesPage.BubbleViewAddPublicip.isDisplayed()).toBeFalsy();
    expect(bubblesPage.SnowInput.isPresent()).toBeFalsy();
    // Verify Bubble View page is closed by pressing the 'Close' button
    bubblesPage.BubbleViewClose.click();
    expect(bubblesPage.BubbleView.isPresent()).toBeFalsy();
  });

  it("Verify user gets notifications for bubble create - xxx-TestTeam-01'", function() {
    fn.waitForNotifications(bubbleCreateWaitSeconds, 15);

    expect(notificationsPage.notificationCount.getText()).toEqual('15');
    homePage.notifications.click();
    // Verify total number of 'NotificationGroup' and click on first one
    expect(notificationsPage.notificationHeader.count()).toEqual(1);
    notificationsPage.notificationHeader.get(0).click();
    expect(notificationsPage.notificationHeader.get(0).getText()).toEqual("Bubble Creating");

    expect(notificationsPage.NotificationsByindex(0).count()).toEqual(15);
    expect(notificationsPage.NotificationsByindex(0).get(0).getText()).toEqual("Organisation xxx-TestBubble-01 has been created");
    expect(notificationsPage.NotificationsByindex(0).get(1).getText()).toEqual("Adding user to xxx-TestBubble-01. Please wait");
    expect(notificationsPage.NotificationsByindex(0).get(7).getText()).toEqual("vDC for xxx-TestBubble-01 has been created");
    expect(notificationsPage.NotificationsByindex(0).get(8).getText()).toEqual("Edge for xxx-TestBubble-01 is creating");
    expect(notificationsPage.NotificationsByindex(0).get(9).getText()).toEqual("Edge for xxx-TestBubble-01 has been created");
    expect(notificationsPage.NotificationsByindex(0).get(10).getText()).toEqual("FE network for xxx-TestBubble-01 is creating");
    expect(notificationsPage.NotificationsByindex(0).get(11).getText()).toEqual("FE network for xxx-TestBubble-01 has been created");
    expect(notificationsPage.NotificationsByindex(0).get(12).getText()).toEqual("BE network for xxx-TestBubble-01 is creating");
    expect(notificationsPage.NotificationsByindex(0).get(13).getText()).toEqual("BE network for xxx-TestBubble-01 has been created");
    expect(notificationsPage.NotificationsByindex(0).get(14).getText()).toEqual("Bubble status is now CREATED");
  });

  it("Verify details in the bubbles page while bubble is created (mystack.user1) - 'xxx-TestBubble-01'", function() {
    homePage.bubbles.click();

    // Verify the bubble details
    expect(bubblesPage.bubbles.count()).toEqual(1);
    expect(bubblesPage.getBubbleNameByindex(0)).toEqual('xxx-TestBubble-01');
    expect(bubblesPage.getBubbleRegionByindex(0)).toEqual('LO3REF');
    expect(bubblesPage.getBubbleTeamByindex(0)).toEqual('xxx-TestTeam-01');
    expect(bubblesPage.getBubbleCreatedDateByindex(0)).toEqual(new Date().toDateString());
    expect(bubblesPage.getBubbleProductionFlagByindex(0).isPresent()).toEqual(false);
  });

  it("Verify status of the bubble is changed while it is created - 'xxx-TestBubble-01'", function() {
    homePage.bubbles.click();

    expect(bubblesPage.bubbles.count()).toEqual(1);
    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-01')), waitSeconds);
    bubblesPage.bubble('xxx-TestBubble-01').click();
    // element.all(by.className("Bubble")).get(0).click();

    // Verify BubbleView page is displayed while submitting request to create bubble
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    expect(bubblesPage.BubbleViewName.getText()).toEqual('xxx-TestBubble-01');
    expect(bubblesPage.BubbleViewDelete.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewRegion.getText()).toEqual('LO3REF');
    expect(bubblesPage.BubbleViewStatus.getText()).toEqual('CREATED');
    expect(bubblesPage.BubbleViewToken.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewTeam.getText()).toEqual('xxx-TestTeam-01');
    expect(bubblesPage.BubbleViewMoveteams.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewMember.count()).toEqual(3);
    expect(bubblesPage.BubbleViewMemberByindex(0).getText()).toEqual('mystack user1');
    expect(bubblesPage.BubbleViewMemberByindex(1).getText()).toEqual('mystack user3');
    expect(bubblesPage.BubbleViewMemberByindex(2).getText()).toEqual('mystack user2');
    expect(bubblesPage.BubbleViewAddLocalUser.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewAddPublicip.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewip).toEqual([]);
    expect(bubblesPage.BubbleViewGateway.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewEdgeGateway.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewNetwork.get(0).isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewNetwork.get(1).isDisplayed()).toBeTruthy();
    expect(bubblesPage.SnowInput.isPresent()).toBeTruthy();
    // Verify Bubble View page is closed by pressing the 'Close' button
    bubblesPage.BubbleViewClose.click();
    expect(bubblesPage.BubbleView.isPresent()).toBeFalsy();
  });

  it("Verify user able to add 'Notes' to the bubble", function() {
    homePage.bubbles.click();

    expect(bubblesPage.bubbles.count()).toEqual(1);
    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-01')), waitSeconds);
    bubblesPage.bubble('xxx-TestBubble-01').click();

    // Verify BubbleView page is displayed while submitting request to create bubble
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);

    bubblesPage.BubbleViewNotes.clear().sendKeys("This is the notes created by the Test Automation script. Bubble 'xxx-TestBubble-01'");
    bubblesPage.BubbleViewClose.click();

    // Verify notes are visible
    browser.refresh();
    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-01')), waitSeconds);
    bubblesPage.bubble('xxx-TestBubble-01').click();
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    expect(bubblesPage.BubbleViewNotes.getText()).toEqual("This is the notes created by the Test Automation script. Bubble 'xxx-TestBubble-01'");
    bubblesPage.BubbleViewClose.click();
  });

  it("Verify user able to set 'Production' FLAG to the bubble", function() {
    homePage.bubbles.click();

    expect(bubblesPage.bubbles.count()).toEqual(1);
    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-01')), waitSeconds);
    bubblesPage.bubble('xxx-TestBubble-01').click();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbbleViewProduction), waitSeconds);
    bubblesPage.BubbbleViewProduction.click();

    // Verify Bubbles page get updated
    expect(bubblesPage.getBubbleProductionFlagByindex(0).isPresent()).toEqual(true);
  });

  it("Verify team 'xxx-TestTeam-01' got updated for recently created bubble - 'xxx-TestBubble-01'", function() {
    homePage.teams.click();
    expect(teamsPage.teams.count()).toEqual(2);
    expect(teamsPage.getTeamNameByindex(0)).toEqual('xxx-TestTeam-01');
    expect(teamsPage.getTeamBubblesByindex(0)).toEqual('1');
    expect(teamsPage.getTeamMemberCountByindex(0)).toEqual('3');

    // Details in TeamView page
    // element.all(by.className("Team")).get(0).click();
    browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-01')), waitSeconds);
    teamsPage.team('xxx-TestTeam-01').click();
    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-01');
    expect(teamsPage.teamViewDelete.isPresent()).toBeFalsy();
    expect(teamsPage.teamViewAddMember.isPresent()).toBeTruthy();
    expect(teamsPage.teamViewAddMember.isDisplayed()).toBeTruthy();
    expect(teamsPage.teamViewMembers.count()).toEqual(3);

    // Verify the bubble details
    expect(teamsPage.TeamViewBubbles.count()).toEqual(1);
    expect(teamsPage.TeamViewBubbleNameByindex(0).getText()).toEqual('xxx-TestBubble-01 (LO3REF)');
  });

  it("Verify team 'xxx-TestTeam-02' not changed for recently created bubble - 'xxx-TestBubble-01'", function() {
    homePage.teams.click();
    expect(teamsPage.teams.count()).toEqual(2);
    expect(teamsPage.getTeamNameByindex(1)).toEqual('xxx-TestTeam-02');
    expect(teamsPage.getTeamBubblesByindex(1)).toEqual('0');
    expect(teamsPage.getTeamMemberCountByindex(1)).toEqual('3');

    // Details in TeamView page
    browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-02')), waitSeconds);
    teamsPage.team('xxx-TestTeam-02').click();
    // element.all(by.className("Team")).get(1).click();
    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-02');
    expect(teamsPage.teamViewDelete.isPresent()).toBeTruthy();
    expect(teamsPage.teamViewAddMember.isPresent()).toBeTruthy();
    expect(teamsPage.teamViewAddMember.isDisplayed()).toBeTruthy();
    expect(teamsPage.teamViewMembers.count()).toEqual(3);
    // Verify the bubble details
    expect(teamsPage.TeamViewBubbles.count()).toEqual(0);
  });

  it("Verify user (mystack.user1) able to navigate to Bubbles page by clicking bubble name in Teams page - 'xxx-TestBubble-01'", function() {
    homePage.teams.click();
    browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-01')), waitSeconds);
    teamsPage.team('xxx-TestTeam-01').click();
    // element.all(by.className("Team")).get(0).click();

    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(browser.getCurrentUrl()).toContain(homePageUrl + "teams/");
    teamsPage.TeamViewBubbleByname('xxx-TestBubble-01').click();

    // Verify bubbles page is loaded
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    expect(browser.getCurrentUrl()).toContain(homePageUrl + "bubbles/");
    expect(bubblesPage.BubbleViewName.getText()).toEqual('xxx-TestBubble-01');
  });

  it("Verify user 'mystack.user3' gets notifications for bubble creation - 'xxx-TestBubble-01'", function() {
    fn.login("mystack.user3", "");

    fn.waitForNotifications(10000, 1);

    expect(notificationsPage.notificationCount.getText()).toEqual('15');
    homePage.notifications.click();
    // Verify total number of 'NotificationGroup' and click on first one
    expect(notificationsPage.notificationHeader.count()).toEqual(1);
    notificationsPage.notificationHeader.get(0).click();

    expect(notificationsPage.notificationHeader.get(0).getText()).toEqual("Bubble Creating");
    expect(notificationsPage.NotificationsByindex(0).count()).toEqual(15);
    expect(notificationsPage.NotificationsByindex(0).get(14).getText()).toEqual("Bubble status is now CREATED");
  });

  it("Verify details in the bubbles page while bubble is created (mystack.user3) - 'xxx-TestBubble-01'", function() {
    homePage.bubbles.click();

    // Verify the bubble details
    expect(bubblesPage.bubbles.count()).toEqual(1);
    expect(bubblesPage.getBubbleNameByindex(0)).toEqual('xxx-TestBubble-01');
    expect(bubblesPage.getBubbleRegionByindex(0)).toEqual('LO3REF');
    expect(bubblesPage.getBubbleTeamByindex(0)).toEqual('xxx-TestTeam-01');
    expect(bubblesPage.getBubbleCreatedDateByindex(0)).toEqual(new Date().toDateString());
    expect(bubblesPage.getBubbleProductionFlagByindex(0).isPresent()).toEqual(true);
  });

  it("Verify permission check on bubbles page for user with 'CREATOR' role on a team - 'mystack.user3'", function() {
    homePage.bubbles.click();
    expect(bubblesPage.bubbles.count()).toEqual(1);
    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-01')), waitSeconds);
    bubblesPage.bubble('xxx-TestBubble-01').click();
    // element.all(by.className("Bubble")).get(0).click();

    // Verify BubbleView page is displayed while submitting request to create bubble
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    expect(bubblesPage.BubbleViewName.getText()).toEqual('xxx-TestBubble-01');
    expect(bubblesPage.BubbleViewDelete.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewRegion.getText()).toEqual('LO3REF');
    expect(bubblesPage.BubbleViewStatus.getText()).toEqual('CREATED');
    expect(bubblesPage.BubbleViewToken.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewTeam.getText()).toEqual('xxx-TestTeam-01');
    expect(bubblesPage.BubbleViewMoveteams.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewMember.count()).toEqual(3);
    expect(bubblesPage.BubbleViewMemberByindex(0).getText()).toEqual('mystack user1');
    expect(bubblesPage.BubbleViewMemberByindex(1).getText()).toEqual('mystack user3');
    expect(bubblesPage.BubbleViewMemberByindex(2).getText()).toEqual('mystack user2');
    expect(bubblesPage.BubbleViewAddLocalUser.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewAddPublicip.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewip).toEqual([]);
    expect(bubblesPage.BubbleViewGateway.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewEdgeGateway.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewNetwork.get(0).isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewNetwork.get(1).isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewNotes.getText()).toEqual("This is the notes created by the Test Automation script. Bubble 'xxx-TestBubble-01'");
    expect(bubblesPage.SnowInput.isPresent()).toBeTruthy();
    // Verify Bubble View page is closed by pressing the 'Close' button
    bubblesPage.BubbleViewClose.click();
    expect(bubblesPage.BubbleView.isPresent()).toBeFalsy();
  });

  it("Verify team 'xxx-TestTeam-01' got updated for recently created bubble - 'xxx-TestBubble-01' for user 'mystack.user3'", function() {
    homePage.teams.click();
    expect(teamsPage.teams.count()).toEqual(2);
    expect(teamsPage.getTeamNameByindex(0)).toEqual('xxx-TestTeam-01');
    expect(teamsPage.getTeamBubblesByindex(0)).toEqual('1');
    expect(teamsPage.getTeamMemberCountByindex(0)).toEqual('3');

    // Details in TeamView page
    // element.all(by.className("Team")).get(0).click();
    browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-01')), waitSeconds);
    teamsPage.team('xxx-TestTeam-01').click();
    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-01');
    expect(teamsPage.teamViewDelete.isPresent()).toBeFalsy();
    expect(teamsPage.teamViewAddMember.isPresent()).toBeTruthy();
    expect(teamsPage.teamViewMembers.count()).toEqual(3);

    // Verify the bubble details
    expect(teamsPage.TeamViewBubbles.count()).toEqual(1);
    expect(teamsPage.TeamViewBubbleNameByindex(0).getText()).toEqual('xxx-TestBubble-01 (LO3REF)');
  });

  it("Verify team 'xxx-TestTeam-02' not changed for recently created bubble - 'xxx-TestBubble-01' for user 'mystack.user3'", function() {
    homePage.teams.click();
    expect(teamsPage.teams.count()).toEqual(2);
    expect(teamsPage.getTeamNameByindex(1)).toEqual('xxx-TestTeam-02');
    expect(teamsPage.getTeamBubblesByindex(1)).toEqual('0');
    expect(teamsPage.getTeamMemberCountByindex(1)).toEqual('3');

    // Details in TeamView page
    // element.all(by.className("Team")).get(1).click();
    browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-02')), waitSeconds);
    teamsPage.team('xxx-TestTeam-02').click();
    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-02');
    expect(teamsPage.teamViewDelete.isPresent()).toBeFalsy();
    expect(teamsPage.teamViewAddMember.isPresent()).toBeFalsy();
    expect(teamsPage.teamViewMembers.count()).toEqual(3);

    // Verify the bubble details
    expect(teamsPage.TeamViewBubbles.count()).toEqual(0);
  });

  it("Verify user (mystack.user3) able to navigate to Bubbles page by clicking bubble name in Teams page - 'xxx-TestBubble-01'", function() {
    homePage.teams.click();
    browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-01')), waitSeconds);
    teamsPage.team('xxx-TestTeam-01').click();
    // element.all(by.className("Team")).get(0).click();

    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(browser.getCurrentUrl()).toContain(homePageUrl + "teams/");
    teamsPage.TeamViewBubbleByname('xxx-TestBubble-01').click();

    // Verify bubbles page is loaded
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    expect(browser.getCurrentUrl()).toContain(homePageUrl + "bubbles/");
    expect(bubblesPage.BubbleViewName.getText()).toEqual('xxx-TestBubble-01');
  });

  it("Verify user (mystack.user1) able to login vCD by clicking 'Open vCloud' link on the Bubbles page - 'xxx-testbubble-01'", function() {
    homePage.bubbles.click();
    expect(bubblesPage.bubbles.count()).toEqual(1);
    bubblesPage.BubblevCloudLink(0).click();

    browser.getAllWindowHandles().then(function (handles) {
      if (handles.length == 2) {
        browser.switchTo().window(handles[1]);
        expect(browser.getCurrentUrl()).toContain("https://lo3ref-mystack.pearson.com/cloud/org/xxx-testbubble-01/");
        browser.close();

        // Change focus back to Original window - myStack portal
        browser.switchTo().window(handles[0]);
        expect(browser.getCurrentUrl()).toContain(homePageUrl + "bubbles/");
      } else {
        var errorMessage = "New windows might not be opened while clicking the 'Knowledge base | Learn'. Actual number of windows - " + handles.length;
        this.fail(errorMessage);
      }
    });
  });

  it("Verify user 'mystack.user2' gets notifications for bubble creation - 'xxx-TestBubble-01'", function() {
    fn.login("mystack.user2", "");

    fn.waitForNotifications(10000, 1);

    expect(notificationsPage.notificationCount.getText()).toEqual('15');
    homePage.notifications.click();
    // Verify total number of 'NotificationGroup' and click on first one
    expect(notificationsPage.notificationHeader.count()).toEqual(1);
    notificationsPage.notificationHeader.get(0).click();

    expect(notificationsPage.notificationHeader.get(0).getText()).toEqual("Bubble Creating");
    expect(notificationsPage.NotificationsByindex(0).count()).toEqual(15);
    expect(notificationsPage.NotificationsByindex(0).get(14).getText()).toEqual("Bubble status is now CREATED");
  });

  it("Verify details in the bubbles page while bubble is created (mystack.user2) - 'xxx-TestBubble-01'", function() {
    homePage.bubbles.click();

    // Verify the bubble details
    expect(bubblesPage.bubbles.count()).toEqual(1);
    expect(bubblesPage.getBubbleNameByindex(0)).toEqual('xxx-TestBubble-01');
    expect(bubblesPage.getBubbleRegionByindex(0)).toEqual('LO3REF');
    expect(bubblesPage.getBubbleTeamByindex(0)).toEqual('xxx-TestTeam-01');
    expect(bubblesPage.getBubbleCreatedDateByindex(0)).toEqual(new Date().toDateString());
    expect(bubblesPage.getBubbleProductionFlagByindex(0).isPresent()).toEqual(true);
  });

  it("Verify permission check on bubbles page for use with 'USER' role on team - 'mystack.user2'", function() {
    homePage.bubbles.click();
    expect(bubblesPage.bubbles.count()).toEqual(1);
    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-01')), waitSeconds);
    bubblesPage.bubble('xxx-TestBubble-01').click();

    // Verify BubbleView page is displayed while submitting request to create bubble
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    expect(bubblesPage.BubbleViewName.getText()).toEqual('xxx-TestBubble-01');
    expect(bubblesPage.BubbleViewDelete.isPresent()).toBeFalsy();
    expect(bubblesPage.BubbleViewRegion.getText()).toEqual('LO3REF');
    expect(bubblesPage.BubbleViewStatus.getText()).toEqual('CREATED');
    expect(bubblesPage.BubbleViewToken.isPresent()).toBeFalsy();
    expect(bubblesPage.BubbleViewTeam.getText()).toEqual('xxx-TestTeam-01');
    expect(bubblesPage.BubbleViewMoveteams.isDisplayed()).toBeFalsy();
    expect(bubblesPage.BubbleViewMember.count()).toEqual(3);
    expect(bubblesPage.BubbleViewMemberByindex(0).getText()).toEqual('mystack user1');
    expect(bubblesPage.BubbleViewMemberByindex(1).getText()).toEqual('mystack user3');
    expect(bubblesPage.BubbleViewMemberByindex(2).getText()).toEqual('mystack user2');
    expect(bubblesPage.BubbleViewAddLocalUser.isDisplayed()).toBeFalsy();
    expect(bubblesPage.BubbleViewAddPublicip.isDisplayed()).toBeFalsy();
    expect(bubblesPage.BubbleViewip).toEqual([]);
    expect(bubblesPage.BubbleViewGateway.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewEdgeGateway.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewNetwork.get(0).isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewNetwork.get(1).isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewNotes.getText()).toEqual("This is the notes created by the Test Automation script. Bubble 'xxx-TestBubble-01'");
    expect(bubblesPage.SnowInput.isPresent()).toBeFalsy();
    // Verify Bubble View page is closed by pressing the 'Close' button
    bubblesPage.BubbleViewClose.click();
    expect(bubblesPage.BubbleView.isPresent()).toBeFalsy();
  });

  it("Verify team 'xxx-TestTeam-01' got updated for recently created bubble - 'xxx-TestBubble-01' for user 'mystack.user2'", function() {
    homePage.teams.click();
    expect(teamsPage.teams.count()).toEqual(2);
    expect(teamsPage.getTeamNameByindex(0)).toEqual('xxx-TestTeam-01');
    expect(teamsPage.getTeamBubblesByindex(0)).toEqual('1');
    expect(teamsPage.getTeamMemberCountByindex(0)).toEqual('3');

    // Details in TeamView page
    // element.all(by.className("Team")).get(0).click();
    browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-01')), waitSeconds);
    teamsPage.team('xxx-TestTeam-01').click();
    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-01');
    expect(teamsPage.teamViewDelete.isPresent()).toBeFalsy();
    expect(teamsPage.teamViewAddMember.isPresent()).toBeFalsy();
    expect(teamsPage.teamViewMembers.count()).toEqual(3);

    // Verify the bubble details
    expect(teamsPage.TeamViewBubbles.count()).toEqual(1);
    expect(teamsPage.TeamViewBubbleNameByindex(0).getText()).toEqual('xxx-TestBubble-01 (LO3REF)');
  });

  it("Verify team 'xxx-TestTeam-02' not changed for recently created bubble - 'xxx-TestBubble-01' for user 'mystack.user2'", function() {
    homePage.teams.click();
    expect(teamsPage.teams.count()).toEqual(2);
    expect(teamsPage.getTeamNameByindex(1)).toEqual('xxx-TestTeam-02');
    expect(teamsPage.getTeamBubblesByindex(1)).toEqual('0');
    expect(teamsPage.getTeamMemberCountByindex(1)).toEqual('3');

    // Details in TeamView page
    // element.all(by.className("Team")).get(1).click();
    browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-02')), waitSeconds);
    teamsPage.team('xxx-TestTeam-02').click();
    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-02');
    expect(teamsPage.teamViewDelete.isPresent()).toBeTruthy();
    expect(teamsPage.teamViewAddMember.isPresent()).toBeTruthy();
    expect(teamsPage.teamViewAddMember.isDisplayed()).toBeTruthy();
    expect(teamsPage.teamViewMembers.count()).toEqual(3);

    // Verify the bubble details
    expect(teamsPage.TeamViewBubbles.count()).toEqual(0);
  });

  it("Verify user (mystack.user4) who is not part of a team didn't get notifications for bubble create - 'xxx-TestBubble-01'", function() {
    fn.login("mystack.user4", "");

    // Verify there is not notification for user 'mystack.user4'
    fn.waitForNotifications(2000, 1);
    homePage.notifications.click();
    expect(notificationsPage.notificationHeader.count()).toEqual(0);
  });

  it("Verify user (mystack.user5) who is not part of a team didn't get notifications for bubble create - 'xxx-TestBubble-01'", function() {
    fn.login("mystack.user5", "");

    // Verify there is not notification for user 'mystack.user5'
    fn.waitForNotifications(2000, 1);
    homePage.notifications.click();
    expect(notificationsPage.notificationHeader.count()).toEqual(0);
  });


  describe('Bubble creation on vCD - xxx-TestBubble-01', function() {

    it("Verify org 'xxx-TestBubble-01' is created on vCD", function() {
      response = fn.runHttpRequests("POST", headers, vcd + '/api/sessions');
      if (response.statusCode == 200) {
        headers = {"Accept": "application/*+xml;version=5.1", "x-vcloud-authorization": response.headers['x-vcloud-authorization']};

        // Run the Query and get list of Orgs
        response = fn.runHttpRequests("GET", headers, vcd + '/api/admin');
        jsonResult = xmlobj.parseXML(response.body.toString());
        for(var key in jsonResult.VCloud.OrganizationReferences.OrganizationReference) {
          if (jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-name"] == "xxx-TestBubble-01") {
            orgExists = true;
            orghref = jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-href"];
            break;
          }
        }
        expect("Org 'xxx-TestBubble-01' exits - true").toEqual("Org 'xxx-TestBubble-01' exits - " + orgExists);
      } else {
        this.fail("Failed to get the auth key. statusCode - ", response.statusCode);
      }
    });

    it("Check Org is enabled for the Bubble 'xxx-TestBubble-01'", function() {
      var errorMessage;
      if (orgExists) {
        response = fn.runHttpRequests("GET", headers, orghref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResultOrg = xmlobj.parseXML(response.body.toString());

          var orgEnabled = jsonResultOrg.AdminOrg.IsEnabled;
          // Verify the name of the vDC
          expect("Bubble xxx-TestBubble-01 enabled - " + orgEnabled).toEqual("Bubble xxx-TestBubble-01 enabled - true");

          // Loop through each Org and check Org vDC created - Set the FLAG
          if (jsonResultOrg.AdminOrg.Vdcs !== undefined) {
            if (jsonResultOrg.AdminOrg.Vdcs.Vdc["-type"] == "application/vnd.vmware.admin.vdc+xml") {
              orgvdcExists  = true;
              orgvdchref    = jsonResultOrg.AdminOrg.Vdcs.Vdc["-href"];
            }
          }
          // Loop through org json response and get the href for Guest Personalisation
          for(var key in jsonResultOrg.AdminOrg.Settings.Link) {
            if (jsonResultOrg.AdminOrg.Settings.Link[key]["-type"] == "application/vnd.vmware.admin.guestPersonalizationSettings+xml") {
              guestSettingshref = jsonResultOrg.AdminOrg.Settings.Link[key]["-href"];
              break;
            }
          }
        // orgUrl ran okey?
        } else {
          errorMessage = "Failed to run the URL - " + orghref + ". statusCode - " + response.statusCode + ". So can't verify whether Org vdc is created Or not";
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "Org not created for bubble xxx-TestBubble-01. So can't verify whether Org vdc is created Or not";
        this.fail(errorMessage);
      }
    });

    it("Verify Org VApp Lease settings for the bubble 'xxx-TestBubble-01'", function() {
      if (orgExists) {
        // vApp lease
        var maximumRuntimeLeasevApp   = jsonResultOrg.AdminOrg.Settings.VAppLeaseSettings.DeploymentLeaseSeconds;
        var maximumStorageLeasevApp   = jsonResultOrg.AdminOrg.Settings.VAppLeaseSettings.StorageLeaseSeconds;
        var storageCleanupvApp        = jsonResultOrg.AdminOrg.Settings.VAppLeaseSettings.DeleteOnStorageLeaseExpiration;
        // Verify the Maximum runtime lease, Maximum storage lease, Storage cleanup for vApp lease
        expect("vApp leases | Maximum runtime lease - " + maximumRuntimeLeasevApp).toEqual("vApp leases | Maximum runtime lease - 0");
        expect("vApp leases | Maximum storage lease - " + maximumStorageLeasevApp).toEqual("vApp leases | Maximum storage lease - 0");
        expect("vApp leases | Storage cleanup - "       + storageCleanupvApp).toEqual("vApp leases | Storage cleanup - false");
      } else {
        var errorMessage = "orgExists - " + orgExists + " for bubble xxx-TestBubble-01. So can't verify Org vApp lease settings for Bubble xxx-TestBubble-01";
        this.fail(errorMessage);
      }
    });

    it("Verify Org VApp Template Lease settings for the bubble 'xxx-TestBubble-01'", function() {
      if (orgExists) {
        // vApp Template lease
        var maximumStorageLeasevAppTemplate   = jsonResultOrg.AdminOrg.Settings.VAppTemplateLeaseSettings.StorageLeaseSeconds;
        var storageCleanupvAppTemplate        = jsonResultOrg.AdminOrg.Settings.VAppTemplateLeaseSettings.DeleteOnStorageLeaseExpiration;
        // Verify the Maximum storage lease, Storage cleanup for vApp Template lease
        expect("vApp template lease | Maximum storage lease - " + maximumStorageLeasevAppTemplate).toEqual("vApp template lease | Maximum storage lease - 0");
        expect("vApp template lease | Storage cleanup - "       + storageCleanupvAppTemplate).toEqual("vApp template lease | Storage cleanup - false");
      } else {
        var errorMessage = "orgExists - " + orgExists + " for bubble xxx-TestBubble-01. So can't verify Org vApp lease settings for Bubble xxx-TestBubble-01";
        this.fail(errorMessage);
      }
    });

    it("Verify Org Default Quota settings for the bubble 'xxx-TestBubble-01'", function() {
      if (orgExists) {
        var allVMQuota      = jsonResultOrg.AdminOrg.Settings.OrgGeneralSettings.StoredVmQuota;
        var runningVMQuota  = jsonResultOrg.AdminOrg.Settings.OrgGeneralSettings.DeployedVMQuota;
        // Verify the CPU Quota, Memory Quota anx Maximum number of VMs user can create
        expect("All VMs quota - "     + allVMQuota).toEqual("All VMs quota - 0");
        expect("Running VMs quota - " + runningVMQuota).toEqual("Running VMs quota - 0");
      } else {
        var errorMessage = "orgExists - " + orgExists + " for bubble xxx-TestBubble-01. So can't verify Org vApp lease settings for Bubble xxx-TestBubble-01";
        this.fail(errorMessage);
      }
    });

    it("Verify Org Limit settings for the bubble 'xxx-TestBubble-01'", function() {
      if (orgExists) {
        var OperationsPerUser   = jsonResultOrg.AdminOrg.Settings.OrgOperationLimitsSettings.OperationsPerUser;
        var OperationsPerOrg    = jsonResultOrg.AdminOrg.Settings.OrgOperationLimitsSettings.OperationsPerOrg;
        var ConsolesPerVmLimit  = jsonResultOrg.AdminOrg.Settings.OrgOperationLimitsSettings.ConsolesPerVmLimit;
        // Verify the CPU Quota, Memory Quota anx Maximum number of VMs user can create
        expect("Number of resource intensive operations per user - "          + OperationsPerUser).toEqual("Number of resource intensive operations per user - 0");
        expect("Number of resource intensive operations per organization - "  + OperationsPerOrg).toEqual("Number of resource intensive operations per organization - 0");
        expect("Number of simultaneous connections per VM - "                 + ConsolesPerVmLimit).toEqual("Number of simultaneous connections per VM - 0");
      } else {
        var errorMessage = "orgExists - " + orgExists + " for bubble xxx-TestBubble-01. So can't verify Org vApp lease settings for Bubble xxx-TestBubble-01";
        this.fail(errorMessage);
      }
    });

    it("Verify Org Password Policy settings for the bubble 'xxx-TestBubble-01'", function() {
      if (orgExists) {
        var accountLockout                = jsonResultOrg.AdminOrg.Settings.OrgPasswordPolicySettings.AccountLockoutEnabled;
        var invalidLoginsBeforeLockout    = jsonResultOrg.AdminOrg.Settings.OrgPasswordPolicySettings.InvalidLoginsBeforeLockout;
        var accountLockoutIntervalMinutes = jsonResultOrg.AdminOrg.Settings.OrgPasswordPolicySettings.AccountLockoutIntervalMinutes;

        // Verify Account lock-out values
        expect("Account lockout enabled - "             + accountLockout).toEqual("Account lockout enabled - true");
        expect("Invalid logins before lockout - "       + invalidLoginsBeforeLockout).toEqual("Invalid logins before lockout - 5");
        expect("Account lockout interval - "            + accountLockoutIntervalMinutes).toEqual("Account lockout interval - 2147483647");
      } else {
        var errorMessage = "orgExists - " + orgExists + " for bubble xxx-TestBubble-01. So can't verify Org vApp lease settings for Bubble xxx-TestBubble-01";
        this.fail(errorMessage);
      }
    });

    it("Verify Org Federation settings for the bubble 'xxx-TestBubble-01'", function() {
      if (orgExists) {
        var OrgFederationSettings = jsonResultOrg.AdminOrg.Settings.OrgFederationSettings.Enabled;
        // Verify the CPU Quota, Memory Quota anx Maximum number of VMs user can create
        expect("Org Federation settings - " + OrgFederationSettings).toEqual("Org Federation settings - false");
      } else {
        var errorMessage = "orgExists - " + orgExists + " for bubble xxx-TestBubble-01. So can't verify Org Org Federation settings for Bubble xxx-TestBubble-01";
        this.fail(errorMessage);
      }
    });

    it("Verify Guest Personalisation settings for the bubble 'xxx-TestBubble-01'", function() {
      var errorMessage, jsonResult;
      if (guestSettingshref !== "") {
        response = fn.runHttpRequests("GET", headers, guestSettingshref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResult = xmlobj.parseXML(response.body.toString());

          var AllowDomainSettings = jsonResult.OrgGuestPersonalizationSettings.AllowDomainSettings;
          expect("AllowDomainSettings - " + AllowDomainSettings).toEqual("AllowDomainSettings - false");
        } else {
          errorMessage = "Failed while running - " + guestSettingshref;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + " for bubble xxx-TestBubble-01. So can't verify whether org is enabled Or not";
        this.fail(errorMessage);
      }
    });

    it("Verify Org vDC is enabled for the bubble 'xxx-TestBubble-01'", function() {
      var errorMessage;
      if (orgvdcExists) {
        response = fn.runHttpRequests("GET", headers, orgvdchref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResultOrgVdc = xmlobj.parseXML(response.body.toString());

          // Loop through and get the href for Edgegateway Query
          for(var key in jsonResultOrgVdc.AdminVdc.Link) {
            if (jsonResultOrgVdc.AdminVdc.Link[key]["-type"] == "application/vnd.vmware.vcloud.query.records+xml" && jsonResultOrgVdc.AdminVdc.Link[key]["-rel"] == "edgeGateways") {
              edgeGwqueryhref = jsonResultOrgVdc.AdminVdc.Link[key]["-href"];
              break;
            }
          }

          // Loop through and get the href for Edgegateway Query
          for(key in jsonResultOrgVdc.AdminVdc.Link) {
            if (jsonResultOrgVdc.AdminVdc.Link[key]["-type"] == "application/vnd.vmware.vcloud.query.records+xml" && jsonResultOrgVdc.AdminVdc.Link[key]["-rel"] == "orgVdcNetworks") {
              networkshref = jsonResultOrgVdc.AdminVdc.Link[key]["-href"];
              break;
            }
          }

          // Verify Org vDC enabled ?.
          expect("Org vDC enabled for bubble xxx-TestBubble-01 - " + jsonResultOrgVdc.AdminVdc.IsEnabled).toEqual("Org vDC enabled for bubble xxx-TestBubble-01 - true");
        } else {
          errorMessage = "Failed while running - " + orgvdchref;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + " & orgvdcExists - " + orgvdcExists + " for bubble xxx-TestBubble-01. So can't verify whether org is enabled Or not";
        this.fail(errorMessage);
      }
    });

    it("Verify name and description of Org vDC for the bubble 'xxx-TestBubble-01'", function() {
      var errorMessage;
      if (orgvdcExists) {
        // Verify Org vDC enabled ?.
        expect("Name of Org vDC bubble xxx-TestBubble-01 - " + jsonResultOrgVdc.AdminVdc["-name"]).toEqual("Name of Org vDC bubble xxx-TestBubble-01 - xxx-TestBubble-01-vdc");
        expect("Description for vDC bubble xxx-TestBubble-01 - " + jsonResultOrgVdc.AdminVdc.Description).toEqual("Description for vDC bubble xxx-TestBubble-01 - undefined");
      } else {
        errorMessage = "orgExists - " + orgExists + " . orgvdcExists - " + orgvdcExists + " for bubble xxx-TestBubble-01. So can't verify name of org vDC for bubble xxx-TestBubble-01";
        this.fail(errorMessage);
      }
    });

    it("Verify vDC Allocation for the bubble 'xxx-TestBubble-01'", function() {
      var errorMessage;
      if (orgvdcExists) {
        var CpuQuota                  = jsonResultOrgVdc.AdminVdc.ComputeCapacity.Cpu.Limit;
        var VCpuInMhz                 = jsonResultOrgVdc.AdminVdc.VCpuInMhz;
        var CpuResourceGuaranteed     = jsonResultOrgVdc.AdminVdc.ResourceGuaranteedCpu;
        var MemoryResourceGuaranteed  = jsonResultOrgVdc.AdminVdc.ResourceGuaranteedMemory;
        var Memoryquota               = jsonResultOrgVdc.AdminVdc.ComputeCapacity.Memory.Limit;
        var maximumVMs                = jsonResultOrgVdc.AdminVdc.VmQuota;
        // Verify the CPU Quota, Memory Quota anx Maximum number of VMs user can create
        expect("CPU quota - "                 + CpuQuota).toEqual("CPU quota - 0");
        expect("vCPU speed - "                + VCpuInMhz).toEqual("vCPU speed - 2000");
        expect("CpuResourceGuaranteed - "     + CpuResourceGuaranteed).toEqual("CpuResourceGuaranteed - 0.0");
        expect("Memory quota - "              + Memoryquota).toEqual("Memory quota - 0");
        expect("Maximum number of VMs - "     + maximumVMs).toEqual("Maximum number of VMs - 0");
        expect("MemoryResourceGuaranteed - "  + MemoryResourceGuaranteed).toEqual("MemoryResourceGuaranteed - 0.0");
      } else {
        errorMessage = "orgExists - " + orgExists + " . orgvdcExists - " + orgvdcExists + " for bubble xxx-TestBubble-01. So can't verify name of org vDC for bubble xxx-TestBubble-01";
        this.fail(errorMessage);
      }
    });

    it("Verify vDC Storage (Enable thin provisioning, Enable fast provisioning & Storage Profile) name for the bubble 'xxx-TestBubble-01'", function() {
      var errorMessage;
      if (orgvdcExists && jsonResultOrgVdc) {
        // Verify the Storage Profile, Enable thin provisioning & Enable fast provisioning values
        var VdcStorageProfile = jsonResultOrgVdc.AdminVdc.VdcStorageProfiles.VdcStorageProfile["-name"];
        var thinProvisioning  = jsonResultOrgVdc.AdminVdc.IsThinProvision;
        var fastProvisioning  = jsonResultOrgVdc.AdminVdc.UsesFastProvisioning;

        // Verify the Storage Profile
        expect("Storage Profile - "          + VdcStorageProfile).toEqual("Storage Profile - Gold-Standard");
        expect("Enable thin provisioning - " + thinProvisioning).toEqual("Enable thin provisioning - false");
        expect("Enable fast provisioning - " + fastProvisioning).toEqual("Enable fast provisioning - true");
      } else {
        errorMessage = "orgExists - " + orgExists + " . orgvdcExists - " + orgvdcExists + " for bubble xxx-TestBubble-01. So can't verify name of org vDC for bubble xxx-TestBubble-01";
        this.fail(errorMessage);
      }
    });

    it("Verify edge gateway created and also check name of the edge gateway for Master Bubble 'xxx-TestBubble-01'", function() {
      var errorMessage;
      if (edgeGwqueryhref !== "") {
        response = fn.runHttpRequests("GET", headers, edgeGwqueryhref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResult = xmlobj.parseXML(response.body.toString());

          if (jsonResult.QueryResultRecords.EdgeGatewayRecord !== undefined) {
            if (jsonResult.QueryResultRecords.EdgeGatewayRecord["-numberOfExtNetworks"] == "1") {
              edgeGwExists = true;
              edgeGwname   = jsonResult.QueryResultRecords.EdgeGatewayRecord["-name"];
              edgeGwhref   = jsonResult.QueryResultRecords.EdgeGatewayRecord["-href"];

              // Verify name of teh Edge Gateway
              expect("Name of edgegateway for bubble xxx-TestBubble-01 - " + edgeGwname).toEqual("Name of edgegateway for bubble xxx-TestBubble-01 - xxx-TestBubble-01");
            }
          }
          // Edge Gateway exists ?.
          expect("Edge Gateway created for bubble xxx-TestBubble-01 - " + edgeGwExists).toEqual("Edge Gateway created for bubble xxx-TestBubble-01 - true");
        } else {
          errorMessage = "Failed while running - " + edgeGwqueryhref;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + " , orgvdcExists - " + orgvdcExists + "& edgeGwqueryhref - " + edgeGwqueryhref + " for bubble xxx-TestBubble-01. So can't verify whether edge gate is created Or not";
        this.fail(errorMessage);
      }
    });

    it("Verify Description, Edge Gateway configuration, Enable High Availabiltiy in the Edge gateway for the Bubble - 'xxx-TestBubble-01'", function() {
      var errorMessage;
      if (edgeGwExists) {
        response = fn.runHttpRequests("GET", headers, edgeGwhref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResultEdgeGw = xmlobj.parseXML(response.body.toString());

          var Configuration      = jsonResultEdgeGw.EdgeGateway.Configuration.GatewayBackingConfig;
          var HaEnabled          = jsonResultEdgeGw.EdgeGateway.Configuration.HaEnabled;
          var UseDefaultRouteForDnsRelay = jsonResultEdgeGw.EdgeGateway.Configuration.UseDefaultRouteForDnsRelay;

          //expect("Description - "                + Description).toEqual("Description - edgegateway description");
          expect("Edge Gateway configuration - " + Configuration).toEqual("Edge Gateway configuration - full");
          expect("Enable High Availabiltiy - "   + HaEnabled).toEqual("Enable High Availabiltiy - true");
          expect("UseDefaultRouteForDnsRelay - " + UseDefaultRouteForDnsRelay).toEqual("UseDefaultRouteForDnsRelay - false");
        } else {
          errorMessage = "Failed while running - " + edgeGwhref;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + ", orgvdcExists - " + orgvdcExists + " & edgeGwExists - " + edgeGwExists + " for bubble xxx-TestBubble-01. So can't verify Description, Edge Gateway configuration, Enable High Availabiltiy in the Edge gateway";
        this.fail(errorMessage);
      }
    });

    it("Verify bubble is created on least utilized external network", function() {
      var errorMessage;
      var networkName = [];

      if (edgeGwExists) {
        for(var key in jsonResultEdgeGw.EdgeGateway.Configuration.GatewayInterfaces.GatewayInterface) {
          networkName.push(jsonResultEdgeGw.EdgeGateway.Configuration.GatewayInterfaces.GatewayInterface[key].Name);
        }

        // Verify bubbles are actually created on least utilised external network
        expect(networkName).toContain("External-Corporate-159.182.214.0-25");
      } else {
        errorMessage = "orgExists - " + orgExists + ", orgvdcExists - " + orgvdcExists + " & edgeGwExists - " + edgeGwExists + " for bubble xxx-TestBubble-01. So can't verify Description, Edge Gateway configuration, Enable High Availabiltiy in the Edge gateway";
        this.fail(errorMessage);
      }
    });

    it("Verify Firewall configuration in the Edge gateway for the Bubble - 'xxx-TestBubble-01'", function() {
      var errorMessage;
      if (edgeGwExists) {
        // Add a verification here for the Firewall configuration
        var IsEnabled          = jsonResultEdgeGw.EdgeGateway.Configuration.EdgeGatewayServiceConfiguration.FirewallService.IsEnabled;
        var DefaultAction      = jsonResultEdgeGw.EdgeGateway.Configuration.EdgeGatewayServiceConfiguration.FirewallService.DefaultAction;
        var LogDefaultAction   = jsonResultEdgeGw.EdgeGateway.Configuration.EdgeGatewayServiceConfiguration.FirewallService.LogDefaultAction;

        // Verify the firewall configurations
        expect("Firewall IsEnabled - "         + IsEnabled).toEqual("Firewall IsEnabled - true");
        expect("Firewall DefaultAction - "     + DefaultAction).toEqual("Firewall DefaultAction - drop");
        expect("Firewall LogDefaultAction - "  + LogDefaultAction).toEqual("Firewall LogDefaultAction - false");

        var firewallRule = jsonResultEdgeGw.EdgeGateway.Configuration.EdgeGatewayServiceConfiguration.FirewallService.FirewallRule;
        if (firewallRule !== undefined) {
          expect("Firewall totalFirewallRule - " + firewallRule.length).toEqual("Firewall totalFirewallRule - 4");
          firewallExists = true;
        } else {
          errorMessage = "Firewall not exits for bubble xxx-TestBubble-01";
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + ", orgvdcExists - " + orgvdcExists + " & edgeGwExists - " + edgeGwExists + " for bubble xxx-TestBubble-01. So can't verify Description, Edge Gateway configuration, Enable High Availabiltiy in the Edge gateway";
        this.fail(errorMessage);
      }
    });

    it("Verify Firewall configuration (Default-RDP-Inbound) in the Edge gateway for the Bubble - 'xxx-TestBubble-01'", function() {
      var errorMessage;
      if (firewallExists) {
        // Set xpath for the firewall
        firewall = jsonResultEdgeGw.EdgeGateway.Configuration.EdgeGatewayServiceConfiguration.FirewallService.FirewallRule;
        if (firewall.length == 4) {
          expect("IsEnabled - "             + firewall[0].IsEnabled).toEqual("IsEnabled - true");
          expect("MatchOnTranslate - "      + firewall[0].MatchOnTranslate).toEqual("MatchOnTranslate - false");
          expect("Description - "           + firewall[0].Description).toEqual("Description - Default-RDP-Inbound");
          expect("Policy - "                + firewall[0].Policy).toEqual("Policy - allow");
          expect("Protocols - "             + firewall[0].Protocols.Tcp).toEqual("Protocols - true");
          expect("Port - "                  + firewall[0].Port).toEqual("Port - 3389");
          expect("DestinationPortRange - "  + firewall[0].DestinationPortRange).toEqual("DestinationPortRange - 3389");
          expect("DestinationIp - "         + firewall[0].DestinationIp).toEqual("DestinationIp - internal");
          expect("SourcePort - "            + firewall[0].SourcePort).toEqual("SourcePort - -1");
          expect("SourcePortRange - "       + firewall[0].SourcePortRange).toEqual("SourcePortRange - Any");
          expect("SourceIp - "              + firewall[0].SourceIp).toEqual("SourceIp - external");
          expect("EnableLogging - "         + firewall[0].EnableLogging).toEqual("EnableLogging - true");
        } else {
          errorMessage = "Total number of firewall configurations should be 4. But Actual firewall configuration - " + firewall.length + ". So can't verify the configuration for each firewall.";
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + ", orgvdcExists - " + orgvdcExists + " , edgeGwExists - " + edgeGwExists + " & firewallExists - " + firewallExists + " for bubble xxx-TestBubble-01. So can't verify Firewall configurations";
        this.fail(errorMessage);
      }
    });

    it("Verify Firewall configuration (Default-SSH-Inbound) in the Edge gateway for the Bubble - 'xxx-TestBubble-01'", function() {
      var errorMessage;
      if (firewallExists) {
        // Set xpath for the firewall
        firewall = jsonResultEdgeGw.EdgeGateway.Configuration.EdgeGatewayServiceConfiguration.FirewallService.FirewallRule;
        if (firewall.length == 4) {
          expect("IsEnabled - "             + firewall[1].IsEnabled).toEqual("IsEnabled - true");
          expect("MatchOnTranslate - "      + firewall[1].MatchOnTranslate).toEqual("MatchOnTranslate - false");
          expect("Description - "           + firewall[1].Description).toEqual("Description - Default-SSH-Inbound");
          expect("Policy - "                + firewall[1].Policy).toEqual("Policy - allow");
          expect("Protocols - "             + firewall[1].Protocols.Tcp).toEqual("Protocols - true");
          expect("Port - "                  + firewall[1].Port).toEqual("Port - 22");
          expect("DestinationPortRange - "  + firewall[1].DestinationPortRange).toEqual("DestinationPortRange - 22");
          expect("DestinationIp - "         + firewall[1].DestinationIp).toEqual("DestinationIp - internal");
          expect("SourcePort - "            + firewall[1].SourcePort).toEqual("SourcePort - -1");
          expect("SourcePortRange - "       + firewall[1].SourcePortRange).toEqual("SourcePortRange - Any");
          expect("SourceIp - "              + firewall[1].SourceIp).toEqual("SourceIp - external");
          expect("EnableLogging - "         + firewall[1].EnableLogging).toEqual("EnableLogging - true");
        } else {
          errorMessage = "Total number of firewall configurations should be 4. But Actual firewall configuration - " + firewall.length + ". So can't verify the configuration for each firewall.";
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + ", orgvdcExists - " + orgvdcExists + " , edgeGwExists - " + edgeGwExists + " & firewallExists - " + firewallExists + " for bubble xxx-TestBubble-01. So can't verify Firewall configurations";          this.fail(errorMessage);
      }
    });

    it("Verify Firewall configuration (Default-ICMP-Inbound) in the Edge gateway for the Bubble - 'xxx-TestBubble-01'", function() {
      var errorMessage;
      if (firewallExists) {
        // Set xpath for the firewall
        firewall = jsonResultEdgeGw.EdgeGateway.Configuration.EdgeGatewayServiceConfiguration.FirewallService.FirewallRule;
        if (firewall.length == 4) {
          expect("IsEnabled - "             + firewall[2].IsEnabled).toEqual("IsEnabled - true");
          expect("MatchOnTranslate - "      + firewall[2].MatchOnTranslate).toEqual("MatchOnTranslate - false");
          expect("Description - "           + firewall[2].Description).toEqual("Description - Default-ICMP-Inbound");
          expect("Policy - "                + firewall[2].Policy).toEqual("Policy - allow");
          expect("Protocols - "             + firewall[2].Protocols.Icmp).toEqual("Protocols - true");
          expect("Port - "                  + firewall[2].Port).toEqual("Port - -1");
          expect("DestinationPortRange - "  + firewall[2].DestinationPortRange).toEqual("DestinationPortRange - Any");
          expect("DestinationIp - "         + firewall[2].DestinationIp).toEqual("DestinationIp - Any");
          expect("SourcePort - "            + firewall[2].SourcePort).toEqual("SourcePort - -1");
          expect("SourcePortRange - "       + firewall[2].SourcePortRange).toEqual("SourcePortRange - Any");
          expect("SourceIp - "              + firewall[2].SourceIp).toEqual("SourceIp - Any");
          expect("EnableLogging - "         + firewall[2].EnableLogging).toEqual("EnableLogging - true");
        } else {
          errorMessage = "Total number of firewall configurations should be 4. But Actual firewall configuration - " + firewall.length + ". So can't verify the configuration for each firewall.";
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + ", orgvdcExists - " + orgvdcExists + " , edgeGwExists - " + edgeGwExists + " & firewallExists - " + firewallExists + " for bubble xxx-TestBubble-01. So can't verify Firewall configurations";
        this.fail(errorMessage);
      }
    });

    it("Verify Firewall configuration (Default-Any-Outbound) in the Edge gateway for the Bubble - 'xxx-TestBubble-01'", function() {
      var errorMessage;
      if (firewallExists && jsonResultEdgeGw) {
        // Set xpath for the firewall
        firewall = jsonResultEdgeGw.EdgeGateway.Configuration.EdgeGatewayServiceConfiguration.FirewallService.FirewallRule;
        if (firewall.length == 4) {
          expect("IsEnabled - "             + firewall[3].IsEnabled).toEqual("IsEnabled - true");
          expect("MatchOnTranslate - "      + firewall[3].MatchOnTranslate).toEqual("MatchOnTranslate - false");
          expect("Description - "           + firewall[3].Description).toEqual("Description - Default-Any-Outbound");
          expect("Policy - "                + firewall[3].Policy).toEqual("Policy - allow");
          expect("Protocols - "             + firewall[3].Protocols.Any).toEqual("Protocols - true");
          expect("Port - "                  + firewall[3].Port).toEqual("Port - -1");
          expect("DestinationPortRange - "  + firewall[3].DestinationPortRange).toEqual("DestinationPortRange - Any");
          expect("DestinationIp - "         + firewall[3].DestinationIp).toEqual("DestinationIp - external");
          expect("SourcePort - "            + firewall[3].SourcePort).toEqual("SourcePort - -1");
          expect("SourcePortRange - "       + firewall[3].SourcePortRange).toEqual("SourcePortRange - Any");
          expect("SourceIp - "              + firewall[3].SourceIp).toEqual("SourceIp - internal");
          expect("EnableLogging - "         + firewall[3].EnableLogging).toEqual("EnableLogging - true");
        } else {
          errorMessage = "Total number of firewall configurations should be 4. But Actual firewall configuration - " + firewall.length + ". So can't verify the configuration for each firewall.";
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + ", orgvdcExists - " + orgvdcExists + " , edgeGwExists - " + edgeGwExists + " & firewallExists - " + firewallExists + " for bubble xxx-TestBubble-01. So can't verify Firewall configurations";
        this.fail(errorMessage);
      }
    });

    it("Verify networks are created for the bubble 'xxx-TestBubble-01'", function() {
      var errorMessage = "";
      if (networkshref !== "") {
        response = fn.runHttpRequests("GET", headers, networkshref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          var jsonResult = xmlobj.parseXML(response.body.toString());

          // No record for 'QueryResultRecords.OrgVdcNetworkRecord' ?, then no network exists.
          if (jsonResult.QueryResultRecords.OrgVdcNetworkRecord !== undefined) {
            var totalNetworks = jsonResult.QueryResultRecords.OrgVdcNetworkRecord.length;
            if (totalNetworks == 2) {
              var name, taskStatus;

              // Verify FE network
              name        = jsonResult.QueryResultRecords.OrgVdcNetworkRecord[0]["-name"];
              taskStatus  = jsonResult.QueryResultRecords.OrgVdcNetworkRecord[0]["-taskStatus"];
              if (taskStatus == "success") {
                feNetworkhref = jsonResult.QueryResultRecords.OrgVdcNetworkRecord[0]["-href"];
              }
              expect("Network " + name + " created - " + taskStatus).toEqual("Network " + name + " created - success");

              // Verify BE network
              name        = jsonResult.QueryResultRecords.OrgVdcNetworkRecord[1]["-name"];
              taskStatus  = jsonResult.QueryResultRecords.OrgVdcNetworkRecord[1]["-taskStatus"];
              if (taskStatus == "success") {
                beNetworkhref = jsonResult.QueryResultRecords.OrgVdcNetworkRecord[1]["-href"];
              }
              expect("Network " + name + " created - " + taskStatus).toEqual("Network " + name + " created - success");
            } else {
              errorMessage = "Either FE Or BE is not created. \n\tExpected networks on vCD - 2\n\tActual networks on vCD   - " + totalNetworks;
              this.fail(errorMessage);
            }
          }
        } else {
          errorMessage = "Failed while running - " + networkshref;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + ", orgvdcExists - " + orgvdcExists + " & edgeGwExists - " + edgeGwExists + " for bubble xxx-TestBubble-01. So can't verify whether FE & BE networks are created";
        this.fail(errorMessage);
      }
    });

    it("Verify FE networks details for the bubble 'xxx-TestBubble-01'", function() {
      var errorMessage;
      if (feNetworkhref !== "") {
        response = fn.runHttpRequests("GET", headers, feNetworkhref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          var jsonResult = xmlobj.parseXML(response.body.toString());

          var Description = jsonResult.OrgVdcNetwork.Description;
          var IsInherited = jsonResult.OrgVdcNetwork.Configuration.IpScopes.IpScope.IsInherited;
          var DnsSuffix   = jsonResult.OrgVdcNetwork.Configuration.IpScopes.IpScope.DnsSuffix;
          var IsEnabled   = jsonResult.OrgVdcNetwork.Configuration.IpScopes.IpScope.IsEnabled;
          var Dns1        = jsonResult.OrgVdcNetwork.Configuration.IpScopes.IpScope.Dns1;
          var Dns2        = jsonResult.OrgVdcNetwork.Configuration.IpScopes.IpScope.Dns2;
          var FenceMode   = jsonResult.OrgVdcNetwork.Configuration.FenceMode;
          var IsShared    = jsonResult.OrgVdcNetwork.IsShared;

          expect("IsInherited for - " + Description + " - " + IsInherited).toEqual("IsInherited for - " + Description + " - false");
          expect(  "DnsSuffix for - " + Description + " - " + DnsSuffix).toEqual(  "DnsSuffix for - "   + Description + " - cloud.pearson.com");
          expect(       "Dns1 for - " + Description + " - " + Dns1).toEqual(       "Dns1 for - "        + Description + " - 10.162.33.150");
          expect(       "Dns2 for - " + Description + " - " + Dns2).toEqual(       "Dns2 for - "        + Description + " - 10.162.65.150");
          expect(  "FenceMode for - " + Description + " - " + FenceMode).toEqual(  "FenceMode for - "   + Description + " - natRouted");
          expect(   "IsShared for - " + Description + " - " + IsShared).toEqual(   "IsShared for - "    + Description + " - true");
          expect(  "IsEnabled for - " + Description + " - " + IsEnabled).toEqual(  "IsEnabled for - "   + Description + " - true");
        } else {
          errorMessage = "Failed while running - " + feNetworkhref;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + ", orgvdcExists - " + orgvdcExists + " , edgeGwExists - " + edgeGwExists + " & feNetworkhref - " + feNetworkhref + " for bubble xxx-TestBubble-01. So can't verify whether FE & BE networks are created";
        this.fail(errorMessage);
      }
    });

    it("Verify BE networks details for the bubble 'xxx-TestBubble-01'", function() {
      var errorMessage;
      if (beNetworkhref !== "") {
        response = fn.runHttpRequests("GET", headers, beNetworkhref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          var jsonResult = xmlobj.parseXML(response.body.toString());

          var Description = jsonResult.OrgVdcNetwork.Description;
          var IsInherited = jsonResult.OrgVdcNetwork.Configuration.IpScopes.IpScope.IsInherited;
          var DnsSuffix   = jsonResult.OrgVdcNetwork.Configuration.IpScopes.IpScope.DnsSuffix;
          var IsEnabled   = jsonResult.OrgVdcNetwork.Configuration.IpScopes.IpScope.IsEnabled;
          var Dns1        = jsonResult.OrgVdcNetwork.Configuration.IpScopes.IpScope.Dns1;
          var Dns2        = jsonResult.OrgVdcNetwork.Configuration.IpScopes.IpScope.Dns2;
          var FenceMode   = jsonResult.OrgVdcNetwork.Configuration.FenceMode;
          var IsShared    = jsonResult.OrgVdcNetwork.IsShared;

          expect("IsInherited for - " + Description + " - " + IsInherited).toEqual("IsInherited for - " + Description + " - false");
          expect(  "DnsSuffix for - " + Description + " - " + DnsSuffix).toEqual(  "DnsSuffix for - "   + Description + " - cloud.pearson.com");
          expect(       "Dns1 for - " + Description + " - " + Dns1).toEqual(       "Dns1 for - "        + Description + " - 10.162.33.150");
          expect(       "Dns2 for - " + Description + " - " + Dns2).toEqual(       "Dns2 for - "        + Description + " - 10.162.65.150");
          expect(  "FenceMode for - " + Description + " - " + FenceMode).toEqual(  "FenceMode for - "   + Description + " - natRouted");
          expect(   "IsShared for - " + Description + " - " + IsShared).toEqual(   "IsShared for - "    + Description + " - true");
          expect(  "IsEnabled for - " + Description + " - " + IsEnabled).toEqual(  "IsEnabled for - "   + Description + " - true");
        } else {
          errorMessage = "Failed while running - " + beNetworkhref;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + ", orgvdcExists - " + orgvdcExists + " , edgeGwExists - " + edgeGwExists + "& beNetworkhref - " + beNetworkhref + " for bubble xxx-TestBubble-01. So can't verify whether FE & BE networks are created";
        this.fail(errorMessage);
      }
    });

    it("Verify number of users for the Bubble - 'xxx-TestBubble-01'", function() {
      var errorMessage;
      var users = [];

      if (orgExists) {
        // User is created on vCD ?
        if (jsonResultOrg.AdminOrg.Users !== undefined) {
          // Store the href for each user
          for (var key in jsonResultOrg.AdminOrg.Users.UserReference) {
            if (jsonResultOrg.AdminOrg.Users.UserReference[key]["-name"] == "mystack.user1@pearson.com") {
              user1_href = jsonResultOrg.AdminOrg.Users.UserReference[key]["-href"];
            } else if (jsonResultOrg.AdminOrg.Users.UserReference[key]["-name"] == "mystack.user2@pearson.com") {
              user2_href = jsonResultOrg.AdminOrg.Users.UserReference[key]["-href"];
            } else if (jsonResultOrg.AdminOrg.Users.UserReference[key]["-name"] == "mystack.user3@pearson.com") {
              user3_href = jsonResultOrg.AdminOrg.Users.UserReference[key]["-href"];
            }
            // Append list of users to the list
            users.push(jsonResultOrg.AdminOrg.Users.UserReference[key]["-name"]);
          }
          // Verify the total number of users
          expect("Total number of bubble users - " + jsonResultOrg.AdminOrg.Users.UserReference.length).toEqual("Total number of bubble users - 3");
          expect(users).toContain("mystack.user1@pearson.com");
          expect(users).toContain("mystack.user2@pearson.com");
          expect(users).toContain("mystack.user3@pearson.com");
        } else {
          errorMessage = "User is not created on the vCD. So can't continue this test";
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "Org not created for bubble xxx-TestBubble-01. So can't verify whether whether local user are created Or not.";
        this.fail(errorMessage);
      }
    });

    it("Verify details for user mysatck.user1@pearson for bubble - 'xxx-TestBubble-01'", function() {
      var errorMessage, jsonResult;
      if (user1_href !== "") {
        response = fn.runHttpRequests("GET", headers, user1_href);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResult = xmlobj.parseXML(response.body.toString());

          // Verify the values
          expect("Fullname - "      + jsonResult.User.FullName).toEqual("Fullname - mystack user1");
          expect("Email Address - " + jsonResult.User.EmailAddress).toEqual("Email Address - mystack.user1@pearson.com");
          expect("Is Enabled - "    + jsonResult.User.IsEnabled).toEqual("Is Enabled - true");
          expect("Is Locked - "     + jsonResult.User.IsLocked).toEqual("Is Locked - false");
          expect("Is External - "   + jsonResult.User.IsExternal).toEqual("Is External - true");
        } else {
          errorMessage = "Failed while running - " + user1_href;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "User mystack.user1 is not added to bubble xxx-TestBubble-01. So can't verify the details for this bubble";
        this.fail(errorMessage);
      }
    });

    it("Verify details for user mysatck.user2@pearson for bubble - 'xxx-TestBubble-01'", function() {
      var errorMessage, jsonResult;
      if (user2_href !== "") {
        response = fn.runHttpRequests("GET", headers, user2_href);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResult = xmlobj.parseXML(response.body.toString());

          // Verify the values
          expect("Fullname - "      + jsonResult.User.FullName).toEqual("Fullname - mystack user2");
          expect("Email Address - " + jsonResult.User.EmailAddress).toEqual("Email Address - mystack.user2@pearson.com");
          expect("Is Enabled - "    + jsonResult.User.IsEnabled).toEqual("Is Enabled - true");
          expect("Is Locked - "     + jsonResult.User.IsLocked).toEqual("Is Locked - false");
          expect("Is External - "   + jsonResult.User.IsExternal).toEqual("Is External - true");
        } else {
          errorMessage = "Failed while running - " + user2_href;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "User mystack.user2 is not added to bubble xxx-TestBubble-01. So can't verify the details for this bubble";
        this.fail(errorMessage);
      }
    });

    it("Verify details for user mysatck.user3@pearson for bubble - 'xxx-TestBubble-01'", function() {
      var errorMessage, jsonResult;
      if (user3_href !== "") {
        response = fn.runHttpRequests("GET", headers, user3_href);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResult = xmlobj.parseXML(response.body.toString());

          // Verify the values
          expect("Fullname - "      + jsonResult.User.FullName).toEqual("Fullname - mystack user3");
          expect("Email Address - " + jsonResult.User.EmailAddress).toEqual("Email Address - mystack.user3@pearson.com");
          expect("Is Enabled - "    + jsonResult.User.IsEnabled).toEqual("Is Enabled - true");
          expect("Is Locked - "     + jsonResult.User.IsLocked).toEqual("Is Locked - false");
          expect("Is External - "   + jsonResult.User.IsExternal).toEqual("Is External - true");
        } else {
          errorMessage = "Failed while running - " + user3_href;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "User mystack.user2 is not added to bubble xxx-TestBubble-01. So can't verify the details for this bubble";
        this.fail(errorMessage);
      }
    });

    it("Verify Bubble's Public IP's, Edge gateway IP & networks value in Portal is matching with vCD", function() {
      var response, key, errorMessage;
      var edgeGateway = "";
      var gateway     = "";
      var networks    = [];

      fn.login("mystack.user1", "");
      homePage.bubbles.click();
      element.all(by.className("Bubble")).get(0).click();

      // Edge gateway created ?. Get the edge gateway details from the vCD
      if (edgeGwqueryhref !== "") {
        response = fn.runHttpRequests("GET", headers, edgeGwqueryhref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResult = xmlobj.parseXML(response.body.toString());

          if (jsonResult.QueryResultRecords.EdgeGatewayRecord !== undefined) {
            if (jsonResult.QueryResultRecords.EdgeGatewayRecord["-numberOfExtNetworks"] == "1") {
              edgeGwhref = jsonResult.QueryResultRecords.EdgeGatewayRecord["-href"];

              response = fn.runHttpRequests("GET", headers, edgeGwhref);
              if (response.statusCode == 200) {
                // Convert XML -> JSON
                jsonResult = xmlobj.parseXML(response.body.toString());
                for(key in jsonResult.EdgeGateway.Configuration.GatewayInterfaces.GatewayInterface) {
                  interfaceType = jsonResult.EdgeGateway.Configuration.GatewayInterfaces.GatewayInterface[key].InterfaceType;
                  if (interfaceType == "uplink") {
                    // Get the range of external IP address purchased via the myIP and Marketplace it in list
                    edgeGateway = jsonResult.EdgeGateway.Configuration.GatewayInterfaces.GatewayInterface[key].SubnetParticipation.IpAddress;
                    gateway     = jsonResult.EdgeGateway.Configuration.GatewayInterfaces.GatewayInterface[key].SubnetParticipation.Gateway;

                    var IpRanges  = jsonResult.EdgeGateway.Configuration.GatewayInterfaces.GatewayInterface[key].SubnetParticipation.IpRanges;
                    // if (IpRanges.IpRange.length !== undefined) {
                    //   IpRanges = IpRanges.IpRange;
                    // }
                    // for(var key2 in IpRanges) {
                    //   StartAddress = ip.toLong(IpRanges[key2].StartAddress);
                    //   EndAddress   = ip.toLong(IpRanges[key2].EndAddress);
                    //   // Start address and End address is same ?. Then just add the start address to the list
                    //   if (StartAddress == EndAddress) {
                    //     publicIp.push(ip.fromLong(StartAddress));
                    //   } else {
                    //     // Start address and End address is not same ?. Then add Start address and all the IP's in between them to the list
                    //     // Example: 10.0.0.2 - 10.0.0.4 then list should be - 10.0.0.2, 10.0.0.3, 10.0.0.4
                    //     publicIp.push(ip.fromLong(StartAddress));
                    //     totalIP = EndAddress - StartAddress;
                    //     for(var i=0; i<totalIP; i++) {
                    //       publicIp.push(ip.fromLong(StartAddress + i + 1));
                    //     }
                    //   }
                    // }
                  }
                }
                // Verify Public IP, Gateway IP & Edge Gatway IP displayed on the UI is matching with the vCD
                // expect(element(by.className("BubbleView-networking")).all(by.className("BubbleView-ip")).count()).toEqual(publicIp.length);
                // expect(publicIp).toContain(element(by.className("BubbleView-networking")).all(by.className("BubbleView-ip")).get(0).getText());
                expect(IpRanges).toBeUndefined();
                expect(bubblesPage.BubbleViewip).toEqual([]);
                expect(bubblesPage.BubbleViewGateway.getText()).toEqual(gateway);
                expect(bubblesPage.BubbleViewEdgeGateway.getText()).toEqual(edgeGateway);
              } else {
                errorMessage = "Failed while running - " + edgeGwhref;
                this.fail(errorMessage);
              }
            }
          }
        } else {
          errorMessage = "Failed while running - " + edgeGwqueryhref;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + " , orgvdcExists - " + orgvdcExists + "& edgeGwqueryhref - " + edgeGwqueryhref + " for bubble xxx-TestBubble-01. So can't verify whether edge gate is created Or not";
        this.fail(errorMessage);
      }

      // Networks created ?. Get network details from the vCD
      if (networkshref !== "") {
        response = fn.runHttpRequests("GET", headers, networkshref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResult = xmlobj.parseXML(response.body.toString());

          // No record for 'QueryResultRecords.OrgVdcNetworkRecord' ?, then no network exists.
          if (jsonResult.QueryResultRecords.OrgVdcNetworkRecord !== undefined) {
            for(key in jsonResult.QueryResultRecords.OrgVdcNetworkRecord) {
              // Parse network name from the XML reponse and convert it in the format - 'BE 10.5.65.97/27'
              networks.push(jsonResult.QueryResultRecords.OrgVdcNetworkRecord[key]["-name"].replace("-", " ").replace("-", "/"));
            }
          }
          expect(networks.length).toEqual(bubblesPage.BubbleViewNetwork.count());
          expect(networks).toContain(bubblesPage.BubbleViewNetwork.get(0).getText());
          expect(networks).toContain(bubblesPage.BubbleViewNetwork.get(1).getText());
        } else {
          errorMessage = "Failed while running - " + networkshref;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + ", orgvdcExists - " + orgvdcExists + " & edgeGwExists - " + edgeGwExists + " for bubble xxx-TestBubble-01. So can't verify whether FE & BE networks are created";
        this.fail(errorMessage);
      }
    });

    describe('AddLocalUserToBubble', function() {

      var jsonResultOrg;
      var localuser1href = "";

      it("Verify user with CREATOR role of a team able to add local user to the bubble", function() {
        fn.login("mystack.user1", "");

        fn.clearNotifications();

        homePage.bubbles.click();
        // element.all(by.className("Bubble")).get(0).click();
        browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-01')), waitSeconds);
        bubblesPage.bubble('xxx-TestBubble-01').click();
        expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();

        // Add local user - 'mystack.local1'
        browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewAddLocalUser), waitSeconds);
        bubblesPage.BubbleViewAddLocalUser.click();
        bubblesPage.BubbleViewLocalUserName.sendKeys('mystack.local1');
        bubblesPage.BubbleViewLocalUserPassword.sendKeys('Password1!');
        bubblesPage.BubbleViewLocalUserAddButton.click();

        // Add local user - 'mystack.local2'
        bubblesPage.BubbleViewLocalUserName.sendKeys('mystack.local2');
        bubblesPage.BubbleViewLocalUserPassword.sendKeys('Password1!');
        bubblesPage.BubbleViewLocalUserAddButton.click();

        // Add local user - 'mystack.local2'
        bubblesPage.BubbleViewLocalUserName.sendKeys('mystack.local3');
        bubblesPage.BubbleViewLocalUserPassword.sendKeys('Password1!');
        bubblesPage.BubbleViewLocalUserAddButton.click();

        // Verify name of the user on the UI
        expect(bubblesPage.BubbleViewLocalUserByindex(0).getText()).toEqual("mystack.local1");
        expect(bubblesPage.BubbleViewLocalUserByindex(1).getText()).toEqual("mystack.local2");
        expect(bubblesPage.BubbleViewLocalUserByindex(2).getText()).toEqual("mystack.local3");
      });

      it("Verify user gets notifications while adding local user(s) to the bubble - 'xxx-TestBubble-01'", function() {
        fn.waitForNotifications(20000, 6);

        // Verify user gets notification while adding local user to team
        expect(notificationsPage.notificationCount.getText()).toEqual('6');
        homePage.notifications.click();
        // Verify total number of 'NotificationGroup' and click on first one
        expect(notificationsPage.notificationHeader.count()).toEqual(3);

        // Verify notifcations while adding user 'mystack.local1' to team 'xxx-TestTeam-01'
        notificationsPage.notificationHeader.get(0).click();
        expect(notificationsPage.notificationHeader.get(0).getText()).toEqual("Add User");
        expect(notificationsPage.NotificationsByindex(0).count()).toEqual(2);
        expect(notificationsPage.NotificationsByindex(0).get(0).getText()).toEqual("Adding user to xxx-TestBubble-01. Please wait");
        expect(notificationsPage.NotificationsByindex(0).get(1).getText()).toContain("has been added as a user to xxx-TestBubble-01");

        // Verify notifcations while adding user 'mystack.local2' to team 'xxx-TestTeam-01'
        notificationsPage.notificationHeader.get(1).click();
        expect(notificationsPage.notificationHeader.get(1).getText()).toEqual("Add User");
        expect(element.all(by.className("NotificationGroup")).get(1).all(by.css(".Notifications em")).count()).toEqual(2);
        expect(element.all(by.className("NotificationGroup")).get(1).all(by.css(".Notifications em")).get(0).getText()).toEqual("Adding user to xxx-TestBubble-01. Please wait");
        expect(element.all(by.className("NotificationGroup")).get(1).all(by.css(".Notifications em")).get(1).getText()).toContain("has been added as a user to xxx-TestBubble-01");

        // Verify notifcations while adding user 'mystack.local3' to team 'xxx-TestTeam-01'
        element.all(by.className("NotificationGroup")).get(2).click();
        expect(notificationsPage.notificationHeader.get(2).getText()).toEqual("Add User");
        expect(element.all(by.className("NotificationGroup")).get(2).all(by.css(".Notifications em")).count()).toEqual(2);
        expect(element.all(by.className("NotificationGroup")).get(2).all(by.css(".Notifications em")).get(0).getText()).toEqual("Adding user to xxx-TestBubble-01. Please wait");
        expect(element.all(by.className("NotificationGroup")).get(2).all(by.css(".Notifications em")).get(1).getText()).toContain("has been added as a user to xxx-TestBubble-01");
      });

      it("Verify whether local user added on the vcd -  'xxx-TestBubble-01'", function() {
        var errorMessage;
        var users = [];

        if (orgExists) {
          response = fn.runHttpRequests("GET", headers, orghref);
          if (response.statusCode == 200) {
            // Convert XML -> JSON
            jsonResultOrg = xmlobj.parseXML(response.body.toString());
            // User is created on vCD ?
            if (jsonResultOrg.AdminOrg.Users !== undefined) {
              // Store the href for each user
              for (var key in jsonResultOrg.AdminOrg.Users.UserReference) {
                if (jsonResultOrg.AdminOrg.Users.UserReference[key]["-name"] == "mystack.local1") {
                  localuser1href = jsonResultOrg.AdminOrg.Users.UserReference[key]["-href"];
                }

                // Append list of users to the list
                users.push(jsonResultOrg.AdminOrg.Users.UserReference[key]["-name"]);
              }
              // Verify total number of user in the vCD - Make sure LDAP are not deleted. Total users = LDAP users + local users
              expect("Total number of bubble users - " + jsonResultOrg.AdminOrg.Users.UserReference.length).toEqual("Total number of bubble users - 6");
              expect(users).toContain("mystack.user1@pearson.com");
              expect(users).toContain("mystack.user2@pearson.com");
              expect(users).toContain("mystack.user3@pearson.com");
              expect(users).toContain("mystack.local1");
              expect(users).toContain("mystack.local2");
              expect(users).toContain("mystack.local3");
            } else {
              errorMessage = "User is not created on the vCD. So can't continue this test";
              this.fail(errorMessage);
            }
          // orgUrl ran okey?
          } else {
            errorMessage = "Failed to run the URL - " + orghref + ". statusCode - " + response.statusCode + ". So can't verify whether whether local user are created Or not.";
            this.fail(errorMessage);
          }
        } else {
          errorMessage = "Org not created for bubble xxx-TestBubble-01. So can't verify whether whether local user are created Or not.";
          this.fail(errorMessage);
        }
      });

      it("Verify details for user mysatck.local1 for bubble - 'xxx-TestBubble-01'", function() {
        var errorMessage, jsonResult;
        if (localuser1href !== "") {
          response = fn.runHttpRequests("GET", headers, localuser1href);
          if (response.statusCode == 200) {
            // Convert XML -> JSON
            jsonResult = xmlobj.parseXML(response.body.toString());

            // Verify the values
            expect("Fullname - "      + jsonResult.User.FullName).toEqual("Fullname - mystack.local1");
            expect("Is Enabled - "    + jsonResult.User.IsEnabled).toEqual("Is Enabled - true");
            expect("Is Locked - "     + jsonResult.User.IsLocked).toEqual("Is Locked - false");
            expect("Is External - "   + jsonResult.User.IsExternal).toEqual("Is External - false");
          } else {
            errorMessage = "Failed while running - " + localuser1href;
            this.fail(errorMessage);
          }
        } else {
          errorMessage = "User mystack.user2 is not added to bubble xxx-TestBubble-01. So can't verify the details for this bubble";
          this.fail(errorMessage);
        }
      });

      it("Verify user with CREATOR role of a team able to delete a local user", function() {
        fn.clearNotifications();

        homePage.bubbles.click();
        // element.all(by.className("Bubble")).get(0).click();
        browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-01')), waitSeconds);
        bubblesPage.bubble('xxx-TestBubble-01').click();
        expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
        browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);

        // Verify total number of local users shown on the UI
        expect(bubblesPage.BubbleViewLocalUser.count()).toEqual(3);

        browser.actions().mouseUp(bubblesPage.BubbleViewLocalUserByindex(1)).perform();
        bubblesPage.BubbleViewLocalUserDeleteByindex(1).click();

        // Verify other users still exists on the portal UI
        browser.sleep(1000);
        expect(bubblesPage.BubbleViewLocalUser.count()).toEqual(2);

        expect(bubblesPage.BubbleViewLocalUserByindex(0).getText()).toEqual("mystack.local1");
        expect(bubblesPage.BubbleViewLocalUserByindex(1).getText()).toEqual("mystack.local3");
      });

      it("Verify user gets notifications while deleting local user from the bubble - 'xxx-TestBubble-01'", function() {
        fn.waitForNotifications(20000, 2);

        // Verify user gets notification while adding local user to team
        expect(notificationsPage.notificationCount.getText()).toEqual('2');
        homePage.notifications.click();
        // Verify total number of 'NotificationGroup' and click on first one
        expect(notificationsPage.notificationHeader.count()).toEqual(1);

        // Verify notifcations while adding user 'mystack.user5' to team 'xxx-TestTeam-01'
        notificationsPage.notificationHeader.get(0).click();
        expect(notificationsPage.notificationHeader.get(0).getText()).toEqual("Remove User");
        expect(notificationsPage.NotificationsByindex(0).count()).toEqual(2);
        expect(notificationsPage.NotificationsByindex(0).get(0).getText()).toEqual("Removing user from xxx-TestBubble-01. Please wait");
        expect(notificationsPage.NotificationsByindex(0).get(1).getText()).toEqual("mystack.local2 has been removed from xxx-TestBubble-01");
      });

      it("Verify whether local users are deleted on the vCD - 'xxx-TestBubble-01'", function() {
        var users = [];
        var errorMessage;

        if (orgExists) {
          response = fn.runHttpRequests("GET", headers, orghref);
          if (response.statusCode == 200) {
            // Convert XML -> JSON
            jsonResultOrg = xmlobj.parseXML(response.body.toString());
            // User is created on vCD ?
            if (jsonResultOrg.AdminOrg.Users !== undefined) {
              // Store the href for each user
              for (var key in jsonResultOrg.AdminOrg.Users.UserReference) {
                // Append list of users to the list
                users.push(jsonResultOrg.AdminOrg.Users.UserReference[key]["-name"]);
              }
              expect("Total number of bubble users - " + jsonResultOrg.AdminOrg.Users.UserReference.length).toEqual("Total number of bubble users - 5");
              expect(users).toContain("mystack.user1@pearson.com");
              expect(users).toContain("mystack.user2@pearson.com");
              expect(users).toContain("mystack.user3@pearson.com");
              expect(users).toContain("mystack.local1");
              expect(users).not.toContain("mystack.local2");
              expect(users).toContain("mystack.local3");
            } else {
              errorMessage = "User is not created on the vCD. So can't continue this test";
              this.fail(errorMessage);
            }
          // orgUrl ran okey?
          } else {
            errorMessage = "Failed to run the URL - " + orghref + ". statusCode - " + response.statusCode + ". So can't verify whether whether local user is deleted Or not.";
            this.fail(errorMessage);
          }
        } else {
          errorMessage = "Org not created for bubble xxx-TestBubble-01. So can't verify whether whether local user are deleted Or not.";
          this.fail(errorMessage);
        }
      });

    });

    describe("AddingUserToTeam", function() {

      it("Verify user with CREATOR role able to add user to the team - 'xxx-TestTeam-01'", function() {
        fn.login("mystack.user1", "");

        fn.clearNotifications();

        homePage.teams.click();
        browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-01')), waitSeconds);
        teamsPage.team('xxx-TestTeam-01').click();
        browser.wait(EC.elementToBeClickable(teamsPage.teamViewAddMember), waitSeconds);
        teamsPage.teamViewAddMember.click();
        teamsPage.TeamInviteMemberRole('Creator').click();
        teamsPage.teamVieweMail.sendKeys('mystack.user4@pearson.com');
        teamsPage.teamViewAddButton.click();

        browser.wait(EC.elementToBeClickable(teamsPage.teamViewAddMember), waitSeconds);
        teamsPage.teamViewAddMember.click();
        teamsPage.TeamInviteMemberRole('Creator').click();
        teamsPage.teamVieweMail.sendKeys('mystack.user5@pearson.com');
        teamsPage.teamViewAddButton.click();

        browser.sleep(1000);
        expect(teamsPage.teamViewMembers.count()).toEqual(5);

        expect(teamsPage.TeamViewMemberNameByindex(0).getText()).toEqual('mystack user1');
        expect(teamsPage.TeamViewMemberNameByindex(1).getText()).toEqual('mystack user2');
        expect(teamsPage.TeamViewMemberNameByindex(2).getText()).toEqual('mystack user3');
        expect(teamsPage.TeamViewMemberNameByindex(3).getText()).toEqual('mystack user4');
        expect(teamsPage.TeamViewMemberNameByindex(4).getText()).toEqual('mystack user5');
      });

      it("Verify user gets notifications while adding user to the team - 'xxx-TestTeam-01'", function() {
        fn.waitForNotifications(20000, 4);

        expect(notificationsPage.notificationCount.getText()).toEqual('4');
        homePage.notifications.click();
        // Verify total number of 'NotificationGroup' and click on first one
        expect(notificationsPage.notificationHeader.count()).toEqual(2);

        // Verify notifcations while adding user 'mystack.user5' to team 'xxx-TestTeam-01'
        notificationsPage.notificationHeader.get(0).click();
        expect(notificationsPage.notificationHeader.get(0).getText()).toEqual("Add User");
        expect(notificationsPage.NotificationsByindex(0).count()).toEqual(2);
        expect(notificationsPage.NotificationsByindex(0).get(0).getText()).toEqual("Adding user to xxx-TestBubble-01. Please wait");
        expect(notificationsPage.NotificationsByindex(0).get(1).getText()).toContain(" has been added as a user to xxx-TestBubble-01");

        // Verify notifcations while adding user 'mystack.user4' to team 'xxx-TestTeam-01'
        notificationsPage.notificationHeader.get(1).click();
        expect(notificationsPage.notificationHeader.get(1).getText()).toEqual("Add User");
        expect(element.all(by.className("NotificationGroup")).get(1).all(by.css(".Notifications em")).count()).toEqual(2);
        expect(element.all(by.className("NotificationGroup")).get(1).all(by.css(".Notifications em")).get(0).getText()).toEqual("Adding user to xxx-TestBubble-01. Please wait");
        expect(element.all(by.className("NotificationGroup")).get(1).all(by.css(".Notifications em")).get(1).getText()).toContain(" has been added as a user to xxx-TestBubble-01");
      });

      it("Verify user able to expand and collapse notifcations", function() {
        fn.waitForNotifications(10000, 1);

        homePage.notifications.click();
        // Verify total number of 'NotificationGroup' and click on first one
        expect(notificationsPage.notificationHeader.count()).toEqual(2);

        expect(notificationsPage.NotificationsByindex(0).get(0).isDisplayed()).toBeFalsy();
        expect(element.all(by.className("NotificationGroup")).get(1).all(by.css(".Notifications em")).get(0).isDisplayed()).toBeFalsy();

        // Click on 1st notification group and verify it expands
        notificationsPage.notificationHeader.get(0).click();
        expect(notificationsPage.NotificationsByindex(0).get(0).isDisplayed()).toBeTruthy();
        expect(element.all(by.className("NotificationGroup")).get(1).all(by.css(".Notifications em")).get(0).isDisplayed()).toBeFalsy();

        // Click on 2nd notification group and verify it expands
        notificationsPage.notificationHeader.get(1).click();
        expect(notificationsPage.NotificationsByindex(0).get(0).isDisplayed()).toBeTruthy();
        expect(element.all(by.className("NotificationGroup")).get(1).all(by.css(".Notifications em")).get(0).isDisplayed()).toBeTruthy();

        // Click on 1st notification group and verify it collapses
        notificationsPage.notificationHeader.get(0).click();
        expect(notificationsPage.NotificationsByindex(0).get(0).isDisplayed()).toBeFalsy();
        expect(element.all(by.className("NotificationGroup")).get(1).all(by.css(".Notifications em")).get(0).isDisplayed()).toBeTruthy();

        // Click on 2nd notification group and verify it expands
        notificationsPage.notificationHeader.get(1).click();
        expect(notificationsPage.NotificationsByindex(0).get(0).isDisplayed()).toBeFalsy();
        expect(element.all(by.className("NotificationGroup")).get(1).all(by.css(".Notifications em")).get(0).isDisplayed()).toBeFalsy();
      });

      it("Verify newly added user to the team is reflected on the vCD - 'xxx-TestBubble-01'", function() {
        var errorMessage;
        var users = [];

        if (orgExists) {
          response = fn.runHttpRequests("GET", headers, orghref);
          if (response.statusCode == 200) {
            // Convert XML -> JSON
            jsonResultOrg = xmlobj.parseXML(response.body.toString());
            if (jsonResultOrg.AdminOrg.Users !== undefined) {
              for (var key in jsonResultOrg.AdminOrg.Users.UserReference) {
                // Append list of users to the list
                users.push(jsonResultOrg.AdminOrg.Users.UserReference[key]["-name"]);
              }
              expect("Total number of bubble users - " + jsonResultOrg.AdminOrg.Users.UserReference.length).toEqual("Total number of bubble users - 7");
              expect(users).toContain("mystack.user1@pearson.com");
              expect(users).toContain("mystack.user2@pearson.com");
              expect(users).toContain("mystack.user3@pearson.com");
              expect(users).toContain("mystack.user4@pearson.com");
              expect(users).toContain("mystack.user5@pearson.com");
              expect(users).toContain("mystack.local1");
              expect(users).toContain("mystack.local3");
            } else {
              errorMessage = "User is not created on the vCD. So can't continue this test";
              this.fail(errorMessage);
            }
          // orgUrl ran okey?
          } else {
            errorMessage = "Failed to run the URL - " + orghref + ". statusCode - " + response.statusCode + ". So can't verify whether whether user is created Or not.";
            this.fail(errorMessage);
          }
        } else {
          errorMessage = "Org not created for bubble xxx-TestBubble-01. So can't verify whether whether user is created Or not";
          this.fail(errorMessage);
        }
      });

      it("Verify user with CREATOR role able to delete a user from the team - 'xxx-TestTeam-01'", function() {
        fn.clearNotifications();

        homePage.teams.click();
        browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-01')), waitSeconds);
        teamsPage.team('xxx-TestTeam-01').click();
        browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);

        // Verify Creator role able to delete user from the team
        browser.actions().mouseUp(teamsPage.teamViewMembers.get(3)).perform();
        teamsPage.TeamViewMemberDeleteByindex(3).click();
        browser.sleep(1000);
        expect(teamsPage.teamViewMembers.count()).toEqual(4);
      });

      it("Verify user gets notifications while deleting user from the team - 'xxx-TestTeam-01'", function() {
        fn.waitForNotifications(20000, 2);

        expect(notificationsPage.notificationCount.getText()).toEqual('2');
        homePage.notifications.click();
        // Verify total number of 'NotificationGroup' and click on first one
        expect(notificationsPage.notificationHeader.count()).toEqual(1);

        // Verify notifcations while adding user 'mystack.user5' to team 'xxx-TestTeam-01'
        notificationsPage.notificationHeader.get(0).click();
        expect(notificationsPage.notificationHeader.get(0).getText()).toEqual("Remove User");
        expect(notificationsPage.NotificationsByindex(0).count()).toEqual(2);
        expect(notificationsPage.NotificationsByindex(0).get(0).getText()).toEqual("Removing user from xxx-TestBubble-01. Please wait");
        expect(notificationsPage.NotificationsByindex(0).get(1).getText()).toEqual("mystack.user4@pearson.com has been removed from xxx-TestBubble-01");
      });

      it("Verify recently deleted user from the team reflects on the vCD - 'xxx-TestBubble-01'", function() {
        var errorMessage;
        var users = [];

        if (orgExists) {
          response = fn.runHttpRequests("GET", headers, orghref);
          if (response.statusCode == 200) {
            // Convert XML -> JSON
            jsonResultOrg = xmlobj.parseXML(response.body.toString());
            if (jsonResultOrg.AdminOrg.Users !== undefined) {
              for (var key in jsonResultOrg.AdminOrg.Users.UserReference) {
                // Append list of users to the list
                users.push(jsonResultOrg.AdminOrg.Users.UserReference[key]["-name"]);
              }
              expect("Total number of bubble users - " + jsonResultOrg.AdminOrg.Users.UserReference.length).toEqual("Total number of bubble users - 6");
              expect(users).toContain("mystack.user1@pearson.com");
              expect(users).toContain("mystack.user2@pearson.com");
              expect(users).toContain("mystack.user3@pearson.com");
              expect(users).toContain("mystack.user5@pearson.com");
              expect(users).toContain("mystack.local1");
              expect(users).toContain("mystack.local3");
              expect(users).not.toContain("mystack.user4@pearson.com");
            } else {
              errorMessage = "User is not created on the vCD. So can't continue this test";
              this.fail(errorMessage);
            }
          // orgUrl ran okey?
          } else {
            errorMessage = "Failed to run the URL - " + orghref + ". statusCode - " + response.statusCode + ". So can't verify whether whether user is deleted Or not";
            this.fail(errorMessage);
          }
        } else {
          errorMessage = "Org not created for bubble xxx-TestBubble-01. So can't verify whether Org vdc is created Or not";
          this.fail(errorMessage);
        }
      });

      it("Verify user (mystack.user4) is not removed/deleted from Portal while deleting a user from the team which got bubble - 'xxx-TestTeam-01'", function() {
        fn.login("mystack.root", "");

        homePage.users.click();
        browser.wait(EC.elementToBeClickable(usersPage.searchUser), waitSeconds);
        usersPage.searchUser.click();
        usersPage.searchUser.sendKeys('mystack.user4');

        expect(usersPage.users.count()).toEqual(1);
        expect(usersPage.user('mystack user4').isPresent()).toBeTruthy();
      });

      // xit("Verify target user (mystack.user4) gets notification while adding/removing the user from team which got bubble - 'xxx-TestBubble-01'", function() {
      //   fn.login("mystack.user4", "");

      //   fn.waitForNotifications(5000, 1);

      //   expect(notificationsPage.notificationCount.getText()).toEqual('4');
      //   homePage.notifications.click();
      //   // Verify total number of 'NotificationGroup' and click on first one
      //   expect(notificationsPage.notificationHeader.count()).toEqual(2);

      //   // Verify notifcations while adding user 'mystack.user5' to team 'xxx-TestTeam-01'
      //   notificationsPage.notificationHeader.get(0).click();
      //   expect(notificationsPage.notificationHeader.get(0).getText()).toEqual("Add User");
      //   expect(notificationsPage.NotificationsByindex(0).count()).toEqual(2);
      //   expect(notificationsPage.NotificationsByindex(0).get(0).getText()).toEqual("Adding user to xxx-TestBubble-01. Please wait");
      //   expect(notificationsPage.NotificationsByindex(0).get(1).getText()).toContain("mystack.user4@pearson.com has been added as a user to xxx-TestBubble-01");

      //   // Verify notifcations while adding user 'mystack.user5' to team 'xxx-TestTeam-01'
      //   notificationsPage.notificationHeader.get(1).click();
      //   expect(notificationsPage.notificationHeader.get(1).getText()).toEqual("Remove User");
      //   expect(element.all(by.className("NotificationGroup")).get(1).all(by.css(".Notifications em")).count()).toEqual(2);
      //   expect(element.all(by.className("NotificationGroup")).get(1).all(by.css(".Notifications em")).get(0).getText()).toEqual("Removing user from xxx-TestBubble-01. Please wait");
      //   expect(element.all(by.className("NotificationGroup")).get(1).all(by.css(".Notifications em")).get(1).getText()).toEqual("mystack.user4@pearson.com has been removed from xxx-TestBubble-01");
      // });

      // xit("Verify target user (mystack.user5) gets notification while adding/removing the user from team which got bubble - 'xxx-TestBubble-01'", function() {
      //   fn.login("mystack.user5", "");

      //   fn.waitForNotifications(5000, 1);

      //   expect(notificationsPage.notificationCount.getText()).toEqual('2');
      //   homePage.notifications.click();
      //   // Verify total number of 'NotificationGroup' and click on first one
      //   expect(notificationsPage.notificationHeader.count()).toEqual(1);

      //   // Verify notifcations while adding user 'mystack.user5' to team 'xxx-TestTeam-01'
      //   notificationsPage.notificationHeader.get(0).click();
      //   expect(notificationsPage.notificationHeader.get(0).getText()).toEqual("Add User");
      //   expect(notificationsPage.NotificationsByindex(0).count()).toEqual(2);
      //   expect(notificationsPage.NotificationsByindex(0).get(0).getText()).toEqual("Adding user to xxx-TestBubble-01. Please wait");
      //   expect(notificationsPage.NotificationsByindex(0).get(1).getText()).toContain("mYsTaCk.uSeR5@pEaRsOn.cOm has been added as a user to xxx-TestBubble-01");
      // });

    });

  });
});

describe("BubbleRefresh", function() {

  // Default values
  var headers                   = {"Accept": "application/*+xml;version=5.1", "Authorization" : authorization};
  var jsonResult                = "";
  var orgExists                 = false;
  var orghref                   = "";
  var orgvdcExists              = false;
  var orgvdchref                = "";

  beforeAll(function() {
    var key;
    // Check Org 'xxx-BrokenBubbleDoNotDelete' exists and set the FLAG
    response = fn.runHttpRequests("POST", headers, vcd + '/api/sessions');
    if (response.statusCode == 200) {
      headers = {"Accept": "application/*+xml;version=5.1", "x-vcloud-authorization": response.headers['x-vcloud-authorization']};

      // Run the Query and get list of Orgs
      response = fn.runHttpRequests("GET", headers, vcd + '/api/admin');
      jsonResult = xmlobj.parseXML(response.body.toString());
      for(key in jsonResult.VCloud.OrganizationReferences.OrganizationReference) {
        if (jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-name"] == "xxx-BrokenBubbleDoNotDelete") {
          orgExists = true;
          orghref = jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-href"];
          break;
        }
      }
      expect("Org 'xxx-BrokenBubbleDoNotDelete' exits - true").toEqual("Org 'xxx-BrokenBubbleDoNotDelete' exits - " + orgExists);
    } else {
      this.fail("Failed to get the auth key. statusCode - ", response.statusCode);
    }

    // Does Org exits ?. Now check whether Org vDC exists and set the FLAG
    if (orgExists) {
      response = fn.runHttpRequests("GET", headers, orghref);
      if (response.statusCode == 200) {
        // Convert XML -> JSON
        jsonResultOrg = xmlobj.parseXML(response.body.toString());

        // Loop through each Org and check Org vDC created - Set the FLAG
        if (jsonResultOrg.AdminOrg.Vdcs !== undefined) {
          if (jsonResultOrg.AdminOrg.Vdcs.Vdc["-type"] == "application/vnd.vmware.admin.vdc+xml") {
            orgvdcExists  = true;
            orgvdchref    = jsonResultOrg.AdminOrg.Vdcs.Vdc["-href"];
          }
        }
      // orgUrl ran okey?
      } else {
        errorMessage = "Failed to run the URL - " + orghref + ". statusCode - " + response.statusCode + ". So can't verify whether Org vdc is created Or not";
        this.fail(errorMessage);
      }
    } else {
      errorMessage = "Org not created for bubble xxx-BrokenBubbleDoNotDelete. So can't verify whether Org vdc is created Or not";
      this.fail(errorMessage);
    }

    // Org vDC exists ?. Check whether edge gateway exits and set the FLAG
    if (orgvdcExists) {
      response = fn.runHttpRequests("GET", headers, orgvdchref);
      if (response.statusCode == 200) {
        // Convert XML -> JSON
        jsonResult = xmlobj.parseXML(response.body.toString());

        // Loop through xml reponse and get the href for vApp 'vAppWithOneVm'
        for(key in jsonResult.AdminVdc.ResourceEntities.ResourceEntity) {
          if (jsonResult.AdminVdc.ResourceEntities.ResourceEntity[key]["-type"] == "application/vnd.vmware.vcloud.vApp+xml" && jsonResult.AdminVdc.ResourceEntities.ResourceEntity[key]["-name"] == "vAppWithOneVM") {
            vAppWithOneVM_Exists = true;
            vAppWithOneVM_href   = jsonResult.AdminVdc.ResourceEntities.ResourceEntity[key]["-href"];
            break;
          }
        }

        // Loop through xml reponse and get the href for vApp 'vAppWithMultipleVm'
        for(key in jsonResult.AdminVdc.ResourceEntities.ResourceEntity) {
          if (jsonResult.AdminVdc.ResourceEntities.ResourceEntity[key]["-type"] == "application/vnd.vmware.vcloud.vApp+xml" && jsonResult.AdminVdc.ResourceEntities.ResourceEntity[key]["-name"] == "vAppWithMultipleVM") {
            vAppWithMultipleVM_Exists = true;
            vAppWithMultipleVM_href   = jsonResult.AdminVdc.ResourceEntities.ResourceEntity[key]["-href"];
            break;
          }
        }

        // Loop through xml reponse and get the href for vApp 'vAppWithNoIp'
        for(key in jsonResult.AdminVdc.ResourceEntities.ResourceEntity) {
          if (jsonResult.AdminVdc.ResourceEntities.ResourceEntity[key]["-type"] == "application/vnd.vmware.vcloud.vApp+xml" && jsonResult.AdminVdc.ResourceEntities.ResourceEntity[key]["-name"] == "vAppWithNoIp") {
            vAppWithNoIp_Exists = true;
            vAppWithNoIp_href   = jsonResult.AdminVdc.ResourceEntities.ResourceEntity[key]["-href"];
            break;
          }
        }

        // Loop through xml reponse and get the href for vApp 'vmWithMultipleNetworks'
        for(key in jsonResult.AdminVdc.ResourceEntities.ResourceEntity) {
          if (jsonResult.AdminVdc.ResourceEntities.ResourceEntity[key]["-type"] == "application/vnd.vmware.vcloud.vApp+xml" && jsonResult.AdminVdc.ResourceEntities.ResourceEntity[key]["-name"] == "vmWithMultipleNetworks") {
            vmWithMultipleNetworks_Exists = true;
            vmWithMultipleNetworks_href   = jsonResult.AdminVdc.ResourceEntities.ResourceEntity[key]["-href"];
            break;
          }
        }
      } else {
        errorMessage = "Failed while running - " + orgvdchref;
        this.fail(errorMessage);
      }
    } else {
      errorMessage = "orgExists - " + orgExists + " & orgvdcExists - " + orgvdcExists + " for bubble xxx-BrokenBubbleDoNotDelete. So can't verify whether org is enabled Or not";
      this.fail(errorMessage);
    }

    // Login as 'mystack.root' and clear all the notifications
    fn.login("mystack.root", "");
    fn.clearNotifications();
  });

  beforeEach(function() {
    browser.get('/');
  });

  it("Refresh the bubbles page to get latest details from the vCD - 'xxx-BrokenBubbleDoNotDelete'", function() {
    homePage.bubbles.click();
    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-BrokenBubbleDoNotDelete')), waitSeconds);
    bubblesPage.bubble('xxx-BrokenBubbleDoNotDelete').click();
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();

    // Wait for element to be available on the UI and click the 'refresh' button
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewRefreshVms), waitSeconds);
    bubblesPage.BubbleViewRefreshVms.click();
    expect(bubblesPage.BubbleViewRefreshVms.getText()).toEqual("refreshing");
    bubblesPage.BubbleViewClose.click();

    fn.waitForNotifications(20000, 2);
    // Verify the notification for the bubble refresh
    expect(notificationsPage.notificationCount.getText()).toEqual('2');
    homePage.notifications.click();
    // Verify total number of 'NotificationGroup' and click on first one
    expect(notificationsPage.notificationHeader.count()).toEqual(1);
    notificationsPage.notificationHeader.get(0).click();
    expect(notificationsPage.notificationHeader.get(0).getText()).toEqual("VM Refresh");
    expect(notificationsPage.NotificationsByindex(0).count()).toEqual(2);
    expect(notificationsPage.NotificationsByindex(0).get(0).getText()).toEqual("Refreshing VMs in xxx-BrokenBubbleDoNotDelete. Please wait");
    expect(notificationsPage.NotificationsByindex(0).get(1).getText()).toContain("VMs within xxx-BrokenBubbleDoNotDelete refreshed successfully");
  });

  it("Verify details are correct for vApp which has mulitple VM's - 'vAppWithMultipleVM'", function() {
    var vmIpAddress_1 = [];
    var vmIpAddress_2 = [];
    var vmCpu_1       = "";
    var vmMemory_1    = "";
    var vmCpu_2       = "";
    var vmMemory_2    = "";
    var vmName_1      = "";
    var vmName_2      = "";
    var description   = "";
    var response, key, vmKey, errorMessage;

    if (vAppWithMultipleVM_Exists) {
      response = fn.runHttpRequests("GET", headers, vAppWithMultipleVM_href);
      if (response.statusCode == 200) {
        // Convert XML -> JSON
        jsonResult = xmlobj.parseXML(response.body.toString());

        // Got 2 VM's ?
        if (jsonResult.VApp.Children.Vm.length == 2) {
          // Parse VM's CPU and Memory from the XML reponse - For VM1
          for (vmKey in jsonResult.VApp.Children.Vm) {
            // Is 'vAppWithMultipleVM-1' on the response ?.
            if (jsonResult.VApp.Children.Vm[vmKey]["-name"] == "vAppWithMultipleVM-1") {
              // Parse VM's CPU and Memory from the XML reponse - For VM2
              for(key in jsonResult.VApp.Children.Vm[vmKey]["ovf:VirtualHardwareSection"]["ovf:Item"]) {
                description = jsonResult.VApp.Children.Vm[vmKey]["ovf:VirtualHardwareSection"]["ovf:Item"][key]["rasd:Description"];
                if (description == "Number of Virtual CPUs") {
                  // Parse number of vurtual CPU value from XML reponse and convert it in the format - '1VCPUs'
                  vmCpu_1 = jsonResult.VApp.Children.Vm[vmKey]["ovf:VirtualHardwareSection"]["ovf:Item"][key]["rasd:ElementName"].replace("virtual CPU(s)", "VCPUs");
                } else if (description == "Memory Size") {
                  // Parse VM memory from XML reponse and convert it in the format - '1024Mb'
                  vmMemory_1 = jsonResult.VApp.Children.Vm[vmKey]["ovf:VirtualHardwareSection"]["ovf:Item"][key]["rasd:ElementName"].replace(" MB of memory", "");
                }
              }

              // Parse the VM's ip address
              // VM with one network ?
              if (jsonResult.VApp.Children.Vm[vmKey].NetworkConnectionSection.NetworkConnection.IpAddress !== undefined) {
                vmIpAddress_1.push(jsonResult.VApp.Children.Vm[vmKey].NetworkConnectionSection.NetworkConnection.IpAddress);
              } else {
                //  VM with Multiple network ?
                for(key in jsonResult.VApp.Children.Vm[vmKey].NetworkConnectionSection.NetworkConnection) {
                  vmIpAddress_1.push(jsonResult.VApp.Children.Vm[vmKey].NetworkConnectionSection.NetworkConnection[key].IpAddress);
                }
              }
              vmName_1 = jsonResult.VApp.Children.Vm[vmKey]["-name"];
            // Is 'vAppWithMultipleVM-2' on the response ?.
            } else if (jsonResult.VApp.Children.Vm[vmKey]["-name"] == "vAppWithMultipleVM-2") {
              for(key in jsonResult.VApp.Children.Vm[vmKey]["ovf:VirtualHardwareSection"]["ovf:Item"]) {
                description = jsonResult.VApp.Children.Vm[vmKey]["ovf:VirtualHardwareSection"]["ovf:Item"][key]["rasd:Description"];
                if (description == "Number of Virtual CPUs") {
                  // Parse number of vurtual CPU value from XML reponse and convert it in the format - '1VCPUs'
                  vmCpu_2 = jsonResult.VApp.Children.Vm[vmKey]["ovf:VirtualHardwareSection"]["ovf:Item"][key]["rasd:ElementName"].replace("virtual CPU(s)", "VCPUs");
                } else if (description == "Memory Size") {
                  // Parse VM memory from XML reponse and convert it in the format - '1024Mb'
                  vmMemory_2 = jsonResult.VApp.Children.Vm[vmKey]["ovf:VirtualHardwareSection"]["ovf:Item"][key]["rasd:ElementName"].replace(" MB of memory", "");
                }
              }

              // Parse the VM's ip address
              // VM with one network ?
              if (jsonResult.VApp.Children.Vm[vmKey].NetworkConnectionSection.NetworkConnection.IpAddress !== undefined) {
                vmIpAddress_2.push(jsonResult.VApp.Children.Vm[vmKey].NetworkConnectionSection.NetworkConnection.IpAddress);
              } else {
                //  VM with Multiple network ?
                for(key in jsonResult.VApp.Children.Vm[vmKey].NetworkConnectionSection.NetworkConnection) {
                  vmIpAddress_2.push(jsonResult.VApp.Children.Vm[vmKey].NetworkConnectionSection.NetworkConnection[key].IpAddress);
                }
              }
              vmName_2 = jsonResult.VApp.Children.Vm[vmKey]["-name"];
            }
          }

          // Navigate to Bubbles view page
          homePage.bubbles.click();
          browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-BrokenBubbleDoNotDelete')), waitSeconds);
          bubblesPage.bubble('xxx-BrokenBubbleDoNotDelete').click();
          expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
          browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);

          // Verify values in vCD is matching on the UI for vApp - vAppWithMultipleVM-1
          expect(bubblesPage.BubbleViewVmNameByindex(0).getText()).toEqual(vmName_1);
          expect(bubblesPage.BubbleViewVmIpByindex(0).count()).toEqual(vmIpAddress_1.length);
          expect(vmIpAddress_1).toContain(bubblesPage.BubbleViewVmIpByindex(0).get(0).getText());
          expect(bubblesPage.BubbleViewVmMemoryByindex(0).getText()).toEqual(fn.bytesToSize(vmMemory_1));
          expect(bubblesPage.BubbleViewVmCpu(0).getText()).toEqual(vmCpu_1);

          // Verify values in vCD is matching on the UI for vApp - vAppWithMultipleVM-1
          expect(bubblesPage.BubbleViewVmNameByindex(1).getText()).toEqual(vmName_2);
          expect(bubblesPage.BubbleViewVmIpByindex(1).count()).toEqual(vmIpAddress_2.length);
          expect(vmIpAddress_2).toContain(bubblesPage.BubbleViewVmIpByindex(1).get(0).getText());
          expect(bubblesPage.BubbleViewVmMemoryByindex(1).getText()).toEqual(fn.bytesToSize(vmMemory_2));
          expect(bubblesPage.BubbleViewVmCpu(1).getText()).toEqual(vmCpu_2);
        } else {
          errorMessage = "Expected VM's in 'vAppWithMultipleVM' - 2. " + "Actual VM's - " + jsonResult.VApp.Children.Vm.length;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "Failed while running - " + vAppWithMultipleVM_href;
        this.fail(errorMessage);
      }
    } else {
      errorMessage = "vApp 'vAppWithMultipleVM' exists - " + vAppWithMultipleVM_Exists + ". So can't run this test.";
      this.fail(errorMessage);
    }
  });

  it("Verify details are correct for the vApp - 'vAppWithNoIp'", function() {
    var vmCpu       = "";
    var vmMemory    = "";
    var vmName      = "";
    var description = "";
    var response, key, errorMessage;

    // vApp exits ?.
    if (vAppWithNoIp_Exists) {
      response = fn.runHttpRequests("GET", headers, vAppWithNoIp_href);
      if (response.statusCode == 200) {
        // Convert XML -> JSON
        jsonResult = xmlobj.parseXML(response.body.toString());

        // Parse VM's CPU and Memory from the XML reponse
        for(key in jsonResult.VApp.Children.Vm["ovf:VirtualHardwareSection"]["ovf:Item"]) {
          description = jsonResult.VApp.Children.Vm["ovf:VirtualHardwareSection"]["ovf:Item"][key]["rasd:Description"];
          if (description == "Number of Virtual CPUs") {
            // Parse number of vurtual CPU value from XML reponse and convert it in the format - '1VCPUs'
            vmCpu = jsonResult.VApp.Children.Vm["ovf:VirtualHardwareSection"]["ovf:Item"][key]["rasd:ElementName"].replace("virtual CPU(s)", "VCPUs");
          } else if (description == "Memory Size") {
            // Parse VM memory from XML reponse and convert it in the format - '1024Mb'
            vmMemory = jsonResult.VApp.Children.Vm["ovf:VirtualHardwareSection"]["ovf:Item"][key]["rasd:ElementName"].replace(" MB of memory", "");
          }
        }
        vmName = jsonResult.VApp.Children.Vm["-name"];

        // Navigate to Bubbles view page
        homePage.bubbles.click();
        browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-BrokenBubbleDoNotDelete')), waitSeconds);
        bubblesPage.bubble('xxx-BrokenBubbleDoNotDelete').click();
        expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
        browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);

        // Verify values in vCD is matching on the UI for vApp - vAppWithMultipleVM-1
        expect(bubblesPage.BubbleViewVmNameByindex(2).getText()).toEqual(vmName);
        // SHAN TODO - Might have to fix this
        // expect(bubblesPage.BubbleViewVmIpByindex(2).getText()).toEqual("");
        expect(bubblesPage.BubbleViewVmMemoryByindex(2).getText()).toEqual(fn.bytesToSize(vmMemory));
        expect(bubblesPage.BubbleViewVmCpu(2).getText()).toEqual(vmCpu);
      } else {
        errorMessage = "Failed while running - " + vAppWithNoIp_href;
        this.fail(errorMessage);
      }
    } else {
      errorMessage = "vApp 'vAppWithNoIp' exists - " + vAppWithNoIp_Exists + ". So can't run this test.";
      this.fail(errorMessage);
    }
  });

  it("Verify details are correct for the vApp - 'vAppWithOneVM'", function() {
    var vmIpAddress = [];
    var vmCpu       = "";
    var vmMemory    = "";
    var vmName      = "";
    var description = "";
    var response, key, errorMessage;

    // vApp exits ?.
    if (vAppWithOneVM_Exists) {
      response = fn.runHttpRequests("GET", headers, vAppWithOneVM_href);
      if (response.statusCode == 200) {
        // Convert XML -> JSON
        jsonResult = xmlobj.parseXML(response.body.toString());

        // Parse VM's CPU and Memory from the XML reponse
        for(key in jsonResult.VApp.Children.Vm["ovf:VirtualHardwareSection"]["ovf:Item"]) {
          description = jsonResult.VApp.Children.Vm["ovf:VirtualHardwareSection"]["ovf:Item"][key]["rasd:Description"];
          if (description == "Number of Virtual CPUs") {
            // Parse number of vurtual CPU value from XML reponse and convert it in the format - '1VCPUs'
            vmCpu = jsonResult.VApp.Children.Vm["ovf:VirtualHardwareSection"]["ovf:Item"][key]["rasd:ElementName"].replace("virtual CPU(s)", "VCPUs");
          } else if (description == "Memory Size") {
            // Parse VM memory from XML reponse and convert it in the format - '1024Mb'
            vmMemory = jsonResult.VApp.Children.Vm["ovf:VirtualHardwareSection"]["ovf:Item"][key]["rasd:ElementName"].replace(" MB of memory", "");
          }
        }

        // Parse the VM's ip address
        if (jsonResult.VApp.Children.Vm.NetworkConnectionSection.NetworkConnection.IpAddress !== undefined) {
          vmIpAddress.push(jsonResult.VApp.Children.Vm.NetworkConnectionSection.NetworkConnection.IpAddress);
        } else {
          for(key in jsonResult.VApp.Children.Vm.NetworkConnectionSection.NetworkConnection) {
            vmIpAddress.push(jsonResult.VApp.Children.Vm.NetworkConnectionSection.NetworkConnection[key].IpAddress);
          }
        }
        vmName = jsonResult.VApp.Children.Vm["-name"];

        // Navigate to Bubbles view page
        homePage.bubbles.click();
        browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-BrokenBubbleDoNotDelete')), waitSeconds);
        bubblesPage.bubble('xxx-BrokenBubbleDoNotDelete').click();
        expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
        browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);

        // Verify values in vCD is matching on the UI for vApp - vAppWithMultipleVM-1
        expect(bubblesPage.BubbleViewVmNameByindex(3).getText()).toEqual(vmName);
        expect(bubblesPage.BubbleViewVmIpByindex(3).count()).toEqual(vmIpAddress.length);
        expect(vmIpAddress).toContain(bubblesPage.BubbleViewVmIpByindex(3).get(0).getText());
        expect(bubblesPage.BubbleViewVmMemoryByindex(3).getText()).toEqual(fn.bytesToSize(vmMemory));
        expect(bubblesPage.BubbleViewVmCpu(3).getText()).toEqual(vmCpu);
      } else {
        errorMessage = "Failed while running - " + vAppWithOneVM_href;
        this.fail(errorMessage);
      }
    } else {
      errorMessage = "vApp 'vAppWithOneVM' exists - " + vAppWithOneVM_Exists + ". So can't run this test.";
      this.fail(errorMessage);
    }
  });

  it("Verify details are correct for the vApp - 'vmWithMultipleNetworks'", function() {
    var vmIpAddress = [];
    var vmCpu       = "";
    var vmMemory    = "";
    var vmName      = "";
    var description = "";
    var response, key, errorMessage;

    // vApp exits ?.
    if (vmWithMultipleNetworks_Exists) {
      response = fn.runHttpRequests("GET", headers, vmWithMultipleNetworks_href);
      if (response.statusCode == 200) {
        // Convert XML -> JSON
        jsonResult = xmlobj.parseXML(response.body.toString());

        // Parse VM's CPU and Memory from the XML reponse
        for(key in jsonResult.VApp.Children.Vm["ovf:VirtualHardwareSection"]["ovf:Item"]) {
          description = jsonResult.VApp.Children.Vm["ovf:VirtualHardwareSection"]["ovf:Item"][key]["rasd:Description"];
          if (description == "Number of Virtual CPUs") {
            // Parse number of vurtual CPU value from XML reponse and convert it in the format - '1VCPUs'
            vmCpu = jsonResult.VApp.Children.Vm["ovf:VirtualHardwareSection"]["ovf:Item"][key]["rasd:ElementName"].replace("virtual CPU(s)", "VCPUs");
          } else if (description == "Memory Size") {
            // Parse VM memory from XML reponse and convert it in the format - '1024Mb'
            vmMemory = jsonResult.VApp.Children.Vm["ovf:VirtualHardwareSection"]["ovf:Item"][key]["rasd:ElementName"].replace(" MB of memory", "");
          }
        }

        // Parse the VM's ip address
        // VM with one network ?
        if (jsonResult.VApp.Children.Vm.NetworkConnectionSection.NetworkConnection.IpAddress !== undefined) {
          vmIpAddress.push(jsonResult.VApp.Children.Vm.NetworkConnectionSection.NetworkConnection.IpAddress);
        } else {
          //  VM with Multiple network ?
          for(key in jsonResult.VApp.Children.Vm.NetworkConnectionSection.NetworkConnection) {
            vmIpAddress.push(jsonResult.VApp.Children.Vm.NetworkConnectionSection.NetworkConnection[key].IpAddress);
          }
        }
        vmName = jsonResult.VApp.Children.Vm["-name"];

        // Navigate to Bubbles view page
        homePage.bubbles.click();
        browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-BrokenBubbleDoNotDelete')), waitSeconds);
        bubblesPage.bubble('xxx-BrokenBubbleDoNotDelete').click();
        expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
        browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);

        // Verify values in vCD is matching on the UI for vApp - vAppWithMultipleVM-1
        expect(element.all(by.className("BubbleVM")).get(4).element(by.className("BubbleVM-name")).getText()).toEqual(vmName);
        // expect(bubblesPage.BubbleViewVmIpByindex(4).count()).toEqual(vmIpAddress.length);
        expect(vmIpAddress).toContain(bubblesPage.BubbleViewVmIpByindex(4).get(0).getText());
        expect(bubblesPage.BubbleViewVmMemoryByindex(4).getText()).toEqual(fn.bytesToSize(vmMemory));
        expect(bubblesPage.BubbleViewVmCpu(4).getText()).toEqual(vmCpu);
      } else {
        errorMessage = "Failed while running - " + vmWithMultipleNetworks_href;
        this.fail(errorMessage);
      }
    } else {
      errorMessage = "vApp 'vmWithMultipleNetworks' exists - " + vmWithMultipleNetworks_Exists + ". So can't run this test.";
      this.fail(errorMessage);
    }
  });

});


xdescribe("ServiceNowIntegration", function() {

  it("Get the org_id for the bubble - 'xxx-TestBubble-01'", function(done) {
    var errorMessage;
    // Get org_id for the bubble 'xxx-TestBubble-01'
    db.execute("SELECT * FROM bubble where name='xxx-TestBubble-01'", function(err, result) {
      if (err) {
        errorMessage = "Failure message - " + err;
        this.fail(errorMessage);
      } else {
        if (result.rows.length == 1) {
          org_id = result.rows[0].org_id;
          expect(org_id).not.toEqual("00000000-0000-0000-0000-000000000000");
        } else {
          expect(result.rows.length).toEqual(1);
        }
      }
    });

    done();
  });

  it("Verify CMDB is updated for recently created bubble - 'xxx-TestBubble-01'", function() {
    var response, jsonResponse, errorMessage;
    if (org_id != "") {
      var href = "https://pearsontest.service-now.com/api/now/v1/table/cmdb_ci_zone?sysparm_query=correlation_id=" + org_id;
      response = fn.runHttpRequests("GET", snowHeaders, href);
      if (response.statusCode == 200) {
        jsonResponse = JSON.parse(response.body);

        expect(jsonResponse["result"].length).toEqual(1);
        expect(jsonResponse["result"][0]["name"]).toEqual("xxx-TestBubble-01");
        expect(jsonResponse["result"][0]["u_retirement_date"]).toEqual("");
        expect(jsonResponse["result"][0]["support_group"]).toEqual("");
      } else {
        errorMessage = "Failed to run the query - " + href;
        this.fail(errorMessage);
      }
    } else {
      errorMessage = "Org 'xxx-TestBubble-01' not exists in DB so bubble might not be created.";
      this.fail(errorMessage);
    }
  });

  it("Verify user able to search for list of ServiceNow support groups by typing partial text", function() {
    fn.login("mystack.user1", "");

    homePage.bubbles.click();
    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-01')), waitSeconds);
    bubblesPage.bubble('xxx-TestBubble-01').click();
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);

    // Verify no results are shown if user didn't input anything
    bubblesPage.SnowSelectArrow.click();
    expect(bubblesPage.SnowNoResults.getText()).toEqual("Type to search");

    // Send text 'test' in search field and make sure Portal shows serviceNow group
    bubblesPage.SnowInput.clear().sendKeys("test");
    browser.wait(EC.presenceOf(bubblesPage.SnowSearchResults), 20000);
    expect(bubblesPage.SnowSearchResults.isPresent()).toBeTruthy();
    expect(bubblesPage.SnowSearchResults.count()).toBeGreaterThan(5);

    // Send text 'test' in search field for 2nd time and make sure Portal shows serviceNow group
    bubblesPage.SnowInput.clear().sendKeys("test");
    browser.wait(EC.presenceOf(bubblesPage.SnowSearchResults), 20000);
    expect(bubblesPage.SnowSearchResults.isPresent()).toBeTruthy();
    expect(bubblesPage.SnowSearchResults.count()).toBeGreaterThan(5);

    // Send text 'SOS' in search field for 2nd time and make sure Portal shows serviceNow group
    bubblesPage.SnowInput.clear().sendKeys("SOS");
    browser.wait(EC.presenceOf(bubblesPage.SnowSearchResults), 20000);
    expect(bubblesPage.SnowSearchResults.isPresent()).toBeTruthy();
    expect(bubblesPage.SnowSearchResults.count()).toBeGreaterThan(1);
  });

  it("Verify user able to assign 'ServiceNow Support Group' for a bubble - 'xxx-TestBubble-01'", function() {
    // Add ServiceNow Support group to this bubble
    homePage.bubbles.click();
    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-01')), waitSeconds);
    bubblesPage.bubble('xxx-TestBubble-01').click();
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    expect(SnowGroupGetName.getText()).toEqual('');
    bubblesPage.SnowInput.clear().sendKeys("PI LTG Test");
    browser.wait(EC.presenceOf(bubblesPage.SnowSearchResults), 20000);
    expect(bubblesPage.SnowSearchResults.isPresent()).toBeTruthy();
    bubblesPage.SnowGroupSelect('PI LTG Testing').click();
    expect(bubblesPage.SnowGroupGetName.getText()).toEqual('PI LTG Testing');

    // browser.refresh();
    // expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    // browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    // expect(element(by.className("Select-placeholder")).getText()).toEqual('PI LTG Testing');
    bubblesPage.BubbleViewClose.click();
  });

  it("Verify recently chosen 'ServiceNow Support Group' for a bubble - 'xxx-TestBubble-01' is visible after page refresh", function() {
    // Add ServiceNow Support group to this bubble
    homePage.bubbles.click();
    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-01')), waitSeconds);
    bubblesPage.bubble('xxx-TestBubble-01').click();
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);

    expect(bubblesPage.SnowGroupGetName.getText()).toEqual('PI LTG Testing');
    bubblesPage.BubbleViewClose.click();
  });

  it("Verify 'ServiceNow Support Group' (PI LTG Testing) is updated on the CMDB for bubble - 'xxx-TestBubble-01'", function() {
    var response, jsonResponse, errorMessage;
    if (org_id != "") {

      // Verify ServiceNow Support group is added on the CMDB for the bubble 'xxx-TestBubble-01'
      var href = "https://pearsontest.service-now.com/api/now/v1/table/cmdb_ci_zone?sysparm_query=correlation_id=" + org_id;
      response = fn.runHttpRequests("GET", snowHeaders, href);
      if (response.statusCode == 200) {
        jsonResponse = JSON.parse(response.body);
        supportGroupHref = jsonResponse["result"][0]["support_group"]["link"];
        expect(supportGroupHref).not.toEqual("");

        // Verify bubble is assigned to correct support group
        response = fn.runHttpRequests("GET", snowHeaders, supportGroupHref);
        if (response.statusCode == 200) {
          jsonResponse = JSON.parse(response.body);
          expect(jsonResponse["result"]["name"]).toEqual("PI LTG Testing");
          expect(jsonResponse["result"]["active"]).toBeTruthy();
        } else {
          errorMessage = "Failed to run the query - " + supportGroupHref + " Headers - " + snowHeaders;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "Failed to run the query - " + href + " Headers - " + snowHeaders;
        this.fail(errorMessage);
      }
    } else {
      errorMessage = "Org 'xxx-TestBubble-01' not exists in DB (bubble might not be created). So can't continue this test";
      this.fail(errorMessage);
    }
  });

  it("Verify user able to update ServiceNow support group for bubble - 'xxx-TestBubble-01'", function() {
    // Update the ServiceNow Support group
    homePage.bubbles.click();
    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-01')), waitSeconds);
    bubblesPage.bubble('xxx-TestBubble-01').click();
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    expect(bubblesPage.SnowGroupGetName.getText()).toEqual('PI LTG Testing');
    bubblesPage.SnowInput.clear().sendKeys("SOS Test 2");
    browser.wait(EC.presenceOf(bubblesPage.SnowSearchResults), 20000);
    expect(bubblesPage.SnowSearchResults.isPresent()).toBeTruthy();
    bubblesPage.SnowGroupSelect('SOS Test 2').click();
    expect(bubblesPage.SnowGroupGetName.getText()).toEqual('SOS Test 2');

    browser.refresh();
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    expect(bubblesPage.SnowGroupGetName.getText()).toEqual('SOS Test 2');
    bubblesPage.BubbleViewClose.click();
  });

  it("Verify 'ServiceNow Support Group' (SOS test 2) is updated on the CMDB for bubble - 'xxx-TestBubble-01'", function() {
    var response, jsonResponse, errorMessage;
    if (org_id != "") {
      // Verify ServiceNow Support group gets updated in the CMDB
      var href = "https://pearsontest.service-now.com/api/now/v1/table/cmdb_ci_zone?sysparm_query=correlation_id=" + org_id;
      response = fn.runHttpRequests("GET", snowHeaders, href);
      if (response.statusCode == 200) {
        jsonResponse = JSON.parse(response.body);
        supportGroupHref = jsonResponse["result"][0]["support_group"]["link"];
        expect(supportGroupHref).not.toEqual("");

        // Verify bubble is assigned to correct support group
        response = fn.runHttpRequests("GET", snowHeaders, supportGroupHref);
        if (response.statusCode == 200) {
          jsonResponse = JSON.parse(response.body);
          expect(jsonResponse["result"]["name"]).toEqual("SOS Test 2");
          expect(jsonResponse["result"]["active"]).toBeTruthy();
        } else {
          errorMessage = "Failed to run the query - " + supportGroupHref + " Headers - " + snowHeaders;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "Failed to run the query - " + href + " Headers - " + snowHeaders;
        this.fail(errorMessage);
      }
    } else {
      errorMessage = "Org 'xxx-TestBubble-01' not exists in DB (bubble might not be created). So can't continue this test";
      this.fail(errorMessage);
    }
  });

});

describe("Search", function() {

  beforeEach(function() {
    browser.get('/');
  });

  it("Verify user able to search the VM/vApp by typing the IP address - 'vAppWithOneVM'", function() {
    var headers     = {"Accept": "application/*+xml;version=5.1", "Authorization" : authorization};
    var vmIpAddress = [];
    var vmName      = "";
    var response, key, errorMessage;

    // Get the Auth key
    response = fn.runHttpRequests("POST", headers, vcd + '/api/sessions');
    if (response.statusCode == 200) {
      headers = {"Accept": "application/*+xml;version=5.1", "x-vcloud-authorization": response.headers['x-vcloud-authorization']};

      // vApp exits ?.
      if (vAppWithOneVM_Exists) {
        response = fn.runHttpRequests("GET", headers, vAppWithOneVM_href);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResult = xmlobj.parseXML(response.body.toString());

          // Parse the VM's ip address
          if (jsonResult.VApp.Children.Vm.NetworkConnectionSection.NetworkConnection.IpAddress !== undefined) {
            vmIpAddress.push(jsonResult.VApp.Children.Vm.NetworkConnectionSection.NetworkConnection.IpAddress);
          } else {
            for(key in jsonResult.VApp.Children.Vm.NetworkConnectionSection.NetworkConnection) {
              vmIpAddress.push(jsonResult.VApp.Children.Vm.NetworkConnectionSection.NetworkConnection[key].IpAddress);
            }
          }
          vmName = jsonResult.VApp.Children.Vm["-name"];

          fn.login("mystack.root", "");
          homePage.search.click();
          browser.wait(EC.elementToBeClickable(element(by.buttonText("search"))), waitSeconds);
          element(by.xpath('//*[@type="search"]')).clear().sendKeys(vmIpAddress);
          element(by.buttonText("search")).click();

          // Verify the search results
          expect(element(by.className("Bubble-name")).getText()).toEqual('xxx-BrokenBubbleDoNotDeleteXXXLO3');
          expect(element(by.className("Bubble-region")).getText()).toEqual('XXXLO3');
          expect(vmIpAddress).toContain(element(by.className("Bubble-ipAddress")).getText());
          expect(element(by.className("Bubble-vmName")).getText()).toEqual(vmName);
        } else {
          errorMessage = "Failed while running - " + vAppWithOneVM_href;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "vApp 'vAppWithOneVM' exists - " + vAppWithOneVM_Exists + ". So can't run this test.";
        this.fail(errorMessage);
      }
    } else {
      errorMessage = "Failed to get the Auth key";
      this.fail(errorMessage);
    }
  });

  it("Verify user able to search the VM/vApp by typing the IP address - 'vmWithMultipleNetworks'", function() {
    var headers     = {"Accept": "application/*+xml;version=5.1", "Authorization" : authorization};
    var vmIpAddress = [];
    var vmName      = "";
    var response, key, errorMessage;

    // Get the Auth key
    response = fn.runHttpRequests("POST", headers, vcd + '/api/sessions');
    if (response.statusCode == 200) {
      headers = {"Accept": "application/*+xml;version=5.1", "x-vcloud-authorization": response.headers['x-vcloud-authorization']};
      // vApp exits ?.
      if (vmWithMultipleNetworks_Exists) {
        response = fn.runHttpRequests("GET", headers, vmWithMultipleNetworks_href);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResult = xmlobj.parseXML(response.body.toString());

          // Parse the VM's ip address
          // VM with one network ?
          if (jsonResult.VApp.Children.Vm.NetworkConnectionSection.NetworkConnection.IpAddress !== undefined) {
            vmIpAddress.push(jsonResult.VApp.Children.Vm.NetworkConnectionSection.NetworkConnection.IpAddress);
          } else {
            //  VM with Multiple network ?
            for(key in jsonResult.VApp.Children.Vm.NetworkConnectionSection.NetworkConnection) {
              vmIpAddress.push(jsonResult.VApp.Children.Vm.NetworkConnectionSection.NetworkConnection[key].IpAddress);
            }
          }
          vmName = jsonResult.VApp.Children.Vm["-name"];

          fn.login("mystack.root", "");
          homePage.search.click();
          browser.wait(EC.elementToBeClickable(element(by.buttonText("search"))), waitSeconds);
          element(by.xpath('//*[@type="search"]')).clear().sendKeys(vmIpAddress[0]);
          element(by.buttonText("search")).click();

          // Verify the search results
          expect(element(by.className("Bubble-name")).getText()).toEqual('xxx-BrokenBubbleDoNotDeleteXXXLO3');
          expect(element(by.className("Bubble-region")).getText()).toEqual('XXXLO3');
          expect(vmIpAddress).toContain(element(by.className("Bubble-ipAddress")).getText());
          expect(element(by.className("Bubble-vmName")).getText()).toEqual(vmName);
        } else {
          errorMessage = "Failed while running - " + vmWithMultipleNetworks_href;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "vApp 'vmWithMultipleNetworks' exists - " + vmWithMultipleNetworks_Exists + ". So can't run this test.";
        this.fail(errorMessage);
      }
    } else {
      errorMessage = "Failed to get the Auth key";
      this.fail(errorMessage);
    }
  });

  it("Verify user gets 'No Result' while trying to search for the IP address that is not portal", function() {
    fn.login("mystack.root", "");
    homePage.search.click();
    browser.wait(EC.elementToBeClickable(element(by.buttonText("search"))), waitSeconds);
    element(by.xpath('//*[@type="search"]')).clear().sendKeys("255.255.255.255");
    element(by.buttonText("search")).click();

    // Verify the result
    expect(element(by.className("SearchPage-results")).element(by.deepCss('div')).getText()).toEqual("No Results");
  });

});

