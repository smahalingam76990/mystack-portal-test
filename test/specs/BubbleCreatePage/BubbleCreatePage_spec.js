/*
*****************************************************************************************************************************************************
SUMMARY:         Verifies error messages in bubble create page
ATTENTION:       Should be run after 'UserAndTeams_spec.js'

List Of Tests:
*****************************************************************************************************************************************************
*/
describe('BubblesPage', function() {

  var homePage    = require("../../pages/home_page.js");
  var bubblesPage = require("../../pages/bubbles_page.js");

  beforeAll(function() {
    browser.get('/');
    fn.login("mystack.root", "");
  });

  beforeEach(function() {
    browser.get('/');
  });

  it("Verify user able to see all the bubbles in the 'BUBBLES' page", function() {
    homePage.bubbles.click();
    expect(browser.getCurrentUrl()).toEqual(homePageUrl + "bubbles/");
    expect(bubblesPage.bubbles.count()).toBeGreaterThan(10);
  });

  it("Verify user able to filter the bubble by typing partial bubble name on the search box - 'xxx-BrokenBubbleDoNotDelete'", function() {
    homePage.bubbles.click();
    browser.wait(EC.elementToBeClickable(bubblesPage.searchBubble), waitSeconds);
    bubblesPage.searchBubble.click();
    bubblesPage.searchBubble.sendKeys('xxx-');

    // Verify the bubble details
    expect(bubblesPage.bubbles.count()).toEqual(1);
    expect(bubblesPage.getBubbleNameByindex(0)).toEqual('xxx-BrokenBubbleDoNotDelete');
    expect(bubblesPage.getBubbleRegionByindex(0)).toEqual('XXXLO3');
    expect(bubblesPage.getBubbleTeamByindex(0)).toEqual('xxx-BrokenTeam');
  });

  it("Verify user able to filter the bubble by typing partial bubble name on the search box (different case) - 'xxx-BrokenBubbleDoNotDelete'", function() {
    homePage.bubbles.click();
    expect(browser.getCurrentUrl()).toEqual(homePageUrl + "bubbles/");
    browser.wait(EC.elementToBeClickable(bubblesPage.searchBubble), waitSeconds);
    bubblesPage.searchBubble.click();
    bubblesPage.searchBubble.sendKeys('XXX-');

    // Verify the bubble details
    expect(bubblesPage.bubbles.count()).toEqual(1);
    expect(bubblesPage.getBubbleNameByindex(0)).toEqual('xxx-BrokenBubbleDoNotDelete');
    expect(bubblesPage.getBubbleRegionByindex(0)).toEqual('XXXLO3');
    expect(bubblesPage.getBubbleTeamByindex(0)).toEqual('xxx-BrokenTeam');
  });

  it("Verify details in bubbles page while no bubble exists for this user - 'mystack.user2'", function() {
    fn.login("mystack.user2", "");

    homePage.bubbles.click();
    expect(bubblesPage.bubbles).toEqual([]);
  });

  it("Verify default values on bubble create page for user 'mystack.user2", function() {
    homePage.bubbles.click();

    browser.wait(EC.elementToBeClickable(bubblesPage.createBubble), waitSeconds);
    bubblesPage.createBubble.click();
    expect(bubblesPage.createBubbleName.isPresent()).toBeTruthy();
    expect(bubblesPage.createBubbleTeam.count()).toEqual(2);
    expect(bubblesPage.createBubbleTeam.getText()).toContain("xxx-TestTeam-02");
    expect(bubblesPage.createBubbleRegion.count()).toEqual(3);
    expect(bubblesPage.createBubbleRegion.getText()).toContain("LO3REF - Reference");
    expect(bubblesPage.createBubbleRegion.getText()).toContain("XXXLO3 - Iceland");
    // Verify details in 'Line of Business' drop down menu
    expect(bubblesPage.createBubblelob.count()).toEqual(10);
    expect(bubblesPage.createBubblelob.getText()).toContain('CIO-CorePlat');
    expect(bubblesPage.createBubblelob.getText()).toContain('REG-NA');
    expect(bubblesPage.tandcCheckbox.isSelected()).toBeFalsy();
    bubblesPage.tandc.click();
    bubblesPage.tandcDecline.click();
    expect(bubblesPage.tandcCheckbox.isSelected()).toBeFalsy();
    bubblesPage.createBubbleCancel.click();
    expect(bubblesPage.createBubbleName.isPresent()).toBeFalsy();
  });

  it("Verify details in bubbles page while no bubble exists for this user - 'mystack.user3'", function() {
    fn.login("mystack.user3", "");

    homePage.bubbles.click();
    expect(bubblesPage.bubbles).toEqual([]);
  });

  it("Verify default values on bubble create page for user 'mystack.user3", function() {
    homePage.bubbles.click();

    browser.wait(EC.elementToBeClickable(bubblesPage.createBubble), waitSeconds);
    bubblesPage.createBubble.click();
    expect(bubblesPage.createBubbleName.isPresent()).toBeTruthy();
    expect(bubblesPage.createBubbleTeam.count()).toEqual(2);
    expect(bubblesPage.createBubbleTeam.getText()).toContain("xxx-TestTeam-01");
    expect(bubblesPage.createBubbleRegion.count()).toEqual(3);
    // expect(element.all(by.css('.CreateBubble-region option')).getText()).toContain("DEV - Developer");
    expect(bubblesPage.createBubbleRegion.getText()).toContain("LO3REF - Reference");
    // expect(element.all(by.css('.CreateBubble-region option')).getText()).toContain("XXX - Easter Island");
    expect(bubblesPage.createBubbleRegion.getText()).toContain("XXXLO3 - Iceland");
    // Verify details in 'Line of Business' drop down menu
    expect(bubblesPage.createBubblelob.count()).toEqual(10);
    expect(bubblesPage.createBubblelob.getText()).toContain('CIO-CorePlat');
    expect(bubblesPage.createBubblelob.getText()).toContain('REG-NA');
    expect(bubblesPage.tandcCheckbox.isSelected()).toBeFalsy();
    bubblesPage.tandc.click();
    bubblesPage.tandcDecline.click();
    expect(bubblesPage.tandcCheckbox.isSelected()).toBeFalsy();
    bubblesPage.createBubbleCancel.click();
    expect(bubblesPage.createBubbleName.isPresent()).toBeFalsy();
  });

  it("Verify details in bubbles page while no bubble exists for this user - 'mystack.user1'", function() {
    fn.login("mystack.user1", "");

    homePage.bubbles.click();
    expect(bubblesPage.bubbles).toEqual([]);
  });

  it("Verify default values on bubble create page for user 'mystack.user1", function() {
    homePage.bubbles.click();

    browser.wait(EC.elementToBeClickable(bubblesPage.createBubble), waitSeconds);
    bubblesPage.createBubble.click();
    expect(bubblesPage.createBubbleName.isPresent()).toBeTruthy();
    expect(bubblesPage.createBubbleTeam.count()).toEqual(3);
    expect(bubblesPage.createBubbleTeam.getText()).toContain("xxx-TestTeam-01");
    expect(bubblesPage.createBubbleTeam.getText()).toContain("xxx-TestTeam-02");
    expect(bubblesPage.createBubbleRegion.count()).toEqual(3);
    // expect(element.all(by.css('.CreateBubble-region option')).getText()).toContain("DEV - Developer");
    expect(bubblesPage.createBubbleRegion.getText()).toContain("LO3REF - Reference");
    // expect(element.all(by.css('.CreateBubble-region option')).getText()).toContain("XXX - Easter Island");
    expect(bubblesPage.createBubbleRegion.getText()).toContain("XXXLO3 - Iceland");
    // Verify details in 'Line of Business' drop down menu
    expect(bubblesPage.createBubblelob.count()).toEqual(10);
    expect(bubblesPage.createBubblelob.getText()).toContain('CIO-CorePlat');
    expect(bubblesPage.createBubblelob.getText()).toContain('REG-NA');
    expect(bubblesPage.tandcCheckbox.isSelected()).toBeFalsy();
    bubblesPage.tandc.click();
    bubblesPage.tandcDecline.click();
    expect(bubblesPage.tandcCheckbox.isSelected()).toBeFalsy();
    bubblesPage.createBubbleCancel.click();
    expect(bubblesPage.createBubbleName.isPresent()).toBeFalsy();
  });


  describe("BubbleCreatePageErrorMessage", function() {

    beforeEach(function() {
      browser.get('/');

      // Set network available for region 'XXXLO3'
      db.execute("SELECT * FROM network WHERE region='XXXLO3'", function(err, result) {
        if (err) {
          var errorMessage = "Failure message - " + err;
          this.fail(errorMessage);
        } else {
          for (var i=0; i < result.rows.length; i++) {
            var query = "UPDATE network SET bubble=null, status=0 WHERE id=" + result.rows[i].id;
            db.execute(query, function(err, update) {
              if (err) {
                var errorMessage = "Failed to run - " + query + ". Failure message - " + err;
                this.fail(errorMessage);
              }
            });
          }
        }
      });
    });

    it("Should throw error message when bubble name, team name and region are empty", function() {
      fn.login("mystack.root", "");

      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.createBubble), waitSeconds);
      bubblesPage.createBubble.click();

      // Try to create bubble without filling any form and verify the error message on UI
      bubblesPage.createButton.click();
      browser.wait(EC.visibilityOf(bubblesPage.bubbleCreateError), 5000);
      expect(bubblesPage.bubbleCreateError.getText()).toEqual("Please provide name, team and location");
      expect(bubblesPage.createBubbleName.isPresent()).toBeTruthy();
    });

    it("Should throw error when team name and region are empty", function() {
      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.createBubble), waitSeconds);
      bubblesPage.createBubble.click();
      bubblesPage.createBubbleName.sendKeys("xxx-NotFilledForms");

      // Try to create bubble without filling any team & region and verify the error message on UI
      bubblesPage.createButton.click();
      browser.wait(EC.visibilityOf(bubblesPage.bubbleCreateError), 5000);
      expect(bubblesPage.bubbleCreateError.getText()).toEqual("Please provide name, team and location");
      expect(bubblesPage.createBubbleName.isPresent()).toBeTruthy();
    });

    it("Should throw error when region is empty", function() {
      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.createBubble), waitSeconds);
      bubblesPage.createBubble.click();
      bubblesPage.createBubbleName.sendKeys("xxx-NotFilledForms");
      bubblesPage.createBubbleChooseTeam('xxx-FailureTests-01');

      // Try to create bubble without filling any team & region and verify the error message on UI
      bubblesPage.createButton.click();
      browser.wait(EC.visibilityOf(bubblesPage.bubbleCreateError), 5000);
      expect(bubblesPage.bubbleCreateError.getText()).toEqual("Please provide name, team and location");
      expect(bubblesPage.createBubbleName.isPresent()).toBeTruthy();
    });

    it("Should throw error when user didn't accept the T&C's", function() {
      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.createBubble), waitSeconds);
      bubblesPage.createBubble.click();
      bubblesPage.createBubbleName.sendKeys("xxx-ExpectedEdgeFailure");
      bubblesPage.createBubbleChooseTeam('xxx-FailureTests-01');
      bubblesPage.createBubbleChooseRegion('XXXLO3 - Iceland');

      // Try to create bubble without without acceptinng T&C's and verify the error message on the UI
      bubblesPage.createButton.click();
      browser.wait(EC.visibilityOf(bubblesPage.bubbleCreateError), 5000);
      expect(bubblesPage.bubbleCreateError.getText()).toEqual("Please read and accept the terms and conditions");
      expect(bubblesPage.createBubbleName.isPresent()).toBeTruthy();
    });

    it("Should not create bubble when user declines the T&C's", function() {
      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.createBubble), waitSeconds);
      bubblesPage.createBubble.click();
      bubblesPage.createBubbleName.sendKeys("xxx-ExpectedEdgeFailure");
      bubblesPage.createBubbleChooseTeam('xxx-FailureTests-01');
      bubblesPage.createBubbleChooseRegion('XXXLO3 - Iceland');
      bubblesPage.tandc.click();
      bubblesPage.tandcDecline.click();
      expect(bubblesPage.tandcCheckbox.isSelected()).toBeFalsy();

      // Try to create bubble without without acceptinng T&C's and verify the error message on the UI
      bubblesPage.createButton.click();
      browser.wait(EC.visibilityOf(bubblesPage.bubbleCreateError), 5000);
      expect(bubblesPage.bubbleCreateError.getText()).toEqual("Please read and accept the terms and conditions");
      expect(bubblesPage.createBubbleName.isPresent()).toBeTruthy();
    });

    it("Should throw error message when user enters invalid Jira ticker number", function() {
      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.createBubble), waitSeconds);
      bubblesPage.createBubble.click();
      bubblesPage.createBubbleName.sendKeys("xxx-IvalidJiraTicket");
      bubblesPage.createBubbleTicket.sendKeys("MPSPLAT-456");
      bubblesPage.createBubbleChooseTeam('xxx-FailureTests-01');
      bubblesPage.createBubbleChooseRegion('XXXLO3 - Iceland');
      bubblesPage.tandc.click();
      bubblesPage.tandcAccept.click();
      expect(bubblesPage.tandcCheckbox.isSelected()).toBeTruthy();

      // Try to create bubble without without acceptinng T&C's and verify the error message on the UI
      bubblesPage.createButton.click();
      browser.wait(EC.visibilityOf(bubblesPage.bubbleCreateError), 5000);
      expect(bubblesPage.bubbleCreateError.getText()).toEqual("Enter the numeric part of the ticket only without the CPL prefix");
      expect(bubblesPage.createBubbleName.isPresent()).toBeTruthy();
    });

  });

});
