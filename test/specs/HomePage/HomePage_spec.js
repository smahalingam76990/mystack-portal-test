/*
*****************************************************************************************************************************************************
SUMMARY: It verifies whether links/buttons on the myStack portal Home page is working

ATTENTION: None

List Of Tests:
  HomePage: On clicking myCloudLink, user should be navigated to login page and back to Home page by pressing Back button
  HomePage: On clicking myHelp, user should be navigated to login page and back to Home page by pressing Back button
  HomePage: On clicking myStore, user should be navigated to login page and back to Home page by pressing Back button
  HomePage: On clicking myID, user should be navigated to login page and back to Home page by pressing Back button
  HomePage: On clicking SignOnLink (Top right hand side), user should be navigated to login page and back to Home page by pressing Back button
  HomePage: On clicking 'Access my Stuff link' user should be navigated to login page and back to Home Page by clicking on Back button
  HomePage: On clicking Join myStack, user should be navigated to login page and back to Home page by pressing Back button
  HomePage: On clicking Support link user should be navigated to Support page and back to Home page by clicking on Back button
  HomePage: Check whether user able to click on Knowledgebase link and come back to home page by clicking Back button
  HomePage: On clicking the 'Learn' link (Knowledgebase), user should naviagted to Pearson's article page and should come back to mystack portal by clicking back button
  HomePage: On clicking the 'As a Question' link (Knowledgebase), user should naviagted to Pearson's article page and should come back to mystack portal by clicking back button
  HomePage: Check whether user able to click on Marketplace link and come back to home page by clicking Back button
*****************************************************************************************************************************************************
*/

describe('HomePage', function() {

  beforeAll(function() {
    browser.get('/');
    fn.logout();
  });

  beforeEach(function() {
    browser.get('/');
  });

  // xit('On clicking myCloudLink, user should be navigated to login page and back to Home page by pressing Back button', function() {
  //   element(by.linkText("myCloud")).click();

  //   expect(browser.getCurrentUrl()).toContain('https://sts.pearson.com/adfs/ls/?wa=wsignin1.0&wtrealm=');
  //   element(by.id('SubmitButton')).click();
  //   expect(element(by.id('ErrorTextLabel')).getText()).toEqual('The user name or password is incorrect.');

  //   browser.navigate().back();
  //   browser.navigate().back();
  //   // Wait for the page to load
  //   // fn.waitForPageToLoad(waitSeconds, homePageUrl);
  //   expect(browser.getCurrentUrl()).toContain(homePageUrl);
  // });

  // xit('On clicking myHelp, user should be navigated to login page and back to Home page by pressing Back button', function() {
  //   element(by.linkText("myHelp")).click();

  //   expect(browser.getCurrentUrl()).toContain('https://sts.pearson.com/adfs/ls/?SAMLRequest=');
  //   element(by.id('SubmitButton')).click();
  //   expect(element(by.id('ErrorTextLabel')).getText()).toEqual('The user name or password is incorrect.');

  //   browser.navigate().back();
  //   browser.navigate().back();
  //   expect(browser.getCurrentUrl()).toEqual(homePageUrl);
  // });

  // xit('On clicking myStore, user should be navigated to login page and back to Home page by pressing Back button', function() {
  //   element(by.linkText("myStore")).click();

  //   expect(browser.getCurrentUrl()).toContain('https://sts.pearson.com/adfs/ls/?SAMLRequest=');
  //   element(by.id('SubmitButton')).click();
  //   expect(element(by.id('ErrorTextLabel')).getText()).toEqual('The user name or password is incorrect.');

  //   browser.navigate().back();
  //   browser.navigate().back();
  //   expect(browser.getCurrentUrl()).toEqual(homePageUrl);
  // });

  // xit('On clicking myID, user should be navigated to login page and back to Home page by pressing Back button', function() {
  //   element(by.linkText("myID")).click();

  //   expect(browser.getCurrentUrl()).toContain('https://sts.pearson.com/adfs/ls/?SAMLRequest=');
  //   element(by.id('SubmitButton')).click();
  //   expect(element(by.id('ErrorTextLabel')).getText()).toEqual('The user name or password is incorrect.');

  //   browser.navigate().back();
  //   browser.navigate().back();
  //   expect(browser.getCurrentUrl()).toEqual(homePageUrl);
  // });

  it('On clicking SignOnLink (Top right hand side), user should be navigated to login page and back to Home page by pressing Back button', function() {
    element(by.className('headerSignOn')).click();

    expect(browser.getCurrentUrl()).toContain(loginPage + 'adfs/ls/?SAMLRequest');
    element(by.id('SubmitButton')).click();
    expect(element(by.id('ErrorTextLabel')).getText()).toEqual('The user name or password is incorrect.');

    browser.navigate().back();
    browser.navigate().back();
    expect(browser.getCurrentUrl()).toEqual(homePageUrl);
  });

  it("On clicking 'Access my Stuff link' user should be navigated to login page and back to Home Page by clicking on Back button", function() {
    element(by.linkText("Access my Stuff")).click();

    expect(browser.getCurrentUrl()).toContain(loginPage + 'adfs/ls/?SAMLRequest');
    element(by.id('SubmitButton')).click();
    expect(element(by.id('ErrorTextLabel')).getText()).toEqual('The user name or password is incorrect.');

    browser.navigate().back();
    browser.navigate().back();
    expect(browser.getCurrentUrl()).toEqual(homePageUrl);
  });

  it('On clicking Join myStack, user should be navigated to login page and back to Home page by pressing Back button', function() {
    element(by.linkText("Join myStack")).click();
    expect(browser.getCurrentUrl()).toEqual(homePageUrl + 'signup');
    expect(element(by.xpath('//h2')).getText()).toEqual("Want to join myStack? Here's how... - Failure for John");

    browser.navigate().back();
    expect(browser.getCurrentUrl()).toEqual(homePageUrl);
  });

  it('On clicking Support link user should be navigated to Support page and back to Home page by clicking on Back button', function() {
    element(by.linkText("Support")).click();
    expect(browser.getCurrentUrl()).toContain(homePageUrl + 'support');
    expect(element(by.xpath('//h1')).getText()).toEqual("Support");

    browser.navigate().back();
    expect(browser.getCurrentUrl()).toEqual(homePageUrl);
  });

  it('Check whether user able to click on Knowledgebase link and come back to home page by clicking Back button', function() {
    element(by.linkText("Knowledge base")).click();
    expect(browser.getCurrentUrl()).toEqual(homePageUrl + 'knowledgebase');
    expect(element(by.xpath('//h1')).getText()).toEqual("Knowledge Base");

    browser.navigate().back();
    expect(browser.getCurrentUrl()).toEqual(homePageUrl);
  });

  it("On clicking the 'Knowledgebase | Learn' link, user should naviagted to Pearson's article page and should come back to mystack portal by clicking back button", function() {
    element(by.linkText("Knowledge base")).click();
    element(by.linkText("Learn")).click();

    browser.getAllWindowHandles().then(function (handles) {
      if (handles.length == 2) {
        browser.switchTo().window(handles[1]);
        expect(browser.getCurrentUrl()).toContain("https://mycloud.atlassian.net/login?");
        browser.close();

        // Change focus back to Original window - myStack portal
        browser.switchTo().window(handles[0]);
        expect(browser.getCurrentUrl()).toEqual(homePageUrl + 'knowledgebase');

        // Should navigate to home page while pressing the back button
        browser.navigate().back();
        browser.sleep(1000);
        expect(browser.getCurrentUrl()).toEqual(homePageUrl);
      } else {
        err_msg = "New windows might not be opened while clicking the 'Knowledge base | Learn'. Actual number of windows - " + handles.length;
        this.fail(err_msg);
      }
    });
  });

  it("On clicking the 'Knowledgebase | As a Question' link, user should naviagted to Pearson's article page and should come back to mystack portal by clicking back button", function() {
    element(by.linkText("Knowledge base")).click();
    element(by.linkText("Ask a question")).click();

    browser.getAllWindowHandles().then(function (handles) {
      if (handles.length == 2) {
        browser.switchTo().window(handles[1]);
        expect(browser.getCurrentUrl()).toContain("https://mycloud.atlassian.net/login?");
        browser.close();

        // Change focus back to Original window - myStack portal
        browser.switchTo().window(handles[0]);
        expect(browser.getCurrentUrl()).toEqual(homePageUrl + 'knowledgebase');

        // Should navigate to home page while pressing the back button
        browser.navigate().back();
        expect(browser.getCurrentUrl()).toEqual(homePageUrl);
      } else {
        err_msg = "New windows might not be opened while clicking the 'Knowledge base | Ask a Question'. Actual number of windows - " + handles.length;
        this.fail(err_msg);
      }
    });
  });

  it('Check whether user able to click on Marketplace link and come back to home page by clicking Back button', function() {
    element(by.linkText("Marketplace")).click();
    // Wait for the page to load
    expect(browser.getCurrentUrl()).toContain(loginPage + 'adfs/ls/?SAMLRequest');
    element(by.id('SubmitButton')).click();
    expect(element(by.id('ErrorTextLabel')).getText()).toEqual('The user name or password is incorrect.');
  });

  xit("Verify logged in user (mystack.user1) able to access the Knowledgebase, mycloud, myHelp, myStore and so on ...", function() {
    fn.login("mystack.user1", "");

    // Verify the 'Knowledge Base' link
    element(by.className("UserMenu-name")).click();
    element(by.linkText("Knowledge Base")).click();
    browser.getAllWindowHandles().then(function (handles) {
      if (handles.length == 2) {
        browser.switchTo().window(handles[1]);
        expect(browser.getCurrentUrl()).toContain("https://mycloud.atlassian.net/login?");
        browser.close();

        // Change focus back to Original window - myStack portal
        browser.switchTo().window(handles[0]);
        expect(browser.getCurrentUrl()).toEqual(homePageUrl);
      } else {
        err_msg = "New windows might not be opened while clicking the 'Knowledge base'. Actual number of windows - " + handles.length;
        this.fail(err_msg);
      }
    });

    // Verify the 'myCloud' link
    element(by.className("UserMenu-name")).click();
    element(by.linkText("myCloud")).click();
    browser.getAllWindowHandles().then(function (handles) {
      if (handles.length == 2) {
        browser.switchTo().window(handles[1]);
        expect(browser.getCurrentUrl()).toContain("https://sts.pearson.com/adfs/ls/?wa=wsignin1.0&wtrealm=");
        browser.close();

        // Change focus back to Original window - myStack portal
        browser.switchTo().window(handles[0]);
        expect(browser.getCurrentUrl()).toEqual(homePageUrl);
      } else {
        err_msg = "New windows might not be opened while clicking the 'myCloud'. Actual number of windows - " + handles.length;
        this.fail(err_msg);
      }
    });

    // Verify the 'myHelp' link
    element(by.className("UserMenu-name")).click();
    element(by.linkText("myHelp")).click();
    browser.getAllWindowHandles().then(function (handles) {
      if (handles.length == 2) {
        browser.switchTo().window(handles[1]);
        expect(browser.getCurrentUrl()).toContain("https://sts.pearson.com/adfs/ls/?SAMLRequest=");
        browser.close();

        // Change focus back to Original window - myStack portal
        browser.switchTo().window(handles[0]);
        expect(browser.getCurrentUrl()).toEqual(homePageUrl);
      } else {
        err_msg = "New windows might not be opened while clicking the 'myHelp'. Actual number of windows - " + handles.length;
        this.fail(err_msg);
      }
    });

    // Verify the 'myStore' link
    element(by.className("UserMenu-name")).click();
    element(by.linkText("myStore")).click();
    browser.getAllWindowHandles().then(function (handles) {
      if (handles.length == 2) {
        browser.switchTo().window(handles[1]);
        expect(browser.getCurrentUrl()).toContain("https://sts.pearson.com/adfs/ls/?SAMLRequest=");
        browser.close();

        // Change focus back to Original window - myStack portal
        browser.switchTo().window(handles[0]);
        expect(browser.getCurrentUrl()).toEqual(homePageUrl);
      } else {
        err_msg = "New windows might not be opened while clicking the 'myStore'. Actual number of windows - " + handles.length;
        this.fail(err_msg);
      }
    });

    // Verify the 'myID' link
    element(by.className("UserMenu-name")).click();
    element(by.linkText("myID")).click();
    browser.getAllWindowHandles().then(function (handles) {
      if (handles.length == 2) {
        browser.switchTo().window(handles[1]);
        expect(browser.getCurrentUrl()).toContain("https://sts.pearson.com/adfs/ls/?SAMLRequest=");
        browser.close();

        // Change focus back to Original window - myStack portal
        browser.switchTo().window(handles[0]);
        expect(browser.getCurrentUrl()).toEqual(homePageUrl);
      } else {
        err_msg = "New windows might not be opened while clicking the 'myID'. Actual number of windows - " + handles.length;
        this.fail(err_msg);
      }
    });

    // TODO - Add test for 'Change Password' link
  });

  it("Verify logged in user (mystack.user2) able to access the Knowledgebase, mycloud, myHelp, myStore and so on ... ", function() {
    fn.login("mystack.user2", "");

    // Verify the 'Knowledge Base' link
    element(by.className("UserMenu-name")).click();
    expect(element(by.linkText("Knowledge Base")).isPresent()).toBeTruthy();
    expect(element(by.linkText("myCloud")).isPresent()).toBeTruthy();
    expect(element(by.linkText("myHelp")).isPresent()).toBeTruthy();
    expect(element(by.linkText("myStore")).isPresent()).toBeTruthy();
    expect(element(by.linkText("myID")).isPresent()).toBeTruthy();
    expect(element(by.linkText("Change Password")).isPresent()).toBeTruthy();
  });

  it("Verify logged in user (mystack.user6) able to access the Knowledgebase, mycloud, myHelp, myStore and so on ... ", function() {
    fn.login("mystack.user6", "");

    // Verify the 'Knowledge Base' link
    element(by.className("UserMenu-name")).click();
    expect(element(by.linkText("Knowledge Base")).isPresent()).toBeTruthy();
    expect(element(by.linkText("myCloud")).isPresent()).toBeTruthy();
    expect(element(by.linkText("myHelp")).isPresent()).toBeTruthy();
    expect(element(by.linkText("myStore")).isPresent()).toBeTruthy();
    expect(element(by.linkText("myID")).isPresent()).toBeTruthy();
    expect(element(by.linkText("Change Password")).isPresent()).toBeTruthy();
  });

});
