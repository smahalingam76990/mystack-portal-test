/*
*****************************************************************************************************************************************************
SUMMARY:      Bubble delete

ATTENTION:    Should be run after 'UserAndTeams_spec.js', 'BubbleCreate-01_spec.js', 'BubbleCreate-02_spec.js', 'BubbleDelete-01_spec.js' & 'BubbleDelete-02_spec.js'

List Of Tests:
    BubbleDelete: Verify user can't see the deleted bubble on the UI - 'mystack.user1'
    BubbleDelete: Verify user can't see the deleted bubble on the UI - 'mystack.user2'
    BubbleDelete: Verify user can't see the deleted bubble on the UI - 'mystack.user3'
*****************************************************************************************************************************************************
*/

var homePage          = require("../../pages/home_page.js");
var teamsPage         = require("../../pages/teams_page.js");
var bubblesPage       = require("../../pages/bubbles_page.js");

describe('BubbleDelete', function() {

  beforeEach(function() {
    browser.get('/');
  });

  it("Verify user can't see the deleted bubble on the UI - 'mystack.user1'", function() {
    fn.login("mystack.user1", "");

    homePage.bubbles.click();
    expect(bubblesPage.bubble('xxx-TestBubble-01').isPresent()).toBeFalsy();
  });

  it("Verify team 'xxx-TestTeam-01' got updated for recently deleted bubble - 'xxx-TestBubble-01' & 'xxx-TestBubble-02'", function() {
    homePage.teams.click();
    // Details in TeamView page
    browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-01')), waitSeconds);
    teamsPage.team('xxx-TestTeam-01').click();
    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-01');
    // Verify the deleted is not more visible
    expect(teamsPage.TeamViewBubbles.count()).toEqual(0);
  });

  it("Verify team 'xxx-TestTeam-02' got updated for recently deleted bubble - 'xxx-TestBubble-01' & 'xxx-TestBubble-02'", function() {
    homePage.teams.click();
    // Details in TeamView page
    browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-02')), waitSeconds);
    teamsPage.team('xxx-TestTeam-02').click();
    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-02');
    // Verify the deleted is not more visible
    expect(teamsPage.TeamViewBubbles.count()).toEqual(0);
  });

  it("Verify user can't see the deleted bubble on the UI - 'mystack.user2'", function() {
    fn.login("mystack.user2", "");

    homePage.bubbles.click();
    expect(bubblesPage.bubble('xxx-TestBubble-01').isPresent()).toBeFalsy();
  });

  it("Verify user can't see the deleted bubble on the UI - 'mystack.user3'", function() {
    fn.login("mystack.user3", "");

    homePage.bubbles.click();
    expect(bubblesPage.bubble('xxx-TestBubble-01').isPresent()).toBeFalsy();
  });

  // TODO : Verify teams page got updated after deleting the bubble
});
