/*
*****************************************************************************************************************************************************
SUMMARY:      Creates bubble, adding user to team
ATTENTION:    Should be run after 'UserAndTeams_spec.js' & 'BubbleCreate-01_spec.js'

List Of Tests:
    BubbleCreate-02: User should be notified if fields in bubble create page is empty
    BubbleCreate-02: Verify user able to submit request to create a bubble - 'xxx-TestBubble-02'
    BubbleCreate-02: Verify user gets notifications for bubble create - xxx-TestTeam-02'
    BubbleCreate-02: Verify details in the bubbles page while bubble is created (mystack.user2) - 'xxx-TestBubble-02'
    BubbleCreate-02: Verify status of the bubble is changed while it is created - 'xxx-TestBubble-02'
    BubbleCreate-02: Verify team 'xxx-TestTeam-01' not changed for recently created bubble - 'xxx-TestBubble-02'
    BubbleCreate-02: Verify team 'xxx-TestTeam-02' got updated for recently created bubble - 'xxx-TestBubble-02'
    BubbleCreate-02: Verify details in the bubbles page while bubble is created (mystack.user3) - 'xxx-TestBubble-02'
    BubbleCreate-02: Verify permission check on bubbles page for user with 'CREATOR' role on a team - 'mystack.user3'
    BubbleCreate-02: Verify details in the bubbles page while bubble is created (mystack.user1) - 'xxx-TestBubble-02'
    BubbleCreate-02: Verify permission check on bubbles page for use with 'USER' role on team - 'mystack.user1'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify org 'xxx-TestBubble-02' is created on vCD
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Check Org is enabled for the Bubble 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify Org VApp Lease settings for the bubble 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify Org VApp Template Lease settings for the bubble 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify Org Default Quota settings for the bubble 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify Org Limit settings for the bubble 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify Org Password Policy settings for the bubble 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify Org Federation settings for the bubble 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify Guest Personalisation settings for the bubble 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify Org vDC is enabled for the bubble 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify name and description of Org vDC for the bubble 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify vDC Allocation for the bubble 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify vDC Storage (Enable thin provisioning, Enable fast provisioning & Storage Profile) name for the bubble 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify edge gateway created and also check name of the edge gateway for Master Bubble 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify Description, Edge Gateway configuration, Enable High Availabiltiy in the Edge gateway for the Bubble - 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify Firewall configuration in the Edge gateway for the Bubble - 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify Firewall configuration (Default-RDP-Inbound) in the Edge gateway for the Bubble - 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify Firewall configuration (Default-SSH-Inbound) in the Edge gateway for the Bubble - 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify Firewall configuration (Default-ICMP-Inbound) in the Edge gateway for the Bubble - 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify Firewall configuration (Default-Any-Outbound) in the Edge gateway for the Bubble - 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify networks are created for the bubble 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify FE networks details for the bubble 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify BE networks details for the bubble 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify number of users for the Bubble - 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify details for user mysatck.user1@pearson for bubble - 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify details for user mysatck.user2@pearson for bubble - 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify details for user mysatck.user3@pearson for bubble - 'xxx-TestBubble-02'
    BubbleCreate-02: Bubble creation on vCD - xxx-TestBubble-02: Verify Bubble's Public IP's, Edge gateway IP & networks value in Portal is matching with vCD
*****************************************************************************************************************************************************
*/

// Set the default values
var authorization       = vcdAuthKey;
var headers             = {"Accept": "application/*+xml;version=5.1", "Authorization" : vcdAuthKey};
var orgExists           = false;
var orghref             = "";
var jsonResultOrg       = "";
var guestSettingshref   = "";
var orgvdcExists        = false;
var orgvdchref          = "";
var jsonResultOrgVdc    = "";
var edgeGwqueryhref     = "";
var edgeGwExists        = false;
var edgeGwhref          = "";
var jsonResultEdgeGw    = "";
var firewallExists      = false;
var networkshref        = true;
var feNetworkhref       = "";
var beNetworkhref       = "";
var user1_href          = "";
var user2_href          = "";
var user3_href          = "";

// serviceNow default values
var serviceNowAuthorization = serviceNowAuthKey;
var snowHeaders = {"Accept": "application/json", "Authorization" : serviceNowAuthorization};
var org_id      = ""

var homePage          = require("../../pages/home_page.js");
var usersPage         = require("../../pages/users_page.js");
var teamsPage         = require("../../pages/teams_page.js");
var bubblesPage       = require("../../pages/bubbles_page.js");
var notificationsPage = require("../../pages/notifications_page.js");

describe("BubbleCreate-02", function() {

  beforeAll(function() {
    browser.get('/');
    fn.login("mystack.user1", "");
    fn.clearNotifications();
    fn.login("mystack.user3", "");
    fn.clearNotifications();
    fn.login("mystack.user4", "");
    fn.clearNotifications();
    fn.login("mystack.user5", "");
    fn.clearNotifications();
  });

  beforeEach(function() {
    browser.get('/');
  });

  it("Verify user able to submit request to create a bubble - 'xxx-TestBubble-02'", function() {
    fn.login("mystack.user2", "");
    // Clear the existing notifications on the UI
    fn.clearNotifications();

    homePage.bubbles.click();

    browser.wait(EC.elementToBeClickable(bubblesPage.createBubble), waitSeconds);
    bubblesPage.createBubble.click();
    bubblesPage.createBubbleName.sendKeys("xxx-TestBubble-02");
    bubblesPage.createBubbleChooseTeam('xxx-TestTeam-02');
    bubblesPage.createBubbleChooseRegion('LO3REF - Reference');
    bubblesPage.tandc.click();
    bubblesPage.tandcAccept.click();
    expect(bubblesPage.tandcCheckbox.isSelected()).toBeTruthy();
    bubblesPage.createButton.click();

    // Verify BubbleView page is displayed while submitting request to create bubble
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    expect(bubblesPage.BubbleViewName.getText()).toEqual('xxx-TestBubble-02');
    expect(bubblesPage.BubbleViewDelete.isPresent()).toBeFalsy();
    expect(bubblesPage.BubbleViewRegion.getText()).toEqual('LO3REF');
    expect(bubblesPage.BubbleViewStatus.getText()).toEqual('CREATING');
    expect(bubblesPage.BubbleViewToken.isPresent()).toBeFalsy();
    expect(bubblesPage.BubbleViewTeam.getText()).toEqual('xxx-TestTeam-02');
    expect(bubblesPage.BubbleViewMoveteams.isDisplayed()).toBeFalsy();
    expect(bubblesPage.BubbleViewMember.count()).toEqual(3);
    expect(bubblesPage.BubbleViewMemberByindex(0).getText()).toEqual('mystack user1');
    expect(bubblesPage.BubbleViewMemberByindex(1).getText()).toEqual('mystack user2');
    expect(bubblesPage.BubbleViewMemberByindex(2).getText()).toEqual('mystack user3');
    expect(bubblesPage.BubbleViewAddLocalUser.isDisplayed()).toBeFalsy();
    expect(bubblesPage.BubbleViewAddPublicip.isDisplayed()).toBeFalsy();
    expect(bubblesPage.SnowInput.isPresent()).toBeFalsy();
    // browser.refresh();
    // browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    // expect(element(by.className("BubbleView-networking")).element(by.className("BubbleView-ip")).isDisplayed()).toBeFalsy();
    // expect(bubblesPage.BubbleViewGateway.isDisplayed()).toBeFalsy();
    // expect(bubblesPage.BubbleViewEdgeGateway.isDisplayed()).toBeFalsy();
    // expect(bubblesPage.BubbleViewNetwork.get(0).isDisplayed()).toBeFalsy();
    // expect(bubblesPage.BubbleViewNetwork.get(1).isDisplayed()).toBeFalsy();
    // Verify Bubble View page is closed by pressing the 'Close' button
    bubblesPage.BubbleViewClose.click();
    expect(bubblesPage.BubbleView.isPresent()).toBeFalsy();
  });

  it("Verify user gets notifications for bubble create - xxx-TestTeam-02'", function() {
    fn.waitForNotifications(bubbleCreateWaitSeconds, 15);

    expect(notificationsPage.notificationCount.getText()).toEqual('15');
    homePage.notifications.click();
    // Verify total number of 'NotificationGroup' and click on first one
    expect(notificationsPage.notificationHeader.count()).toEqual(1);
    notificationsPage.notificationHeader.get(0).click();
    expect(notificationsPage.notificationHeader.get(0).getText()).toEqual("Bubble Creating");

    expect(notificationsPage.NotificationsByindex(0).count()).toEqual(15);
    expect(notificationsPage.NotificationsByindex(0).get(0).getText()).toEqual("Organisation xxx-TestBubble-02 has been created");
    expect(notificationsPage.NotificationsByindex(0).get(1).getText()).toEqual("Adding user to xxx-TestBubble-02. Please wait");
    expect(notificationsPage.NotificationsByindex(0).get(7).getText()).toEqual("vDC for xxx-TestBubble-02 has been created");
    expect(notificationsPage.NotificationsByindex(0).get(8).getText()).toEqual("Edge for xxx-TestBubble-02 is creating");
    expect(notificationsPage.NotificationsByindex(0).get(9).getText()).toEqual("Edge for xxx-TestBubble-02 has been created");
    expect(notificationsPage.NotificationsByindex(0).get(10).getText()).toEqual("FE network for xxx-TestBubble-02 is creating");
    expect(notificationsPage.NotificationsByindex(0).get(11).getText()).toEqual("FE network for xxx-TestBubble-02 has been created");
    expect(notificationsPage.NotificationsByindex(0).get(12).getText()).toEqual("BE network for xxx-TestBubble-02 is creating");
    expect(notificationsPage.NotificationsByindex(0).get(13).getText()).toEqual("BE network for xxx-TestBubble-02 has been created");
    expect(notificationsPage.NotificationsByindex(0).get(14).getText()).toEqual("Bubble status is now CREATED");
  });

  it("Verify details in the bubbles page while bubble is created (mystack.user2) - 'xxx-TestBubble-02'", function() {
    homePage.bubbles.click();

    // Verify the bubble details
    expect(bubblesPage.bubbles.count()).toEqual(2);
    expect(bubblesPage.getBubbleNameByindex(0).getText()).toEqual('xxx-TestBubble-01');
    expect(bubblesPage.getBubbleRegionByindex(0).getText()).toEqual('LO3REF');
    expect(bubblesPage.getBubbleTeamByindex(0).getText()).toEqual('xxx-TestTeam-01');
    expect(bubblesPage.getBubbleCreatedDateByindex(0).getText()).toEqual(new Date().toDateString());
    expect(bubblesPage.getBubbleProductionFlagByindex(0).isPresent()).toEqual(true)
    expect(bubblesPage.getBubbleNameByindex(1).getText()).toEqual('xxx-TestBubble-02');
    expect(bubblesPage.getBubbleRegionByindex(1).getText()).toEqual('LO3REF');
    expect(bubblesPage.getBubbleTeamByindex(1).getText()).toEqual('xxx-TestTeam-02');
    expect(bubblesPage.getBubbleCreatedDateByindex(1).getText()).toEqual(new Date().toDateString());
    expect(bubblesPage.getBubbleProductionFlagByindex(1).isPresent()).toEqual(false)
  });

  it("Verify status of the bubble is changed while it is created - 'xxx-TestBubble-02'", function() {
    homePage.bubbles.click();

    expect(bubblesPage.bubbles.count()).toEqual(2);
    // bubblesPage.bubbles.get(1).click();
    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-02')), waitSeconds);
    bubblesPage.bubble('xxx-TestBubble-02').click();

    // Verify BubbleView page is displayed while submitting request to create bubble
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    expect(bubblesPage.BubbleViewName.getText()).toEqual('xxx-TestBubble-02');
    expect(bubblesPage.BubbleViewDelete.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewRegion.getText()).toEqual('LO3REF');
    expect(bubblesPage.BubbleViewStatus.getText()).toEqual('CREATED');
    expect(bubblesPage.BubbleViewToken.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewTeam.getText()).toEqual('xxx-TestTeam-02');
    expect(bubblesPage.BubbleViewMoveteams.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewMember.count()).toEqual(3);
    expect(bubblesPage.BubbleViewMemberByindex(0).getText()).toEqual('mystack user1');
    expect(bubblesPage.BubbleViewMemberByindex(1).getText()).toEqual('mystack user2');
    expect(bubblesPage.BubbleViewMemberByindex(2).getText()).toEqual('mystack user3');
    expect(bubblesPage.BubbleViewAddLocalUser.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewAddPublicip.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewip).toEqual([]);
    expect(bubblesPage.BubbleViewGateway.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewEdgeGateway.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewNetwork.get(0).isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewNetwork.get(1).isDisplayed()).toBeTruthy();
    expect(bubblesPage.SnowInput.isPresent()).toBeTruthy();
    // Verify Bubble View page is closed by pressing the 'Close' button
    bubblesPage.BubbleViewClose.click();
    expect(bubblesPage.BubbleView.isPresent()).toBeFalsy();
  });

  it("Verify user able to add 'Notes' to the bubble", function() {
    homePage.bubbles.click();

    expect(bubblesPage.bubbles.count()).toEqual(2);
    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-02')), waitSeconds);
    bubblesPage.bubble('xxx-TestBubble-02').click();

    // Verify BubbleView page is displayed while submitting request to create bubble
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);

    bubblesPage.BubbleViewNotes.clear().sendKeys("This is the notes created by the Test Automation script. Bubble 'xxx-TestBubble-02'");
    bubblesPage.BubbleViewClose.click();

    // Verify notes are visible
    browser.refresh();
    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-02')), waitSeconds);
    bubblesPage.bubble('xxx-TestBubble-02').click();
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    expect(bubblesPage.BubbleViewNotes.getText()).toEqual("This is the notes created by the Test Automation script. Bubble 'xxx-TestBubble-02'");
    bubblesPage.BubbleViewClose.click();
  });


  it("Verify team 'xxx-TestTeam-01' not changed for recently created bubble - 'xxx-TestBubble-02' for user 'mystack.user2'", function() {
    homePage.teams.click();
    expect(teamsPage.teams.count()).toEqual(2);
    expect(teamsPage.getTeamNameByindex(0).getText()).toEqual('xxx-TestTeam-01');
    expect(teamsPage.getTeamBubblesByindex(0).getText()).toEqual('1');

    // Details in TeamView page
    browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-01')), waitSeconds);
    teamsPage.team('xxx-TestTeam-01').click();
    // teamsPage.team('xxx-TestTeam-01').click();
    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-01');
    // Verify the bubble details
    expect(teamsPage.TeamViewBubbles.count()).toEqual(1);
    expect(teamsPage.TeamViewBubbleNameByindex(0).getText()).toEqual('xxx-TestBubble-01 (LO3REF)');
  });

  it("Verify team 'xxx-TestTeam-02' got updated for recently created bubble - 'xxx-TestBubble-02' for user 'mystack.user2", function() {
    homePage.teams.click();
    expect(teamsPage.teams.count()).toEqual(2);
    expect(teamsPage.getTeamNameByindex(1).getText()).toEqual('xxx-TestTeam-02');
    expect(teamsPage.getTeamBubblesByindex(1).getText()).toEqual('1');
    expect(teamsPage.getTeamMemberCountByindex(1).getText()).toEqual('3');

    // Details in TeamView page
    // teamsPage.team('xxx-TestTeam-02').click();
    browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-02')), waitSeconds);
    teamsPage.team('xxx-TestTeam-02').click();
    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-02');
    expect(teamsPage.teamViewDelete.isPresent()).toBeFalsy();
    expect(teamsPage.teamViewAddMember.isPresent()).toBeTruthy();
    expect(teamsPage.teamViewAddMember.isDisplayed()).toBeTruthy();
    expect(teamsPage.teamViewMembers.count()).toEqual(3);
    // Verify the bubble details
    expect(teamsPage.TeamViewBubbles.count()).toEqual(1);
    expect(teamsPage.TeamViewBubbleNameByindex(0).getText()).toEqual('xxx-TestBubble-02 (LO3REF)');
  });

  it("Verify user (mystack.user2) able to navigate to Bubbles page by clicking bubble name in Teams page - 'xxx-TestBubble-01'", function() {
    homePage.teams.click();
    browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-01')), waitSeconds);
    teamsPage.team('xxx-TestTeam-01').click();
    // teamsPage.team('xxx-TestTeam-01').click();

    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(browser.getCurrentUrl()).toContain(homePageUrl + "teams/");
    teamsPage.TeamViewBubbleByname('xxx-TestBubble-01').click();

    // Verify bubbles page is loaded
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    expect(browser.getCurrentUrl()).toContain(homePageUrl + "bubbles/");
    expect(bubblesPage.BubbleViewName.getText()).toEqual('xxx-TestBubble-01');
  });

  it("Verify user (mystack.user2) able to navigate to Bubbles page by clicking bubble name in Teams page - 'xxx-TestBubble-02'", function() {
    homePage.teams.click();
    browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-02')), waitSeconds);
    teamsPage.team('xxx-TestTeam-02').click();

    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(browser.getCurrentUrl()).toContain(homePageUrl + "teams/");
    teamsPage.TeamViewBubbleByname('xxx-TestBubble-02').click();

    // Verify bubbles page is loaded
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    expect(browser.getCurrentUrl()).toContain(homePageUrl + "bubbles/");
    expect(bubblesPage.BubbleViewName.getText()).toEqual('xxx-TestBubble-02');
  });

  it("Verify user (mystack.user2) able to login vCD by clicking 'Open vCloud' link on the Bubbles page - 'xxx-testbubble-01'", function() {
    homePage.bubbles.click();
    expect(bubblesPage.bubbles.count()).toEqual(2);
    bubblesPage.BubblevCloudLink(0).click();

    browser.getAllWindowHandles().then(function (handles) {
      if (handles.length == 2) {
        browser.switchTo().window(handles[1]);
        expect(browser.getCurrentUrl()).toContain("https://lo3ref-mystack.pearson.com/cloud/org/xxx-testbubble-01/");
        browser.close();

        // Change focus back to Original window - myStack portal
        browser.switchTo().window(handles[0]);
        expect(browser.getCurrentUrl()).toContain(homePageUrl + "bubbles/");
      } else {
        err_msg = "New windows might not be opened while clicking the 'Knowledge base | Learn'. Actual number of windows - " + handles.length;
        this.fail(err_msg);
      }
    });
  });

  it("Verify user (mystack.user2) able to login vCD by clicking 'Open vCloud' link on the Bubbles page - 'xxx-testbubble-02'", function() {
    homePage.bubbles.click();
    expect(bubblesPage.bubbles.count()).toEqual(2);
    bubblesPage.BubblevCloudLink(1).click();

    browser.getAllWindowHandles().then(function (handles) {
      if (handles.length == 2) {
        browser.switchTo().window(handles[1]);
        expect(browser.getCurrentUrl()).toContain("https://lo3ref-mystack.pearson.com/cloud/org/xxx-testbubble-02/");
        browser.close();

        // Change focus back to Original window - myStack portal
        browser.switchTo().window(handles[0]);
        expect(browser.getCurrentUrl()).toContain(homePageUrl + "bubbles/");
      } else {
        err_msg = "New windows might not be opened while clicking the 'Knowledge base | Learn'. Actual number of windows - " + handles.length;
        this.fail(err_msg);
      }
    });
  });

  it("Verify user 'mystack.user3' gets notifications for bubble creation - 'xxx-TestBubble-02'", function() {
    fn.login("mystack.user3", "");

    fn.waitForNotifications(10000, 1);

    expect(notificationsPage.notificationCount.getText()).toEqual('15');
    homePage.notifications.click();
    // Verify total number of 'NotificationGroup' and click on first one
    expect(notificationsPage.notificationHeader.count()).toEqual(1);
    notificationsPage.notificationHeader.get(0).click();

    expect(notificationsPage.notificationHeader.get(0).getText()).toEqual("Bubble Creating");
    expect(notificationsPage.NotificationsByindex(0).count()).toEqual(15);
    expect(notificationsPage.NotificationsByindex(0).get(14).getText()).toEqual("Bubble status is now CREATED");
  });

  it("Verify details in the bubbles page while bubble is created (mystack.user3) - 'xxx-TestBubble-02'", function() {
    homePage.bubbles.click();

    // Verify the bubble details
    expect(bubblesPage.bubbles.count()).toEqual(2);
    expect(bubblesPage.getBubbleNameByindex(0).getText()).toEqual('xxx-TestBubble-01');
    expect(bubblesPage.getBubbleRegionByindex(0).getText()).toEqual('LO3REF');
    expect(bubblesPage.getBubbleTeamByindex(0).getText()).toEqual('xxx-TestTeam-01');
    expect(bubblesPage.getBubbleCreatedDateByindex(0).getText()).toEqual(new Date().toDateString());
    expect(bubblesPage.getBubbleProductionFlagByindex(0).isPresent()).toEqual(true)
    expect(bubblesPage.getBubbleNameByindex(1).getText()).toEqual('xxx-TestBubble-02');
    expect(bubblesPage.getBubbleRegionByindex(1).getText()).toEqual('LO3REF');
    expect(bubblesPage.getBubbleTeamByindex(1).getText()).toEqual('xxx-TestTeam-02');
    expect(bubblesPage.getBubbleCreatedDateByindex(1).getText()).toEqual(new Date().toDateString());
    expect(bubblesPage.getBubbleProductionFlagByindex(1).isPresent()).toEqual(false)
  });

  it("Verify permission check on bubbles page for user with 'CREATOR' role on a team - 'mystack.user3'", function() {
    homePage.bubbles.click();
    expect(bubblesPage.bubbles.count()).toEqual(2);
    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-02')), waitSeconds);
    bubblesPage.bubble('xxx-TestBubble-02').click();
    // element.all(by.className("Bubble")).get(1).click();

    // Verify BubbleView page is displayed while submitting request to create bubble
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    expect(bubblesPage.BubbleViewName.getText()).toEqual('xxx-TestBubble-02');
    expect(bubblesPage.BubbleViewDelete.isPresent()).toBeFalsy();
    expect(bubblesPage.BubbleViewRegion.getText()).toEqual('LO3REF');
    expect(bubblesPage.BubbleViewStatus.getText()).toEqual('CREATED');
    expect(bubblesPage.BubbleViewToken.isPresent()).toBeFalsy();
    expect(bubblesPage.BubbleViewTeam.getText()).toEqual('xxx-TestTeam-02');
    expect(bubblesPage.BubbleViewMoveteams.isDisplayed()).toBeFalsy();
    expect(bubblesPage.BubbleViewMember.count()).toEqual(3);
    expect(bubblesPage.BubbleViewMemberByindex(0).getText()).toEqual('mystack user1');
    expect(bubblesPage.BubbleViewMemberByindex(1).getText()).toEqual('mystack user2');
    expect(bubblesPage.BubbleViewMemberByindex(2).getText()).toEqual('mystack user3');
    expect(bubblesPage.BubbleViewAddLocalUser.isDisplayed()).toBeFalsy();
    expect(bubblesPage.BubbleViewAddPublicip.isDisplayed()).toBeFalsy();
    expect(bubblesPage.BubbleViewip).toEqual([]);
    expect(bubblesPage.BubbleViewGateway.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewEdgeGateway.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewNetwork.get(0).isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewNetwork.get(1).isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewNotes.getText()).toEqual("This is the notes created by the Test Automation script. Bubble 'xxx-TestBubble-02'");
    expect(bubblesPage.SnowInput.isPresent()).toBeFalsy();
    // Verify Bubble View page is closed by pressing the 'Close' button
    bubblesPage.BubbleViewClose.click();
    expect(bubblesPage.BubbleView.isPresent()).toBeFalsy();
  });

  it("Verify team 'xxx-TestTeam-01' not changed for recently created bubble - 'xxx-TestBubble-02' for user 'mystack.user3'", function() {
    homePage.teams.click();
    expect(teamsPage.teams.count()).toEqual(2);
    expect(teamsPage.getTeamNameByindex(0).getText()).toEqual('xxx-TestTeam-01');

    // Details in TeamView page
    teamsPage.team('xxx-TestTeam-01').click();
    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-01');
    expect(teamsPage.teamViewMembers.count()).toEqual(4);
    // Verify the bubble details
    expect(teamsPage.TeamViewBubbles.count()).toEqual(1);
    expect(teamsPage.TeamViewBubbleNameByindex(0).getText()).toEqual('xxx-TestBubble-01 (LO3REF)');
  });

  it("Verify team 'xxx-TestTeam-02' got updated for recently created bubble - 'xxx-TestBubble-02' for user 'mystack.user3", function() {
    homePage.teams.click();
    expect(teamsPage.teams.count()).toEqual(2);
    expect(teamsPage.getTeamNameByindex(1).getText()).toEqual('xxx-TestTeam-02');
    expect(teamsPage.getTeamBubblesByindex(1).getText()).toEqual('1');
    expect(teamsPage.getTeamMemberCountByindex(1).getText()).toEqual('3');

    // Details in TeamView page
    teamsPage.team('xxx-TestTeam-02').click();
    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-02');
    expect(teamsPage.teamViewDelete.isPresent()).toBeFalsy();
    expect(teamsPage.teamViewAddMember.isPresent()).toBeFalsy();
    expect(teamsPage.teamViewMembers.count()).toEqual(3);
    // Verify the bubble details
    expect(teamsPage.TeamViewBubbleNameByindex(0).getText()).toEqual('xxx-TestBubble-02 (LO3REF)');
  });

  it("Verify user 'mystack.user1' gets notifications for bubble creation - 'xxx-TestBubble-02'", function() {
    fn.login("mystack.user1", "");

    fn.waitForNotifications(10000, 1);

    expect(notificationsPage.notificationCount.getText()).toEqual('15');
    homePage.notifications.click();
    // Verify total number of 'NotificationGroup' and click on first one
    expect(notificationsPage.notificationHeader.count()).toEqual(1);
    notificationsPage.notificationHeader.get(0).click();

    expect(notificationsPage.notificationHeader.get(0).getText()).toEqual("Bubble Creating");
    expect(notificationsPage.NotificationsByindex(0).count()).toEqual(15);
    expect(notificationsPage.NotificationsByindex(0).get(14).getText()).toEqual("Bubble status is now CREATED");
  });

  it("Verify details in the bubbles page while bubble is created (mystack.user1) - 'xxx-TestBubble-02'", function() {
    homePage.bubbles.click();

    // Verify the bubble details
    expect(bubblesPage.bubbles.count()).toEqual(2);
    expect(bubblesPage.getBubbleNameByindex(0).getText()).toEqual('xxx-TestBubble-01');
    expect(bubblesPage.getBubbleRegionByindex(0).getText()).toEqual('LO3REF');
    expect(bubblesPage.getBubbleTeamByindex(0).getText()).toEqual('xxx-TestTeam-01');
    expect(bubblesPage.getBubbleCreatedDateByindex(0).getText()).toEqual(new Date().toDateString());
    expect(bubblesPage.getBubbleProductionFlagByindex(0).isPresent()).toEqual(true)
    expect(bubblesPage.getBubbleNameByindex(1).getText()).toEqual('xxx-TestBubble-02');
    expect(bubblesPage.getBubbleRegionByindex(1).getText()).toEqual('LO3REF');
    expect(bubblesPage.getBubbleTeamByindex(1).getText()).toEqual('xxx-TestTeam-02');
    expect(bubblesPage.getBubbleCreatedDateByindex(1).getText()).toEqual(new Date().toDateString());
    expect(bubblesPage.getBubbleProductionFlagByindex(1).isPresent()).toEqual(false)
  });

  it("Verify permission check on bubbles page for use with 'USER' role on team - 'mystack.user1'", function() {
    homePage.bubbles.click();
    expect(element.all(by.className("Bubble")).count()).toEqual(2);
    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-02')), waitSeconds);
    bubblesPage.bubble('xxx-TestBubble-02').click();
    // element.all(by.className("Bubble")).get(1).click();

    // Verify BubbleView page is displayed while submitting request to create bubble
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    expect(bubblesPage.BubbleViewName.getText()).toEqual('xxx-TestBubble-02');
    expect(bubblesPage.BubbleViewDelete.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewRegion.getText()).toEqual('LO3REF');
    expect(bubblesPage.BubbleViewStatus.getText()).toEqual('CREATED');
    expect(bubblesPage.BubbleViewToken.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewTeam.getText()).toEqual('xxx-TestTeam-02');
    expect(bubblesPage.BubbleViewMoveteams.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewMember.count()).toEqual(3);
    expect(bubblesPage.BubbleViewAddLocalUser.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewAddPublicip.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewip).toEqual([]);
    expect(bubblesPage.BubbleViewGateway.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewEdgeGateway.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewNetwork.get(0).isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewNetwork.get(1).isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewNotes.getText()).toEqual("This is the notes created by the Test Automation script. Bubble 'xxx-TestBubble-02'");
    expect(bubblesPage.SnowInput.isPresent()).toBeTruthy();
    // Verify Bubble View page is closed by pressing the 'Close' button
    bubblesPage.BubbleViewClose.click();
    expect(bubblesPage.BubbleView.isPresent()).toBeFalsy();
  });

  it("Verify team 'xxx-TestTeam-01' not changed for recently created bubble - 'xxx-TestBubble-02' for user 'mystack.user1'", function() {
    homePage.teams.click();
    expect(teamsPage.teams.count()).toEqual(2);
    expect(teamsPage.getTeamNameByindex(0).getText()).toEqual('xxx-TestTeam-01');

    // Details in TeamView page
    teamsPage.team('xxx-TestTeam-01').click();
    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-01');
    expect(teamsPage.teamViewMembers.count()).toEqual(4);
    // Verify the bubble details
    expect(teamsPage.TeamViewBubbles.count()).toEqual(1);
    expect(teamsPage.TeamViewBubbleNameByindex(0).getText()).toEqual('xxx-TestBubble-01 (LO3REF)');
  });

  it("Verify team 'xxx-TestTeam-02' got updated for recently created bubble - 'xxx-TestBubble-02' for user 'mystack.user1'", function() {
    homePage.teams.click();
    expect(teamsPage.teams.count()).toEqual(2);
    expect(teamsPage.getTeamNameByindex(1).getText()).toEqual('xxx-TestTeam-02');
    expect(teamsPage.getTeamBubblesByindex(1).getText()).toEqual('1');
    expect(teamsPage.getTeamMemberCountByindex(1).getText()).toEqual('3');

    // Details in TeamView page
    teamsPage.team('xxx-TestTeam-02').click();
    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-02');
    expect(teamsPage.teamViewDelete.isPresent()).toBeFalsy();
    expect(teamsPage.teamViewAddMember.isPresent()).toBeTruthy();
    expect(teamsPage.teamViewAddMember.isDisplayed()).toBeTruthy();
    expect(teamsPage.teamViewMembers.count()).toEqual(3);
    // Verify the bubble details
    expect(teamsPage.TeamViewBubbleNameByindex(0).getText()).toEqual('xxx-TestBubble-02 (LO3REF)');
  });

  it("Verify user who (mystack.user4) is not part of a team didn't get notifications for bubble create - 'xxx-TestBubble-02'", function() {
    fn.login("mystack.user4", "");

    // Verify there is not notification for user 'mystack.user4'
    fn.waitForNotifications(2000, 1);
    homePage.notifications.click();
    expect(notificationsPage.notificationHeader.count()).toEqual(0);
  });

  it("Verify user who (mystack.user4) is not part of a team didn't get notifications for bubble create - 'xxx-TestBubble-02'", function() {
    fn.login("mystack.user5", "");

    // Verify there is not notification for user 'mystack.user5'
    fn.waitForNotifications(2000, 1);
    homePage.notifications.click();
    expect(notificationsPage.notificationHeader.count()).toEqual(0);
  });

  xdescribe('Bubble creation on vCD - xxx-TestBubble-02', function() {

    it("Verify org 'xxx-TestBubble-02' is created on vCD", function() {
      response = fn.runHttpRequests("POST", headers, vcd + '/api/sessions');
      if (response.statusCode == 200) {
        headers = {"Accept": "application/*+xml;version=5.1", "x-vcloud-authorization": response.headers['x-vcloud-authorization']};

        // Run the Query and get list of Orgs
        response = fn.runHttpRequests("GET", headers, vcd + '/api/admin');
        jsonResult = xmlobj.parseXML(response.body.toString());
        for(var key in jsonResult.VCloud.OrganizationReferences.OrganizationReference) {
          if (jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-name"] == "xxx-TestBubble-02") {
            orgExists = true;
            orghref = jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-href"];
            break;
          }
        }
        expect("Org 'xxx-TestBubble-02' exits - true").toEqual("Org 'xxx-TestBubble-02' exits - " + orgExists);
      } else {
        this.fail("Failed to get the auth key. statusCode - ", response.statusCode);
      }
    });

    it("Check Org is enabled for the Bubble 'xxx-TestBubble-02'", function() {
      var errorMessage;
      if (orgExists) {
        response = fn.runHttpRequests("GET", headers, orghref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResultOrg = xmlobj.parseXML(response.body.toString());

          var orgEnabled = jsonResultOrg.AdminOrg.IsEnabled;
          // Verify the name of the vDC
          expect("Bubble xxx-TestBubble-02 enabled - " + orgEnabled).toEqual("Bubble xxx-TestBubble-02 enabled - true");

          // Loop through each Org and check Org vDC created - Set the FLAG
          if (jsonResultOrg.AdminOrg.Vdcs !== undefined) {
            if (jsonResultOrg.AdminOrg.Vdcs.Vdc["-type"] == "application/vnd.vmware.admin.vdc+xml") {
              orgvdcExists  = true;
              orgvdchref    = jsonResultOrg.AdminOrg.Vdcs.Vdc["-href"];
            }
          }
          // Loop through org json response and get the href for Guest Personalisation
          for(var key in jsonResultOrg.AdminOrg.Settings.Link) {
            if (jsonResultOrg.AdminOrg.Settings.Link[key]["-type"] == "application/vnd.vmware.admin.guestPersonalizationSettings+xml") {
              guestSettingshref = jsonResultOrg.AdminOrg.Settings.Link[key]["-href"];
              break;
            }
          }
        // orgUrl ran okey?
        } else {
          errorMessage = "Failed to run the URL - " + orghref + ". statusCode - " + response.statusCode + ". So can't verify whether Org vdc is created Or not";
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "Org not created for bubble xxx-TestBubble-02. So can't verify whether Org vdc is created Or not";
        this.fail(errorMessage);
      }
    });

    it("Verify Org VApp Lease settings for the bubble 'xxx-TestBubble-02'", function() {
      if (orgExists) {
        // vApp lease
        var maximumRuntimeLeasevApp   = jsonResultOrg.AdminOrg.Settings.VAppLeaseSettings.DeploymentLeaseSeconds;
        var maximumStorageLeasevApp   = jsonResultOrg.AdminOrg.Settings.VAppLeaseSettings.StorageLeaseSeconds;
        var storageCleanupvApp        = jsonResultOrg.AdminOrg.Settings.VAppLeaseSettings.DeleteOnStorageLeaseExpiration;
        // Verify the Maximum runtime lease, Maximum storage lease, Storage cleanup for vApp lease
        expect("vApp leases | Maximum runtime lease - " + maximumRuntimeLeasevApp).toEqual("vApp leases | Maximum runtime lease - 0");
        expect("vApp leases | Maximum storage lease - " + maximumStorageLeasevApp).toEqual("vApp leases | Maximum storage lease - 0");
        expect("vApp leases | Storage cleanup - "       + storageCleanupvApp).toEqual("vApp leases | Storage cleanup - false");
      } else {
        var errorMessage = "orgExists - " + orgExists + " for bubble xxx-TestBubble-02. So can't verify Org vApp lease settings for Bubble xxx-TestBubble-02";
        this.fail(errorMessage);
      }
    });

    it("Verify Org VApp Template Lease settings for the bubble 'xxx-TestBubble-02'", function() {
      if (orgExists) {
        // vApp Template lease
        var maximumStorageLeasevAppTemplate   = jsonResultOrg.AdminOrg.Settings.VAppTemplateLeaseSettings.StorageLeaseSeconds;
        var storageCleanupvAppTemplate        = jsonResultOrg.AdminOrg.Settings.VAppTemplateLeaseSettings.DeleteOnStorageLeaseExpiration;
        // Verify the Maximum storage lease, Storage cleanup for vApp Template lease
        expect("vApp template lease | Maximum storage lease - " + maximumStorageLeasevAppTemplate).toEqual("vApp template lease | Maximum storage lease - 0");
        expect("vApp template lease | Storage cleanup - "       + storageCleanupvAppTemplate).toEqual("vApp template lease | Storage cleanup - false");
      } else {
        var errorMessage = "orgExists - " + orgExists + " for bubble xxx-TestBubble-02. So can't verify Org vApp lease settings for Bubble xxx-TestBubble-02";
        this.fail(errorMessage);
      }
    });

    it("Verify Org Default Quota settings for the bubble 'xxx-TestBubble-02'", function() {
      if (orgExists) {
        var allVMQuota      = jsonResultOrg.AdminOrg.Settings.OrgGeneralSettings.StoredVmQuota;
        var runningVMQuota  = jsonResultOrg.AdminOrg.Settings.OrgGeneralSettings.DeployedVMQuota;
        // Verify the CPU Quota, Memory Quota anx Maximum number of VMs user can create
        expect("All VMs quota - "     + allVMQuota).toEqual("All VMs quota - 0");
        expect("Running VMs quota - " + runningVMQuota).toEqual("Running VMs quota - 0");
      } else {
        var errorMessage = "orgExists - " + orgExists + " for bubble xxx-TestBubble-02. So can't verify Org vApp lease settings for Bubble xxx-TestBubble-02";
        this.fail(errorMessage);
      }
    });

    it("Verify Org Limit settings for the bubble 'xxx-TestBubble-02'", function() {
      if (orgExists) {
        var OperationsPerUser   = jsonResultOrg.AdminOrg.Settings.OrgOperationLimitsSettings.OperationsPerUser;
        var OperationsPerOrg    = jsonResultOrg.AdminOrg.Settings.OrgOperationLimitsSettings.OperationsPerOrg;
        var ConsolesPerVmLimit  = jsonResultOrg.AdminOrg.Settings.OrgOperationLimitsSettings.ConsolesPerVmLimit;
        // Verify the CPU Quota, Memory Quota anx Maximum number of VMs user can create
        expect("Number of resource intensive operations per user - "          + OperationsPerUser).toEqual("Number of resource intensive operations per user - 0");
        expect("Number of resource intensive operations per organization - "  + OperationsPerOrg).toEqual("Number of resource intensive operations per organization - 0");
        expect("Number of simultaneous connections per VM - "                 + ConsolesPerVmLimit).toEqual("Number of simultaneous connections per VM - 0");
      } else {
        var errorMessage = "orgExists - " + orgExists + " for bubble xxx-TestBubble-02. So can't verify Org vApp lease settings for Bubble xxx-TestBubble-02";
        this.fail(errorMessage);
      }
    });

    it("Verify Org Password Policy settings for the bubble 'xxx-TestBubble-02'", function() {
      if (orgExists) {
        var accountLockout                = jsonResultOrg.AdminOrg.Settings.OrgPasswordPolicySettings.AccountLockoutEnabled;
        var invalidLoginsBeforeLockout    = jsonResultOrg.AdminOrg.Settings.OrgPasswordPolicySettings.InvalidLoginsBeforeLockout;
        var accountLockoutIntervalMinutes = jsonResultOrg.AdminOrg.Settings.OrgPasswordPolicySettings.AccountLockoutIntervalMinutes;

        // Verify Account lock-out values
        expect("Account lockout enabled - "             + accountLockout).toEqual("Account lockout enabled - true");
        expect("Invalid logins before lockout - "       + invalidLoginsBeforeLockout).toEqual("Invalid logins before lockout - 5");
        expect("Account lockout interval - "            + accountLockoutIntervalMinutes).toEqual("Account lockout interval - 2147483647");
      } else {
        var errorMessage = "orgExists - " + orgExists + " for bubble xxx-TestBubble-02. So can't verify Org vApp lease settings for Bubble xxx-TestBubble-02";
        this.fail(errorMessage);
      }
    });

    it("Verify Org Federation settings for the bubble 'xxx-TestBubble-02'", function() {
      if (orgExists) {
        var OrgFederationSettings = jsonResultOrg.AdminOrg.Settings.OrgFederationSettings.Enabled;
        // Verify the CPU Quota, Memory Quota anx Maximum number of VMs user can create
        expect("Org Federation settings - " + OrgFederationSettings).toEqual("Org Federation settings - false");
      } else {
        var errorMessage = "orgExists - " + orgExists + " for bubble xxx-TestBubble-02. So can't verify Org Org Federation settings for Bubble xxx-TestBubble-02";
        this.fail(errorMessage);
      }
    });

    it("Verify Guest Personalisation settings for the bubble 'xxx-TestBubble-02'", function() {
      var errorMessage, jsonResult;
      if (guestSettingshref !== "") {
        response = fn.runHttpRequests("GET", headers, guestSettingshref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResult = xmlobj.parseXML(response.body.toString());

          var AllowDomainSettings = jsonResult.OrgGuestPersonalizationSettings.AllowDomainSettings;
          expect("AllowDomainSettings - " + AllowDomainSettings).toEqual("AllowDomainSettings - false");
        } else {
          errorMessage = "Failed while running - " + guestSettingshref;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + " for bubble xxx-TestBubble-02. So can't verify whether org is enabled Or not";
        this.fail(errorMessage);
      }
    });

    it("Verify Org vDC is enabled for the bubble 'xxx-TestBubble-02'", function() {
      var errorMessage;
      if (orgvdcExists) {
        response = fn.runHttpRequests("GET", headers, orgvdchref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResultOrgVdc = xmlobj.parseXML(response.body.toString());

          // Loop through and get the href for Edgegateway Query
          for(var key in jsonResultOrgVdc.AdminVdc.Link) {
            if (jsonResultOrgVdc.AdminVdc.Link[key]["-type"] == "application/vnd.vmware.vcloud.query.records+xml" && jsonResultOrgVdc.AdminVdc.Link[key]["-rel"] == "edgeGateways") {
              edgeGwqueryhref = jsonResultOrgVdc.AdminVdc.Link[key]["-href"];
              break;
            }
          }

          // Loop through and get the href for Edgegateway Query
          for(key in jsonResultOrgVdc.AdminVdc.Link) {
            if (jsonResultOrgVdc.AdminVdc.Link[key]["-type"] == "application/vnd.vmware.vcloud.query.records+xml" && jsonResultOrgVdc.AdminVdc.Link[key]["-rel"] == "orgVdcNetworks") {
              networkshref = jsonResultOrgVdc.AdminVdc.Link[key]["-href"];
              break;
            }
          }

          // Verify Org vDC enabled ?.
          expect("Org vDC enabled for bubble xxx-TestBubble-02 - " + jsonResultOrgVdc.AdminVdc.IsEnabled).toEqual("Org vDC enabled for bubble xxx-TestBubble-02 - true");
        } else {
          errorMessage = "Failed while running - " + orgvdchref;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + " & orgvdcExists - " + orgvdcExists + " for bubble xxx-TestBubble-02. So can't verify whether org is enabled Or not";
        this.fail(errorMessage);
      }
    });

    it("Verify name and description of Org vDC for the bubble 'xxx-TestBubble-02'", function() {
      var errorMessage;
      if (orgvdcExists) {
        // Verify Org vDC enabled ?.
        expect("Name of Org vDC bubble xxx-TestBubble-02 - " + jsonResultOrgVdc.AdminVdc["-name"]).toEqual("Name of Org vDC bubble xxx-TestBubble-02 - xxx-TestBubble-02-vdc");
        expect("Description for vDC bubble xxx-TestBubble-02 - " + jsonResultOrgVdc.AdminVdc.Description).toEqual("Description for vDC bubble xxx-TestBubble-02 - undefined");
      } else {
        errorMessage = "orgExists - " + orgExists + " . orgvdcExists - " + orgvdcExists + " for bubble xxx-TestBubble-02. So can't verify name of org vDC for bubble xxx-TestBubble-02";
        this.fail(errorMessage);
      }
    });

    it("Verify vDC Allocation for the bubble 'xxx-TestBubble-02'", function() {
      var errorMessage;
      if (orgvdcExists) {
        var CpuQuota                  = jsonResultOrgVdc.AdminVdc.ComputeCapacity.Cpu.Limit;
        var VCpuInMhz                 = jsonResultOrgVdc.AdminVdc.VCpuInMhz;
        var CpuResourceGuaranteed     = jsonResultOrgVdc.AdminVdc.ResourceGuaranteedCpu;
        var MemoryResourceGuaranteed  = jsonResultOrgVdc.AdminVdc.ResourceGuaranteedMemory;
        var Memoryquota               = jsonResultOrgVdc.AdminVdc.ComputeCapacity.Memory.Limit;
        var maximumVMs                = jsonResultOrgVdc.AdminVdc.VmQuota;
        // Verify the CPU Quota, Memory Quota anx Maximum number of VMs user can create
        expect("CPU quota - "                 + CpuQuota).toEqual("CPU quota - 0");
        expect("vCPU speed - "                + VCpuInMhz).toEqual("vCPU speed - 2000");
        expect("CpuResourceGuaranteed - "     + CpuResourceGuaranteed).toEqual("CpuResourceGuaranteed - 0.0");
        expect("Memory quota - "              + Memoryquota).toEqual("Memory quota - 0");
        expect("Maximum number of VMs - "     + maximumVMs).toEqual("Maximum number of VMs - 0");
        expect("MemoryResourceGuaranteed - "  + MemoryResourceGuaranteed).toEqual("MemoryResourceGuaranteed - 0.0");
      } else {
        errorMessage = "orgExists - " + orgExists + " . orgvdcExists - " + orgvdcExists + " for bubble xxx-TestBubble-02. So can't verify name of org vDC for bubble xxx-TestBubble-02";
        this.fail(errorMessage);
      }
    });

    it("Verify vDC Storage (Enable thin provisioning, Enable fast provisioning & Storage Profile) name for the bubble 'xxx-TestBubble-02'", function() {
      var errorMessage;
      if (orgvdcExists && jsonResultOrgVdc) {
        // Verify the Storage Profile, Enable thin provisioning & Enable fast provisioning values
        var VdcStorageProfile = jsonResultOrgVdc.AdminVdc.VdcStorageProfiles.VdcStorageProfile["-name"];
        var thinProvisioning  = jsonResultOrgVdc.AdminVdc.IsThinProvision;
        var fastProvisioning  = jsonResultOrgVdc.AdminVdc.UsesFastProvisioning;

        // Verify the Storage Profile
        expect("Storage Profile - "          + VdcStorageProfile).toEqual("Storage Profile - Gold-Standard");
        expect("Enable thin provisioning - " + thinProvisioning).toEqual("Enable thin provisioning - false");
        expect("Enable fast provisioning - " + fastProvisioning).toEqual("Enable fast provisioning - true");
      } else {
        errorMessage = "orgExists - " + orgExists + " . orgvdcExists - " + orgvdcExists + " for bubble xxx-TestBubble-02. So can't verify name of org vDC for bubble xxx-TestBubble-02";
        this.fail(errorMessage);
      }
    });

    it("Verify edge gateway created and also check name of the edge gateway for Master Bubble 'xxx-TestBubble-02'", function() {
      var errorMessage;
      if (edgeGwqueryhref !== "") {
        response = fn.runHttpRequests("GET", headers, edgeGwqueryhref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResult = xmlobj.parseXML(response.body.toString());

          if (jsonResult.QueryResultRecords.EdgeGatewayRecord !== undefined) {
            if (jsonResult.QueryResultRecords.EdgeGatewayRecord["-numberOfExtNetworks"] == "1") {
              edgeGwExists = true;
              edgeGwname   = jsonResult.QueryResultRecords.EdgeGatewayRecord["-name"];
              edgeGwhref   = jsonResult.QueryResultRecords.EdgeGatewayRecord["-href"];

              // Verify name of teh Edge Gateway
              expect("Name of edgegateway for bubble xxx-TestBubble-02 - " + edgeGwname).toEqual("Name of edgegateway for bubble xxx-TestBubble-02 - xxx-TestBubble-02");
            }
          }
          // Edge Gateway exists ?.
          expect("Edge Gateway created for bubble xxx-TestBubble-02 - " + edgeGwExists).toEqual("Edge Gateway created for bubble xxx-TestBubble-02 - true");
        } else {
          errorMessage = "Failed while running - " + edgeGwqueryhref;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + " , orgvdcExists - " + orgvdcExists + "& edgeGwqueryhref - " + edgeGwqueryhref + " for bubble xxx-TestBubble-02. So can't verify whether edge gate is created Or not";
        this.fail(errorMessage);
      }
    });

    it("Verify Description, Edge Gateway configuration, Enable High Availabiltiy in the Edge gateway for the Bubble - 'xxx-TestBubble-02'", function() {
      var errorMessage;
      if (edgeGwExists) {
        response = fn.runHttpRequests("GET", headers, edgeGwhref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResultEdgeGw = xmlobj.parseXML(response.body.toString());

          var Configuration      = jsonResultEdgeGw.EdgeGateway.Configuration.GatewayBackingConfig;
          var HaEnabled          = jsonResultEdgeGw.EdgeGateway.Configuration.HaEnabled;
          var UseDefaultRouteForDnsRelay = jsonResultEdgeGw.EdgeGateway.Configuration.UseDefaultRouteForDnsRelay;

          //expect("Description - "                + Description).toEqual("Description - edgegateway description");
          expect("Edge Gateway configuration - " + Configuration).toEqual("Edge Gateway configuration - full");
          expect("Enable High Availabiltiy - "   + HaEnabled).toEqual("Enable High Availabiltiy - true");
          expect("UseDefaultRouteForDnsRelay - " + UseDefaultRouteForDnsRelay).toEqual("UseDefaultRouteForDnsRelay - false");
        } else {
          errorMessage = "Failed while running - " + edgeGwhref;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + ", orgvdcExists - " + orgvdcExists + " & edgeGwExists - " + edgeGwExists + " for bubble xxx-TestBubble-02. So can't verify Description, Edge Gateway configuration, Enable High Availabiltiy in the Edge gateway";
        this.fail(errorMessage);
      }
    });

    it("Verify bubble is created on least utilized external network", function() {
      var errorMessage;
      var networkName = [];

      if (edgeGwExists) {
        for(var key in jsonResultEdgeGw.EdgeGateway.Configuration.GatewayInterfaces.GatewayInterface) {
          networkName.push(jsonResultEdgeGw.EdgeGateway.Configuration.GatewayInterfaces.GatewayInterface[key].Name);
        }

        // Verify bubbles are actually created on least utilised external network
        expect(networkName).toContain("External-Corporate-159.182.214.0-25");
      } else {
        errorMessage = "orgExists - " + orgExists + ", orgvdcExists - " + orgvdcExists + " & edgeGwExists - " + edgeGwExists + " for bubble xxx-TestBubble-01. So can't verify Description, Edge Gateway configuration, Enable High Availabiltiy in the Edge gateway";
        this.fail(errorMessage);
      }
    });

    it("Verify Firewall configuration in the Edge gateway for the Bubble - 'xxx-TestBubble-02'", function() {
      var errorMessage;
      if (edgeGwExists) {
        // Add a verification here for the Firewall configuration
        var IsEnabled          = jsonResultEdgeGw.EdgeGateway.Configuration.EdgeGatewayServiceConfiguration.FirewallService.IsEnabled;
        var DefaultAction      = jsonResultEdgeGw.EdgeGateway.Configuration.EdgeGatewayServiceConfiguration.FirewallService.DefaultAction;
        var LogDefaultAction   = jsonResultEdgeGw.EdgeGateway.Configuration.EdgeGatewayServiceConfiguration.FirewallService.LogDefaultAction;

        // Verify the firewall configurations
        expect("Firewall IsEnabled - "         + IsEnabled).toEqual("Firewall IsEnabled - true");
        expect("Firewall DefaultAction - "     + DefaultAction).toEqual("Firewall DefaultAction - drop");
        expect("Firewall LogDefaultAction - "  + LogDefaultAction).toEqual("Firewall LogDefaultAction - false");

        var firewallRule = jsonResultEdgeGw.EdgeGateway.Configuration.EdgeGatewayServiceConfiguration.FirewallService.FirewallRule;
        if (firewallRule !== undefined) {
          expect("Firewall totalFirewallRule - " + firewallRule.length).toEqual("Firewall totalFirewallRule - 4");
          firewallExists = true;
        } else {
          errorMessage = "Firewall not exits for bubble xxx-TestBubble-02";
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + ", orgvdcExists - " + orgvdcExists + " & edgeGwExists - " + edgeGwExists + " for bubble xxx-TestBubble-02. So can't verify Description, Edge Gateway configuration, Enable High Availabiltiy in the Edge gateway";
        this.fail(errorMessage);
      }
    });

    it("Verify Firewall configuration (Default-RDP-Inbound) in the Edge gateway for the Bubble - 'xxx-TestBubble-02'", function() {
      var errorMessage;
      if (firewallExists) {
        // Set xpath for the firewall
        firewall = jsonResultEdgeGw.EdgeGateway.Configuration.EdgeGatewayServiceConfiguration.FirewallService.FirewallRule;
        if (firewall.length == 4) {
          expect("IsEnabled - "             + firewall[0].IsEnabled).toEqual("IsEnabled - true");
          expect("MatchOnTranslate - "      + firewall[0].MatchOnTranslate).toEqual("MatchOnTranslate - false");
          expect("Description - "           + firewall[0].Description).toEqual("Description - Default-RDP-Inbound");
          expect("Policy - "                + firewall[0].Policy).toEqual("Policy - allow");
          expect("Protocols - "             + firewall[0].Protocols.Tcp).toEqual("Protocols - true");
          expect("Port - "                  + firewall[0].Port).toEqual("Port - 3389");
          expect("DestinationPortRange - "  + firewall[0].DestinationPortRange).toEqual("DestinationPortRange - 3389");
          expect("DestinationIp - "         + firewall[0].DestinationIp).toEqual("DestinationIp - internal");
          expect("SourcePort - "            + firewall[0].SourcePort).toEqual("SourcePort - -1");
          expect("SourcePortRange - "       + firewall[0].SourcePortRange).toEqual("SourcePortRange - Any");
          expect("SourceIp - "              + firewall[0].SourceIp).toEqual("SourceIp - external");
          expect("EnableLogging - "         + firewall[0].EnableLogging).toEqual("EnableLogging - true");
        } else {
          errorMessage = "Total number of firewall configurations should be 4. But Actual firewall configuration - " + firewall.length + ". So can't verify the configuration for each firewall.";
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + ", orgvdcExists - " + orgvdcExists + " , edgeGwExists - " + edgeGwExists + " & firewallExists - " + firewallExists + " for bubble xxx-TestBubble-02. So can't verify Firewall configurations";
        this.fail(errorMessage);
      }
    });

    it("Verify Firewall configuration (Default-SSH-Inbound) in the Edge gateway for the Bubble - 'xxx-TestBubble-02'", function() {
      var errorMessage;
      if (firewallExists) {
        // Set xpath for the firewall
        firewall = jsonResultEdgeGw.EdgeGateway.Configuration.EdgeGatewayServiceConfiguration.FirewallService.FirewallRule;
        if (firewall.length == 4) {
          expect("IsEnabled - "             + firewall[1].IsEnabled).toEqual("IsEnabled - true");
          expect("MatchOnTranslate - "      + firewall[1].MatchOnTranslate).toEqual("MatchOnTranslate - false");
          expect("Description - "           + firewall[1].Description).toEqual("Description - Default-SSH-Inbound");
          expect("Policy - "                + firewall[1].Policy).toEqual("Policy - allow");
          expect("Protocols - "             + firewall[1].Protocols.Tcp).toEqual("Protocols - true");
          expect("Port - "                  + firewall[1].Port).toEqual("Port - 22");
          expect("DestinationPortRange - "  + firewall[1].DestinationPortRange).toEqual("DestinationPortRange - 22");
          expect("DestinationIp - "         + firewall[1].DestinationIp).toEqual("DestinationIp - internal");
          expect("SourcePort - "            + firewall[1].SourcePort).toEqual("SourcePort - -1");
          expect("SourcePortRange - "       + firewall[1].SourcePortRange).toEqual("SourcePortRange - Any");
          expect("SourceIp - "              + firewall[1].SourceIp).toEqual("SourceIp - external");
          expect("EnableLogging - "         + firewall[1].EnableLogging).toEqual("EnableLogging - true");
        } else {
          errorMessage = "Total number of firewall configurations should be 4. But Actual firewall configuration - " + firewall.length + ". So can't verify the configuration for each firewall.";
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + ", orgvdcExists - " + orgvdcExists + " , edgeGwExists - " + edgeGwExists + " & firewallExists - " + firewallExists + " for bubble xxx-TestBubble-02. So can't verify Firewall configurations";          this.fail(errorMessage);
      }
    });

    it("Verify Firewall configuration (Default-ICMP-Inbound) in the Edge gateway for the Bubble - 'xxx-TestBubble-02'", function() {
      var errorMessage;
      if (firewallExists) {
        // Set xpath for the firewall
        firewall = jsonResultEdgeGw.EdgeGateway.Configuration.EdgeGatewayServiceConfiguration.FirewallService.FirewallRule;
        if (firewall.length == 4) {
          expect("IsEnabled - "             + firewall[2].IsEnabled).toEqual("IsEnabled - true");
          expect("MatchOnTranslate - "      + firewall[2].MatchOnTranslate).toEqual("MatchOnTranslate - false");
          expect("Description - "           + firewall[2].Description).toEqual("Description - Default-ICMP-Inbound");
          expect("Policy - "                + firewall[2].Policy).toEqual("Policy - allow");
          expect("Protocols - "             + firewall[2].Protocols.Icmp).toEqual("Protocols - true");
          expect("Port - "                  + firewall[2].Port).toEqual("Port - -1");
          expect("DestinationPortRange - "  + firewall[2].DestinationPortRange).toEqual("DestinationPortRange - Any");
          expect("DestinationIp - "         + firewall[2].DestinationIp).toEqual("DestinationIp - Any");
          expect("SourcePort - "            + firewall[2].SourcePort).toEqual("SourcePort - -1");
          expect("SourcePortRange - "       + firewall[2].SourcePortRange).toEqual("SourcePortRange - Any");
          expect("SourceIp - "              + firewall[2].SourceIp).toEqual("SourceIp - Any");
          expect("EnableLogging - "         + firewall[2].EnableLogging).toEqual("EnableLogging - true");
        } else {
          errorMessage = "Total number of firewall configurations should be 4. But Actual firewall configuration - " + firewall.length + ". So can't verify the configuration for each firewall.";
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + ", orgvdcExists - " + orgvdcExists + " , edgeGwExists - " + edgeGwExists + " & firewallExists - " + firewallExists + " for bubble xxx-TestBubble-02. So can't verify Firewall configurations";
        this.fail(errorMessage);
      }
    });

    it("Verify Firewall configuration (Default-Any-Outbound) in the Edge gateway for the Bubble - 'xxx-TestBubble-02'", function() {
      var errorMessage;
      if (firewallExists && jsonResultEdgeGw) {
        // Set xpath for the firewall
        firewall = jsonResultEdgeGw.EdgeGateway.Configuration.EdgeGatewayServiceConfiguration.FirewallService.FirewallRule;
        if (firewall.length == 4) {
          expect("IsEnabled - "             + firewall[3].IsEnabled).toEqual("IsEnabled - true");
          expect("MatchOnTranslate - "      + firewall[3].MatchOnTranslate).toEqual("MatchOnTranslate - false");
          expect("Description - "           + firewall[3].Description).toEqual("Description - Default-Any-Outbound");
          expect("Policy - "                + firewall[3].Policy).toEqual("Policy - allow");
          expect("Protocols - "             + firewall[3].Protocols.Any).toEqual("Protocols - true");
          expect("Port - "                  + firewall[3].Port).toEqual("Port - -1");
          expect("DestinationPortRange - "  + firewall[3].DestinationPortRange).toEqual("DestinationPortRange - Any");
          expect("DestinationIp - "         + firewall[3].DestinationIp).toEqual("DestinationIp - external");
          expect("SourcePort - "            + firewall[3].SourcePort).toEqual("SourcePort - -1");
          expect("SourcePortRange - "       + firewall[3].SourcePortRange).toEqual("SourcePortRange - Any");
          expect("SourceIp - "              + firewall[3].SourceIp).toEqual("SourceIp - internal");
          expect("EnableLogging - "         + firewall[3].EnableLogging).toEqual("EnableLogging - true");
        } else {
          errorMessage = "Total number of firewall configurations should be 4. But Actual firewall configuration - " + firewall.length + ". So can't verify the configuration for each firewall.";
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + ", orgvdcExists - " + orgvdcExists + " , edgeGwExists - " + edgeGwExists + " & firewallExists - " + firewallExists + " for bubble xxx-TestBubble-02. So can't verify Firewall configurations";
        this.fail(errorMessage);
      }
    });

    it("Verify networks are created for the bubble 'xxx-TestBubble-02'", function() {
      var errorMessage = "";
      if (networkshref !== "") {
        response = fn.runHttpRequests("GET", headers, networkshref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          var jsonResult = xmlobj.parseXML(response.body.toString());

          // No record for 'QueryResultRecords.OrgVdcNetworkRecord' ?, then no network exists.
          if (jsonResult.QueryResultRecords.OrgVdcNetworkRecord !== undefined) {
            var totalNetworks = jsonResult.QueryResultRecords.OrgVdcNetworkRecord.length;
            if (totalNetworks == 2) {
              var name, taskStatus;

              // Verify FE network
              name        = jsonResult.QueryResultRecords.OrgVdcNetworkRecord[0]["-name"];
              taskStatus  = jsonResult.QueryResultRecords.OrgVdcNetworkRecord[0]["-taskStatus"];
              if (taskStatus == "success") {
                feNetworkhref = jsonResult.QueryResultRecords.OrgVdcNetworkRecord[0]["-href"];
              }
              expect("Network " + name + " created - " + taskStatus).toEqual("Network " + name + " created - success");

              // Verify BE network
              name        = jsonResult.QueryResultRecords.OrgVdcNetworkRecord[1]["-name"];
              taskStatus  = jsonResult.QueryResultRecords.OrgVdcNetworkRecord[1]["-taskStatus"];
              if (taskStatus == "success") {
                beNetworkhref = jsonResult.QueryResultRecords.OrgVdcNetworkRecord[1]["-href"];
              }
              expect("Network " + name + " created - " + taskStatus).toEqual("Network " + name + " created - success");
            } else {
              errorMessage = "Either FE Or BE is not created. \n\tExpected networks on vCD - 2\n\tActual networks on vCD   - " + totalNetworks;
              this.fail(errorMessage);
            }
          }
        } else {
          errorMessage = "Failed while running - " + networkshref;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + ", orgvdcExists - " + orgvdcExists + " & edgeGwExists - " + edgeGwExists + " for bubble xxx-TestBubble-02. So can't verify whether FE & BE networks are created";
        this.fail(errorMessage);
      }
    });

    it("Verify FE networks details for the bubble 'xxx-TestBubble-02'", function() {
      var errorMessage;
      if (feNetworkhref !== "") {
        response = fn.runHttpRequests("GET", headers, feNetworkhref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          var jsonResult = xmlobj.parseXML(response.body.toString());

          var Description = jsonResult.OrgVdcNetwork.Description;
          var IsInherited = jsonResult.OrgVdcNetwork.Configuration.IpScopes.IpScope.IsInherited;
          var DnsSuffix   = jsonResult.OrgVdcNetwork.Configuration.IpScopes.IpScope.DnsSuffix;
          var IsEnabled   = jsonResult.OrgVdcNetwork.Configuration.IpScopes.IpScope.IsEnabled;
          var Dns1        = jsonResult.OrgVdcNetwork.Configuration.IpScopes.IpScope.Dns1;
          var Dns2        = jsonResult.OrgVdcNetwork.Configuration.IpScopes.IpScope.Dns2;
          var FenceMode   = jsonResult.OrgVdcNetwork.Configuration.FenceMode;
          var IsShared    = jsonResult.OrgVdcNetwork.IsShared;

          expect("IsInherited for - " + Description + " - " + IsInherited).toEqual("IsInherited for - " + Description + " - false");
          expect(  "DnsSuffix for - " + Description + " - " + DnsSuffix).toEqual(  "DnsSuffix for - "   + Description + " - cloud.pearson.com");
          expect(       "Dns1 for - " + Description + " - " + Dns1).toEqual(       "Dns1 for - "        + Description + " - 10.162.33.150");
          expect(       "Dns2 for - " + Description + " - " + Dns2).toEqual(       "Dns2 for - "        + Description + " - 10.162.65.150");
          expect(  "FenceMode for - " + Description + " - " + FenceMode).toEqual(  "FenceMode for - "   + Description + " - natRouted");
          expect(   "IsShared for - " + Description + " - " + IsShared).toEqual(   "IsShared for - "    + Description + " - true");
          expect(  "IsEnabled for - " + Description + " - " + IsEnabled).toEqual(  "IsEnabled for - "   + Description + " - true");
        } else {
          errorMessage = "Failed while running - " + feNetworkhref;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + ", orgvdcExists - " + orgvdcExists + " , edgeGwExists - " + edgeGwExists + " & feNetworkhref - " + feNetworkhref + " for bubble xxx-TestBubble-02. So can't verify whether FE & BE networks are created";
        this.fail(errorMessage);
      }
    });

    it("Verify BE networks details for the bubble 'xxx-TestBubble-02'", function() {
      var errorMessage;
      if (beNetworkhref !== "") {
        response = fn.runHttpRequests("GET", headers, beNetworkhref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          var jsonResult = xmlobj.parseXML(response.body.toString());

          var Description = jsonResult.OrgVdcNetwork.Description;
          var IsInherited = jsonResult.OrgVdcNetwork.Configuration.IpScopes.IpScope.IsInherited;
          var DnsSuffix   = jsonResult.OrgVdcNetwork.Configuration.IpScopes.IpScope.DnsSuffix;
          var IsEnabled   = jsonResult.OrgVdcNetwork.Configuration.IpScopes.IpScope.IsEnabled;
          var Dns1        = jsonResult.OrgVdcNetwork.Configuration.IpScopes.IpScope.Dns1;
          var Dns2        = jsonResult.OrgVdcNetwork.Configuration.IpScopes.IpScope.Dns2;
          var FenceMode   = jsonResult.OrgVdcNetwork.Configuration.FenceMode;
          var IsShared    = jsonResult.OrgVdcNetwork.IsShared;

          expect("IsInherited for - " + Description + " - " + IsInherited).toEqual("IsInherited for - " + Description + " - false");
          expect(  "DnsSuffix for - " + Description + " - " + DnsSuffix).toEqual(  "DnsSuffix for - "   + Description + " - cloud.pearson.com");
          expect(       "Dns1 for - " + Description + " - " + Dns1).toEqual(       "Dns1 for - "        + Description + " - 10.162.33.150");
          expect(       "Dns2 for - " + Description + " - " + Dns2).toEqual(       "Dns2 for - "        + Description + " - 10.162.65.150");
          expect(  "FenceMode for - " + Description + " - " + FenceMode).toEqual(  "FenceMode for - "   + Description + " - natRouted");
          expect(   "IsShared for - " + Description + " - " + IsShared).toEqual(   "IsShared for - "    + Description + " - true");
          expect(  "IsEnabled for - " + Description + " - " + IsEnabled).toEqual(  "IsEnabled for - "   + Description + " - true");
        } else {
          errorMessage = "Failed while running - " + beNetworkhref;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + ", orgvdcExists - " + orgvdcExists + " , edgeGwExists - " + edgeGwExists + "& beNetworkhref - " + beNetworkhref + " for bubble xxx-TestBubble-02. So can't verify whether FE & BE networks are created";
        this.fail(errorMessage);
      }
    });

    it("Verify number of users for the Bubble - 'xxx-TestBubble-02'", function() {
      var errorMessage;
      var users = [];

      if (orgExists) {
        // User is created on vCD ?
        if (jsonResultOrg.AdminOrg.Users !== undefined) {
          // Store the href for each user
          for (var key in jsonResultOrg.AdminOrg.Users.UserReference) {
            if (jsonResultOrg.AdminOrg.Users.UserReference[key]["-name"] == "mystack.user1@pearson.com") {
              user1_href = jsonResultOrg.AdminOrg.Users.UserReference[key]["-href"];
            } else if (jsonResultOrg.AdminOrg.Users.UserReference[key]["-name"] == "mystack.user2@pearson.com") {
              user2_href = jsonResultOrg.AdminOrg.Users.UserReference[key]["-href"];
            } else if (jsonResultOrg.AdminOrg.Users.UserReference[key]["-name"] == "mystack.user3@pearson.com") {
              user3_href = jsonResultOrg.AdminOrg.Users.UserReference[key]["-href"];
            }

            // Append list of users to the list
            users.push(jsonResultOrg.AdminOrg.Users.UserReference[key]["-name"]);
          }
          // Verify the total number of users
          expect("Total number of bubble users - " + jsonResultOrg.AdminOrg.Users.UserReference.length).toEqual("Total number of bubble users - 3");
          expect(users).toContain("mystack.user1@pearson.com");
          expect(users).toContain("mystack.user2@pearson.com");
          expect(users).toContain("mystack.user3@pearson.com");
        } else {
          errorMessage = "User is not created on the vCD. So can't continue this test";
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "Org not created for bubble xxx-TestBubble-01. So can't verify whether whether local user are created Or not.";
        this.fail(errorMessage);
      }
    });

    it("Verify details for user mysatck.user1@pearson for bubble - 'xxx-TestBubble-02'", function() {
      var errorMessage, jsonResult;
      if (user1_href !== "") {
        response = fn.runHttpRequests("GET", headers, user1_href);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResult = xmlobj.parseXML(response.body.toString());

          // Verify the values
          expect("Fullname - "      + jsonResult.User.FullName).toEqual("Fullname - mystack user1");
          expect("Email Address - " + jsonResult.User.EmailAddress).toEqual("Email Address - mystack.user1@pearson.com");
          expect("Is Enabled - "    + jsonResult.User.IsEnabled).toEqual("Is Enabled - true");
          expect("Is Locked - "     + jsonResult.User.IsLocked).toEqual("Is Locked - false");
          expect("Is External - "   + jsonResult.User.IsExternal).toEqual("Is External - true");
        } else {
          errorMessage = "Failed while running - " + user1_href;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "User mystack.user1 is not added to bubble xxx-TestBubble-02. So can't verify the details for this bubble";
        this.fail(errorMessage);
      }
    });

    it("Verify details for user mysatck.user2@pearson for bubble - 'xxx-TestBubble-02'", function() {
      var errorMessage, jsonResult;
      if (user2_href !== "") {
        response = fn.runHttpRequests("GET", headers, user2_href);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResult = xmlobj.parseXML(response.body.toString());

          // Verify the values
          expect("Fullname - "      + jsonResult.User.FullName).toEqual("Fullname - mystack user2");
          expect("Email Address - " + jsonResult.User.EmailAddress).toEqual("Email Address - mystack.user2@pearson.com");
          expect("Is Enabled - "    + jsonResult.User.IsEnabled).toEqual("Is Enabled - true");
          expect("Is Locked - "     + jsonResult.User.IsLocked).toEqual("Is Locked - false");
          expect("Is External - "   + jsonResult.User.IsExternal).toEqual("Is External - true");
        } else {
          errorMessage = "Failed while running - " + user2_href;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "User mystack.user2 is not added to bubble xxx-TestBubble-02. So can't verify the details for this bubble";
        this.fail(errorMessage);
      }
    });

    it("Verify details for user mysatck.user3@pearson for bubble - 'xxx-TestBubble-02'", function() {
      var errorMessage, jsonResult;
      if (user3_href !== "") {
        response = fn.runHttpRequests("GET", headers, user3_href);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResult = xmlobj.parseXML(response.body.toString());

          // Verify the values
          expect("Fullname - "      + jsonResult.User.FullName).toEqual("Fullname - mystack user3");
          expect("Email Address - " + jsonResult.User.EmailAddress).toEqual("Email Address - mystack.user3@pearson.com");
          expect("Is Enabled - "    + jsonResult.User.IsEnabled).toEqual("Is Enabled - true");
          expect("Is Locked - "     + jsonResult.User.IsLocked).toEqual("Is Locked - false");
          expect("Is External - "   + jsonResult.User.IsExternal).toEqual("Is External - true");
        } else {
          errorMessage = "Failed while running - " + user3_href;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "User mystack.user2 is not added to bubble xxx-TestBubble-02. So can't verify the details for this bubble";
        this.fail(errorMessage);
      }
    });

    it("Verify Bubble's Public IP's, Edge gateway IP & networks value in Portal is matching with vCD", function() {
      var response, key, errorMessage;
      var edgeGateway = "";
      var gateway     = "";
      var publicIp    = [];
      var networks    = [];

      fn.login("mystack.user2", "");
      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-02')), waitSeconds);
      bubblesPage.bubble('xxx-TestBubble-02').click();
      // element.all(by.className("Bubble")).get(1).click();

      // Edge gateway created ?. Get the edge gateway details from the vCD
      if (edgeGwqueryhref !== "") {
        response = fn.runHttpRequests("GET", headers, edgeGwqueryhref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResult = xmlobj.parseXML(response.body.toString());

          if (jsonResult.QueryResultRecords.EdgeGatewayRecord !== undefined) {
            if (jsonResult.QueryResultRecords.EdgeGatewayRecord["-numberOfExtNetworks"] == "1") {
              edgeGwhref = jsonResult.QueryResultRecords.EdgeGatewayRecord["-href"];

              response = fn.runHttpRequests("GET", headers, edgeGwhref);
              if (response.statusCode == 200) {
                // Convert XML -> JSON
                jsonResult = xmlobj.parseXML(response.body.toString());
                for(key in jsonResult.EdgeGateway.Configuration.GatewayInterfaces.GatewayInterface) {
                  interfaceType = jsonResult.EdgeGateway.Configuration.GatewayInterfaces.GatewayInterface[key].InterfaceType;
                  if (interfaceType == "uplink") {
                    // Get the range of external IP address purchased via the myIP and Marketplace it in list
                    edgeGateway = jsonResult.EdgeGateway.Configuration.GatewayInterfaces.GatewayInterface[key].SubnetParticipation.IpAddress;
                    gateway     = jsonResult.EdgeGateway.Configuration.GatewayInterfaces.GatewayInterface[key].SubnetParticipation.Gateway;

                    var IpRanges  = jsonResult.EdgeGateway.Configuration.GatewayInterfaces.GatewayInterface[key].SubnetParticipation.IpRanges;
                    // if (IpRanges.IpRange.length !== undefined) {
                    //   IpRanges = IpRanges.IpRange;
                    // }
                    // for(var key2 in IpRanges) {
                    //   StartAddress = ip.toLong(IpRanges[key2].StartAddress);
                    //   EndAddress   = ip.toLong(IpRanges[key2].EndAddress);
                    //   // Start address and End address is same ?. Then just add the start address to the list
                    //   if (StartAddress == EndAddress) {
                    //     publicIp.push(ip.fromLong(StartAddress));
                    //   } else {
                    //     // Start address and End address is not same ?. Then add Start address and all the IP's in between them to the list
                    //     // Example: 10.0.0.2 - 10.0.0.4 then list should be - 10.0.0.2, 10.0.0.3, 10.0.0.4
                    //     publicIp.push(ip.fromLong(StartAddress));
                    //     totalIP = EndAddress - StartAddress;
                    //     for(var i=0; i<totalIP; i++) {
                    //       publicIp.push(ip.fromLong(StartAddress + i + 1));
                    //     }
                    //   }
                    // }
                  }
                }
                // Verify Public IP, Gateway IP & Edge Gatway IP displayed on the UI is matching with the vCD
                // expect("External IP on Bubble - undefined").toEqual("External IP on Bubble - " + IpRanges);
                expect(IpRanges).toBeUndefined();
                expect(bubblesPage.BubbleViewip).toEqual([]);
                expect(bubblesPage.BubbleViewGateway.getText()).toEqual(gateway);
                expect(bubblesPage.BubbleViewEdgeGateway.getText()).toEqual(edgeGateway);
              } else {
                errorMessage = "Failed while running - " + edgeGwhref;
                this.fail(errorMessage);
              }
            }
          }
        } else {
          errorMessage = "Failed while running - " + edgeGwqueryhref;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + " , orgvdcExists - " + orgvdcExists + "& edgeGwqueryhref - " + edgeGwqueryhref + " for bubble xxx-TestBubble-02. So can't verify whether edge gate is created Or not";
        this.fail(errorMessage);
      }

      // Networks created ?. Get network details from the vCD
      if (networkshref !== "") {
        response = fn.runHttpRequests("GET", headers, networkshref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResult = xmlobj.parseXML(response.body.toString());

          // No record for 'QueryResultRecords.OrgVdcNetworkRecord' ?, then no network exists.
          if (jsonResult.QueryResultRecords.OrgVdcNetworkRecord !== undefined) {
            for(key in jsonResult.QueryResultRecords.OrgVdcNetworkRecord) {
              // Parse network name from the XML reponse and convert it in the format - 'BE 10.5.65.97/27'
              networks.push(jsonResult.QueryResultRecords.OrgVdcNetworkRecord[key]["-name"].replace("-", " ").replace("-", "/"));
            }
          }
          expect(networks.length).toEqual(bubblesPage.BubbleViewNetwork.count());
          expect(networks).toContain(bubblesPage.BubbleViewNetwork.get(0).getText());
          expect(networks).toContain(bubblesPage.BubbleViewNetwork.get(1).getText());
        } else {
          errorMessage = "Failed while running - " + networkshref;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "orgExists - " + orgExists + ", orgvdcExists - " + orgvdcExists + " & edgeGwExists - " + edgeGwExists + " for bubble xxx-TestBubble-02. So can't verify whether FE & BE networks are created";
        this.fail(errorMessage);
      }
    });

  });

  xdescribe("ServiceNowIntegration", function() {

    it("Get the org_id for the bubble - 'xxx-TestBubble-02'", function(done) {
      var errorMessage;
      // Get org_id for the bubble 'xxx-TestBubble-01'
      db.execute("SELECT * FROM bubble where name='xxx-TestBubble-02'", function(err, result) {
        if (err) {
          errorMessage = "Failure message - " + err;
          this.fail(errorMessage);
        } else {
          if (result.rows.length == 1) {
            org_id = result.rows[0].org_id;
            expect(org_id).not.toEqual("00000000-0000-0000-0000-000000000000");
          } else {
            expect(result.rows.length).toEqual(1);
          }
        }
      });

      done();
    });

    it("Verify CMDB is updated for recently created bubble - 'xxx-TestBubble-02'", function() {
      var response, jsonResponse, errorMessage;
      if (org_id != "") {
        var href = "https://pearsontest.service-now.com/api/now/v1/table/cmdb_ci_zone?sysparm_query=correlation_id=" + org_id;
        response = fn.runHttpRequests("GET", snowHeaders, href);
        if (response.statusCode == 200) {
          jsonResponse = JSON.parse(response.body);

          expect(jsonResponse["result"].length).toEqual(1);
          expect(jsonResponse["result"][0]["name"]).toEqual("xxx-TestBubble-02");
          expect(jsonResponse["result"][0]["u_retirement_date"]).toEqual("");
          expect(jsonResponse["result"][0]["support_group"]).toEqual("");
        } else {
          errorMessage = "Failed to run the query - " + href + " Headers - " + snowHeaders;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "Org 'xxx-TestBubble-02' not exists in DB so bubble might not be created.";
        this.fail(errorMessage);
      }
    });

    it("Verify user able to search for list of ServiceNow support groups by typing partial text", function() {
      fn.login("mystack.user1", "");

      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-02')), waitSeconds);
      bubblesPage.bubble('xxx-TestBubble-02').click();

      expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
      browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);

      // Verify no results are shown if user didn't input anything
      bubblesPage.SnowSelectArrow.click();
      expect(bubblesPage.SnowNoResults.getText()).toEqual("Type to search");

      // Send text 'test' in search field and make sure Portal shows serviceNow group
      bubblesPage.SnowInput.clear().sendKeys("test");
      browser.wait(EC.presenceOf(bubblesPage.SnowSearchResults), 20000);
      expect(bubblesPage.SnowSearchResults.isPresent()).toBeTruthy();
      expect(bubblesPage.SnowSearchResults.count()).toBeGreaterThan(5);

      // Send text 'test' in search field for 2nd time and make sure Portal shows serviceNow group
      bubblesPage.SnowInput.clear().sendKeys("test");
      browser.wait(EC.presenceOf(bubblesPage.SnowSearchResults), 20000);
      expect(bubblesPage.SnowSearchResults.isPresent()).toBeTruthy();
      expect(bubblesPage.SnowSearchResults.count()).toBeGreaterThan(5);

      // Send text 'SOS' in search field for 2nd time and make sure Portal shows serviceNow group
      bubblesPage.SnowInput.clear().sendKeys("SOS");
      browser.wait(EC.presenceOf(bubblesPage.SnowSearchResults), 20000);
      expect(bubblesPage.SnowSearchResults.isPresent()).toBeTruthy();
      expect(bubblesPage.SnowSearchResults.count()).toBeGreaterThan(1);
    });

    it("Verify user able to assign 'ServiceNow Support Group' for a bubble - 'xxx-TestBubble-02'", function() {
      // Add ServiceNow Support group to this bubble
      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-02')), waitSeconds);
      bubblesPage.bubble('xxx-TestBubble-02').click();
      expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
      browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
      expect(bubblesPage.SnowGroupGetName).toEqual('');
      bubblesPage.SnowInput.clear().sendKeys("PI LTG Test");
      browser.wait(EC.presenceOf(bubblesPage.SnowSearchResults), 20000);
      expect(bubblesPage.SnowSearchResults.isPresent()).toBeTruthy();
      bubblesPage.SnowGroupSelect('PI LTG Testing').click();
      expect(bubblesPage.SnowGroupGetName).toEqual('PI LTG Testing');

      // browser.refresh();
      // browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
      // expect(bubblesPage.SnowGroupGetName).toEqual('PI LTG Testing');
      // bubblesPage.BubbleViewClose.click();
    });

    it("Verify recently chosen 'ServiceNow Support Group' for a bubble - 'xxx-TestBubble-02' is visible after page refresh", function() {
      // Add ServiceNow Support group to this bubble
      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-02')), waitSeconds);
      bubblesPage.bubble('xxx-TestBubble-02').click();
      expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
      browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);

      expect(bubblesPage.SnowGroupGetName).toEqual('PI LTG Testing');
      bubblesPage.BubbleViewClose.click();
    });

    it("Verify 'ServiceNow Support Group' (PI LTG Testing) is updated on the CMDB for bubble - 'xxx-TestBubble-02'", function() {
      var response, jsonResponse, errorMessage;
      if (org_id != "") {

        // Verify ServiceNow Support group is added on the CMDB for the bubble 'xxx-TestBubble-02'
        var href = "https://pearsontest.service-now.com/api/now/v1/table/cmdb_ci_zone?sysparm_query=correlation_id=" + org_id;
        response = fn.runHttpRequests("GET", snowHeaders, href);
        if (response.statusCode == 200) {
          jsonResponse = JSON.parse(response.body);
          supportGroupHref = jsonResponse["result"][0]["support_group"]["link"];
          expect(supportGroupHref).not.toEqual("");

          // Verify bubble is assigned to correct support group
          response = fn.runHttpRequests("GET", snowHeaders, supportGroupHref);
          if (response.statusCode == 200) {
            jsonResponse = JSON.parse(response.body);
            expect(jsonResponse["result"]["name"]).toEqual("PI LTG Testing");
            expect(jsonResponse["result"]["active"]).toBeTruthy();
          } else {
            errorMessage = "Failed to run the query - " + supportGroupHref + " Headers - " + snowHeaders;
            this.fail(errorMessage);
          }
        } else {
          errorMessage = "Failed to run the query - " + href;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "Org 'xxx-TestBubble-02' not exists in DB (bubble might not be created). So can't continue this test";
        this.fail(errorMessage);
      }
    });

    it("Verify user able to update ServiceNow support group for bubble - 'xxx-TestBubble-02'", function() {
      // Update the ServiceNow Support group
      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-02')), waitSeconds);
      bubblesPage.bubble('xxx-TestBubble-02').click();
      expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
      browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
      expect(bubblesPage.SnowGroupGetName).toEqual('PI LTG Testing');
      bubblesPage.SnowInput.clear().sendKeys("SOS Test 2");
      browser.wait(EC.presenceOf(bubblesPage.SnowSearchResults), 20000);
      expect(bubblesPage.SnowSearchResults.isPresent()).toBeTruthy();
      bubblesPage.SnowGroupSelect('SOS Test 2').click();
      expect(bubblesPage.SnowGroupGetName).toEqual('SOS Test 2');

      browser.refresh();
      browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
      expect(bubblesPage.SnowGroupGetName).toEqual('SOS Test 2');
      bubblesPage.BubbleViewClose.click();
    });

    it("Verify 'ServiceNow Support Group' (SOS test 2) is updated on the CMDB for bubble - 'xxx-TestBubble-02'", function() {
      var response, jsonResponse, errorMessage;
      if (org_id != "") {
        // Verify ServiceNow Support group gets updated in the CMDB
        var href = "https://pearsontest.service-now.com/api/now/v1/table/cmdb_ci_zone?sysparm_query=correlation_id=" + org_id;
        response = fn.runHttpRequests("GET", snowHeaders, href);
        if (response.statusCode == 200) {
          jsonResponse = JSON.parse(response.body);
          supportGroupHref = jsonResponse["result"][0]["support_group"]["link"];
          expect(supportGroupHref).not.toEqual("");

          // Verify bubble is assigned to correct support group
          response = fn.runHttpRequests("GET", snowHeaders, supportGroupHref);
          if (response.statusCode == 200) {
            jsonResponse = JSON.parse(response.body);
            expect(jsonResponse["result"]["name"]).toEqual("SOS Test 2");
            expect(jsonResponse["result"]["active"]).toBeTruthy();
          } else {
            errorMessage = "Failed to run the query - " + supportGroupHref + " Headers - " + snowHeaders;
            this.fail(errorMessage);
          }
        } else {
          errorMessage = "Failed to run the query - " + href + " Headers - " + snowHeaders;
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "Org 'xxx-TestBubble-02' not exists in DB (bubble might not be created). So can't continue this test";
        this.fail(errorMessage);
      }
    });

  });

});

