/*
*****************************************************************************************************************************************************
SUMMARY:      Verify whether user able to move bubbles across the team and users should be reconcilled

ATTENTION:    Should be run after 'UserAndTeams_spec.js', 'BubbleCreate-01_spec.js' & 'BubbleCreate-02_spec.js'

List Of Tests:
    BubbleMove: Verify 'update' & 'X' button on bubble move page - 'xxx-TestBubble-02'
    BubbleMove: Verify user with CREARTOR on team able to move bubble to other teams - xxx-TestTeam-02->xxx-TestTeam-01
    BubbleMove: Verify team 'xxx-TestTeam-01' got updated for recently moved bubble - 'xxx-TestBubble-02'
    BubbleMove: Verify team 'xxx-TestTeam-02' changed for recently moved bubble - 'xxx-TestBubble-02'
    BubbleMove: Verify details on Bubbles page is changed for recently moved bubble
    BubbleMove: Verify team 'xxx-TestTeam-02' got updated for recently moved bubble - 'xxx-TestBubble-02'
    BubbleMove: Verify users are reconcilled while moving the bubble 'xxx-TestBubble-02' from team xxx-TestTeam-01->xxx-TestTeam-02
*****************************************************************************************************************************************************
*/
var homePage          = require("../../pages/home_page.js");
var usersPage         = require("../../pages/users_page.js");
var teamsPage         = require("../../pages/teams_page.js");
var bubblesPage       = require("../../pages/bubbles_page.js");
var notificationsPage = require("../../pages/notifications_page.js");

var authorization       = vcdAuthKey;
var headers             = {"Accept": "application/*+xml;version=5.1", "Authorization" : vcdAuthKey};

describe("BubbleMove", function() {

  beforeAll(function() {
    browser.get('/');
    fn.login("mystack.user2", "");
    fn.clearNotifications();
    // fn.login("mystack.user3", "");
    // fn.clearNotifications();
  });

  beforeEach(function() {
    browser.get('/');
  });

  it("Verify 'update' & 'X' button on bubble move page - 'xxx-TestBubble-02'", function() {
    fn.login("mystack.user1", "");

    fn.clearNotifications();

    homePage.bubbles.click();
    expect(bubblesPage.bubbles.count()).toEqual(2);
    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-02')), waitSeconds);
    bubblesPage.bubble('xxx-TestBubble-02').click();
    // bubblesPage.bubbles.get(1).click();

    // Verify BubbleView page is displayed while submitting request to create bubble
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    expect(bubblesPage.BubbleViewName.getText()).toEqual('xxx-TestBubble-02');

    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewMoveteams), waitSeconds);
    bubblesPage.BubbleViewMoveteams.click();

    expect(bubblesPage.BubbleViewMoveTeamOptions.count()).toEqual(2);
    expect(bubblesPage.BubbleViewMoveTeamOptions.getText()).toContain('xxx-TestTeam-01');
    expect(bubblesPage.BubbleViewMoveTeamOptions.getText()).toContain('xxx-TestTeam-02');
    expect(bubblesPage.BubbleViewTeamSubmit.isDisplayed()).toBeTruthy();
    expect(bubblesPage.BubbleViewTeamCancel.isDisplayed()).toBeTruthy();
    bubblesPage.BubbleViewTeamCancel.click();
    expect(bubblesPage.BubbleViewTeamSubmit.isPresent()).toBeFalsy();
    expect(bubblesPage.BubbleViewTeamCancel.isPresent()).toBeFalsy();
    // Verify details not updated in the Bubble View page
    expect(bubblesPage.BubbleViewTeam.getText()).toEqual('xxx-TestTeam-02');
    expect(bubblesPage.BubbleViewMember.count()).toEqual(3);
    expect(bubblesPage.BubbleViewMember.getText()).toContain('mystack user1');
    expect(bubblesPage.BubbleViewMember.getText()).toContain('mystack user2');
    expect(bubblesPage.BubbleViewMember.getText()).toContain('mystack user3');
    // Verify details in Bubbles page not updated
    expect(bubblesPage.getBubbleNameByindex(0)).toEqual('xxx-TestBubble-01');
    expect(bubblesPage.getBubbleRegionByindex(0)).toEqual('LO3REF');
    expect(bubblesPage.getBubbleTeamByindex(0)).toEqual('xxx-TestTeam-01');
    expect(bubblesPage.getBubbleNameByindex(1)).toEqual('xxx-TestBubble-02');
    expect(bubblesPage.getBubbleRegionByindex(1)).toEqual('LO3REF');
    expect(bubblesPage.getBubbleTeamByindex(1)).toEqual('xxx-TestTeam-02');
  });

  it("Verify user with CREARTOR role on team able to move bubble to other teams - xxx-TestTeam-02->xxx-TestTeam-01", function() {
    homePage.bubbles.click();
    expect(bubblesPage.bubbles.count()).toEqual(2);
    // element.all(by.className("Bubble")).get(1).click();
    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-02')), waitSeconds);
    bubblesPage.bubble('xxx-TestBubble-02').click();

    // Verify BubbleView page is displayed while submitting request to create bubble
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    expect(bubblesPage.BubbleViewName.getText()).toEqual('xxx-TestBubble-02');

    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewMoveteams), waitSeconds);
    bubblesPage.BubbleViewMoveteams.click();

    // Move the buble from team xxx-TestTeam-02->xxx-TestTeam-01
    bubblesPage.BubbleViewMoveTeamOptions.get(0).click();
    bubblesPage.BubbleViewTeamSubmit.click();

    // Verify values are changed while moving the bubble to another team
    expect(bubblesPage.BubbleViewTeam.getText()).toEqual('xxx-TestTeam-01');
    // TODO - Replace this sleep
    browser.sleep(2000);
    expect(bubblesPage.BubbleViewMember.count()).toEqual(4);
    expect(bubblesPage.BubbleViewMember.getText()).toContain('mystack user1');
    expect(bubblesPage.BubbleViewMember.getText()).toContain('mystack user3');
    expect(bubblesPage.BubbleViewMember.getText()).toContain('mystack user5');
    expect(bubblesPage.BubbleViewMember.getText()).toContain('mystack user2');
    // Verify details in Bubbles page got updated
    expect(bubblesPage.getBubbleNameByindex(0)).toEqual('xxx-TestBubble-01');
    expect(bubblesPage.getBubbleRegionByindex(0)).toEqual('LO3REF');
    expect(bubblesPage.getBubbleTeamByindex(0)).toEqual('xxx-TestTeam-01');
    expect(bubblesPage.getBubbleCreatedDateByindex(0)).toEqual(new Date().toDateString());
    expect(bubblesPage.getBubbleNameByindex(1)).toEqual('xxx-TestBubble-02');
    expect(bubblesPage.getBubbleRegionByindex(1)).toEqual('LO3REF');
    expect(bubblesPage.getBubbleTeamByindex(1)).toEqual('xxx-TestTeam-01');
    expect(bubblesPage.getBubbleCreatedDateByindex(1)).toEqual(new Date().toDateString());
  });

  it("Verify user 'mystack.user1' gets notifications for user reconcilliation while moving the bubble to different team", function() {
    fn.waitForNotifications(20000, 4);
    expect(notificationsPage.notificationCount.getText()).toEqual('2');
    homePage.notifications.click();
    // Verify total number of 'NotificationGroup' and click on first one
    expect(notificationsPage.notificationHeader.count()).toEqual(1);

    // Verify notifcations while adding user 'mystack.user5' to team 'xxx-TestTeam-02'
    notificationsPage.notificationHeader.get(0).click();
    expect(notificationsPage.notificationHeader.get(0).getText()).toEqual("Add User");
    expect(notificationsPage.NotificationsByindex(0).count()).toEqual(2);
    expect(notificationsPage.NotificationsByindex(0).get(0).getText()).toEqual("Adding user to xxx-TestBubble-02. Please wait");
    expect(notificationsPage.NotificationsByindex(0).get(1).getText()).toContain("has been added as a user to xxx-TestBubble-02");
  });

  it("Verify team 'xxx-TestTeam-01' got updated for recently moved bubble 'xxx-TestBubble-02' for user 'mystack.user1'", function() {
    homePage.teams.click();
    expect(teamsPage.teams.count()).toEqual(2);
    expect(teamsPage.getTeamNameByindex(0)).toEqual('xxx-TestTeam-01');
    expect(teamsPage.getTeamBubblesByindex(0)).toEqual('2');
    expect(teamsPage.getTeamMemberCountByindex(0)).toEqual('4');

    // Details in TeamView page
    browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-01')), waitSeconds);
    teamsPage.team('xxx-TestTeam-01').click();
    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-01');
    expect(teamsPage.teamViewDelete.isPresent()).toBeFalsy();
    expect(teamsPage.teamViewAddMember.isDisplayed()).toBeTruthy();
    expect(teamsPage.teamViewMembers.count()).toEqual(4);

    // Verify the bubble details
    expect(teamsPage.TeamViewBubbles.count()).toEqual(2);
    expect(teamsPage.TeamViewBubbleNameByindex(0).getText()).toEqual('xxx-TestBubble-01 (LO3REF)');
    expect(teamsPage.TeamViewBubbleNameByindex(1).getText()).toEqual('xxx-TestBubble-02 (LO3REF)');
  });

  it("Verify team 'xxx-TestTeam-02' changed for recently moved bubble 'xxx-TestBubble-02' for user 'mystack.user1'", function() {
    homePage.teams.click();
    expect(teamsPage.teams.count()).toEqual(2);
    expect(teamsPage.getTeamNameByindex(1)).toEqual('xxx-TestTeam-02');
    expect(teamsPage.getTeamBubblesByindex(1)).toEqual('0');
    expect(teamsPage.getTeamMemberCountByindex(1)).toEqual('3');

    // Details in TeamView page
    browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-02')), waitSeconds);
    teamsPage.team('xxx-TestTeam-02').click();
    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-02');
    expect(teamsPage.teamViewDelete.isPresent()).toBeTruthy();
    expect(teamsPage.teamViewAddMember.isDisplayed()).toBeTruthy();
    expect(teamsPage.teamViewMembers.count()).toEqual(3);
    // Verify the bubble details
    expect(teamsPage.TeamViewBubbles).toEqual([]);
  });

  it("Verify user (mystack.user1) able to navigate to Bubbles page by clicking bubble name in Teams page - 'xxx-TestBubble-01'", function() {
    homePage.teams.click();
    browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-01')), waitSeconds);
    teamsPage.team('xxx-TestTeam-01').click();

    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(browser.getCurrentUrl()).toContain(homePageUrl + "teams/");
    teamsPage.TeamViewBubbleByname('xxx-TestBubble-01').click();

    // Verify bubbles page is loaded
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    expect(browser.getCurrentUrl()).toContain(homePageUrl + "bubbles/");
    expect(bubblesPage.BubbleViewName.getText()).toEqual('xxx-TestBubble-01');
  });

  it("Verify user (mystack.user1) able to navigate to Bubbles page by clicking bubble name in Teams page - 'xxx-TestBubble-02'", function() {
    homePage.teams.click();
    browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-01')), waitSeconds);
    teamsPage.team('xxx-TestTeam-01').click();

    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(browser.getCurrentUrl()).toContain(homePageUrl + "teams/");
    teamsPage.TeamViewBubbleByname('xxx-TestBubble-02').click();

    // Verify bubbles page is loaded
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    expect(browser.getCurrentUrl()).toContain(homePageUrl + "bubbles/");
    expect(bubblesPage.BubbleViewName.getText()).toEqual('xxx-TestBubble-02');
  });

  it("Verify user 'mystack.user2' gets notifications for user reconcilliation while moving the bubble to different team", function() {
    fn.login("mystack.user2", "");

    fn.waitForNotifications(2000, 1);
    expect(notificationsPage.notificationCount.getText()).toEqual('2');
    homePage.notifications.click();
    // Verify total number of 'NotificationGroup' and click on first one
    expect(notificationsPage.notificationHeader.count()).toEqual(1);

    // Verify notifcations while adding user 'mystack.user5' to team 'xxx-TestTeam-02'
    notificationsPage.notificationHeader.get(0).click();
    expect(notificationsPage.notificationHeader.get(0).getText()).toEqual("Add User");
    expect(notificationsPage.NotificationsByindex(0).count()).toEqual(2);
    expect(notificationsPage.NotificationsByindex(0).get(0).getText()).toEqual("Adding user to xxx-TestBubble-02. Please wait");
    expect(notificationsPage.NotificationsByindex(0).get(1).getText()).toContain("has been added as a user to xxx-TestBubble-02");
  });

  it("Verify details on Bubbles page is changed for recently moved bubble - mystack.user2", function() {
    homePage.bubbles.click();
    expect(bubblesPage.bubbles.count()).toEqual(2);
    // Verify details in Bubbles page got updated
    expect(bubblesPage.getBubbleNameByindex(0)).toEqual('xxx-TestBubble-01');
    expect(bubblesPage.getBubbleRegionByindex(0)).toEqual('LO3REF');
    expect(bubblesPage.getBubbleTeamByindex(0)).toEqual('xxx-TestTeam-01');
    expect(bubblesPage.getBubbleCreatedDateByindex(0)).toEqual(new Date().toDateString());
    expect(bubblesPage.getBubbleNameByindex(1)).toEqual('xxx-TestBubble-02');
    expect(bubblesPage.getBubbleRegionByindex(1)).toEqual('LO3REF');
    expect(bubblesPage.getBubbleTeamByindex(1)).toEqual('xxx-TestTeam-01');
    expect(bubblesPage.getBubbleCreatedDateByindex(1)).toEqual(new Date().toDateString());

    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-02')), waitSeconds);
    bubblesPage.bubble('xxx-TestBubble-02').click();
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    // Verify values are changed while moving the bubble to another team
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    expect(bubblesPage.BubbleViewTeam.getText()).toEqual('xxx-TestTeam-01');
    expect(bubblesPage.BubbleViewMember.count()).toEqual(4);
    expect(bubblesPage.BubbleViewMember.getText()).toContain('mystack user1');
    expect(bubblesPage.BubbleViewMember.getText()).toContain('mystack user3');
    expect(bubblesPage.BubbleViewMember.getText()).toContain('mystack user5');
    expect(bubblesPage.BubbleViewMember.getText()).toContain('mystack user2');
  });

  it("Verify team 'xxx-TestTeam-01' got updated for recently moved bubble 'xxx-TestBubble-02' for user 'mystack.user2'", function() {
    homePage.teams.click();
    expect(teamsPage.teams.count()).toEqual(2);
    expect(teamsPage.getTeamNameByindex(0)).toEqual('xxx-TestTeam-01');
    expect(teamsPage.getTeamBubblesByindex(0)).toEqual('2');
    expect(teamsPage.getTeamMemberCountByindex(0)).toEqual('4');

    // Details in TeamView page
    browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-01')), waitSeconds);
    teamsPage.team('xxx-TestTeam-01').click();
    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-01');
    expect(teamsPage.teamViewMembers.count()).toEqual(4);

    // Verify the bubble details
    expect(teamsPage.TeamViewBubbles.count()).toEqual(2);
    expect(teamsPage.TeamViewBubbleNameByindex(0).getText()).toEqual('xxx-TestBubble-01 (LO3REF)');
    expect(teamsPage.TeamViewBubbleNameByindex(1).getText()).toEqual('xxx-TestBubble-02 (LO3REF)');
  });

  it("Verify team 'xxx-TestTeam-02' got updated for recently moved bubble 'xxx-TestBubble-02' for user 'mystack.user2'", function() {
    homePage.teams.click();
    expect(teamsPage.teams.count()).toEqual(2);
    expect(teamsPage.getTeamNameByindex(1)).toEqual('xxx-TestTeam-02');
    expect(teamsPage.getTeamBubblesByindex(1)).toEqual('0');
    expect(teamsPage.getTeamMemberCountByindex(1)).toEqual('3');

    // Details in TeamView page
    browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-02')), waitSeconds);
    teamsPage.team('xxx-TestTeam-02').click();
    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-02');
    expect(teamsPage.teamViewDelete.isPresent()).toBeTruthy();
    expect(teamsPage.teamViewAddMember.isPresent()).toBeTruthy();
    expect(teamsPage.teamViewAddMember.isDisplayed()).toBeTruthy();
    expect(teamsPage.teamViewMembers.count()).toEqual(3);
    // Verify the bubble details
    expect(teamsPage.TeamViewBubbles.count()).toEqual(0);
  });
/*
  it("Verify user 'mystack.user3' gets notifications for user reconcilliation while moving the bubble to different team", function() {
    fn.login("mystack.user3", "");

    fn.waitForNotifications(2000, 1);
    expect(notificationsPage.notificationCount.getText()).toEqual('2');
    homePage.notifications.click();
    // Verify total number of 'NotificationGroup' and click on first one
    expect(notificationsPage.notificationHeader.count()).toEqual(1);

    // Verify notifcations while adding user 'mystack.user5' to team 'xxx-TestTeam-02'
    notificationsPage.notificationHeader.get(0).click();
    expect(notificationsPage.notificationHeader.get(0).getText()).toEqual("Add User");
    expect(notificationsPage.NotificationsByindex(0).count()).toEqual(2);
    expect(notificationsPage.NotificationsByindex(0).get(0).getText()).toEqual("Adding user to xxx-TestBubble-02. Please wait");
    expect(notificationsPage.NotificationsByindex(0).get(1).getText()).toContain("has been added as a user to xxx-TestBubble-02");
  });

  it("Verify details on Bubbles page is changed for recently moved bubble - mystack.user3", function() {
    homePage.bubbles.click();
    expect(bubblesPage.bubbles.count()).toEqual(2);
    // Verify details in Bubbles page got updated
    expect(bubblesPage.getBubbleNameByindex(0)).toEqual('xxx-TestBubble-01');
    expect(bubblesPage.getBubbleRegionByindex(0)).toEqual('LO3REF');
    expect(bubblesPage.getBubbleTeamByindex(0)).toEqual('xxx-TestTeam-01');
    expect(bubblesPage.getBubbleCreatedDateByindex(0)).toEqual(new Date().toDateString());
    expect(bubblesPage.getBubbleNameByindex(1)).toEqual('xxx-TestBubble-02');
    expect(bubblesPage.getBubbleRegionByindex(1)).toEqual('LO3REF');
    expect(bubblesPage.getBubbleTeamByindex(1)).toEqual('xxx-TestTeam-01');
    expect(bubblesPage.getBubbleCreatedDateByindex(1)).toEqual(new Date().toDateString());

    // element.all(by.className("Bubble")).get(1).click();
    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-02')), waitSeconds);
    bubblesPage.bubble('xxx-TestBubble-02').click();
    // Verify values are changed while moving the bubble to another team
    expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
    expect(bubblesPage.BubbleViewTeam.getText()).toEqual('xxx-TestTeam-01');
    expect(bubblesPage.BubbleViewMember.count()).toEqual(4);
    expect(bubblesPage.BubbleViewMember.getText()).toContain('mystack user1');
    expect(bubblesPage.BubbleViewMember.getText()).toContain('mystack user3');
    expect(bubblesPage.BubbleViewMember.getText()).toContain('mystack user5');
    expect(bubblesPage.BubbleViewMember.getText()).toContain('mystack user2');
  });

  it("Verify team 'xxx-TestTeam-01' got updated for recently moved bubble 'xxx-TestBubble-02' for user 'mystack.user3'", function() {
    homePage.teams.click();
    expect(teamsPage.teams.count()).toEqual(2);
    expect(teamsPage.getTeamNameByindex(0)).toEqual('xxx-TestTeam-01');
    expect(teamsPage.getTeamBubblesByindex(0)).toEqual('2');
    expect(teamsPage.getTeamMemberCountByindex(0)).toEqual('4');

    // Details in TeamView page
    // element.all(by.className("Team")).get(0).click();
    browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-01')), waitSeconds);
    teamsPage.team('xxx-TestTeam-01').click();
    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-01');
    expect(teamsPage.teamViewDelete.isPresent()).toBeFalsy();
    expect(teamsPage.teamViewAddMember.isPresent()).toBeTruthy();
    expect(teamsPage.teamViewAddMember.isDisplayed()).toBeTruthy();
    expect(teamsPage.teamViewMembers.count()).toEqual(4);

    // Verify the bubble details
    expect(teamsPage.TeamViewBubbles.count()).toEqual(2);
    expect(teamsPage.TeamViewBubbleNameByindex(0).getText()).toEqual('xxx-TestBubble-01 (LO3REF)');
    expect(teamsPage.TeamViewBubbleNameByindex(1).getText()).toEqual('xxx-TestBubble-02 (LO3REF)');
  });

  it("Verify team 'xxx-TestTeam-02' got updated for recently moved bubble 'xxx-TestBubble-02' for user 'mystack.user3'", function() {
    homePage.teams.click();
    expect(teamsPage.teams.count()).toEqual(2);
    expect(teamsPage.getTeamNameByindex(1)).toEqual('xxx-TestTeam-02');
    expect(teamsPage.getTeamBubblesByindex(1)).toEqual('0');
    expect(teamsPage.getTeamMemberCountByindex(1)).toEqual('3');

    // Details in TeamView page
    // element.all(by.className("Team")).get(1).click();
    browser.wait(EC.elementToBeClickable(teamsPage.team('xxx-TestTeam-02')), waitSeconds);
    teamsPage.team('xxx-TestTeam-02').click();
    browser.wait(EC.elementToBeClickable(teamsPage.teamViewClose), waitSeconds);
    expect(teamsPage.teamViewName.getText()).toEqual('xxx-TestTeam-02');
    expect(teamsPage.teamViewDelete.isPresent()).toBeFalsy();
    expect(teamsPage.teamViewAddMember.isPresent()).toBeFalsy();
    expect(teamsPage.teamViewMembers.count()).toEqual(3);
    // Verify the bubble details
    expect(teamsPage.TeamViewBubbles.count()).toEqual(0);
  });
*/
  it("Verify users are reconcilled while moving the bubble 'xxx-TestBubble-02' from team 'xxx-TestTeam-01' -> 'xxx-TestTeam-02'", function() {
    var errorMessage;
    var orgExists = false;
    var orghref, key;

    response = fn.runHttpRequests("POST", headers, vcd + '/api/sessions');
    if (response.statusCode == 200) {
      headers = {"Accept": "application/*+xml;version=5.1", "x-vcloud-authorization": response.headers['x-vcloud-authorization']};

      // Run the Query and get list of Orgs
      response = fn.runHttpRequests("GET", headers, vcd + '/api/admin');
      jsonResult = xmlobj.parseXML(response.body.toString());
      for(key in jsonResult.VCloud.OrganizationReferences.OrganizationReference) {
        if (jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-name"] == "xxx-TestBubble-02") {
          orgExists = true;
          orghref = jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-href"];
          break;
        }
      }
      expect("Org 'xxx-TestBubble-02' exits - true").toEqual("Org 'xxx-TestBubble-02' exits - " + orgExists);
    } else {
      this.fail("Failed to get the auth key. statusCode - ", response.statusCode);
    }

    if (orgExists) {
      response = fn.runHttpRequests("GET", headers, orghref);
      if (response.statusCode == 200) {
        // Convert XML -> JSON
        jsonResultOrg = xmlobj.parseXML(response.body.toString());

        var users = [];

        if (jsonResultOrg.AdminOrg.Users !== undefined) {
          // Store the href for each user
          for (key in jsonResultOrg.AdminOrg.Users.UserReference) {
            // Append list of users to the list
            users.push(jsonResultOrg.AdminOrg.Users.UserReference[key]["-name"]);
          }

          // Verify the total number of users
          expect("Total number of bubble users - " + jsonResultOrg.AdminOrg.Users.UserReference.length).toEqual("Total number of bubble users - 4");
          // Verify users are reconcilled while moving to other teams
          expect(users).toContain("mystack.user1@pearson.com");
          expect(users).toContain("mystack.user2@pearson.com");
          expect(users).toContain("mystack.user3@pearson.com");
          expect(users).toContain("mystack.user5@pearson.com");
          // orgUrl ran okey?
        } else {
          errorMessage = "User is not created on the vCD. So can't continue this test";
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "Failed to run the URL - " + orghref + ". statusCode - " + response.statusCode + ". So can't verify whether whether users are reconcilled for the bubble - 'xxx-TestBubble-02'";
        this.fail(errorMessage);
      }
    } else {
      errorMessage = "Org not created for bubble xxx-TestBubble-02. So can't verify whether Org vdc is created Or not";
      this.fail(errorMessage);
    }
  });

  it("Verify users in bubble 'xxx-TestBubble-01' are not updated while moving the bubble 'xxx-TestBubble-02' from team 'xxx-TestTeam-01' -> 'xxx-TestTeam-02'", function() {
    var errorMessage;
    var orgExists = false;
    var orghref, key;
    var headers = {"Accept": "application/*+xml;version=5.1", "Authorization" : authorization};

    response = fn.runHttpRequests("POST", headers, vcd + '/api/sessions');
    if (response.statusCode == 200) {
      headers = {"Accept": "application/*+xml;version=5.1", "x-vcloud-authorization": response.headers['x-vcloud-authorization']};

      // Run the Query and get list of Orgs
      response = fn.runHttpRequests("GET", headers, vcd + '/api/admin');
      jsonResult = xmlobj.parseXML(response.body.toString());
      for(key in jsonResult.VCloud.OrganizationReferences.OrganizationReference) {
        if (jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-name"] == "xxx-TestBubble-01") {
          orgExists = true;
          orghref = jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-href"];
          break;
        }
      }
      expect("Org 'xxx-TestBubble-01' exits - true").toEqual("Org 'xxx-TestBubble-01' exits - " + orgExists);
    } else {
      this.fail("Failed to get the auth key. statusCode - ", response.statusCode);
    }

    if (orgExists) {
      response = fn.runHttpRequests("GET", headers, orghref);
      if (response.statusCode == 200) {
        // Convert XML -> JSON
        jsonResultOrg = xmlobj.parseXML(response.body.toString());

        var users = [];

        if (jsonResultOrg.AdminOrg.Users !== undefined) {
          // Store the href for each user
          for (key in jsonResultOrg.AdminOrg.Users.UserReference) {
            // Append list of users to the list
            users.push(jsonResultOrg.AdminOrg.Users.UserReference[key]["-name"]);
          }

          // Verify the total number of users
          expect("Total number of bubble users - " + jsonResultOrg.AdminOrg.Users.UserReference.length).toEqual("Total number of bubble users - 6");
          // Verify users in bubble 'xxx-TestBubble-01' are not affected while moving the bubble 'xxx-TestBubble-02' from team 'xxx-TestTeam-02' -> 'xxx-TestTeam-01'
          expect(users).toContain("mystack.user1@pearson.com");
          expect(users).toContain("mystack.user2@pearson.com");
          expect(users).toContain("mystack.user3@pearson.com");
          expect(users).toContain("mystack.user5@pearson.com");
          expect(users).toContain("mystack.local1");
          expect(users).toContain("mystack.local3");
          // orgUrl ran okey?
        } else {
          errorMessage = "User is not created on the vCD. So can't continue this test";
          this.fail(errorMessage);
        }
      } else {
        errorMessage = "Failed to run the URL - " + orghref + ". statusCode - " + response.statusCode + ". So can't verify whether whether users are reconcilled for the bubble - 'xxx-TestBubble-01'";
        this.fail(errorMessage);
      }
    } else {
      errorMessage = "Org not created for bubble xxx-TestBubble-01. So can't verify whether Org vdc is created Or not";
      this.fail(errorMessage);
    }
  });

});

