/*
*****************************************************************************************************************************************************
SUMMARY: Verifies whether portal shows warning mesasge for different scenario. Please see below test for more details

ATTENTION:  Should be run after 'UsersAndTeams_spec.js Or users/teams required for this test should be created Manually

List Of Tests:
    ErrorMessages: Login to the Portal as mystack.user4
    ErrorMessages: External IP purchase when no IP available: Set status of public IP address as 'OCCUPIED' in the 'public_ip' table for the region 'XXX'
    ErrorMessages: External IP purchase when no IP available: Verify portal throws warning message on UI while user tries to purchase external IP address and no IP available on DB - 'xxx-BrokenBubbleDoNotDelete'
    ErrorMessages: Network not available: Set status of 'network' & 'public_ip' table as 'Occupied' for the region 'XXX'
    ErrorMessages: Network not available: Verify user not able to create bubble while no network and public IP available on the db
    ErrorMessages: Network not available: Verify the bubble/org 'xxx-NoNetworkAvailable' is not created on the vCD
    ErrorMessages: Delete Bubble which has vApp: Refresh the bubbles page to get latest details from the vCD - 'xxx-BrokenBubbleDoNotDelete'
    ErrorMessages: Delete Bubble which has vApp: Verify user not able to delete the bubble which has vApp - 'xxx-BrokenBubbleDoNotDelete'
    ErrorMessages: Duplicate bubble: Verify user gets error message while tryig to create duplicate bubble ie., Bubble name exists on the portal - 'xxx-BrokenBubbleDoNotDelete'
    ErrorMessages: Duplicate bubble: verify user gets notifications while trying to create duplicate bubble ie., Bubble name exits in vCD but on exists in Portal DB - 'xxx-DoNotDeleteBubble'
*****************************************************************************************************************************************************
*/
var authorization = vcdAuthKey;
var headers       = {"Accept": "application/*+xml;version=5.1", "Authorization" : vcdAuthKey};
var orgExists     = false;

var homePage          = require("../../pages/home_page.js");
var bubblesPage       = require("../../pages/bubbles_page.js");
var notificationsPage = require("../../pages/notifications_page.js");


describe('ErrorMessages', function() {

  describe("Edge gateway creation failure", function() {

     beforeAll(function() {
      // Set network is available for region 'XXXLO3'
      db.execute("SELECT * FROM network WHERE region='XXXLO3'", function(err, result) {
        if (err) {
          var errorMessage = "Failure message - " + err;
          this.fail(errorMessage);
        } else {
          for (var i=0; i < result.rows.length; i++) {
            var query = "UPDATE network SET bubble=null, status=0 WHERE id=" + result.rows[i].id;
            db.execute(query, function(err, update) {
              if (err) {
                var errorMessage = "Failed to run - " + query + ". Failure message - " + err;
                this.fail(errorMessage);
              }
            });
          }
        }
      });

      browser.get('/');
      fn.login("mystack.user4", "");
      // Clear the existing notifications on the UI
      fn.clearNotifications();
    });

    it("Verify error notifications are sent to user while bubble creation fails at edge gateway creation - 'xxx-ExpectedEdgeFailure'", function() {
      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.createBubble), waitSeconds);
      bubblesPage.createBubble.click();
      bubblesPage.createBubbleName.sendKeys("xxx-ExpectedEdgeFailure");
      bubblesPage.createBubbleChooseTeam('xxx-FailureTests-01');
      bubblesPage.createBubbleChooseRegion('XXXLO3 - Iceland');
      bubblesPage.tandc.click();
      bubblesPage.tandcAccept.click();
      expect(bubblesPage.tandcCheckbox.isSelected()).toBeTruthy();
      bubblesPage.createButton.click();

      expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
      browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
      bubblesPage.BubbleViewClose.click();
      fn.waitForNotifications(120000, 6);

      // Verify the notifications
      expect(notificationsPage.notificationCount.getText()).toEqual('6');
      homePage.notifications.click();
      notificationsPage.notificationHeader.get(0).click();
      expect(notificationsPage.NotificationsByindex(0).count()).toEqual(6);
      expect(notificationsPage.NotificationsByindex(0).get(5).getText()).toEqual("A problem occurred creating your bubble. We are investigating");
    });

    afterAll(function() {
      //  Default values
      var response, key;
      var orghref           = "";
      var orgDisablehref    = "";
      var orgvdchref        = "";
      var orgvdcDisablehref = "";
      var edgeGwqueryhref   = "";

      headers = {"Accept": "application/*+xml;version=5.1", "Authorization": authorization};
      response = fn.runHttpRequests("POST", headers, vcd + '/api/sessions');
      if (response.statusCode == 200) {
        headers = {"Accept": "application/*+xml;version=5.1", "x-vcloud-authorization": response.headers['x-vcloud-authorization']};
        // Run the Query and get list of Orgs
        response = fn.runHttpRequests("GET", headers, vcd + '/api/admin');
        jsonResult = xmlobj.parseXML(response.body.toString());
        for(key in jsonResult.VCloud.OrganizationReferences.OrganizationReference) {
          if (jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-name"] == "xxx-ExpectedEdgeFailure") {
            orghref = jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-href"];
            break;
          }
        }
      } else {
        this.fail("Failed to get the auth key. statusCode - ", response.statusCode);
      }

      if (orghref !== "") {
        response = fn.runHttpRequests("GET", headers, orghref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResultOrg = xmlobj.parseXML(response.body.toString());

          // Get the disable link for Org href
          for(key in jsonResultOrg.AdminOrg.Link) {
            if (jsonResultOrg.AdminOrg.Link[key]["-rel"] == "disable") {
              orgDisablehref = jsonResultOrg.AdminOrg.Link[key]["-href"];
              break;
            }
          }

          // Loop through each Org and check Org vDC created - Set the FLAG
          if (jsonResultOrg.AdminOrg.Vdcs !== undefined) {
            if (jsonResultOrg.AdminOrg.Vdcs.Vdc["-type"] == "application/vnd.vmware.admin.vdc+xml") {
              orgvdchref = jsonResultOrg.AdminOrg.Vdcs.Vdc["-href"];
            }
          }
        }
      }

      if (orgvdchref !== "") {
        response = fn.runHttpRequests("GET", headers, orgvdchref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResultOrgVdc = xmlobj.parseXML(response.body.toString());

          // Get the disable link for Org vDC href
          for(key in jsonResultOrgVdc.AdminVdc.Link) {
            if (jsonResultOrgVdc.AdminVdc.Link[key]["-rel"] == "disable") {
              orgvdcDisablehref = jsonResultOrgVdc.AdminVdc.Link[key]["-href"];
              break;
            }
          }

          // Loop through and get the href for Edgegateway Query
          for(key in jsonResultOrgVdc.AdminVdc.Link) {
            if (jsonResultOrgVdc.AdminVdc.Link[key]["-type"] == "application/vnd.vmware.vcloud.query.records+xml" && jsonResultOrgVdc.AdminVdc.Link[key]["-rel"] == "edgeGateways") {
              edgeGwqueryhref = jsonResultOrgVdc.AdminVdc.Link[key]["-href"];
              break;
            }
          }
        } else {
          errorMessage = "Failed while running - " + orgvdchref;
          this.fail(errorMessage);
        }
      }

      if (edgeGwqueryhref !== "") {
        response = fn.runHttpRequests("GET", headers, edgeGwqueryhref);
        if (response.statusCode == 200) {
          // Convert XML -> JSON
          jsonResult = xmlobj.parseXML(response.body.toString());

          // Edge gateway exists?. Delete it
          if (jsonResult.QueryResultRecords.EdgeGatewayRecord !== undefined) {
            response = fn.runHttpRequests("DELETE", headers, jsonResult.QueryResultRecords.EdgeGatewayRecord["-href"]);
          }
        } else {
          errorMessage = "Failed while running - " + edgeGwqueryhref;
          this.fail(errorMessage);
        }
      }
      // Delet Org & Org vDC
      if (orgvdcDisablehref !== "") {fn.runHttpRequests("POST",   headers, orgvdcDisablehref);}
      if (orgvdchref        !== "") {fn.runHttpRequests("DELETE", headers, orgvdchref);}
      if (orgDisablehref    !== "") {fn.runHttpRequests("POST",   headers, orgDisablehref);}
      browser.sleep(8000);
      if (orghref           !== "") {fn.runHttpRequests("DELETE", headers, orghref);}

      // Verify whether org is actually deleted on the vCD
      var orgExists = false;
      browser.sleep(10000);
      response = fn.runHttpRequests("GET", headers, vcd + '/api/admin');
      jsonResult = xmlobj.parseXML(response.body.toString());
      for(key in jsonResult.VCloud.OrganizationReferences.OrganizationReference) {
        if (jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-name"] == "xxx-ExpectedEdgeFailure") {
          orgExists = true;
          break;
        }
      }
      expect("Org 'xxx-ExpectedEdgeFailure' exits - false").toEqual("Org 'xxx-ExpectedEdgeFailure' exits - " + orgExists);
    });

  });

  describe("External IP purchase when no IP available", function() {

    beforeAll(function() {
      db.execute("SELECT * FROM public_ip WHERE region='XXXLO3'", function(err, result) {
        if (err) {
          var err_msg = "Failure message - " + err;
          fn.reportFailure(err_msg);
        } else {
          for (var i=0; i < result.rows.length; i++) {
            address = result.rows[i].address;
            externalIP = address[0] + "." + address[1] + "." + address[2] + "." + address[3];

            var query = "UPDATE public_ip SET bubble=null, status=1 WHERE region='XXXLO3' AND address='" + externalIP + "'";
            db.execute(query, function(err, update) {
                var err_msg = "Failed to run - " + query + ". Failure message - " + err;
              if (err) {
                fn.reportFailure(err_msg);
              }
            });
          }
        }
      });

      browser.get('/');
      fn.login("mystack.root", "");
    });

    it("Verify portal throws warning message on UI while user tries to purchase external IP address and no IP available on DB - 'xxx-BrokenBubbleDoNotDelete'", function() {
      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-BrokenBubbleDoNotDelete')), waitSeconds);
      bubblesPage.bubble('xxx-BrokenBubbleDoNotDelete').click();
      browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewAddPublicip), waitSeconds);
      bubblesPage.BubbleViewAddPublicip.click();
      expect(bubblesPage.BubbleViewAddPublicIPError.isPresent()).toBeTruthy();
      expect(bubblesPage.BubbleViewAddPublicIPError.getText()).toEqual("Failed to assign public IP");
    });

  });

  describe("Network not available", function() {

    beforeAll(function() {
      db.execute("SELECT * FROM public_ip WHERE region='XXXLO3' AND status=0", function(err, result) {
        if (err) {
          var err_msg = "Failure message - " + err;
          fn.reportFailure(err_msg);
        } else {
          for (var i=0; i < result.rows.length; i++) {
            address = result.rows[i].address;
            externalIP = address[0] + "." + address[1] + "." + address[2] + "." + address[3];

            var query = "UPDATE public_ip SET bubble=b2e2f27b-73d1-11e4-86e8-0050560e0121 WHERE region='XXXLO3' AND address='" + externalIP + "'";
            db.execute(query, function(err, update) {
              if (err) {
                var err_msg = "Failed to run - " + query + ". Failure message - " + err;
                fn.reportFailure(err_msg);
              }
            });
          }
        }
      });

      db.execute("SELECT * FROM network WHERE region='XXXLO3' AND status=0 allow filtering", function(err, result) {
        if (err) {
          var err_msg = "Failure message - " + err;
          fn.reportFailure(err_msg);
        } else {
          for (var i=0; i < result.rows.length; i++) {
            var query = "UPDATE network SET bubble=b2e2f27b-73d1-11e4-86e8-0050560e0121 WHERE id=" + result.rows[i].id;
            db.execute(query, function(err, update) {
              if (err) {
                var err_msg = "Failed to run - " + query + ". Failure message - " + err;
                fn.reportFailure(err_msg);
              }
            });
          }
        }
      });
    });

    beforeEach(function() {
      browser.get('/');
    });

    it("Verify user not able to create bubble while no network and public IP available on the DB", function() {
      fn.login("mystack.user4", "");

      // Clear the existing notifications on the UI
      fn.clearNotifications();

      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.createBubble), waitSeconds);
      bubblesPage.createBubble.click();
      bubblesPage.createBubbleName.sendKeys("xxx-NoNetworkAvailable");
      bubblesPage.createBubbleChooseTeam('xxx-FailureTests-01');
      bubblesPage.createBubbleChooseRegion('XXXLO3 - Iceland');
      bubblesPage.tandc.click();
      bubblesPage.tandcAccept.click();
      expect(bubblesPage.tandcCheckbox.isSelected()).toBeTruthy();
      bubblesPage.createButton.click();

      expect(bubblesPage.bubbleCreateError.isPresent()).toBeTruthy();
      expect(bubblesPage.bubbleCreateError.getText()).toEqual("There are no available networks in the XXXLO3 region");
    });

    it("Verify the bubble/org 'xxx-NoNetworkAvailable' is not created on the vCD", function() {
      var orgExists = false;
      headers = {"Accept": "application/*+xml;version=5.1", "Authorization": authorization};

      response = fn.runHttpRequests("POST", headers, vcd + '/api/sessions');
      if (response.statusCode == 200) {
        headers = {"Accept": "application/*+xml;version=5.1", "x-vcloud-authorization": response.headers['x-vcloud-authorization']};
        // Run the Query and get list of Orgs
        response = fn.runHttpRequests("GET", headers, vcd + '/api/admin');
        jsonResult = xmlobj.parseXML(response.body.toString());
        for(var key in jsonResult.VCloud.OrganizationReferences.OrganizationReference) {
          if (jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-name"] == "xxx-NoNetworkAvailable") {
            orgExists = true;
            break;
          }
        }
        expect("Org 'xxx-NoNetworkAvailable' exits - false").toEqual("Org 'xxx-NoNetworkAvailable' exits - " + orgExists);
      } else {
        this.fail("Failed to get the auth key. statusCode - ", response.statusCode);
      }
    });

  });

  describe("Delete Bubble which has vApp", function() {

    beforeAll(function() {
      browser.get('/');
      fn.login("mystack.root", "");
      fn.clearNotifications();

      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-BrokenBubbleDoNotDelete')), waitSeconds);
      bubblesPage.bubble('xxx-BrokenBubbleDoNotDelete').click();
      expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();

      // Wait for element to be available on the UI and click the 'refresh' button
      browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewRefreshVms), waitSeconds);
      bubblesPage.BubbleViewRefreshVms.click();
      expect(bubblesPage.BubbleViewRefreshVms.getText()).toEqual("refreshing");
      bubblesPage.BubbleViewClose.click();

      fn.waitForNotifications(20000, 2);
    });

    it("Verify user not able to delete the bubble which has vApp - 'xxx-BrokenBubbleDoNotDelete'", function() {
      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-BrokenBubbleDoNotDelete')), waitSeconds);
      bubblesPage.bubble('xxx-BrokenBubbleDoNotDelete').click();

      // Verify user not able to delete the bubble which has vApp
      expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
      browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
      expect(bubblesPage.BubbleViewDelete.isPresent()).toBeTruthy();

      // Try to click bubble delete button and user should see the pop-up message
      bubblesPage.BubbleViewDelete.click();
      expect(browser.switchTo().alert().getText()).toContain("We will not delete a non-empty Bubble. Please remove all vApps, templates, media and catalogs and try again. If you think your Bubble is empty already, try hitting");
      browser.switchTo().alert().accept();
      expect(bubblesPage.BubbleViewDelete.isPresent()).toBeTruthy();
    });
  });

  describe("Duplicate bubble", function() {

     beforeEach(function() {
      browser.get('/');

      // Set public address available in the table 'public_ip' for region 'XXXLO3'
      db.execute("SELECT * FROM public_ip WHERE region='XXXLO3' AND status=1", function(err, result) {
        if (err) {
          var errorMessage = "Failure message - " + err;
          this.fail(errorMessage);
        } else {
          for (var i=0; i < result.rows.length; i++) {
            address = result.rows[i].address;
            externalIP = address[0] + "." + address[1] + "." + address[2] + "." + address[3];

            var query = "UPDATE public_ip SET bubble=null, status=0 WHERE region='XXXLO3' AND address='" + externalIP + "'";
            db.execute(query, function(err, update) {
              if (err) {
                var errorMessage = "Failed to run - " + query + ". Failure message - " + err;
                this.fail(errorMessage);
              }
            });
          }
        }
      });

      // Set network available for region 'XXXLO3'
      db.execute("SELECT * FROM network WHERE region='XXXLO3'", function(err, result) {
        if (err) {
          var errorMessage = "Failure message - " + err;
          this.fail(errorMessage);
        } else {
          for (var i=0; i < result.rows.length; i++) {
            var query = "UPDATE network SET bubble=null, status=0 WHERE id=" + result.rows[i].id;
            db.execute(query, function(err, update) {
              if (err) {
                var errorMessage = "Failed to run - " + query + ". Failure message - " + err;
                this.fail(errorMessage);
              }
            });
          }
        }
      });

      fn.login("mystack.user4", "");
      fn.clearNotifications();
    });

    it("Verify user gets error message while tryig to create duplicate bubble ie., Bubble name exists on the portal - 'xxx-BrokenBubbleDoNotDelete'", function() {
      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.createBubble), waitSeconds);
      bubblesPage.createBubble.click();
      bubblesPage.createBubbleName.sendKeys("xxx-BrokenBubbleDoNotDelete");
      bubblesPage.createBubbleChooseTeam('xxx-FailureTests-01');
      bubblesPage.createBubbleChooseRegion('XXXLO3 - Iceland');
      bubblesPage.tandc.click();
      bubblesPage.tandcAccept.click();
      expect(bubblesPage.tandcCheckbox.isSelected()).toBeTruthy();
      bubblesPage.createButton.click();

      expect(bubblesPage.bubbleCreateError.isPresent()).toBeTruthy();
      expect(bubblesPage.bubbleCreateError.getText()).toEqual("bubble name 'xxx-BrokenBubbleDoNotDelete' already exists in region XXXLO3");
    });

    it("Verify user gets notifications while trying to create duplicate bubble ie., Bubble name exits in vCD but in Portal DB - 'xxx-DoNotDeleteBubble'", function() {
      homePage.bubbles.click();
      browser.wait(EC.elementToBeClickable(bubblesPage.createBubble), waitSeconds);
      bubblesPage.createBubble.click();
      bubblesPage.createBubbleName.sendKeys("xxx-DoNotDeleteBubble");
      bubblesPage.createBubbleChooseTeam('xxx-FailureTests-01');
      bubblesPage.createBubbleChooseRegion('XXXLO3 - Iceland');
      bubblesPage.tandc.click();
      bubblesPage.tandcAccept.click();
      expect(bubblesPage.tandcCheckbox.isSelected()).toBeTruthy();
      bubblesPage.createButton.click();

      expect(bubblesPage.BubbleView.isPresent()).toBeTruthy();
      browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);
      bubblesPage.BubbleViewClose.click();
      fn.waitForNotifications(20000, 1);

      // Verify the notifications
      expect(notificationsPage.notificationCount.getText()).toEqual('1');
      homePage.notifications.click();
      notificationsPage.notificationHeader.get(0).click();
      expect(notificationsPage.NotificationsByindex(0).count()).toEqual(1);
      expect(notificationsPage.NotificationsByindex(0).get(0).getText()).toEqual("Another bubble named 'xxx-DoNotDeleteBubble' already exists");
    });

  });

});
