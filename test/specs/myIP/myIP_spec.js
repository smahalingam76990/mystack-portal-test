/*
*****************************************************************************************************************************************************
SUMMARY:      EXternal IP Purchase
ATTENTION:    Should be run after 'UserAndTeams_spec.js', 'BubbleCreate-01_spec.js' & 'BubbleCreate-02_spec.js'

List Of Tests:
    External IP Purchase: Purchase the external IP for the bubble - 'xxx-TestBubble-01' (Purchase first IP address)
    External IP Purchase: Purchase the external IP for the bubble - 'xxx-TestBubble-01' (Purchase second IP address)
    External IP Purchase: Verify users gets notifications for public IP purchase for bubble - 'xxx-TestBubble-01'
    External IP Purchase: Verify user able to see recently purchased public IP address on the UI - 'xxx-TestBubble-01'
*****************************************************************************************************************************************************
*/
var homePage          = require("../../pages/home_page.js");
var usersPage         = require("../../pages/users_page.js");
var teamsPage         = require("../../pages/teams_page.js");
var bubblesPage       = require("../../pages/bubbles_page.js");
var notificationsPage = require("../../pages/notifications_page.js");

var authorization  = vcdAuthKey;
var headers        = {"Accept": "application/*+xml;version=5.1", "Authorization" : vcdAuthKey};


describe("External IP Purchase", function() {

  beforeAll(function() {
    browser.get('/');
    fn.login("mystack.user3", "");
    fn.clearNotifications();
    fn.login("mystack.user4", "");
    fn.clearNotifications();
  });

  beforeEach(function() {
    browser.get('/');
  });

  it("Purchase the external IP for the bubble - 'xxx-TestBubble-01' (Purchase first IP address)", function() {
    fn.login("mystack.user1", "");
    // Clear the existing notifications on the UI
    fn.clearNotifications();

    homePage.bubbles.click();
    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-01')), waitSeconds);
    bubblesPage.bubble('xxx-TestBubble-01').click();

    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewAddPublicip), waitSeconds);
    bubblesPage.BubbleViewAddPublicip.click();
    expect(bubblesPage.BubbleViewAddPublicip.getText()).toEqual("adding");
  });

  it("Purchase the external IP for the bubble - 'xxx-TestBubble-01' (Purchase second IP address)", function() {
    // This is temproary wait untill the bug 'MYS-' is fixed
    fn.waitForNotifications(180000, 3);

    homePage.bubbles.click();
    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-01')), waitSeconds);
    bubblesPage.bubble('xxx-TestBubble-01').click();

    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewAddPublicip), waitSeconds);
    bubblesPage.BubbleViewAddPublicip.click();
    expect(bubblesPage.BubbleViewAddPublicip.getText()).toEqual("adding");
  });

  it("Verify users gets notifications for public IP purchase for bubble - 'xxx-TestBubble-01'", function() {
    fn.waitForNotifications(180000, 6);

    // Verify notification for myIP puchase
    expect(notificationsPage.notificationCount.getText()).toEqual('6');
    homePage.notifications.click();
    // Verify total number of 'NotificationGroup' and click on first one
    expect(notificationsPage.notificationHeader.count()).toEqual(2);
    notificationsPage.notificationHeader.get(0).click();
    // Verify notification for first external IP purchase
    expect(notificationsPage.NotificationsByindex(0).count()).toEqual(3);
    expect(notificationsPage.notificationHeader.get(0).getText()).toEqual("Add External IP");
    expect(notificationsPage.NotificationsByindex(0).get(0).getText()).toEqual("Adding external ip address to xxx-TestBubble-01. Please wait");
    expect(notificationsPage.NotificationsByindex(0).get(1).getText()).toContain("are being added to xxx-TestBubble-01");
    expect(notificationsPage.NotificationsByindex(0).get(2).getText()).toContain("have been added to xxx-TestBubble-01");
    // Verify notification for second external IP purchase
    notificationsPage.notificationHeader.get(1).click();
    expect(notificationsPage.NotificationsByindex(1).count()).toEqual(3);
    expect(notificationsPage.notificationHeader.get(1).getText()).toEqual("Add External IP");
    expect(notificationsPage.NotificationsByindex(1).get(0).getText()).toEqual("Adding external ip address to xxx-TestBubble-01. Please wait");
    expect(notificationsPage.NotificationsByindex(1).get(1).getText()).toContain("are being added to xxx-TestBubble-01");
    expect(notificationsPage.NotificationsByindex(1).get(2).getText()).toContain("have been added to xxx-TestBubble-01");
  });

  it("Verify user able to see recently purchased public IP address on the UI and it is matching with vCD - 'xxx-TestBubble-01'", function() {
    var response, key, errorMessage;
    var orgExists       = false;
    var orgvdcExists    = false;
    var orgvdchref      = "";
    var edgeGwExists    = false;
    var edgeGwqueryhref = "";

    // Default values
    var publicIpNew = [];

    homePage.bubbles.click();
    browser.wait(EC.elementToBeClickable(bubblesPage.bubble('xxx-TestBubble-01')), waitSeconds);
    bubblesPage.bubble('xxx-TestBubble-01').click();
    browser.wait(EC.elementToBeClickable(bubblesPage.BubbleViewClose), waitSeconds);

    // Check Org 'xxx-TestBubble-01' exists and set the FLAG
    response = fn.runHttpRequests("POST", headers, vcd + '/api/sessions');
    if (response.statusCode == 200) {
      headers = {"Accept": "application/*+xml;version=5.1", "x-vcloud-authorization": response.headers['x-vcloud-authorization']};

      // Run the Query and get list of Orgs
      response = fn.runHttpRequests("GET", headers, vcd + '/api/admin');
      jsonResult = xmlobj.parseXML(response.body.toString());
      for(key in jsonResult.VCloud.OrganizationReferences.OrganizationReference) {
        if (jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-name"] == "xxx-TestBubble-01") {
          orgExists = true;
          orghref = jsonResult.VCloud.OrganizationReferences.OrganizationReference[key]["-href"];
          break;
        }
      }
      expect("Org 'xxx-TestBubble-01' exits - true").toEqual("Org 'xxx-TestBubble-01' exits - " + orgExists);
    } else {
      this.fail("Failed to get the auth key. statusCode - ", response.statusCode);
    }

    // Does Org exits ?. Now check whether Org vDC exists and set the FLAG
    if (orgExists) {
      response = fn.runHttpRequests("GET", headers, orghref);
      if (response.statusCode == 200) {
        // Convert XML -> JSON
        jsonResultOrg = xmlobj.parseXML(response.body.toString());

        // Loop through each Org and check Org vDC created - Set the FLAG
        if (jsonResultOrg.AdminOrg.Vdcs !== undefined) {
          if (jsonResultOrg.AdminOrg.Vdcs.Vdc["-type"] == "application/vnd.vmware.admin.vdc+xml") {
            orgvdcExists  = true;
            orgvdchref    = jsonResultOrg.AdminOrg.Vdcs.Vdc["-href"];
          }
        }
      // orgUrl ran okey?
      } else {
        errorMessage = "Failed to run the URL - " + orghref + ". statusCode - " + response.statusCode + ". So can't verify whether Org vdc is created Or not";
        this.fail(errorMessage);
      }
    } else {
      errorMessage = "Org not created for bubble xxx-TestBubble-01. So can't verify whether Org vdc is created Or not";
      this.fail(errorMessage);
    }

    // Org vDC exists ?. Check whether edge gateway exits and set the FLAG
    if (orgvdcExists) {
      response = fn.runHttpRequests("GET", headers, orgvdchref);
      if (response.statusCode == 200) {
        // Convert XML -> JSON
        jsonResult = xmlobj.parseXML(response.body.toString());

        // Loop through and get the href for Edgegateway Query
        for(key in jsonResult.AdminVdc.Link) {
          if (jsonResult.AdminVdc.Link[key]["-type"] == "application/vnd.vmware.vcloud.query.records+xml" && jsonResult.AdminVdc.Link[key]["-rel"] == "edgeGateways") {
            edgeGwExists = true;
            edgeGwqueryhref = jsonResult.AdminVdc.Link[key]["-href"];
            break;
          }
        }
      } else {
        errorMessage = "Failed while running - " + orgvdchref;
        this.fail(errorMessage);
      }
    } else {
      errorMessage = "orgExists - " + orgExists + " & orgvdcExists - " + orgvdcExists + " for bubble xxx-TestBubble-01. So can't verify whether org is enabled Or not";
      this.fail(errorMessage);
    }

    if (edgeGwExists) {
      response = fn.runHttpRequests("GET", headers, edgeGwqueryhref);
      if (response.statusCode == 200) {
        // Convert XML -> JSON
        jsonResult = xmlobj.parseXML(response.body.toString());

        if (jsonResult.QueryResultRecords.EdgeGatewayRecord !== undefined) {
          if (jsonResult.QueryResultRecords.EdgeGatewayRecord["-numberOfExtNetworks"] == "1") {
            var edgeGwhref = jsonResult.QueryResultRecords.EdgeGatewayRecord["-href"];

            response = fn.runHttpRequests("GET", headers, edgeGwhref);
            if (response.statusCode == 200) {
              // Convert XML -> JSON
              jsonResult = xmlobj.parseXML(response.body.toString());
              var StartAddress, EndAddress, interfaceType, i;
              for(key in jsonResult.EdgeGateway.Configuration.GatewayInterfaces.GatewayInterface) {
                interfaceType = jsonResult.EdgeGateway.Configuration.GatewayInterfaces.GatewayInterface[key].InterfaceType;
                if (interfaceType == "uplink") {
                  // Get the range of external IP address purchased via the myIP and Marketplace it in list
                  var IpRanges = jsonResult.EdgeGateway.Configuration.GatewayInterfaces.GatewayInterface[key].SubnetParticipation.IpRanges;
                  if (IpRanges.IpRange.length !== undefined) {
                    IpRanges = IpRanges.IpRange;
                  }

                  // Loop through external IP address on the XML reponse and store it on the variable
                  for(var key2 in IpRanges) {
                    StartAddress = ip.toLong(IpRanges[key2].StartAddress);
                    EndAddress   = ip.toLong(IpRanges[key2].EndAddress);
                    // Start address and End address is same ?. Then just add the start address to the list
                    if (StartAddress == EndAddress) {
                      publicIpNew.push(ip.fromLong(StartAddress));
                    } else {
                      // Start address and End address is not same ?. Then add Start address and all the IP's in between them to the list
                      // Example: 10.0.0.2 - 10.0.0.4 then list should be - 10.0.0.2, 10.0.0.3, 10.0.0.4
                      publicIpNew.push(ip.fromLong(StartAddress));
                      totalIP = EndAddress - StartAddress;
                      for(i=0; i<totalIP; i++) {
                        publicIpNew.push(ip.fromLong(StartAddress + i + 1));
                      }
                    }
                  }
                  // Verify total number of external IP address on the vCD. Purchased 2 public IP through the portal so there should be
                  // two public on the vCD for the bubble - 'xxx-TestBubble-01'
                  expect("Total number of external IP address on vCD - " + publicIpNew.length).toEqual("Total number of external IP address on vCD - 2");

                  // Verify Public IP details in Portal is matching with the vCD.
                  expect(bubblesPage.BubbleViewip.count()).toEqual(publicIpNew.length);
                  expect(publicIpNew).toContain(bubblesPage.BubbleViewip.get(0).getText());
                  expect(publicIpNew).toContain(bubblesPage.BubbleViewip.get(1).getText());
                }
              }
            } else {
              errorMessage = "Failed while running - " + edgeGwhref;
              this.fail(errorMessage);
            }
          }
        }
      } else {
        errorMessage = "Failed while running - " + edgeGwqueryhref;
        this.fail(errorMessage);
      }
    } else {
      errorMessage = "orgExists - " + orgExists + " , orgvdcExists - " + orgvdcExists + "& edgeGwExists - " + edgeGwExists + " for bubble xxx-TestBubble-01. So can't verify whether edge gate is created Or not";
      this.fail(errorMessage);
    }
  });

  it("Verify user notification for external IP purchase is sent to all users of a team - mystack.user3", function() {
    fn.login("mystack.user3", "");
    fn.waitForNotifications(5000, 1);

    // Verify notification for myIP puchase
    expect(notificationsPage.notificationCount.getText()).toEqual('6');
    homePage.notifications.click();
    // Verify total number of 'NotificationGroup' and click on first one
    expect(notificationsPage.notificationHeader.count()).toEqual(2);
    notificationsPage.notificationHeader.get(0).click();
    // Verify notification for first external IP purchase
    expect(notificationsPage.NotificationsByindex(0).count()).toEqual(3);
    expect(notificationsPage.notificationHeader.get(0).getText()).toEqual("Add External IP");
    expect(notificationsPage.NotificationsByindex(0).get(0).getText()).toEqual("Adding external ip address to xxx-TestBubble-01. Please wait");
    expect(notificationsPage.NotificationsByindex(0).get(1).getText()).toContain("are being added to xxx-TestBubble-01");
    expect(notificationsPage.NotificationsByindex(0).get(2).getText()).toContain("have been added to xxx-TestBubble-01");
     // Verify notification for second external IP purchase
     notificationsPage.notificationHeader.get(1).click();
    expect(notificationsPage.NotificationsByindex(1).count()).toEqual(3);
    expect(notificationsPage.notificationHeader.get(1).getText()).toEqual("Add External IP");
    expect(notificationsPage.NotificationsByindex(1).get(0).getText()).toEqual("Adding external ip address to xxx-TestBubble-01. Please wait");
    expect(notificationsPage.NotificationsByindex(1).get(1).getText()).toContain("are being added to xxx-TestBubble-01");
    expect(notificationsPage.NotificationsByindex(1).get(2).getText()).toContain("have been added to xxx-TestBubble-01");
  });

  it("Verify myIP purchase notifications are sent to the user who is not part of a team - mystack.user4", function() {
    fn.login("mystack.user4", "");
    fn.waitForNotifications(5000, 1);

    homePage.notifications.click();
    expect(notificationsPage.notificationHeader.count()).toEqual(0);
  });

});
